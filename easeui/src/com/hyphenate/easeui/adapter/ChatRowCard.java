package com.hyphenate.easeui.adapter;

import android.content.Context;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.R;
import com.hyphenate.easeui.widget.chatrow.EaseChatRow;
import com.hyphenate.exceptions.HyphenateException;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nsd on 2017/6/6.
 */

public class ChatRowCard extends EaseChatRow {
    private ImageView mDoctorHead;
    private TextView mDoctorName;
    private TextView mDoctorDept;
    private TextView mDoctorType;
    private String mDoctorId;
    public ChatRowCard(Context context, EMMessage message, int position, BaseAdapter adapter) {
        super(context, message, position, adapter);
    }

    @Override
    protected void onInflateView() {
        try {
            if (message.getStringAttribute("type").equals("card")) {
                inflater.inflate(message.direct() == EMMessage.Direct.RECEIVE ? R.layout.ease_row_received_card : R.layout.ease_row_received_card, this);
            }
        } catch (HyphenateException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onFindViewById() {
        mDoctorHead = (ImageView) findViewById(R.id.head);
        mDoctorName = (TextView) findViewById(R.id.doctorName);
        mDoctorDept = (TextView) findViewById(R.id.deptname);
        mDoctorType = (TextView) findViewById(R.id.title);
    }

    @Override
    protected void onSetUpView() {
        try {
            JSONObject jsonObject = message.getJSONObjectAttribute("doctor");
            try {
                String name = jsonObject.getString("name");
                String deptName = jsonObject.getString("deptName");
                String head = jsonObject.getString("logoImgPath");
                String title = jsonObject.getString("title");
                Glide.with(context).load(EaseConstant.HEAD_BASE + "/emedicine" + head).transform(new GlideCircleTransform(context)).into(mDoctorHead);
                mDoctorName.setText(name);
                mDoctorType.setText(title);
                mDoctorDept.setText(deptName);
                handleTextMessage();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (HyphenateException e) {
            e.printStackTrace();
        }


    }

    protected void handleTextMessage() {
        if (message.direct() == EMMessage.Direct.SEND) {
            setMessageSendCallback();
            switch (message.status()) {
                case CREATE:
                    progressBar.setVisibility(View.GONE);
                    statusView.setVisibility(View.VISIBLE);
                    // 发送消息
                    break;
                case SUCCESS: // 发送成功
                    progressBar.setVisibility(View.GONE);
                    statusView.setVisibility(View.GONE);
                    break;
                case FAIL: // 发送失败
                    progressBar.setVisibility(View.GONE);
                    statusView.setVisibility(View.VISIBLE);
                    break;
                case INPROGRESS: // 发送中
                    progressBar.setVisibility(View.VISIBLE);
                    statusView.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onUpdateView() {
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onBubbleClick() {
    }

}
