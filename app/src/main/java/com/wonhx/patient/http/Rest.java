package com.wonhx.patient.http;

/**
 * Rest
 */
public class Rest {
    private static RestPro restPro = null;

    static void init(Config config) {
        RestPro.init(config);
        restPro = RestPro.use();
    }

    public static RestPro use(String configName) {
        return RestPro.use(configName);
    }

    public static  <T> T create(Class<T> service) {
        return restPro.create(service);
    }
}
