package com.wonhx.patient.http;

import java.util.HashMap;
import java.util.Map;
import com.wonhx.patient.http.Rest;
/**
 * RestKit
 */
public class RestKit {
    /**
     * The main Config object for system
     */
    static Config config = null;

    private static Map<String, Config> configNameToConfig = new HashMap<>();

    public static final String MAIN_CONFIG_NAME = "main";

    private RestKit() {}
    public static void init(Config config) {
        Rest.init(config);
    }

    /**
     * Add Config object
     * @param config the Config
     */
    public static void addConfig(Config config) {
        if (config == null)
            throw new IllegalArgumentException("Config can not be null");
        if (configNameToConfig.containsKey(config.getName()))
            throw new IllegalArgumentException("Config already exists: " + config.getName());

        configNameToConfig.put(config.getName(), config);

        /**
         * Replace the main config if current config name is MAIN_CONFIG_NAME
         */
        if (MAIN_CONFIG_NAME.equals(config.getName()))
            RestKit.config = config;

        /**
         * The configName may not be MAIN_CONFIG_NAME,
         * the main config have to set the first comming Config if it is null
         */
        if (RestKit.config == null)
            RestKit.config = config;
    }

    public static Config getConfig() {
        return config;
    }

    public static Config getConfig(String configName) {
        return configNameToConfig.get(configName);
    }
}
