package com.wonhx.patient.http;


import com.squareup.okhttp.OkHttpClient;
import com.wonhx.patient.app.AppContext;
import com.wonhx.patient.app.AppException;
import com.wonhx.patient.kit.GsonKit;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * RestPro
 */
public class RestPro {
    private static boolean isInit = false;
    private static final Map<String, RestPro> map = new HashMap<>();
    private Config config;
    private RestAdapter adapter;
    public RestPro(String configName) {
        this.config = RestKit.getConfig(configName);
        if (this.config == null)
            throw new IllegalArgumentException("Config not found by configName: " + configName);

        final AppContext appContext = (AppContext) config.getContext();
        CookieManager cookieManager = new CookieManager(new PersistentCookieStore(appContext), CookiePolicy.ACCEPT_ALL);
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setCookieHandler(cookieManager);
        okHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);
        Client client = new OkClient(okHttpClient);
        adapter = new RestAdapter.Builder()
                .setEndpoint(config.getEndpoint())
                .setConverter(new GsonConverter(GsonKit.Gson()))
                .setErrorHandler(new ErrorHandler() {
                    @Override
                    public Throwable handleError(RetrofitError cause) {
                        Response response = cause.getResponse();
                        if (response == null) {
                            switch (cause.getKind()) {
                                case HTTP:
                                    return AppException.http(cause);
                                case NETWORK:
                                    return AppException.network(cause);
                                case CONVERSION:
                                    return AppException.json(cause);
                            }
                        }
                        if (response.getStatus() >= 200 && response.getStatus() < 300)
                            return AppException.json(cause);
                        return AppException.http(response.getStatus());
                    }
                }).setClient(client).build();
    }

    public static RestPro use(String configName) {
        RestPro result = map.get(configName);
        if (result == null) {
            result = new RestPro(configName);
            map.put(configName, result);
        }
        return result;
    }

    public static RestPro use() {
        return use(RestKit.config.name);
    }

    static synchronized void init(Config config) {
        if (isInit) return;

        RestKit.addConfig(config);

        isInit = true;
    }

    public <T> T create(Class<T> service) {
        return adapter.create(service);
    }
}
