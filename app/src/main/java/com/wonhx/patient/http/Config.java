package com.wonhx.patient.http;

import android.content.Context;

import com.wonhx.patient.kit.StrKit;

/**
 * Rest Config
 */
public class Config {
    private Context context;
    /**
     * 配置名称
     */
    String name;
    private String endpoint;
    /**
     * 调试模式
     */
    private boolean isDebug = false;

    public Config(Context context, String endpoint) {
        this(context, RestKit.MAIN_CONFIG_NAME, endpoint, null);
    }
    public Config(Context context, String name, String endpoint, Boolean isDebug) {
        if (context == null)
            throw new IllegalArgumentException("Context can not be null");
        if (StrKit.isBlank(name))
            throw new IllegalArgumentException("Config name can not be blank");
        if (StrKit.isBlank(name))
            throw new IllegalArgumentException("Endpoint can not be blank");

        this.context = context;
        this.name = name.trim();
        this.endpoint = endpoint.trim();

        if (isDebug != null)
            this.isDebug = isDebug;
    }

    public Context getContext() {
        return context;
    }

    public String getName() {
        return name;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public boolean isDebug() {
        return isDebug;
    }

    public void setDebug(boolean isDebug) {
        this.isDebug = isDebug;
    }
}
