package com.wonhx.patient.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.wonhx.patient.kit.StrKit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Model
 * @author Shenyz
 */
public abstract class Model<M extends Model> implements Serializable {

    private static final long serialVersionUID = -990334519496260591L;

    transient final Class<? extends Model> modelClass = getClass();

    /**
     * 表名
     * @return
     */
    public String Table() {
        return ModelBuilder.getTableName(getClass());
    }

    private <M> List<M> cursor2List(Cursor cursor) {
        List result = new ArrayList();
        int colAmount = cursor.getColumnCount();
        if (colAmount > 1) {
            while (cursor.moveToNext()) {
                Object[] temp = new Object[colAmount];
                for (int i = 0; i < colAmount; i++) {
                    switch (cursor.getType(i)) {
                        // TODO Impl More Type Later
                        case Cursor.FIELD_TYPE_INTEGER:
                            temp[i] = cursor.getLong(i);
                            break;
                        case Cursor.FIELD_TYPE_FLOAT:
                            temp[i] = cursor.getDouble(i);
                            break;
                        case Cursor.FIELD_TYPE_STRING:
                            temp[i] = cursor.getString(i);
                            break;
                    }
                }
                result.add(temp);
            }
        }
        else if(colAmount == 1) {
            while (cursor.moveToNext()) {
                Object obj = null;
                switch (cursor.getType(0)) {
                    // TODO Impl More Type Later
                    case Cursor.FIELD_TYPE_INTEGER:
                        obj = cursor.getLong(0);
                        break;
                    case Cursor.FIELD_TYPE_FLOAT:
                        obj = cursor.getDouble(0);
                        break;
                    case Cursor.FIELD_TYPE_STRING:
                        obj = cursor.getString(0);
                        break;
                }
                result.add(obj);
            }
        }
        cursor.close();
        return result;
    }
    <M> List<M> query(String sql, String... paras) {
        DbKit.checkTableExist(modelClass);
        String table = ModelBuilder.getTableName(modelClass);
        return cursor2List(DbKit.rawQuery(sql, paras));
    }
    public <M> M queryColumn(String sql, String... paras) {
        List<M> result = query(sql, paras);
        if (result.size() > 0) {
            M temp = result.get(0);
            if (temp instanceof Object[])
                throw new DbException("Only ONE COLUMN can be queried.");
            return temp;
        }
        return null;
    }
    public Long queryLong(String sql, String... paras) {
        return queryColumn(sql, paras);
    }
    <M> Observable<List<M>> queryRx(String sql, String... paras) {
        DbKit.checkTableExist(modelClass);
        String table = ModelBuilder.getTableName(modelClass);
        return DbKit.rxQuery(table, sql, paras).map(new Func1<Cursor, List<M>>() {
            @Override
            public List<M> call(Cursor cursor) {
                return cursor2List(cursor);
            }
        });
    }

    public <M> Observable<M> queryColumnRx(String sql, String... paras) {
        Observable<List<M>> result = queryRx(sql, paras);
        return result.map(new Func1<List<M>, M>() {
            @Override
            public M call(List<M> list) {
                if (list.size() > 0) {
                    M temp = list.get(0);
                    if (temp instanceof Object[])
                        throw new DbException("Only ONE COLUMN can be queried.");
                    return temp;
                }
                return null;
            }
        });
    }
    public Observable<Long> queryLongRx(String sql, String... paras) {
        return queryColumnRx(sql, paras);
    }

    public List<M> find(String sql, String... paras) {
        DbKit.checkTableExist(modelClass);
        Cursor cursor = DbKit.rawQuery(sql, paras);
        return ModelBuilder.build(cursor, modelClass);
    }
    public List<String> findClums(String sql, String... paras) {
        DbKit.checkTableExist(modelClass);
        Cursor cursor = DbKit.rawQuery(sql, paras);
        return ModelBuilder.build(cursor, modelClass);
    }
    public  M findFirst(String sql, String... paras) {
        List<M> list = find(sql, paras);
        return list.size() > 0 ? list.get(0) : null;
    }
    public M findById(Object idValue, String columns) {
        String primaryKey = ModelBuilder.getPrimaryKey(modelClass);
        if (StrKit.isBlank(primaryKey))
            throw new DbException(String.format("%s don't have primary key!", modelClass));
        String tableName = ModelBuilder.getTableName(modelClass);
        String sql = SqlBuilder.forDbFindById(tableName, primaryKey, columns);
        return findFirst(sql, String.valueOf(idValue));
    }
    public M findById(Object idValue) {
        return findById(idValue, "*");
    }

    public Observable<List<M>> findRx(String sql, String... paras) {
        DbKit.checkTableExist(modelClass);
        String table = ModelBuilder.getTableName(modelClass);
        return DbKit.rxQuery(table, sql, paras).map(new Func1<Cursor, List<M>>() {
            @Override
            public List<M> call(Cursor cursor) {
                return ModelBuilder.build(cursor, modelClass);
            }
        });
    }
    public Observable<M> findFirstRx(String sql, String... paras) {
        Observable<List<M>> rxList = findRx(sql, paras);
        return rxList.map(new Func1<List<M>, M>() {
            @Override
            public M call(List<M> list) {
                return list.size() > 0 ? list.get(0) : null;
            }
        });
    }
    public Observable<M> findByIdRx(Object idValue, String columns) {
        String primaryKey = ModelBuilder.getPrimaryKey(modelClass);
        if (StrKit.isBlank(primaryKey))
            throw new DbException(String.format("%s don't have primary key!", modelClass));
        String tableName = ModelBuilder.getTableName(modelClass);
        String sql = SqlBuilder.forDbFindById(tableName, primaryKey, columns);
        return findFirstRx(sql, String.valueOf(idValue));
    }

    public Observable<M> findByIdRx(Object idValue) {
        return findByIdRx(idValue, "*");
    }

    public int deleteById(Object idValue) {
        if (idValue == null)
            throw new IllegalArgumentException("id can not be null");
        DbKit.checkTableExist(modelClass);
        String primaryKey = ModelBuilder.getPrimaryKey(modelClass);
        if (StrKit.isBlank(primaryKey))
            throw new DbException(String.format("%s don't have primary key!", modelClass));
        String table = ModelBuilder.getTableName(modelClass);
        return DbKit.delete(table, primaryKey + "=?", String.valueOf(idValue));
    }

    public int delete() {
        Object primaryValue = ModelBuilder.getPrimaryValue(this);
        return deleteById(primaryValue);
    }
    public int update() {
        DbKit.checkTableExist(modelClass);
        String table = ModelBuilder.getTableName(modelClass);
        ContentValues values = ModelBuilder.getValues(this);
        String primaryKey = ModelBuilder.getPrimaryKey(modelClass);
        Object primaryValue = ModelBuilder.getPrimaryValue(this);
        return DbKit.update(table, values, primaryKey + "=?", String.valueOf(primaryValue));
    }
    public long save() {
        DbKit.checkTableExist(modelClass);
        String table = ModelBuilder.getTableName(modelClass);
        ContentValues values = ModelBuilder.getValues(this);
        return DbKit.insert(table, values);
    }
}
