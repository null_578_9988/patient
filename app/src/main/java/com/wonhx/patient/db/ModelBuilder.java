package com.wonhx.patient.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.orhanobut.logger.Logger;
import com.wonhx.patient.db.annotation.Column;
import com.wonhx.patient.db.annotation.Constraint;
import com.wonhx.patient.db.annotation.Table;
import com.wonhx.patient.db.annotation.Transient;
import com.wonhx.patient.kit.StrKit;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * ModelBuilder
 */
public class ModelBuilder {
    /**
     * Build Model
     * @param cursor
     * @return
     */
    public static <M> List<M> build(Cursor cursor, Class<? extends Model> clazz) {
        List<M> list = new ArrayList<>();
        try {
            while (cursor.moveToNext()) {
                Model<?> model = clazz.newInstance();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    Field field = getColumnField(clazz, cursor.getColumnName(i));
                    if (field == null) continue;
                    field.setAccessible(true);
                    Class<?> type = field.getType();
                    if (type == String.class) {
                        field.set(model, cursor.getString(i));
                    } else if (type == int.class || type == Integer.class) {
                        field.set(model, cursor.getInt(i));
                    } else if (type == float.class || type == Float.class) {
                        field.set(model, cursor.getFloat(i));
                    } else if (type == double.class || type == Double.class) {
                        field.set(model, cursor.getDouble(i));
                    } else if (type == long.class || type == Long.class) {
                        field.set(model, cursor.getLong(i));
                    } else if (type == boolean.class || type == Boolean.class) {
                        field.set(model, 1 == cursor.getInt(i));
                    } else if (type == Date.class) {
                        field.set(model, new Date(cursor.getLong(i)));
                    } else {
                        throw new DbException("Unsupported field type, you can add in Model.build()");
                    }
                }
                list.add((M) model);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return list;
    }

    /**
     * 数据内容
     * @return
     */
    public static ContentValues getValues(Object model) {
        ContentValues values = new ContentValues();
        try {
            List<Field> fields = getColumnFields(model.getClass());
            for (Field field : fields) {
                field.setAccessible(true);
                if (field.get(model) == null)
                    continue; // 忽略空值
                String name = field.getName();
                Class<?> type = field.getType();
                if (type == String.class) {
                    values.put(name, field.get(model).toString());
                } else if (type == int.class || type == Integer.class) {
                    values.put(name, field.getInt(model));
                } else if (type == float.class || type == Float.class) {
                    values.put(name, field.getFloat(model));
                } else if (type == double.class || type == Double.class) {
                    values.put(name, field.getDouble(model));
                } else if (type == long.class || type == Long.class) {
                    values.put(name, field.getLong(model));
                } else if (type == boolean.class || type == Boolean.class) {
                    values.put(name, field.getBoolean(model));
                } else if (type == Date.class) {
                    values.put(name, ((Date) field.get(model)).getTime());
                } else {
                    // Unsupported field type， Ignore
                    Logger.d("Unsupported field type, you can add in Model.ContentValues()");
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return values;
    }

    /**
     * 获取表名
     * @param clazz
     * @return
     */
    public static String getTableName(Class<?> clazz) {
        String tableName = "";
        Table table = clazz.getAnnotation(Table.class);
        // 取得表名
        if (table == null || StrKit.isBlank(table.name()))
            tableName = clazz.getName().replace('.', '_');
        else
            tableName = table.name();
        // 表名+版本号
        if (table != null && table.version() > 0)
            tableName += "_" + table.version();
        return tableName;
    }

    /**
     * 获取主键名
     * @param clazz
     * @return
     */
    public static String getPrimaryKey(Class<?> clazz) {
        List<Field> fields = getColumnFields(clazz);
        for (Field field : fields) {
            Constraint constraint = field.getAnnotation(Constraint.class);
            if (constraint != null) {
                for (Constraint.Type type : constraint.value()) {
                    if (type == Constraint.Type.PRIMARY_KEY) {
                        Column column = field.getAnnotation(Column.class);
                        if (column == null || StrKit.isBlank(column.name()))
                            return field.getName();
                        return column.name();
                    }
                }
            }
        }
        return null;
    }

    /**
     * 获取主键值
     * @param model
     * @return
     */
    public static Object getPrimaryValue(Object model) {
        List<Field> fields = getColumnFields(model.getClass());
        for (Field field : fields) {
            Constraint constraint = field.getAnnotation(Constraint.class);
            if (constraint != null) {
                for (Constraint.Type type : constraint.value()) {
                    if (type == Constraint.Type.PRIMARY_KEY) {
                        field.setAccessible(true);
                        try {
                            return field.get(model);
                        } catch (IllegalAccessException e) {
                            Logger.e(e);
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * 获取列名
     * @param field
     * @return
     */
    public static String getColumnName(Field field) {
        Column column = field.getAnnotation(Column.class);
        if (column == null || StrKit.isBlank(column.name()))
            return field.getName();
        return column.name();
    }

    /**
     * 获取列类型
     * @param field
     * @return
     */
    public static String getColumnType(Field field) {
        Column column = field.getAnnotation(Column.class);
        if (column == null || StrKit.isBlank(column.type())) {
            String columnType = "";
            Class<?> type =  field.getType();
            if (type == int.class || type == Integer.class || type == long.class || type == Long.class) {
                columnType = "INTEGER";
            } else if (type == float.class || type == Float.class || type == double.class || type == Double.class) {
                columnType = "REAL";
            }  else if (type == boolean.class || type == Boolean.class || type == Date.class) {
                columnType = "NUMERIC";
            }
            return columnType;
        }
        return column.type();
    }

    /**
     * 非表列
     * @param field
     * @return
     */
    public static boolean isTransient(Field field) {
        return null != field.getAnnotation(Transient.class);
    }

    /**
     * 获取列约束
     * @param field
     * @return
     */
    public static String getConstraint(Field field) {
        StringBuffer buffer = new StringBuffer();
        Constraint constraint = field.getAnnotation(Constraint.class);
        if (constraint != null) {
            for (Constraint.Type value : constraint.value()) {
                buffer.append(" ").append(value).append(" ");
            }
        }
        return buffer.toString();
    }

    /**
     * 获取该类中可以创建列的字段
     * @param clazz
     * @return
     */
    public static List<Field> getColumnFields(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        for(; clazz != Object.class; clazz = clazz.getSuperclass()) {
            for (Field field : clazz.getDeclaredFields()) {
                if (Modifier.isStatic(field.getModifiers()))
                    continue; // 忽略静态字段
                if (Modifier.isTransient(field.getModifiers()))
                    continue; // 忽略Transient字段
                if (ModelBuilder.isTransient(field))
                    continue; // 忽略注解字段
                fields.add(field);
            }
        }
        return fields;
    }

    /**
     * 通过列名获取Field
     * @param clazz
     * @param name
     * @return
     */
    public static Field getColumnField(Class<?> clazz, String name) {
        if (StrKit.isBlank(name))
            return null;
        List<Field> fields = getColumnFields(clazz);
        for (Field field : fields) {
            if (name.equals(field.getName()))
                return field;
            Column column = field.getAnnotation(Column.class);
            if (column != null && name.equals(column.name()))
                return field;
        }
        return null;
    }
}
