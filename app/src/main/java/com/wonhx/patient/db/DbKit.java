package com.wonhx.patient.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.squareup.sqlbrite.SqlBrite;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * DbKit
 */
public final class DbKit {
    public static final String USER_CONFIG_NAME = "user";
    private static Context mContext;
    private static DbOpenHelper mDbHelper;
    private static SqlBrite db;
    public static void init(Context context, String dbName, int dbVersion) {
        mTableList.clear();
        mContext = context.getApplicationContext();
        mDbHelper = new DbOpenHelper(context, dbName, dbVersion);
        db = SqlBrite.create(mDbHelper);
    }

    public static void beginTransaction() {
        db.beginTransaction();
    }
    public static void endTransaction() {
        db.endTransaction();
    }
    public static void setTransactionSuccessful() {
        db.setTransactionSuccessful();
    }

    public static int delete(String table, String whereCause, String whereArgs) {
        return db.delete(table, whereCause, whereArgs);
    }

    public static int update(String table, ContentValues values, String whereCause, String whereArgs) {
        return db.update(table, values, whereCause, whereArgs);
    }
    public static long insert(String table, ContentValues values) {
        return db.insert(table, values);
    }
    public static Observable<Cursor> rxQuery(String table, String sql, String... args) {
        return db.createQuery(table, sql, args).map(new Func1<SqlBrite.Query, Cursor>() {
            @Override
            public Cursor call(SqlBrite.Query query) {
                return query.run();
            }
        });
    }

    public static Cursor rawQuery(String sql, String... args) {
        return db.query(sql, args);
    }



    public static void execSQL(String sql) {
        mDbHelper.getWritableDatabase().execSQL(sql);
    }

    /**
     * 检查表是否存在（不存在则创建）
     * @param clazz
     */
    public static void checkTableExist(Class<?> clazz) {
        if (!tableIsExist(clazz)) {
            String sql = SqlBuilder.forCreateTable(clazz);
            DbKit.execSQL(sql);
        }
    }
    /**
     * 存在的表
     */
    private static List<Class<?>> mTableList = new ArrayList<>();
    private static boolean tableIsExist(Class<?> clazz) {
        if (mTableList.contains(clazz))
            return true;

        Cursor cursor = null;
        try {
            String sql = "SELECT COUNT(*) AS c FROM sqlite_master WHERE type ='table' AND name ='"
                    + ModelBuilder.getTableName(clazz) + "' ";
            cursor = DbKit.rawQuery(sql);
            if (cursor != null && cursor.moveToNext()) {
                int count = cursor.getInt(0);
                if (count > 0) {
                    mTableList.add(clazz);
                    return true;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
            cursor = null;
        }

        return false;
    }

    /**
     * DbOpenHelper
     * @author Shenyz
     */
    public static class DbOpenHelper extends SQLiteOpenHelper {
        public DbOpenHelper(Context context, String name, int version){
            this(context, name, null, version);
        }

        public DbOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            this(context, name, factory, version, new DefaultDatabaseErrorHandler());
        }

        public DbOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
            super(context, name, factory, version, errorHandler);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }




}
}
