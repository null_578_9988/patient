package com.wonhx.patient.db.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 约束
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Constraint {
    /**
     * 约束
     * @return
     */
    public Type[] value();

    /**
     * 约束类型
     */
    public enum Type {
        NOT_NULL,
        PRIMARY_KEY,
        AUTO_INCREMENT,
        DEFAULT
    }
}
