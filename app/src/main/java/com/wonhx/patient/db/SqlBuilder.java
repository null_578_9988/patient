package com.wonhx.patient.db;

import com.orhanobut.logger.Logger;
import com.wonhx.patient.kit.StrKit;

import java.lang.reflect.Field;
import java.util.List;

/**
 * SqlBuilder
 * @author Shenyz
 */
public class SqlBuilder {
    public static String forDbFindById(String tableName, String primaryKey, String columns) {
        StringBuilder sql = new StringBuilder("select ");
        if (columns.trim().equals("*")) {
            sql.append(columns);
        }
        else {
            String[] columnsArray = columns.split(",");
            for (int i=0; i<columnsArray.length; i++) {
                if (i > 0)
                    sql.append(", ");
                sql.append(columnsArray[i].trim());
            }
        }
        sql.append(" from ");
        sql.append(tableName.trim());
        sql.append(" where ").append(primaryKey).append(" = ?");
        return sql.toString();
    }

    public static String forDbDeleteById(String tableName, String primaryKey) {
        StringBuilder sql = new StringBuilder("delete from ");
        sql.append(tableName.trim());
        sql.append(" where ").append(primaryKey).append(" = ?");
        return sql.toString();
    }

    public static String forDbSave(Object model, List<Object> bindArgs) {
        StringBuilder sql = new StringBuilder();
        sql.append("insert into ");
        sql.append(ModelBuilder.getTableName(model.getClass())).append("(");
        StringBuilder temp = new StringBuilder();
        temp.append(") values(");

        List<Field> fields = ModelBuilder.getColumnFields(model.getClass());
        for(Field field : fields){
            field.setAccessible(true);
            try {
                if (field.get(model) == null)
                    continue; // 忽略NULL值
                bindArgs.add(field.get(model));
                sql.append(ModelBuilder.getColumnName(field)).append(",");
                temp.append("?,");
            } catch (IllegalAccessException e) {
                Logger.e(e);
            }
        }
        if(sql.lastIndexOf(",") == sql.length() - 1)
            sql.deleteCharAt(sql.length() - 1);
        if(temp.lastIndexOf(",") == temp.length() - 1)
            temp.deleteCharAt(temp.length() - 1);
        sql.append(temp).append(")");
        return sql.toString();
    }

    public static String forDbUpdate(Object model, List<Object> bindArgs) {
        String primaryKey = ModelBuilder.getPrimaryKey(model.getClass());
        if (StrKit.isBlank(primaryKey))
            throw new DbException(String.format("%s don't have primary key!", model.getClass()));
        StringBuilder sql = new StringBuilder();
        sql.append("update ").append(ModelBuilder.getTableName(model.getClass())).append(" set ");

        List<Field> fields = ModelBuilder.getColumnFields(model.getClass());
        for(Field field : fields){
            field.setAccessible(true);
            try {
                if (field.get(model) == null)
                    continue; // 忽略NULL值
                bindArgs.add(field.get(model));
                sql.append(ModelBuilder.getColumnName(field)).append("=?,");
            } catch (IllegalAccessException e) {
                Logger.e(e);
            }
        }
        if(sql.lastIndexOf(",") == sql.length() - 1)
            sql.deleteCharAt(sql.length() - 1);
        sql.append(" where ").append(primaryKey).append("=").append(ModelBuilder.getPrimaryValue(model));
        return sql.toString();
    }

    public static String forPaginate(int pageNumber, int pageSize, String select, String sqlExceptSelect) {
        StringBuilder sql = new StringBuilder();
        int offset = pageSize * (pageNumber - 1);
        sql.append(select).append(" ");
        sql.append(sqlExceptSelect);
        sql.append(" limit ").append(offset).append(", ").append(pageSize);
        return sql.toString();
    }
    /**
     * Table Create Sql
     * @param clazz
     * @return
     */
    public static String forCreateTable(Class<?> clazz){
        StringBuffer strSQL = new StringBuffer();
        strSQL.append("CREATE TABLE IF NOT EXISTS ");
        // 表名
        strSQL.append(ModelBuilder.getTableName(clazz));
        strSQL.append(" ( ");

        List<Field> fields = ModelBuilder.getColumnFields(clazz);
        for(Field field : fields){
            // 字段名
            strSQL.append(ModelBuilder.getColumnName(field));
            // 类型
            strSQL.append(" ").append(ModelBuilder.getColumnType(field));
            // 约束
            strSQL.append(ModelBuilder.getConstraint(field));
            strSQL.append(",");
        }
        if(strSQL.lastIndexOf(",") == strSQL.length() - 1)
            strSQL.deleteCharAt(strSQL.length() - 1);

        strSQL.append(" )");
        return strSQL.toString();
    }

    /**
     * 统计表行数
     * @param modelClass
     * @return
     */
    public static String forCountTable(Class<?> modelClass) {
        return "select count(*) from " + ModelBuilder.getTableName(modelClass);
    }
}
