package com.wonhx.patient.wxapi;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.user.DepositActivity;
import com.wonhx.patient.app.activity.user.MyFamilyDoctorActivity;
import com.wonhx.patient.app.activity.user.PayFinishActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.UpdateUIListenner;

public class WXPayEntryActivity extends BaseActivity implements IWXAPIEventHandler {


	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (myReceiver!=null){
			unregisterReceiver(myReceiver);
		}
	}
    private IWXAPI api;
    MyReceiver myReceiver;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wxpay_entry);
    	api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);
		api.handleIntent(getIntent(), this);
    }
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		api.handleIntent(intent, this);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("ExitApp");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String memberId) {
                finish();
            }
        });
	}
	@Override
	public void onReq(BaseReq req) {
	}
	@Override
	public void onResp(BaseResp resp) {
		Log.e("tag",resp.errStr+"sssss");
		switch (resp.errCode) {
			case 0:
				if (Constants.flag == 1) {
					//startActivity(new Intent(WXPayEntryActivity.this, DepositActivity.class));
					Intent intent = new Intent();
					intent.setAction("FINISHA");
					sendBroadcast(intent);
					//充值
					finish();
				} else if (Constants.flag == 2) {
					Intent intent=new Intent(WXPayEntryActivity.this,PayFinishActivity.class);
					intent.putExtra("price","");
					intent.putExtra("flag","1");
					intent.putExtra("msg","支付成功");
					startActivity(intent);
					//药品支付
					finish();
				} else if (Constants.flag == 3) {
					Intent intent = new Intent();
					intent.setAction("PAYFINISH");
					sendBroadcast(intent);
					//服务支付
					finish();
				}else if (Constants.flag==4){
					Intent intent = new Intent();
					intent.setAction("QUIT");
					sendBroadcast(intent);
					//服务支付
					startActivity(new Intent(this, MyFamilyDoctorActivity.class));
					finish();
				}
				break;
			case -1:
				Toast.makeText(getApplicationContext(), "可能的原因：签名错误、未注册APPID、项目设置APPID不正确、注册的APPID与设置的不匹配、其他异常等。", Toast.LENGTH_SHORT).show();
				break;
			case -2:
				if (Constants.flag == 1||Constants.flag == 3){
					finish();
				}else if (Constants.flag==2){
					Intent intent=new Intent(WXPayEntryActivity.this,PayFinishActivity.class);
					intent.putExtra("price","");
					intent.putExtra("flag","0");
					intent.putExtra("msg","用户取消支付");
					startActivity(intent);
                    finish();
				}
				Toast.makeText(getApplicationContext(), "用户取消支付", Toast.LENGTH_SHORT).show();
				break;
		}

	}
}






