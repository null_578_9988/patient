package com.wonhx.patient.app.model;

import java.io.Serializable;

/**
 * Created by nsd on 2017/4/18.
 *
 */

public class AskOrderResult implements Serializable{
    private String order_id;
    private String service_price;
    private String consultation_request_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getService_price() {
        return service_price;
    }

    public void setService_price(String service_price) {
        this.service_price = service_price;
    }

    public String getConsultation_request_id() {
        return consultation_request_id;
    }

    public void setConsultation_request_id(String consultation_request_id) {
        this.consultation_request_id = consultation_request_id;
    }
}
