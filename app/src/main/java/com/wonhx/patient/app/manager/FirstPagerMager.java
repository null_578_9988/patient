package com.wonhx.patient.app.manager;

import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Administrator on 2017/4/1 0001.
 */

public interface FirstPagerMager {
    /**
     * 获取首页轮播图
     * @param subscriber
     * @return
     */
    public Subscription getBanner(Subscriber subscriber);

    /**
     * 获取推荐科室
     * @param subscriber
     * @return
     */
    public Subscription getTuijiankeshi(Subscriber subscriber);
    /**
     * 根据推荐科室的id获取推荐医生的列表
     * @param dept_id
     * @param subscriber
     * @return
     */
    public Subscription getDoctor(String dept_id,String start,String length,Subscriber subscriber);

    /**
     * 患者關注醫生
     * @param member_id
     * @param doctor_id
     * @param flag
     * @param subscriber
     * @return
     */
    public Subscription guanzhuyisheng(String member_id,String doctor_id,String flag ,Subscriber subscriber);



    /**
     * 获取名医列表
     * @param subscriber
     * @return
     */
    public Subscription getSuperDoctor(String start,String length,Subscriber subscriber);

    /**
     * 获取名院列表
     * @param subscriber
     * @return
     */
    public Subscription getSuperhospital(String start,String length,Subscriber subscriber);

    /**
     * 名医名院预约
     * @param patient_name
     * @param birthday
     * @param sex
     * @param phone
     * @param descr
     * @param member_id
     * @param status
     * @param doctor_id
     * @param hos_id
     * @param subscriber
     * @return
     */
    public Subscription postyuyue(String patient_name,String birthday,String sex,String phone,String descr,String  member_id,String status,String doctor_id,String hos_id,Subscriber subscriber);
    /**
     * 获取医生详情
     * @param member_id
     * @param subscriber
     * @return
     */
    public Subscription doctordetial(String member_id,String patient_member_id,Subscriber subscriber);

    /**
     * 服务咨询提交订单
     * @param member_id
     * @param doctor_id
     * @param price_id
     * @param self_description
     * @param sche_id
     * @param time
     * @param hospital
     * @param desc
     * @param medical
     * @param subscriber
     * @return
     */
    public Subscription submitPicAskOrder(
             String member_id
            ,String doctor_id
            ,String price_id
            ,String self_description
            ,String sche_id
            ,String time
            ,String hospital
            ,String desc
            ,String medical
            ,Subscriber subscriber);

    /**
     * 提交订单档案图片
     * @param member_id
     * @param consultation_id
     * @param param_name
     * @param file_content
     * @param subscriber
     * @return
     */
    public Subscription updateOrderImg(String member_id,String consultation_id,String param_name,String file_content,Subscriber subscriber);

    /**
     * 余额支付
     * @param order_id
     * @param subscriber
     * @return
     */
    public Subscription payWithBlance(String order_id,Subscriber subscriber);

    /**
     * 家庭医生余额支付
     * @param order_id
     * @param subscriber
     * @return
     */
    public Subscription payWithBlanceFamily(String order_id,Subscriber subscriber);

    /**
     * 获取环信医生用户名
     * @param member_id
     * @param subscriber
     * @return
     */
    public Subscription getDoctorEMCName(String member_id,Subscriber subscriber);

    /**
     * 搜索医生列表
     * @param keyword
     * @param subscriber
     * @return
     */
    public Subscription search(String keyword,String start,String length,Subscriber subscriber);

    /**
     * 获取省份 城市
     * @param subscriber
     * @return
     */
    public Subscription get_prvince_city(Subscriber subscriber);
    /**
     * 搜索诊所
     * @param keyoord
     * @param
     * @return
     */
    public Subscription searchClinic(String keyoord,Subscriber subscriber);

    /**
     * 获取诊所详情
     * @param clinicId
     * @param subscriber
     * @return
     */
    public Subscription getClinicDetail(String clinicId, Subscriber subscriber);
    /**
     * 获取诊所医生列表
     * @param clinicId
     * @param subscriber
     * @return
     */
    public Subscription getClinicMembers(String clinicId, Subscriber subscriber);

    /**
     * 获取移动诊所列表
     * @param city_id
     * @param dept_id
     * @param start
     * @param length
     * @param subscriber
     * @return
     */
    public Subscription getmovehospital(String city_id,String city_name,String dept_id,String start,String length,Subscriber subscriber);

    /**
     * 获取所有科室
     * @param subscriber
     * @return
     */
    public Subscription get_zhensuo(Subscriber subscriber);

    /**
     * 获取医生服务价格跟时间
     * @param memberId
     * @param subscriber
     * @return
     */
    public Subscription getDoctorPrice(String memberId,Subscriber subscriber);
    /**
     * 获取家庭医生列表
     * @param city_id
     * @param dept_id
     * @param sort
     * @param start
     * @param length
     * @param subscriber
     * @return
     */
    public Subscription getFamilyDoctorList(String city_id,String dept_id,String sort,String start,String length,Subscriber subscriber);

    /**
     * 获取搜索家庭医生列表
     * @param keyword
     * @param start
     * @param length
     * @param subscriber
     * @return
     */
    public Subscription getFamilyDoctor_searchList(String keyword,Subscriber subscriber);

    /**
     * 家庭医生生成订单
     * @param service_price
     * @param family_type
     * @param member_id
     * @param doctor_id
     * @param subscriber
     * @return
     */
    public Subscription getFamilyDoctor_oeder(String service_price,String family_type,String member_id,String doctor_id,Subscriber subscriber);

    /**
     * 获取我的家庭医生列表
     * @param member_id
     * @param start
     * @param length
     * @return
     */
    public Subscription getmyfamilylist(String member_id,String start,String length,Subscriber subscriber);

    /**
     * 获取当前值班的医生id
     * @param clinic_id
     * @param subscriber
     * @return
     */
    public Subscription getNowTimeDoctor(String clinic_id,Subscriber subscriber);

    /**
     * 获取服务器当前时间
     * @param subscriber
     * @return
     */
    public Subscription getSystime(Subscriber subscriber);

    /**
     * 根据经纬度获取附近的家庭医生列表
     * @param lng
     * @param lat
     * @param subscriber
     * @return
     */
    public Subscription getNearDoctor(String lng,String lat,Subscriber subscriber);

    /**
     * 获取附近诊所
     *
     * @param lng
     * @param lat
     * @param subscriber
     * @return
     */
    public Subscription getNearClic(String lng,String lat,Subscriber subscriber);


}
