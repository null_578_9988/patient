package com.wonhx.patient.app.manager.FirstPager;

import com.baidu.mapapi.map.MarkerOptions;
import com.wonhx.patient.app.AppException;
import com.wonhx.patient.app.baidu.map.BikeInfo;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.AskOrderResult;
import com.wonhx.patient.app.model.BannerList;
import com.wonhx.patient.app.model.ClinicMember;
import com.wonhx.patient.app.model.DoctorDetial;
import com.wonhx.patient.app.model.FamialyDoctors;
import com.wonhx.patient.app.model.FamilyDoctorSearchList;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.MoveHospital;
import com.wonhx.patient.app.model.NearClinct;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.ProvinceCity;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.ResultPrice;
import com.wonhx.patient.app.model.Resultt;
import com.wonhx.patient.app.model.SearchHospital;
import com.wonhx.patient.app.model.SupurDoctor;
import com.wonhx.patient.app.model.SupurHosptial;
import com.wonhx.patient.app.model.TuijianDoctor;
import com.wonhx.patient.app.model.Tuijiankeshi;
import com.wonhx.patient.app.model.Zhnsuo;
import com.wonhx.patient.http.Rest;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2017/4/1 0001.
 */

public class FirstPagerMangerImal implements FirstPagerMager{
    @Override
    public Subscription getBanner(Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe< ListProResult<BannerList>>(){
            @Override
            public void call(Subscriber<? super  ListProResult<BannerList>> subscriber) {
                try {
                    ListProResult<BannerList> result=restServer.getbanner();
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getTuijiankeshi(Subscriber subscriber) {
        return  Observable.create(new Observable.OnSubscribe< ListProResult<Tuijiankeshi>>(){
            @Override
            public void call(Subscriber<? super  ListProResult<Tuijiankeshi>> subscriber) {
                try {
                    ListProResult<Tuijiankeshi> result=restServer.gettuijiankehi();
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getDoctor(final String dept_id,  final String start, final String length, Subscriber subscriber) {
        return  Observable.create(new Observable.OnSubscribe< ListProResult<TuijianDoctor>>(){
            @Override
            public void call(Subscriber<? super  ListProResult<TuijianDoctor>> subscriber) {
                try {
                    ListProResult<TuijianDoctor> result=restServer.getDoctor(dept_id,start,length);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription guanzhuyisheng(final String member_id, final String doctor_id, final String flag, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>(){
            @Override
            public void call(Subscriber<? super  Result> subscriber) {
                try {
                    Result result=restServer.guanzhuyisheng(member_id,doctor_id,flag);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }


    @Override
    public Subscription getSuperDoctor(final String start, final String length, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<SupurDoctor>>(){
            @Override
            public void call(Subscriber<? super  ListProResult<SupurDoctor>> subscriber) {
                try {
                    ListProResult<SupurDoctor> result=restServer.getsuperdoctor(start,length);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getSuperhospital(final String start, final String length, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<SupurHosptial>>(){
            @Override
            public void call(Subscriber<? super  ListProResult<SupurHosptial>> subscriber) {
                try {
                    ListProResult<SupurHosptial> result=restServer.getsupehosptial(start,length);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }
    @Override
    public Subscription postyuyue(final String patient_name, final String birthday, final String sex, final String phone, final String descr, final String member_id, final String status, final String doctor_id, final String hos_id, Subscriber subscriber) {
        return  Observable.create(new Observable.OnSubscribe< Result>(){
            @Override
            public void call(Subscriber<? super  Result> subscriber) {
                try {
                    Result result=restServer.submitfamous(patient_name,birthday,sex,phone,descr,member_id,status,doctor_id,hos_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription doctordetial(final String member_id, final String patient_member_id, final Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<DoctorDetial>>(){
            @Override
            public void call(Subscriber<? super  ProResult<DoctorDetial>> subscriber) {
                try {
                    ProResult<DoctorDetial> result=restServer.doctordetial(member_id,patient_member_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription submitPicAskOrder(final String member_id, final String doctor_id, final String price_id
            , final String self_description, final String sche_id, final String time, final String hospital, final String desc, final String medical, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<AskOrderResult>>(){
            @Override
            public void call(Subscriber<? super  ProResult<AskOrderResult>> subscriber) {
                try {
                    ProResult<AskOrderResult> result=restServer.submitPicAskOrder(member_id,doctor_id,price_id,self_description,sche_id,time,hospital,desc,medical);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription updateOrderImg(final String member_id, final String consultation_id, final String param_name, final String file_content, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe< Result>(){
            @Override
            public void call(Subscriber<? super  Result> subscriber) {
                try {
                    Result result=restServer.updateOrderimg(member_id,consultation_id,param_name,file_content);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription payWithBlance(final String order_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe< Result>(){
            @Override
            public void call(Subscriber<? super  Result> subscriber) {
                try {
                    Result result=restServer.payWithBlance(order_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription payWithBlanceFamily(final String order_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe< Result>(){
            @Override
            public void call(Subscriber<? super  Result> subscriber) {
                try {
                    Result result=restServer.postpaywithbanlence(order_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getDoctorEMCName(final String member_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe< Result>(){
            @Override
            public void call(Subscriber<? super  Result> subscriber) {
                try {
                    Result result=restServer.getDoctorEMCName(member_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription search(final String keyword, final String start, final String length, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe< ListProResult<TuijianDoctor>>(){
            @Override
            public void call(Subscriber<? super  ListProResult<TuijianDoctor>> subscriber) {
                try {
                    ListProResult<TuijianDoctor> result=restServer.search(keyword,start,length);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription get_prvince_city(Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe< ListProResult<ProvinceCity>>(){
            @Override
            public void call(Subscriber<? super  ListProResult<ProvinceCity>> subscriber) {
                try {
                    ListProResult<ProvinceCity> result=restServer.getprovienceciry();
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getmovehospital(final String city_id, final String city_name,final String dept_id, final String start, final String length, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe< ListProResult<MoveHospital>>(){
            @Override
            public void call(Subscriber<? super  ListProResult<MoveHospital>> subscriber) {
                try {
                    ListProResult<MoveHospital> result=restServer.getmovehospital(city_id,city_name,dept_id,start,length);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription searchClinic(final String keyoord, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<SearchHospital>>() {
            @Override
            public void call(Subscriber<? super ListProResult<SearchHospital>> subscriber) {
                try {
                    ListProResult<SearchHospital> result = restServer.searchClinic(keyoord);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getClinicDetail(final String clinicId, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<MoveHospital>>() {
            @Override
            public void call(Subscriber<? super ProResult<MoveHospital>> subscriber) {
                try {
                    ProResult<MoveHospital> result = restServer.getClinicDetail(clinicId);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getClinicMembers(final String clinicId, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<ClinicMember>>() {
            @Override
            public void call(Subscriber<? super ListProResult<ClinicMember>> subscriber) {
                try {
                    ListProResult<ClinicMember> result = restServer.getClinicMember(clinicId);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);

    }

    @Override
    public Subscription get_zhensuo(Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe< ListProResult<Zhnsuo>>() {
            @Override
            public void call(Subscriber<? super  ListProResult<Zhnsuo>> subscriber) {
                try {
                    ListProResult<Zhnsuo> result = restServer.getZhnsuo();
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getDoctorPrice(final String memberId, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<ResultPrice>>() {
            @Override
            public void call(Subscriber<? super ProResult<ResultPrice>> subscriber) {
                try {
                    ProResult<ResultPrice> result = restServer.getDoctorPrice(memberId);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getFamilyDoctorList(final String city_id, final String dept_id, final String sort, final String start, final String length, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<FamialyDoctors>>() {
            @Override
            public void call(Subscriber<? super ListProResult<FamialyDoctors>> subscriber) {
                try {
                    ListProResult<FamialyDoctors> result = restServer.getFamilyDoctoes(city_id,dept_id,sort,start,length);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getFamilyDoctor_searchList(final String keyword,  Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<FamilyDoctorSearchList>>() {
            @Override
            public void call(Subscriber<? super ListProResult<FamilyDoctorSearchList>> subscriber) {
                try {
                    ListProResult<FamilyDoctorSearchList> result = restServer.getFamilyDoctoesSearchList(keyword);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getFamilyDoctor_oeder(final String service_price, final String family_type, final String member_id, final String doctor_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = restServer.getorderid(service_price,family_type,member_id,doctor_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getmyfamilylist(final String member_id, final String start, final String length, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<FamialyDoctors>>() {
            @Override
            public void call(Subscriber<? super ListProResult<FamialyDoctors>> subscriber) {
                try {
                    ListProResult<FamialyDoctors> result = restServer.getmydoctorlist(member_id,start,length);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getNowTimeDoctor(final String clinic_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = restServer.getNowTimeDoctor(clinic_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getSystime(Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Resultt>() {
            @Override
            public void call(Subscriber<? super Resultt> subscriber) {
                try {
                    Resultt result = restServer.getSystime();
                    if (!result.getCode().equals("0"))
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getNearDoctor(final String lng, final String lat, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<BikeInfo>>() {
            @Override
            public void call(Subscriber<? super ListProResult<BikeInfo>> subscriber) {
                try {
                    ListProResult<BikeInfo> result = restServer.getneardoctor(lng,lat);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getNearClic(final String lng, final String lat, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<NearClinct>>() {
            @Override
            public void call(Subscriber<? super ListProResult<NearClinct>> subscriber) {
                try {
                    ListProResult<NearClinct> result = restServer.getnearClic(lng,lat);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }


    /**
     * REST Service
     */
    RestServer restServer=Rest.create(RestServer.class);
    interface  RestServer{
        /**
         * 获取轮播图
         * @return
         */
        @GET("/patient_concern/carousel_figure")
        ListProResult<BannerList>getbanner();

        /**
         * 获取推荐科室
         * @return
         */
        @GET("/patient_concern/recommended_dept")
        ListProResult<Tuijiankeshi>gettuijiankehi();

        /**
         * 根据推荐科室的id获取医生列表
         * @param dept_id
         * @return
         */
        @GET("/patient_concern/get_dept_doctor")
        ListProResult<TuijianDoctor>getDoctor(@Query("dept_id")String dept_id,@Query("start")String start,@Query("length")String length);
        /**
         * 患者關注医生 取消关注医生
         * @param member_id
         * @param doctor_id
         * @param flag
         * @return
         */

        @GET("/patient_concern/followed_mecal")
        Result guanzhuyisheng(@Query("member_id") String member_id,@Query("doctor_id") String doctor_id,@Query("flag") String flag);


        /**
         * 获取名医列表
         * @return
         */
        @GET("/doctor_appointment/get_appointment_doctor")
        ListProResult<SupurDoctor>getsuperdoctor(@Query("start")String start,@Query("length")String length);

        /**
         * 获取名院列表
         * @return
         */
        @GET("/doctor_appointment/get_appointment_hospital")
        ListProResult<SupurHosptial>getsupehosptial(@Query("start")String start,@Query("length")String length);

        /**
         * 提交预约名院名医
         * @param patient_name
         * @param birthday
         * @param sex
         * @param phone
         * @param descr
         * @param member_id
         * @param status
         * @param doctor_id
         * @param hos_id
         * @return
         */
        @FormUrlEncoded
        @POST("/doctor_appointment/appointment")
        Result submitfamous(@Field("patient_name")String patient_name,@Field("birthday")String birthday,@Field("sex")String sex,@Field("phone")String phone,@Field("descr")String descr,@Field("member_id")String member_id,@Field("status")String status,@Field("doctor_id")String doctor_id,@Field("hos_id")String hos_id);
        /**
         * 获取医生详情
         * @param member_id
         * @return
         */
        @GET("/patient_concern/get_doctor_message")
        ProResult<DoctorDetial> doctordetial(@Query("member_id")String member_id,@Query("patient_member_id") String patient_member_id);

        /**
         * 服务咨询提交订单
         * @param member_id
         * @param doctor_id
         * @param price_id
         * @param self_description
         * @param sche_id
         * @param time
         * @param hospital
         * @param desc
         * @param medical
         * @return
         */
        @FormUrlEncoded
        @POST("/consultation_service/service_order_submit")
        ProResult<AskOrderResult> submitPicAskOrder(@Field("member_id") String member_id
                ,@Field("doctor_id") String doctor_id,@Field("price_id") String price_id
                ,@Field("self_description") String self_description,@Field("sche_id") String sche_id
                ,@Field("time") String time,@Field("hospital") String hospital
                ,@Field("desc") String desc,@Field("medical") String medical);

        /**
         * 提交订单档案图片
         * @param member_id
         * @param consultation_id
         * @param param_name
         * @param file_content
         * @return
         */
        @FormUrlEncoded
        @POST("/consultation_service/upload_medical_record")
        Result updateOrderimg(@Field("member_id") String member_id
                ,@Field("consultation_id") String consultation_id
                ,@Field("param_name") String param_name
                ,@Field("file_content") String file_content);

        /**
         * 余额支付
         * @param order_id
         * @return
         */
        @FormUrlEncoded
        @POST("/pay/service_pay")
        Result payWithBlance(@Field("order_id") String order_id);

        /**
         * 根据ID获取医生环信用户名
         * @param member_id
         * @return
         */
        @GET("/consultation_service/member_to_huanxin")
        Result getDoctorEMCName(@Query("member_id") String member_id);

        /**
         * 搜索医生列表
         * @param keyword
         * @return
         */
        @GET("/patient_concern/doctor_search")
        ListProResult<TuijianDoctor> search(@Query("keyword")String keyword,@Query("start")String start,@Query("length")String length);

        /**
         * 获取省份和城市
         * @return
         */
        @POST("/doctor_message/get_city")
        ListProResult<ProvinceCity>getprovienceciry();
        /**
         * 搜索诊所
         *
         * @return
         */
        @GET("/mobile_clinic/search_clinic")
        ListProResult<SearchHospital> searchClinic(@Query("keyword") String keyword);

        /**
         * 获取诊所详情
         * @param clinic_id
         * @return
         */
        @GET("/mobile_clinic/patient_clinic_detail")
        ProResult<MoveHospital> getClinicDetail(@Query("clinic_id") String clinic_id);

        /**
         * 获取诊所成员列表
         *
         * @return
         */
        @GET("/mobile_clinic/clinic_medicine_list")
        ListProResult<ClinicMember> getClinicMember(@Query("clinic_id") String clinic_id);
        /**
         * 获取移动诊所列表
         * @param city_id
         * @param dept_id
         * @param start
         * @param length
         * @return
         */
        @GET("/mobile_clinic/area_search_clinic")
        ListProResult<MoveHospital>getmovehospital(@Query("city_id")String city_id,@Query("city_name")String city_name,@Query("dept_id")String dept_id,@Query("start")String start,@Query("length")String length);
        /**
         * 获取所有科室
         * @return
         */
        @GET("/patient_concern/all_dept/")
       ListProResult<Zhnsuo>  getZhnsuo();
        /**
         * 获取医生服务价格跟时间
         * @param memberId
         * @return
         */
        @GET("/patient_concern/doctor_seivice_price")
        ProResult<ResultPrice> getDoctorPrice(@Query("member_id") String memberId);

        /**
         * 获取家庭医生列表
         * @param city_id
         * @param dept_id
         * @param sort
         * @param start
         * @param length
         * @return
         */
        @GET("/family_doctor/search_doctor")
        ListProResult<FamialyDoctors> getFamilyDoctoes(@Query("city_id")String city_id,@Query("dept_id")String dept_id,@Query("sort")String sort,@Query("start")String start,@Query("length")String length);

        /**
         * 获取搜索家庭医生列表
         * @param keyword

         * @return
         */
        @GET("/family_doctor/search_keyword")
        ListProResult<FamilyDoctorSearchList>getFamilyDoctoesSearchList(@Query("keyword")String keyword);
        /**
         * 家庭医生余额支付
         * @param order_id
         * @return
         */
        @FormUrlEncoded
        @POST("/pay/family_doctor_pay")
        Result postpaywithbanlence(@Field("order_id")String order_id);
        /**
         *获取家庭医生支付id
         * @param
         * @return
         */
        @FormUrlEncoded
        @POST("/family_doctor/submit_family_order")
        Result getorderid(@Field("service_price")String service_price,@Field("family_type")String family_type,@Field("member_id")String member_id,@Field("doctor_id")String doctor_id);
        /**
         * 获取我的家庭医生；列表
         * @param member_id
         * @param start
         * @param length
         * @return
         */
        @GET("/family_doctor/my_family_doctor")
        ListProResult<FamialyDoctors> getmydoctorlist(@Query("member_id")String member_id,@Query("start")String start,@Query("length")String length);

        /**
         * 获取当前值班医生的id
         * @param clinic_id
         * @return
         */
        @GET("/mobile_clinic/current_rota_doctor")
        Result getNowTimeDoctor(@Query("clinic_id")String clinic_id);

        /**
         * 获取当前系统时间
         * @return
         */
        @GET("/family_doctor/get_sys_time")
        Resultt getSystime();

        /**
         * 根据经纬度获取附近的家庭医生
         * @param lng
         * @param lat
         * @return
         */
        @FormUrlEncoded
        @POST("/family_doctor/near_by_doctor")
        ListProResult<BikeInfo> getneardoctor(@Field("lng")String lng,@Field("lat")String lat);
        /**
         * 根据经纬度获取附近的诊所
         * @param lng
         * @param lat
         * @return
         */
        @FormUrlEncoded
        @POST("/mobile_clinic/near_by_clinic")
        ListProResult<NearClinct> getnearClic(@Field("lng")String lng, @Field("lat")String lat);
    }
}
