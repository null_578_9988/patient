package com.wonhx.patient.app.activity.user;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.tencent.mm.opensdk.constants.Build;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.unionpay.UPPayAssistEx;
import com.unionpay.uppay.PayActivity;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.WeXinBean;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.PayResult;

import butterknife.InjectView;
import butterknife.OnClick;

public class DepositActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView tv_title;
    @InjectView(R.id.deposit_value)
    EditText ed_rmb;
    @InjectView(R.id.zhifubao)
    TextView aliButton;
    @InjectView(R.id.yinlian)
    TextView yinlianBtn;
    @InjectView(R.id.tv_weixin)
    TextView tv_weixin;
    int payType=1;
    UserManager  userManager=new UserManagerImpl();
    String zhifubaopayInfo="",yinlianpayinfo="";
    private String mMode = "00";//设置测试模式:01为测试 00为正式环境
    private String tn="";
    private IWXAPI api;
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        //startActivity(new Intent(ZhifuActivity.this, DingdanActivity.class));
                        Toast.makeText(DepositActivity.this, "充值成功！" +
                                "", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(DepositActivity.this, "支付结果确认中", Toast.LENGTH_SHORT).show();

                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            //Toast.makeText(ZhifuActivity.this, "支付失败", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                }
                case  2:{
                    if (msg.obj == null || ((String) msg.obj).length() == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(DepositActivity.this);
                        builder.setTitle("错误提示");
                        builder.setMessage("网络连接失败,请重试!");
                        builder.setNegativeButton("确定",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.create().show();
                    } else {
                        tn = (String) msg.obj;
                        doStartUnionPayPlugin(DepositActivity.this, tn, mMode);
                    }
                }
                default:
                    break;
            }

        }
    };
    @Override
    protected void onInitView() {
        super.onInitView();
        tv_title.setText("余额充值");
        api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);
        api.registerApp(Constants.APP_ID);
    }
    MyReceiver myReceiver;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            unregisterReceiver(myReceiver);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("FINISHA");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String memberId) {
                finish();
            }
        });
    }
    @OnClick(R.id.left_btn)
    void back(){
        finish();
    }
    @OnClick(R.id.second_pay)
    void second(){
        payType=2;
        aliButton.setVisibility(View.GONE);
        yinlianBtn.setVisibility(View.GONE);
        tv_weixin.setVisibility(View.VISIBLE);
    }
    @OnClick(R.id.one_pay)
    void one(){
        payType=1;
        aliButton.setVisibility(View.VISIBLE);
        yinlianBtn.setVisibility(View.GONE);
        tv_weixin.setVisibility(View.GONE);
    }
    @OnClick(R.id.third_pay)
    void third(){
        payType=3;
        aliButton.setVisibility(View.GONE);
        yinlianBtn.setVisibility(View.VISIBLE);
        tv_weixin.setVisibility(View.GONE);
    }
    @OnClick(R.id.submit)
    void submit(){
        String memberid= AbSharedUtil.getString(DepositActivity.this,"userId");
        if (!TextUtils.isEmpty(ed_rmb.getText().toString())){
//            if (Integer.parseInt(ed_rmb.getText().toString())==0){
//                Toaster.showShort(DepositActivity.this,"请输入正确的金额");
//            }else {
                userManager.send_money(memberid,ed_rmb.getText().toString().trim(),new SubscriberAdapter<Result>(){
                    @Override
                    public void success(Result result) {
                        super.success(result);
                        //支付宝
                        if (payType==1){
                            userManager.zhifubao(result.getData(),"C",new SubscriberAdapter<Result>(){
                                @Override
                                public void success(Result result) {
                                    super.success(result);
                                    zhifubaopayInfo=result.getData();
                                         Runnable runnable=new Runnable() {
                                        @Override
                                        public void run() {
                                            PayTask alipay = new PayTask(DepositActivity.this);
                                            // 调用支付接口，获取支付结果
                                            String result = alipay.pay(zhifubaopayInfo, true);
                                            Message msg = new Message();
                                            msg.what = 1;
                                            msg.obj = result;
                                            handler.sendMessage(msg);
                                        }
                                    };
                                    Thread payThread = new Thread(runnable);
                                    payThread.start();
                                }
                            });
                            //微信
                        }if (payType==2){
                            boolean sIsWXAppInstalledAndSupported   = api.isWXAppInstalled()
                                    && api.isWXAppSupportAPI();
                            if (sIsWXAppInstalledAndSupported ) {
                                boolean isPaySupported = api.getWXAppSupportAPI() >= Build.PAY_SUPPORTED_SDK_INT;
                                if (isPaySupported) {
                                    Log.e("ssss", result.getData() + "");
                                    //Toaster.showShort(OrderPayActivity.this,"暂时未开通！");
                                    userManager.weixin(result.getData(), "C2", new SubscriberAdapter<WeXinBean>() {
                                        @Override
                                        public void success(WeXinBean result) {
                                            super.success(result);
                                            weixinpay(result);
                                        }
                                    });
                                }else {
                                    Toaster.showShort(DepositActivity.this,"此版本还不支持微信支付");
                                }
                            }else {
                                Toaster.showShort(DepositActivity.this,"还未安装微信");
                            }
                            //银行卡
                        }else if (payType==3){
                        userManager.yinlian(result.getData(),"C1",new SubscriberAdapter<Result>(){
                            @Override
                            public void success(Result result) {
                                super.success(result);
                                yinlianpayinfo=result.getTn();
                                Runnable runnable=new Runnable() {
                                    @Override
                                    public void run() {
                                        // 调用支付接口，获取支付结果
                                        Message msg = new Message();
                                        msg.what = 2;
                                        msg.obj = yinlianpayinfo;
                                        handler.sendMessage(msg);
                                    }
                                };
                                Thread payThread = new Thread(runnable);
                                payThread.start();

                            }
                        });
                        }

                    }
                });
           // }
        }else {
            Toaster.showShort(DepositActivity.this,"请填写充值金额");
        }
    }
    public void doStartUnionPayPlugin(Activity activity, String tn, String mode) {
        UPPayAssistEx.startPayByJAR(activity, PayActivity.class, null, null,
                tn, mode);
    }
    public void weixinpay(WeXinBean result) {
        PayReq payreq=new PayReq();
        payreq.appId=result.getAppid();
        payreq.partnerId=result.getPartnerid();
        payreq.prepayId=result.getPrepayid();
        payreq.nonceStr=result.getNoncestr();
        payreq.timeStamp=result.getTimestamp();
        payreq.packageValue="Sign=WXPay";
        payreq.sign=result.getSign();
        Constants.flag=1;
        api.sendReq(payreq);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        String msg = "";
        /*
         * 支付控件返回字符串:success、fail、cancel 分别代表支付成功，支付失败，支付取消
         */
        String str = data.getExtras().getString("pay_result");
        Log.e("zftphone", "2 "+data.getExtras().getString("merchantOrderId"));
        if (str.equalsIgnoreCase("success")) {
            msg = "充值成功！";
            Toaster.showShort(DepositActivity.this,msg);
            finish();
        } else if (str.equalsIgnoreCase("fail")) {
            msg = "充值失败！";
            Toaster.showShort(DepositActivity.this,msg);
        } else if (str.equalsIgnoreCase("cancel")) {
            msg = "用户取消了支付";
            Toaster.showShort(DepositActivity.this,msg);
        }
        //支付完成,处理自己的业务逻辑!
    }
}
