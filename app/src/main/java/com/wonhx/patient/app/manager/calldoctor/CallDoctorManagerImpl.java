package com.wonhx.patient.app.manager.calldoctor;

import com.wonhx.patient.app.AppException;
import com.wonhx.patient.app.manager.CallDoctorManager;
import com.wonhx.patient.app.model.CallUser;
import com.wonhx.patient.app.model.CodeData;
import com.wonhx.patient.app.model.DoctorList;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Record;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.http.Rest;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by apple on 17/3/30.
 * 呼叫医生模块接口实现
 */
public class CallDoctorManagerImpl implements CallDoctorManager {
    @Override
    public Subscription login(final String username, final String password, final String device_type, final String device_token, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<CallUser>>() {
            @Override
            public void call(Subscriber<? super ProResult<CallUser>> subscriber) {
                try {
                    ProResult<CallUser> result = rest.login(username, password,device_type,device_token);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription quit(final String user_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {

            @Override
            public void call(Subscriber<? super Result> subscriber) {
                Result result = rest.quit(user_id);
                if (result.getCode()) {
                    subscriber.onNext(result);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);

    }

    @Override
    public Subscription obtaindoctorlist(Subscriber subscribers) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<DoctorList>>() {
            @Override
            public void call(Subscriber<? super ListProResult<DoctorList>> subscriber) {
                try {
                    ListProResult<DoctorList> result = rest.obtaindoctorlist();
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscribers);
    }

    @Override
    public Subscription phoneserver(final String dept_id, final String phone, final String user_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.phoneserver(dept_id, phone, user_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription videoserver(final String user_id,final String dept_id, final String user_name, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>(){
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result=rest.videoserver(user_id, dept_id,user_name);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getRecords(final String user_id,Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<Record>>(){
            @Override
            public void call(Subscriber<? super ListProResult<Record>> subscriber) {
                try {
                    ListProResult<Record> result=rest.getRecords(user_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getRecordInfo(final String service_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<Record>>(){
            @Override
            public void call(Subscriber<? super ProResult<Record>> subscriber) {
                try {
                    ProResult<Record> result=rest.getRecordInfo(service_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription evaluateRecord(final String service_id, final String scroe, final String content, final Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>(){
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result=rest.evaluateRecord(service_id,scroe,content);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription reCallPhone(final String dept_id, final String order_id, final String phone, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>(){
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result=rest.reCallPhone(dept_id, order_id, phone);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription reCallVideo(final String dept_id, final String service_id, final String order_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>(){
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result=rest.reCallPhone(dept_id, service_id,order_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getPhoneCode(final String username, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<CodeData>(){
            @Override
            public void call(Subscriber<? super CodeData> subscriber) {
                try {
                    CodeData result=rest.getPhoneCode(username);
                    if (!result.getCode().equals("0"))
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription newPwd(final String username, final String sms_code, final String password,final String phone, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>(){
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result=rest.newPwd(username,sms_code,password,phone);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    /**
     * REST Service
     */
    RestService rest = Rest.create(RestService.class);

    interface RestService {
        /**
         * 登录
         *
         * @param username
         * @param password
         * @return
         */
        @FormUrlEncoded
        @POST("/call_center/check_login")
        ProResult<CallUser> login(@Field("username") String username, @Field("password") String password,@Field("device_type")String device_type,@Field("device_token")String device_token);

        /**
         * 退出
         *
         * @param user_id
         * @return
         */
        @FormUrlEncoded
        @POST("/call_center/login_out")
        Result quit(@Field("user_id") String user_id);

        /**
         * 获取科室列表
         *
         * @return
         */
        @GET("/call_center/dept_name")
        ListProResult<DoctorList> obtaindoctorlist();
        /**
         * 电话咨询服务
         * @param dept_id
         * @param phone
         * @param use_id
         * @return
         */
        @GET("/call_center/call_service")
        Result phoneserver(@Query("dept_id")String dept_id,@Query("phone")String phone,@Query("user_id")String use_id);

        /**
         * 视频咨询服务
         * @param user_id
         * @param dept_id
         * @param user_name
         * @return
         */
        @GET("/call_center/video_service")
        Result videoserver(@Query("user_id")String user_id,@Query("dept_id")String dept_id,@Query("user_name")String user_name);

        /**
         * 获取服务记录列表数据
         * @param user_id
         * @return
         */
        @GET("/call_center/call_patient_service_list")
        ListProResult<Record> getRecords(@Query("user_id") String user_id);

        /**
         * 获取服务记录详情
         * @param service_id
         * @return
         */
        @GET("/call_center/call_patient_service_detail")
        ProResult<Record> getRecordInfo(@Query("service_id") String service_id);

        /**
         * 评价服务
         * @param service_id
         * @param scroe
         * @param content
         * @return
         */
        @FormUrlEncoded
        @POST("/call_center/mark")
        Result evaluateRecord(@Field("service_id") String service_id,@Field("scroe") String scroe,@Field("content") String content);

        /**
         * 重新拨打电话
         * @param dept_id
         * @param order_id
         * @param phone
         * @return
         */
        @GET("/call_center/recall_service")
        Result reCallPhone(@Query("dept_id") String dept_id,@Query("order_id") String order_id,@Query("phone") String phone);

        /**
         * 重新拨打视频
         * @param dept_id
         * @param phone
         * @param order_id
         * @return
         */
        @GET("/call_center/re_dial_video")
        Result reCallVideo(@Query("dept_id") String dept_id,@Query("service_id") String phone,@Query("order_id") String order_id);

        /**
         * 忘记密码获取手机验证码
         * @param username
         * @return
         */
        @FormUrlEncoded
        @POST("/call_center/passwd_smscode")
        CodeData getPhoneCode(@Field("username") String username);

        /**
         * 修改密码
         * @param username
         * @param sms_code
         * @param password
         * @return
         */
        @FormUrlEncoded
        @POST("/call_center/modify_passwd")
        Result newPwd(@Field("username") String username,@Field("sms_code") String sms_code,@Field("password") String password,@Field("phone") String phone);

    }
}
