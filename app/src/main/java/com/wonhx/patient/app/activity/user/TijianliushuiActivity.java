package com.wonhx.patient.app.activity.user;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.bigkoo.pickerview.TimePickerView;
import com.google.gson.Gson;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.activity.firstpage.ImagePagerActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.CheckProgress;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.FileKit;
import com.wonhx.patient.kit.ImageKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.view.NoScrollGridView;

import org.xutils.x;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class TijianliushuiActivity extends BaseActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.phy_item)
    EditText phyItem;
    @InjectView(R.id.phy_item_time)
    TextView phyItemTime;
    @InjectView(R.id.phy_item_result)
    EditText phyItemResult;
    @InjectView(R.id.examination_publish_img_grid)
    NoScrollGridView mImgGridView;
    TimePickerView pvTime;
    QuickAdapter mImgAdapter;
    String mAddImgPath = "ADD_IMG_PATH";
    /**
     * 待提交图片列表
     */
    ArrayList<String> mUpdateImgs = new ArrayList<>();
    /**
     * 压缩图片列表
     */
    List<String> mThumbMediaUrls = new ArrayList<>();
    UserManager userManager=new UserManagerImpl();
    CheckProgress checkProgress;
    // 存放缩略图的文件夹
    String savePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Patient/Camera/";
    @Override
    protected void onInitView() {
        super.onInitView();
        title.setText("编辑体检流水信息");
        Calendar c = Calendar.getInstance();
        pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date,View v) {//选中事件回调
                phyItemTime.setText(getTime(date));
            }
        })
                .setType(TimePickerView.Type.YEAR_MONTH_DAY)//默认全部显示
                .setCancelText("取消")//取消按钮文字
                .gravity(Gravity.CENTER)
                .setSubmitText("确定")//确认按钮文字
                .setContentSize(18)//滚轮文字大小
                .setTitleSize(20)//标题文字大小
                .setTitleText("")//标题文字
                .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(true)//是否循环滚动
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setSubmitColor(Color.BLUE)//确定按钮文字颜色
                .setCancelColor(Color.BLUE)//取消按钮文字颜色
                .setTitleBgColor(getResources().getColor(R.color.line_grey))//标题背景颜色 Night mode
                .setBgColor(Color.WHITE)//滚轮背景颜色 Night mode
                .setRange(c.get(Calendar.YEAR) -100, c.get(Calendar.YEAR) )//默认是1900-2100年
                // .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
                //.setRangDate(startDate,endDate)//起始终止年月日设定
                .setLabel("年","月","日","","","")
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                // .isDialog(true)//是否显示为对话框样式
                .build();
        mImgAdapter = new QuickAdapter<String>(this, R.layout.pic_img_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, String item) {
                ImageView imgView = helper.getView(R.id.img_grid_item);
                if (item.equals(mAddImgPath)) {
                    imgView.setImageResource(R.mipmap.healthadd);
                } else {
                    x.image().bind(imgView, item);
                }
            }
        };
        mImgGridView.setAdapter(mImgAdapter);
        mImgGridView.setOnItemClickListener(this);
        mImgGridView.setOnItemLongClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_item);
        ButterKnife.inject(this);

    }
@OnClick(R.id.commits)
void commit(){
    if (phyItem.getText().toString().equals("")){
        Toaster.showShort(TijianliushuiActivity.this,"体检项目不能为空！");
    }else if (mUpdateImgs.size()<2){
        Toaster.showShort(TijianliushuiActivity.this,"上传图片不能为空！");
    }else if (phyItemTime.getText().toString().trim().equals("")){
        Toaster.showShort(TijianliushuiActivity.this,"请填写日期！");
    }else if (phyItemResult.getText().toString().trim().equals("")){
        Toaster.showShort(TijianliushuiActivity.this,"请填写体检结果！");
    }else {
        checkProgress = new CheckProgress();
        checkProgress.setRecordname(phyItem.getText().toString().trim());
        checkProgress.setEffective_date(phyItemTime.getText().toString().trim());
        checkProgress.setResult(phyItemResult.getText().toString().trim());
        String msg = new Gson().toJson(checkProgress);
        userManager.submitCheckName(AbSharedUtil.getString(TijianliushuiActivity.this, "userId"), AbSharedUtil.getString(TijianliushuiActivity.this, "helthId"), msg, "", new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                //压缩图片   之后上传
                makeThumbImageFile();
                for (int i = 0; i < mThumbMediaUrls.size(); i++) {
                    setShowDialog(false);
                    if (i == (mThumbMediaUrls.size() - 1)) {
                        setDismessDialog(true);
                        updateImgs(result.getData(), mThumbMediaUrls.get(i), true);
                    } else {
                        updateImgs(result.getData(), mThumbMediaUrls.get(i), false);
                    }
                }

            }
        });
    }
}
    /**
     * 提交档案图片
     *
     * @param
     * @param path
     */
    private void updateImgs(final String resulr, String path, final boolean isOver) {
        String imageContent = "";
        try {
            imageContent = FileKit.encodeBase64File(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        userManager.updateCheckImg(AbSharedUtil.getString(TijianliushuiActivity.this,"userId"),resulr,imageContent,new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                if (isOver) {
                    Toaster.showShort(TijianliushuiActivity.this, "提交成功！");
                    finish();
                    //提交成功后  清空缩略图文件夹
                    for (String path : mThumbMediaUrls) {
                        File file = new File(path);
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                }
            }
        });
        }
    @Override
    protected void onInitData() {
        super.onInitData();
        mUpdateImgs.add(mAddImgPath);
        mImgAdapter.addAll(mUpdateImgs);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;
        switch (requestCode) {
            case 1001:
                List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                mUpdateImgs.clear();
                mImgAdapter.clear();
                mUpdateImgs.addAll(path);
                if (mUpdateImgs.size() != 9) {
                    mUpdateImgs.add(mAddImgPath);
                }
                mImgAdapter.addAll(mUpdateImgs);
                break;
        }
    }
    @OnClick({R.id.left_btn, R.id.phy_item_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.phy_item_time:
                pvTime.show();
                break;
        }
    }
    private String getTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (mImgAdapter.getItem(position).toString().contains(mAddImgPath)) {
            mUpdateImgs.remove(mAddImgPath);
            PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(TijianliushuiActivity.this, new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE}, new PermissionsResultAction() {
                @Override
                public void onGranted() {
                    MultiImageSelector.create(TijianliushuiActivity.this)
                            .showCamera(true) // show camera or not. true by default
                            .count(9) // max select image size, 9 by default. used width #.multi()
                            .multi() // multi mode, default mode;
                            .origin(mUpdateImgs) // original select data set, used width #.multi()
                            .start(TijianliushuiActivity.this, 1001);
                }
                @Override
                public void onDenied(String permission) {
                    //没有权限
                    Toast.makeText(TijianliushuiActivity.this, "没有照相权限！", Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            //查看大图
            Bundle bundle = new Bundle();
            ArrayList<String> imageUrls = new ArrayList<>();
            imageUrls.addAll(mUpdateImgs);
            imageUrls.remove(mAddImgPath);
            bundle.putStringArrayList("image_urls", imageUrls);
            bundle.putInt("image_index", position);
            UIKit.open(TijianliushuiActivity.this, ImagePagerActivity.class, bundle);
        }

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return false;
    }

    /**
     * 压缩图片
     */
    private void makeThumbImageFile() {
        for (String path : mUpdateImgs) {
            String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            //缩略图名字
            String thumbName = "thumb_" + timeStamp + FileKit.getFileName(path);
            //缩略图存放路径
            String thubmPath = savePath + thumbName;
            //是否存在
            if (new File(path).exists()) {
                if (new File(thubmPath).exists()) {
                    mThumbMediaUrls.add(thubmPath);
                } else {
                    try {
                        //压缩为宽度800的照片
                        ImageKit.createImageThumbnail(TijianliushuiActivity.this, path, thubmPath, 800, 50);
                        mThumbMediaUrls.add(thubmPath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
