package com.wonhx.patient.app.activity.user;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.AccountDetial;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.view.XListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

public class AccountDetialActivity extends BaseActivity implements XListView.IXListViewListener {
    @InjectView(R.id.title)
    TextView tv_title;
    @InjectView(R.id.lv)
    XListView lv;
    QuickAdapter<AccountDetial>quickAdapter;
    UserManager userManager=new UserManagerImpl();
    String memberid="";
    List<AccountDetial>list=new ArrayList<>();
    @InjectView(R.id.rl_no)
    RelativeLayout rl_no;
    Handler handler;
    int start=0;
    @Override
    protected void onInitView() {
        super.onInitView();
        tv_title.setText("账单明细");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_detial);
        memberid= AbSharedUtil.getString(this,"userId");
        lv.setPullLoadEnable(false);
        quickAdapter=new QuickAdapter<AccountDetial>(this,R.layout.activity_cash_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, AccountDetial item) {
                helper.setText(R.id.type,item.getType())
                        .setText(R.id.amount,"￥"+item.getAmount()+"元")
                        .setText(R.id.time,item.getFlow_time());
            }
        };
        lv.setAdapter(quickAdapter);
        lv.setXListViewListener(this);
        handler=new Handler();
        getlist();

    }

    private void getlist() {
        userManager.getaccountdetial(memberid,start+"","20",new SubscriberAdapter<ListProResult<AccountDetial>>(){
            @Override
            public void onError(Throwable e) {
                // super.onError(e);
                dismissLoadingDialog();
                Toaster.showShort(AccountDetialActivity.this,"没有更多数据");

            }

            @Override
            public void success(ListProResult<AccountDetial> accountDetialListProResult) {
                super.success(accountDetialListProResult);
                rl_no.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);
                list.addAll(accountDetialListProResult.getData());
                if (list.size()>0){
                    quickAdapter.replaceAll(list);
                    if (list.size()>=10){
                        lv.setPullLoadEnable(true);
                    }else {
                        lv.setPullLoadEnable(false);
                    }
                }
            }
        });
    }

    @OnClick(R.id.left_btn)
    void back(){
        finish();
    }

    @Override
    public void onRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start = 0;
                list.clear();
                getlist();
                onLoad();
            }
        }, 1000);

    }
    private void onLoad() {
        lv.stopRefresh();
        lv.stopLoadMore();
        lv.setRefreshTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }

    @Override
    public void onLoadMore() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start=start+20;
                getlist();
                onLoad();
            }
        }, 1000);

    }
}
