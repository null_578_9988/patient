package com.wonhx.patient.app.activity.user;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.SUserInfo;
import com.wonhx.patient.app.model.UserInfo;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.Toaster;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/29.
 * 个人信息页面
 */
public class UserInfoActivty extends BaseActivity {
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.name)
    EditText mUserName;
    @InjectView(R.id.male)
    RadioButton mSexBoy;
    @InjectView(R.id.female)
    RadioButton mSexGirl;
    @InjectView(R.id.birthday)
    TextView mBirthDay;
//    @InjectView(R.id.idCard)
//    EditText mIdCard;
//    @InjectView(R.id.medicalInsuranceCard)
//    EditText mMedicalCard;
    @InjectView(R.id.address)
    EditText mAdress;
    @InjectView(R.id.phone)
    EditText mPhoneNum;
//    @InjectView(R.id.email)
//    EditText mEmail;
    @InjectView(R.id.right_btn)
            TextView mRightBtn;
    TimePickerView pvTime;
    UserInfo userDao = new UserInfo();
    UserManager userManager = new UserManagerImpl();
    String memberId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinfo);
    }
    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("个人信息");
        mRightBtn.setText("完成");
        Calendar c = Calendar.getInstance();

        pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date,View v) {//选中事件回调
                mBirthDay.setText(getTime(date));
            }
        })
                .setType(TimePickerView.Type.YEAR_MONTH_DAY)//默认全部显示
                .setCancelText("取消")//取消按钮文字
                .gravity(Gravity.CENTER)
                .setSubmitText("确定")//确认按钮文字
                .setContentSize(18)//滚轮文字大小
                .setTitleSize(20)//标题文字大小
                .setTitleText("")//标题文字
                .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(true)//是否循环滚动
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setSubmitColor(Color.BLUE)//确定按钮文字颜色
                .setCancelColor(Color.BLUE)//取消按钮文字颜色
                .setTitleBgColor(getResources().getColor(R.color.line_grey))//标题背景颜色 Night mode
                .setBgColor(Color.WHITE)//滚轮背景颜色 Night mode
                .setRange(c.get(Calendar.YEAR) -100, c.get(Calendar.YEAR) )//默认是1900-2100年
               // .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
                //.setRangDate(startDate,endDate)//起始终止年月日设定
                .setLabel("年","月","日","","","")
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
               // .isDialog(true)//是否显示为对话框样式
                .build();
    }
    @Override
    protected void onInitData() {
        super.onInitData();
        memberId = AbSharedUtil.getString(UserInfoActivty.this, "userId");
        userDao = userDao.findById(memberId);
        userManager.getUserInfo(memberId, new SubscriberAdapter<ProResult<SUserInfo>>() {
            @Override
            public void success(ProResult<SUserInfo> sUserInfoProResult) {
                super.success(sUserInfoProResult);
                SUserInfo userInfo = sUserInfoProResult.getData();
                if (userInfo != null) {
                    mUserName.setText(userInfo.getName());
                    mBirthDay.setText(userInfo.getBirthday());
                    mPhoneNum.setText(userInfo.getPhone());
                    mAdress.setText(userInfo.getAddress());
                    if (userInfo.getSex()!=null){
                        if (userInfo.getSex().equals("男")){
                            mSexBoy.setChecked(true);
                        }else{
                            mSexGirl.setChecked(true);
                        }
                    }
                }
            }
        });
    }

    @OnClick(R.id.left_btn)
    void back() {
        finish();
    }

    @OnClick(R.id.right_btn)
    void submit() {
        String sex ="";
        if (mSexBoy.isChecked()){
            sex = "1";
        }else{
            sex = "2";
        }
       userManager.submitUserInfo(memberId,mUserName.getText().toString().trim(),mBirthDay.getText().toString().trim(),sex
               ,mAdress.getText().toString().trim()
               ,mPhoneNum.getText().toString().trim(),new SubscriberAdapter<Result>(){
           @Override
           public void success(Result result) {
               super.success(result);
               Toaster.showShort(UserInfoActivty.this, result.getMsg());
               userDao.setUsername(mUserName.getText().toString().trim());
               userDao.setPhone(mPhoneNum.getText().toString().trim());
               userDao.setAddress(mAdress.getText().toString().trim());
               if (mSexBoy.isChecked()){
                   userDao.setSex("男");
               }else{
                   userDao.setSex("女");
               }
               userDao.saveOrUpdate();
               finish();
           }
       });
    }
    @OnClick(R.id.birthday)
    void birthday_rl(){
        pvTime.show();
    }
    private String getTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }
}
