package com.wonhx.patient.app.activity.user;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.drawee.view.SimpleDraweeView;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.firstpage.DoctorDetialActivity;
import com.wonhx.patient.app.activity.firstpage.ImagePagerActivity;
import com.wonhx.patient.app.activity.servicehistory.ServiceHistoryDetialActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.OrderDetial;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.ListViewForScrollView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class BuyMedicineDetialActivity extends BaseActivity {
        @InjectView(R.id.title)
        TextView title;
        @InjectView(R.id.headimage)
        SimpleDraweeView headimage;
        @InjectView(R.id.name)
        TextView name;
        @InjectView(R.id.keshi)
        TextView keshi;
        @InjectView(R.id.goodat)
        TextView goodat;
        @InjectView(R.id.lv)
        ListViewForScrollView lv;
        @InjectView(R.id.pay)
        TextView pay;
        @InjectView(R.id.doctornama)
        ImageView doctornama;
        @InjectView(R.id.beizhu)
        TextView beizhu;
        @InjectView(R.id.dizhi)
        TextView dizhi;
        @InjectView(R.id.disease)
        TextView diseasea;
        String orderId="",namea="",doctor_id="",logoImgPath="",titlea="",postage_price="",dept_name="",isPay="",disease="",address="",totalPrice="",doctorAdvice="",doctor_memberId="";
        UserManager userManager=new UserManagerImpl();
        QuickAdapter<OrderDetial>adapter;
        List<OrderDetial>list=new ArrayList<>();
        @InjectView(R.id.total)
        TextView tv_totla;
        @InjectView(R.id.past_price)
        TextView past_price;
        @InjectView(R.id.sss)
        RelativeLayout jiesuan_bottom;
        @InjectView(R.id.rl_add)
        RelativeLayout rl_add;
        @Override
        protected void onInitView() {
            super.onInitView();
            title.setText("查看购药清单");
            title.setTextColor(getResources().getColor(R.color.colorAccent));
            adapter=new QuickAdapter<OrderDetial>(BuyMedicineDetialActivity.this,R.layout.medicinelvitem) {
                @Override
                protected void convert(BaseAdapterHelper helper, OrderDetial item) {
                    helper.setText(R.id.name,item.getName())
                            .setText(R.id.specification,item.getSpecification())
                            .setText(R.id.retailPrice,"查看")
                            .setText(R.id.add,"￥"+item.getRetailPrice())
                            .setText(R.id.num,"*"+item.getNum());
                }
            };
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    imageChooseItem(i);
                }
            });


        }
        @OnClick(R.id.topa)
        void detial_doctor(){
            Intent in=new Intent(BuyMedicineDetialActivity.this, DoctorDetialActivity.class);
            in.putExtra("member_id",doctor_memberId);
            startActivity(in);
        }
        MyReceiver myReceiver;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            unregisterReceiver(myReceiver);
        }
    }
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_buy_medicine_detial);
            myReceiver = new MyReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("ExitApp");
            registerReceiver(myReceiver, intentFilter);
            myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
                @Override
                public void UpdateUI(String memberId) {
                    finish();
                }
            });
            ButterKnife.inject(this);
            Intent intent=getIntent();
            orderId=intent.getStringExtra("orderId");
            namea=intent.getStringExtra("name");
            logoImgPath=intent.getStringExtra("logoImgPath");
            titlea=intent.getStringExtra("title");
            dept_name=intent.getStringExtra("dept_name");
            isPay=intent.getStringExtra("isPay");
            disease=intent.getStringExtra("disease");
            address=intent.getStringExtra("address");
            totalPrice=intent.getStringExtra("totalPrice");
            doctorAdvice=intent.getStringExtra("doctorAdvice");
            doctor_memberId=intent.getStringExtra("member_id");
            postage_price=intent.getStringExtra("postage_price");
            doctor_id=intent.getStringExtra("doctor_id");
            name.setText(namea);
            headimage.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine" +logoImgPath));
            keshi.setText(dept_name);
            goodat.setText(titlea);
            if (isPay.equals("0")){
                pay.setText("未付款");
                pay.setTextColor(Color.RED);
                rl_add.setVisibility(View.GONE);
            }else {
                pay.setText("已付款");
                pay.setTextColor(getResources().getColor(R.color.fukuan));
                rl_add.setVisibility(View.VISIBLE);
            }
            Glide.with(BuyMedicineDetialActivity.this).load(Constants.REST_ORGIN + "/emedicine/pub/sign_img/" + doctor_memberId+"/"+ doctor_memberId+".png").into(doctornama);
            beizhu.setText(doctorAdvice);
            dizhi.setText(address);
            diseasea.setText(disease);
            if (isPay.equals("0")){
                jiesuan_bottom.setVisibility(View.VISIBLE);
            }else {
                jiesuan_bottom.setVisibility(View.GONE);
            }
            tv_totla.setText("￥"+totalPrice);
            past_price.setText("￥"+postage_price);
            getmessage();
        }
        @OnClick(R.id.jiesuan)
        void jiesuan(){
            Intent in=new Intent(BuyMedicineDetialActivity.this,OrderPayActivity.class);
            in.putExtra("orderId",orderId);
            in.putExtra("totalPrice",totalPrice);
            in.putExtra("member_id",doctor_memberId);
            startActivity(in);
        }
    private void getmessage() {
        userManager.orderdetial(orderId,new SubscriberAdapter<ListProResult<OrderDetial>>(){
            @Override
            public void success(ListProResult<OrderDetial> orderDetialListProResult) {
                super.success(orderDetialListProResult);
                list.addAll(orderDetialListProResult.getData());
                if (list.size()>0){
                    adapter.addAll(list);
                }
            }
        });
    }

    @OnClick(R.id.left_btn)
    public void onViewClicked() {
        finish();
    }
    /**
     * 查看详情
     */
    private void imageChooseItem(int positon) {
        View view = LayoutInflater.from(BuyMedicineDetialActivity.this).inflate(R.layout.ordrrdetial, null);
        final Dialog dialog = new AlertDialog.Builder(BuyMedicineDetialActivity.this).create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.mydialogstyle);  //添加动画
        window.setBackgroundDrawable(new ColorDrawable());
        dialog.show();
        window.setContentView(view);
        TextView tv_name= (TextView) view.findViewById(R.id.namea);
        String name=list.get(positon).getName();
        if (name!=null&&!name.equals("")) {
            tv_name.setText(name);
        }
        TextView tv_price= (TextView) view.findViewById(R.id.prices);
        String pricr=list.get(positon).getRetailPrice()+"";
        if (pricr!=null&&!pricr.equals("")) {
            tv_price.setText("￥"+pricr);
        }
        TextView tv_guige= (TextView) view.findViewById(R.id.guige);
        String guige=list.get(positon).getSpecification()+"";
        if (guige!=null&&!guige.equals("")) {
            tv_guige.setText(guige);
        }
        TextView tv_yongfa= (TextView) view.findViewById(R.id.yongfa);
        String yongfa=list.get(positon).getUsage()+"";
        if (yongfa!=null&&!yongfa.equals("")) {
            tv_yongfa.setText(yongfa);
        }
        TextView tv_pinci= (TextView) view.findViewById(R.id.pinci);
        String pinci=list.get(positon).getDosage()+"";
        if (pinci!=null&&!pinci.equals("")) {
            tv_pinci.setText(pinci);
        }
        TextView tv_shuliang= (TextView) view.findViewById(R.id.shuliang);
        String shuliang=list.get(positon).getNum()+"";
        if (shuliang!=null&&!shuliang.equals("")) {
            tv_shuliang.setText("*"+shuliang);
        }
        TextView tv_beizhu= (TextView) view.findViewById(R.id.beizhu);
        String beizhu=list.get(positon).getUsageDosage()+"";
        if (beizhu!=null&&!beizhu.equals("")) {
            tv_beizhu.setText(beizhu);
        }
        TextView tv_queren= (TextView) view.findViewById(R.id.queren);
        tv_queren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ImageView iv= (ImageView) view.findViewById(R.id.finish);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

}
