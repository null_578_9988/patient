package com.wonhx.patient.app.fragment.main;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.hyphenate.chat.EMClient;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.firstpage.BannerDetailActivity;
import com.wonhx.patient.app.activity.firstpage.DoctorDetialActivity;
import com.wonhx.patient.app.activity.firstpage.NowConstantActivity;
import com.wonhx.patient.app.activity.user.LoginActivity;
import com.wonhx.patient.app.activity.user.MessageActivity;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.ease.Message;
import com.wonhx.patient.app.manager.DoctorTabManger;
import com.wonhx.patient.app.manager.doctortab.DoctorTabMangerImal;
import com.wonhx.patient.app.model.ListProResultNormal;
import com.wonhx.patient.app.model.LookDoctor;
import com.wonhx.patient.app.model.PushMessage;
import com.wonhx.patient.app.model.TabEntity;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.ListViewForScrollView;
import com.wonhx.patient.view.LoginDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/22.
 * 我的医生
 */
public class DoctorTabFragment extends BaseFragment implements OnTabSelectListener {
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.menutab)
    CommonTabLayout mMenuTab;
    @InjectView(R.id.tab_doctor_logout)
    RelativeLayout ll_nolog;
    @InjectView(R.id.rl_nopoint)
    RelativeLayout rl_nopoint;
    @InjectView(R.id.mListView)
    ListViewForScrollView lv;
    @InjectView(R.id.tv_textss)
    TextView tv_tv_textss;
    @InjectView(R.id.li_nopoint_finds)
    TextView mFindDoctor;
    private String[] mTitles = {"关注", "咨询"};
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    String memberid = "";
    DoctorTabManger doctorTabManger = new DoctorTabMangerImal();
    List<LookDoctor> lookDoctors_guanzhu = new ArrayList<>();
    List<LookDoctor> lookDoctors_zixun = new ArrayList<>();
    List<LookDoctor> lookDoctors_all = new ArrayList<>();
    QuickAdapter<LookDoctor> adapter;
    @InjectView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    int position = 0;
    MyReceiver myReceiver;
    LoginDialog dialog;
    List<PushMessage>all_message=new ArrayList<>();
    List<PushMessage>noread_message=new ArrayList<>();
    PushMessage pushMessage=new PushMessage();
    @InjectView(R.id.red_point)
    TextView tv_red_point;
    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("我的医生");
        mTabEntities.clear();
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i]));
        }
        mMenuTab.setTabData(mTabEntities);
        mMenuTab.setOnTabSelectListener(this);
        mMenuTab.setCurrentTab(0);
        adapter = new QuickAdapter<LookDoctor>(getActivity(), R.layout.lookdoctor_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, LookDoctor item) {
                String dept = item.getDept();
                String hospitalName = item.getHospitalName();
                String title = item.getTitle();
                String name = item.getName();
                String logo = item.getLogo();
                String followedStatus = item.getFollowedStatus();
                if (dept != null && !dept.equals("")) {
                    helper.setText(R.id.dept, dept);
                }
                if (hospitalName != null && !hospitalName.equals("")) {
                    helper.setText(R.id.hospitalname, hospitalName);
                }
                if (title != null && !title.equals("")) {
                    helper.setText(R.id.title, title);
                }
                if (name != null && !name.equals("")) {
                    helper.setText(R.id.name, name);
                }
                SimpleDraweeView simpleDraweeView = helper.getView(R.id.doctor_iv);
                if (logo != null && !logo.equals("")) {
                    simpleDraweeView.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine" + logo));
                }
                if (followedStatus.equals("1")) {
                    helper.setText(R.id.new_consult, "已关注");
                } else {
                    helper.setText(R.id.new_consult, "已咨询");
                }
            }
        };
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent in = new Intent(getActivity(), DoctorDetialActivity.class);
                String member_id1 = "";
                if (mMenuTab.getCurrentTab() == 0) {
                    position = 0;
                    member_id1 = lookDoctors_guanzhu.get(i).getMemberId();
                } else if (mMenuTab.getCurrentTab() == 1) {
                    position = 1;
                    member_id1 = lookDoctors_zixun.get(i).getMemberId();
                }
                in.putExtra("member_id", member_id1);
                startActivity(in);
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getdata();
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_tab_doctor, container, false);
        ButterKnife.inject(this, viewGroup);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light, android.R.color.holo_green_light, android.R.color.holo_blue_bright, android.R.color.holo_orange_light);
        return viewGroup;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //登录或者退出之后刷新
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("REFRESH");
        intentFilter.addAction("UPDATE_MSG_COUNT");
        getActivity().registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String memberId) {
                if (memberId.equals("UPDATE_MSG_COUNT")){
                    noread_message.clear();
                    all_message = pushMessage.findAll();
                    //更新消息数量
                    int unreadMsgCountTotal = 0;
                    //获取环信未读消息数量
                    for (int i = 0; i < all_message.size(); i++) {
                        if (all_message.get(i).getIsread().equals("1")) {
                            noread_message.add(all_message.get(i));
                        }
                    }
                    Message msgDao = new Message();
                    List<Message> mAllDatas = new ArrayList<>();
                    mAllDatas.addAll(msgDao.findAll());
                    for (Message msg : mAllDatas){
                        if (!msg.isRead()){
                            unreadMsgCountTotal++;
                        }
                    }
                    unreadMsgCountTotal = unreadMsgCountTotal + noread_message.size();
                    tv_red_point.setText(unreadMsgCountTotal+"");
                    if (unreadMsgCountTotal > 0) {
                        tv_red_point.setVisibility(View.VISIBLE);
                    } else {
                        tv_red_point.setVisibility(View.GONE);
                    }
                }else{
                    if (memberId .equals("2")) {
                        //登录
                        ll_nolog.setVisibility(View.GONE);
                        getdata();

                    }else   if (memberId .equals("1" )){
                        //退出
                        ll_nolog.setVisibility(View.VISIBLE);
                    }else{
                        tv_red_point.setVisibility(View.GONE);
                    }
                }

            }
        });
    }

    private void getdata() {
        doctorTabManger.getServicelist(memberid, new SubscriberAdapter<ListProResultNormal<LookDoctor>>() {
            @Override
            public void onError(Throwable e) {
                // super.onError(e);
                dissmissProgressDialog();
                mSwipeRefreshLayout.setRefreshing(false);
                rl_nopoint.setVisibility(View.VISIBLE);
                if (position == 0) {
                        tv_tv_textss.setText("您“关注”的医生将会出现在这里，您可以快速找到他们");
                } else if (position == 1) {
                        tv_tv_textss.setText("您“咨询”的医生将会出现在这里，您可以快速找到他们");
                }
            }
            @Override
            public void success(ListProResultNormal<LookDoctor> lookDoctorListProResultNormal) {
                super.success(lookDoctorListProResultNormal);
                mSwipeRefreshLayout.setRefreshing(false);
                if (lookDoctorListProResultNormal.getCode()) {
                    if (lookDoctorListProResultNormal.getDoctorList().size() > 0 && lookDoctorListProResultNormal.getDoctorList() != null) {
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mFindDoctor.setVisibility(View.VISIBLE);
                        rl_nopoint.setVisibility(View.GONE);
                        lookDoctors_guanzhu.clear();
                        lookDoctors_zixun.clear();
                        lookDoctors_all.clear();
                        lookDoctors_all.addAll(lookDoctorListProResultNormal.getDoctorList());
                        for (int i = 0; i < lookDoctorListProResultNormal.getDoctorList().size(); i++) {
                            if (lookDoctorListProResultNormal.getDoctorList().get(i).getFollowedStatus().equals("1")) {
                                lookDoctors_guanzhu.add(lookDoctorListProResultNormal.getDoctorList().get(i));
                            } else {
                                lookDoctors_zixun.add(lookDoctorListProResultNormal.getDoctorList().get(i));
                            }
                        }
                        if (position == 0) {
                            if (lookDoctors_guanzhu.size() > 0) {
                                rl_nopoint.setVisibility(View.GONE);
                                mMenuTab.setCurrentTab(0);
                                adapter.clear();
                                adapter.addAll(lookDoctors_guanzhu);
                            } else {
                                rl_nopoint.setVisibility(View.VISIBLE);
                                tv_tv_textss.setText("您“关注”的医生将会出现在这里，您可以快速找到他们");
                            }
                        } else if (position == 1) {
                            if (lookDoctors_zixun.size() > 0) {
                                rl_nopoint.setVisibility(View.GONE);
                                mMenuTab.setCurrentTab(1);
                                adapter.clear();
                                adapter.addAll(lookDoctors_zixun);
                            } else {
                                rl_nopoint.setVisibility(View.VISIBLE);
                                tv_tv_textss.setText("您“咨询”的医生将会出现在这里，您可以快速找到他们");
                            }
                        }

                    }
                } else {
                    mSwipeRefreshLayout.setVisibility(View.GONE);
                    mFindDoctor.setVisibility(View.GONE);
                    rl_nopoint.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /**
     * menu切换监听
     *
     * @param position1
     */
    @Override
    public void onTabSelect(int position1) {
        if (position1 == 0) {
            if (lookDoctors_guanzhu.size() > 0) {
                adapter.clear();
                adapter.addAll(lookDoctors_guanzhu);
                rl_nopoint.setVisibility(View.GONE);
            } else {
                tv_tv_textss.setText("您“关注”的医生将会出现在这里，您可以快速找到他们");
                rl_nopoint.setVisibility(View.VISIBLE);
            }
            position = 0;

        } else if (position1 == 1) {
            if (lookDoctors_zixun.size() > 0) {
                adapter.clear();
                adapter.addAll(lookDoctors_zixun);
                rl_nopoint.setVisibility(View.GONE);
            } else {
                tv_tv_textss.setText("您“咨询”的医生将会出现在这里，您可以快速找到他们");
                rl_nopoint.setVisibility(View.VISIBLE);
            }
            position = 1;
        }
    }

    @Override
    public void onTabReselect(int position) {

    }

    @Override
    public void onStart() {
        super.onStart();
        int noread=0;
        int unReadCount=0;
        memberid= AbSharedUtil.getString(getActivity(),"userId");
        if (memberid!=null&&!memberid.equals("")) {
            all_message = pushMessage.findAll();
            for (int i = 0; i < all_message.size(); i++) {
                if (all_message.get(i).getIsread().equals("1")) {
                    noread++;
                }
            }
            Message msgDao = new Message();
            List<Message> mAllDatas = new ArrayList<>();
            mAllDatas.addAll(msgDao.findAll());
            for (Message msg : mAllDatas){
                if (!msg.isRead()){
                    unReadCount++;
                }
            }
            noread =noread+ unReadCount;
            tv_red_point.setText(noread+"");
            if (noread > 0) {
                tv_red_point.setVisibility(View.VISIBLE);
            } else {
                tv_red_point.setVisibility(View.GONE);
            }
            ll_nolog.setVisibility(View.GONE);
            getdata();
        } else {
            ll_nolog.setVisibility(View.VISIBLE);
        }
    }

    //去登陆
    @OnClick(R.id.tab_doctor_logout_login)
    void login() {
        if (mMenuTab.getCurrentTab() == 0) {
            position = 0;
        } else if (mMenuTab.getCurrentTab() == 1) {
            position = 1;
        }
        UIKit.open(getActivity(), LoginActivity.class);
    }
    
    @OnClick(R.id.li_nopoint_finds)
    void search() {
        startActivity(new Intent(getActivity(), NowConstantActivity.class));
    }
    @OnClick(R.id.li_nopoint_findsa)
    void searcha() {
        startActivity(new Intent(getActivity(), NowConstantActivity.class));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            getActivity().unregisterReceiver(myReceiver);
        }
    }
    @OnClick(R.id.notice_layout)
    public void notice_layout() {
        String memberId= AbSharedUtil.getString(getActivity(),"userId");
        if (memberId!=null&&!memberId.equals("")) {
            UIKit.open(getActivity(), MessageActivity.class);
        }else {
            dialog();
        }
    }
    // 弹窗
    private void dialog() {
        dialog = new LoginDialog(getActivity());
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setClass(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });
        dialog.show();
    }
}
