package com.wonhx.patient.app.model;

/**
 * Created by Administrator on 2017/7/1.
 */

public class Resultt {
    String code;
    String msg;
    String data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
