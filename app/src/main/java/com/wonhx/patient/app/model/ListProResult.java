package com.wonhx.patient.app.model;

import java.util.List;

/**
 * Created by apple on 17/4/1.
 *
 */
public class ListProResult<T> {
    public String code;
    public String msg;
    public List<T>  data;

    public boolean getCode() {
        return code.equals("0");
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
