package com.wonhx.patient.app.fragment.user;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.user.TijianliushuiActivity;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.CheckProgress;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.GridViewUtils;
import com.wonhx.patient.view.ListViewForScrollView;
import com.wonhx.patient.view.MGridView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 2017/4/7.
 * 健康档案：体检流程
 */

public class CheckProgressFragment extends BaseFragment {
    UserManager userManager = new UserManagerImpl();
    String mHealthyId;
    List<CheckProgress> mCheckProgress=new ArrayList<>();
    @InjectView(R.id.health_exam_lv)
    ListViewForScrollView healthExamLv;
    @InjectView(R.id.addBtn)
    Button addBtn;
    QuickAdapter<CheckProgress>adapter;
    QuickAdapter<CheckProgress.Img_path>quickAdapter;
    MGridView gridView;
    List<CheckProgress.Img_path>list=new ArrayList<>();
    ArrayList<String>list1=new ArrayList<>();
    MyReceiver myReceiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_check_progress, container, false);
        ButterKnife.inject(this, viewGroup);
        adapter=new QuickAdapter<CheckProgress>(getActivity(),R.layout.checkp) {
            @Override
            protected void convert(BaseAdapterHelper helper, CheckProgress item) {
            helper.setText(R.id.xiangmu,item.getRecordname())
                    .setText(R.id.jieguo,item.getResult())
                    .setText(R.id.riqi,item.getEffective_date());
                gridView =helper.getView(R.id.tupian);
                // 设置GridView的Adapter
                quickAdapter=new QuickAdapter<CheckProgress.Img_path>(getActivity(),R.layout.che) {
                    @Override
                    protected void convert(BaseAdapterHelper helper, CheckProgress.Img_path item) {
                        ImageView iv=  helper.getView(R.id.iv);
                        if (item.getPath()!=null&&!item.getPath().equals("")) {
                            Glide.with(getActivity()).load(Constants.REST_ORGIN + "/emedicine" + item.getPath()).centerCrop().into(iv);
                        }
                    }
                };
                gridView.setAdapter(quickAdapter);
                list.clear();
                if (item.getImg_path()!=null&&item.getImg_path().size()>0) {
                    list.addAll(item.getImg_path());
                    quickAdapter.addAll(list);
                    // 计算GridView宽度, 设置默认为numColumns为3.
                   GridViewUtils.updateGridViewLayoutParams(gridView, 3);
                }
            }
        };
        healthExamLv.setAdapter(adapter);
        return viewGroup;
    }
    private void getmessages() {
        mHealthyId = AbSharedUtil.getString(getActivity(), "helthId");
        if (mHealthyId!=null) {
            userManager.getCheckProgress(mHealthyId, new SubscriberAdapter<ListProResult<CheckProgress>>() {
                @Override
                public void onError(Throwable e) {
                    dissmissProgressDialog();
                }

                @Override
                public void success(ListProResult<CheckProgress> checkProgressProResult) {
                    super.success(checkProgressProResult);
                    mCheckProgress.clear();
                    adapter.clear();
                    Log.e("sise",checkProgressProResult.getData().size()+"aaa");
                    mCheckProgress.addAll(checkProgressProResult.getData());
                    if (mCheckProgress != null && mCheckProgress.size() > 0) {
                        adapter.addAll(mCheckProgress);
                    }
                }
            });
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("FLAG");
        getActivity().registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String helthId) {
                if (helthId!=null){
                    getmessages();
                }
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
    @OnClick( R.id.addBtn)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addBtn:
                startActivityForResult(new Intent(getActivity(), TijianliushuiActivity.class),101);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==101){
            getmessages();
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            getActivity().unregisterReceiver(myReceiver);
        }
    }
}
