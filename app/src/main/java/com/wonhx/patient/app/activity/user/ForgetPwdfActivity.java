package com.wonhx.patient.app.activity.user;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/25.
 * 忘记密码页面
 */
public class ForgetPwdfActivity extends BaseActivity{
    @InjectView(R.id.et_phonenumber)
    EditText mPhoneNum;
    @InjectView(R.id.btn_yanzheng)
    Button mSubfmit;
    @InjectView(R.id.iv_clear)
    ImageView mClearImg;
    UserManager userManager = new UserManagerImpl();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pwd);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mPhoneNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(mPhoneNum.getText().toString().trim())) {
                    mClearImg.setVisibility(View.VISIBLE);
                } else {
                    mClearImg.setVisibility(View.GONE);
                }
            }
        });
    }
    @OnClick(R.id.btn_yanzheng)
    void submit(){
        if (StrKit.isBlank(mPhoneNum.getText().toString().trim())){
            Toaster.showShort(ForgetPwdfActivity.this,"请输入手机号码！");
            return;
        }
        if (!StrKit.isMobileNO(mPhoneNum.getText().toString().trim())){
            Toaster.showShort(ForgetPwdfActivity.this,"手机号码格式不正确！");
            return;
        }
        final String phoneNum = mPhoneNum.getText().toString().trim();
        //获取验证码
        userManager.getForgetCode(phoneNum,new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                Toaster.showShort(ForgetPwdfActivity.this, "获取验证码成功！");
                Bundle bundle = new Bundle();
                bundle.putString("phoneNum",phoneNum);
                bundle.putString("code",result.getContent());
                UIKit.open(ForgetPwdfActivity.this,InputForgetCodeActivity.class,bundle);
                finish();
            }
        });
    }
    @OnClick(R.id.iv_bar_left1)
    void back(){
        finish();
    }
}
