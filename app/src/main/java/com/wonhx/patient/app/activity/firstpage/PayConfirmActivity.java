package com.wonhx.patient.app.activity.firstpage;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alipay.sdk.app.PayTask;
import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.EaseConstant;
import com.tencent.mm.opensdk.constants.Build;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.unionpay.UPPayAssistEx;
import com.unionpay.uppay.PayActivity;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.user.DepositActivity;
import com.wonhx.patient.app.activity.user.OrderPayActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.ease.ConversationActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Account;
import com.wonhx.patient.app.model.AskOrderResult;
import com.wonhx.patient.app.model.DoctorDetial;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.WeXinBean;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.LoginDialog;
import com.wonhx.patient.view.PayResult;
import com.wonhx.patient.view.QuiteDialog;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by nsd on 2017/4/18.
 *
 */

public class PayConfirmActivity extends BaseActivity {
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.mPrice)
    TextView mPrice;
    @InjectView(R.id.mAllPrice)
    TextView mAllPrice;
    @InjectView(R.id.mYueCheck)
    ImageView mYueCheck;
    @InjectView(R.id.mAlipayCheck)
    ImageView mAlipayCheck;
    @InjectView(R.id.mKaCheck)
    ImageView mKaCheck;
    @InjectView(R.id.weixin_iv)
    ImageView weixin_iv;
    AskOrderResult mAskOrderResult;
    DoctorDetial mDoctorDetail;
    @InjectView(R.id.tv_kryong)
            TextView tv_kryong;
    int mPayType = 0; // 0 余额，1 支付宝，2 银行卡
    UserManager userManager = new UserManagerImpl();
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    String zhifubaopayInfo = "", yinlianpayinfo = "";
    private String mMode = "00";//设置测试模式:01为测试 00为正式环境
    private String tn="";
    int type = 2;
    private IWXAPI api;
    MyReceiver myReceiver;
    @InjectView(R.id.service_type)
    TextView tvservice_type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payconfirm);
        ButterKnife.inject(this);
        api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);
        api.registerApp(Constants.APP_ID);
       // 微信支付成功后
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("PAYFINISH");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String memberId) {
                showDialog();
            }
        });
    }
    private void getAccount() {
        userManager.getaccount(AbSharedUtil.getString(PayConfirmActivity.this,"userId"),new SubscriberAdapter<ProResult<Account>>(){
            @Override
            public void success(ProResult<Account> accountProResult) {
                super.success(accountProResult);
                if (accountProResult.getData().getAccount_balance()!=null&&!accountProResult.getData().getAccount_balance().equals("")) {
                    tv_kryong.setText("可用余额："+accountProResult.getData().getAccount_balance());
                    ;
                }else {
                    tv_kryong.setText("可用余额："+"0");
                }
            }
        });
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        type = getIntent().getIntExtra("type", 2);
        if (type==2){
            tvservice_type.setText("图文咨询");
        }else if (type==3){
            tvservice_type.setText("电话咨询");
        }else if (type==4){
            tvservice_type.setText("视频咨询");
        }
        mDoctorDetail = (DoctorDetial) getIntent().getSerializableExtra("detail");
        mAskOrderResult = (AskOrderResult) getIntent().getSerializableExtra("orderResult");
        if (mDoctorDetail != null) {
            for (int i = 0; i < mDoctorDetail.getPrice().size(); i++) {
                if (mDoctorDetail.getPrice().get(i).getService_type().equals(type+"")) {
                    mPrice.setText(mDoctorDetail.getPrice().get(i).getService_price() + "元");
                    mAllPrice.setText(mDoctorDetail.getPrice().get(i).getService_price() + "元");

                }
            }
        }
        getAccount();

    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("订单支付");
        mYueCheck.setImageResource(R.mipmap.true_select);
    }
    @OnClick({R.id.left_btn, R.id.mYueRelayout, R.id.mAlipayRelayout, R.id.mKaRelayout, R.id.submit_btn,R.id.rl_weixin})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.mYueRelayout:
                mYueCheck.setImageResource(R.mipmap.true_select);
                mAlipayCheck.setImageResource(0);
                mKaCheck.setImageResource(0);
                weixin_iv.setImageResource(0);
                mPayType = 0;
                break;
            case R.id.mAlipayRelayout:
                mAlipayCheck.setImageResource(R.mipmap.true_select);
                mYueCheck.setImageResource(0);
                mKaCheck.setImageResource(0);
                weixin_iv.setImageResource(0);
                mPayType = 1;
                break;
            case R.id.mKaRelayout:
                mKaCheck.setImageResource(R.mipmap.true_select);
                mYueCheck.setImageResource(0);
                mAlipayCheck.setImageResource(0);
                weixin_iv.setImageResource(0);
                mPayType = 2;
                break;
            case R.id.rl_weixin:
                weixin_iv.setImageResource(R.mipmap.true_select);
                mYueCheck.setImageResource(0);
                mAlipayCheck.setImageResource(0);
                mKaCheck.setImageResource(0);
                mPayType = 3;
                break;
            case R.id.submit_btn:
                //支付
                switch (mPayType) {
                    case 0:
                        //账户余额支付
                        payWithBlance();
                        break;
                    case 1:
                        //支付宝
                        alipay();
                        break;
                    case 2:
                        //银行卡
                        payWithBankCard();
                        break;
                    case 3:
                        //微信
                        payWithWinXin();
                        break;
                }
                break;
        }
    }

    private void payWithWinXin() {
        boolean sIsWXAppInstalledAndSupported   = api.isWXAppInstalled()
                && api.isWXAppSupportAPI();
        if (sIsWXAppInstalledAndSupported ) {
            boolean isPaySupported = api.getWXAppSupportAPI() >= Build.PAY_SUPPORTED_SDK_INT;
            if (isPaySupported) {
                Log.e("ssss", mAskOrderResult.getOrder_id() + "");
                userManager.weixin(mAskOrderResult.getOrder_id(), "A2", new SubscriberAdapter<WeXinBean>() {
                    @Override
                    public void success(WeXinBean result) {
                        super.success(result);
                        weixinpay(result);
                    }
                });
            }else {
                Toaster.showShort(PayConfirmActivity.this,"此版本还不支持微信支付");
            }
        }else {
            Toaster.showShort(PayConfirmActivity.this,"还未安装微信");
        }
    }
    private void weixinpay(WeXinBean result) {
        PayReq payreq=new PayReq();
        payreq.appId=result.getAppid();
        payreq.partnerId=result.getPartnerid();
        payreq.prepayId=result.getPrepayid();
        payreq.nonceStr=result.getNoncestr();
        payreq.timeStamp=result.getTimestamp();
        payreq.packageValue="Sign=WXPay";
        payreq.sign=result.getSign();
        Constants.flag=3;
        api.sendReq(payreq);
    }

    /**
     * 支付宝支付
     */
    private void alipay() {
        userManager.zhifubao(mAskOrderResult.getOrder_id(), "A", new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                zhifubaopayInfo = result.getData();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        PayTask alipay = new PayTask(PayConfirmActivity.this);
                        // 调用支付接口，获取支付结果
                        String result = alipay.pay(zhifubaopayInfo, true);
                        Message msg = new Message();
                        msg.what = 1;
                        msg.obj = result;
                        handler.sendMessage(msg);
                    }
                };
                Thread payThread = new Thread(runnable);
                payThread.start();
            }
        });
    }

    /**
     * 余额支付
     */
    private void payWithBlance(){
        firstPagerMager.payWithBlance(mAskOrderResult.getOrder_id(),new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                Toaster.showShort(PayConfirmActivity.this,result.getMsg());
                showDialog();
            }
        });
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        showDialog();
                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toaster.showShort(PayConfirmActivity.this, "支付结果确认中" + "");
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            //Toast.makeText(ZhifuActivity.this, "支付失败", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                }
                case  2:{
                    if (msg.obj == null || ((String) msg.obj).length() == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(PayConfirmActivity.this);
                        builder.setTitle("错误提示");
                        builder.setMessage("网络连接失败,请重试!");
                        builder.setNegativeButton("确定",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.create().show();
                    } else {
                        tn = (String) msg.obj;
                        doStartUnionPayPlugin(PayConfirmActivity.this, tn, mMode);
                    }
                }
                default:
                    break;
            }
        }
    };
    private void payWithBankCard(){
        userManager.yinlian(mAskOrderResult.getOrder_id(),"A1",new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                yinlianpayinfo=result.getTn();
                Runnable runnable=new Runnable() {
                    @Override
                    public void run() {
                        // 调用支付接口，获取支付结果
                        Message msg = new Message();
                        msg.what = 2;
                        msg.obj = yinlianpayinfo;
                        handler.sendMessage(msg);
                    }
                };
                Thread payThread = new Thread(runnable);
                payThread.start();

            }
        });
    }

    /**
     * 支付成功提示框
     */
    private void showDialog() {
        //type : 2 图文，3 电话，4 视频。
        switch (type){
            case 2:
                final QuiteDialog mDialog = new QuiteDialog(PayConfirmActivity.this);
                mDialog.setCancelable(false);
                mDialog.setContent("付款成功，是否立即咨询？");
                mDialog.setBackText("返回首页");
                mDialog.setSureText("立即咨询");
                mDialog.setOnPositiveListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                        //获取环信用户名
                        firstPagerMager.getDoctorEMCName(mDoctorDetail.getMemberId(),new SubscriberAdapter<Result>(){
                            @Override
                            public void success(Result result) {
                                super.success(result);
                                Bundle args = new Bundle();
                                args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                                args.putString(EaseConstant.EXTRA_USER_ID, result.getHuanxin_username());
                                args.putString("DT_RowId", mAskOrderResult.getConsultation_request_id());
                                args.putString("DOC_NAME",mDoctorDetail.getName());
                                UIKit.open(PayConfirmActivity.this, ConversationActivity.class,args);
                                finish();
                            }
                        });

                    }
                });
                mDialog.setOnNegativeListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        mDialog.dismiss();
                        //支付成功后切换到服务记录
                        Intent intent = new Intent();
                        intent.putExtra("key", "PAY_SUCCESS");
                        intent.setAction("GOTO_HISTORY");
                        sendBroadcast(intent);
                        finish();
                    }
                });
                mDialog.show();
                break;
            case 3:
                showPhoneDialog();
                break;
            case 4:
                showVideoDialog();
                break;
        }
    }
    // 弹窗
    private void showPhoneDialog() {
        final LoginDialog dialog = new LoginDialog(this);
        dialog.setContent("付款成功,医生会准时联系您！");
        dialog.setCancelable(false);
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //支付成功后切换到服务记录
                Intent intent = new Intent();
                intent.putExtra("key", "PAY_SUCCESS");
                intent.setAction("GOTO_HISTORY");
                sendBroadcast(intent);
                finish();
            }
        });
        dialog.show();
    }
    // 弹窗
    private void showVideoDialog() {
        final LoginDialog dialog = new LoginDialog(this);
        dialog.setContent("付款成功, 请到时间视频呼叫医生！");
        dialog.setCancelable(false);
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //支付成功后切换到服务记录
                Intent intent = new Intent();
                intent.putExtra("key", "PAY_SUCCESS");
                intent.setAction("GOTO_HISTORY");
                sendBroadcast(intent);
                finish();
            }
        });
        dialog.show();
    }
    public void doStartUnionPayPlugin(Activity activity, String tn, String mode) {
        UPPayAssistEx.startPayByJAR(activity, PayActivity.class, null, null,
                tn, mode);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        String msg = "";
        /*
         * 支付控件返回字符串:success、fail、cancel 分别代表支付成功，支付失败，支付取消
         */
        String str = data.getExtras().getString("pay_result");
        Log.e("zftphone", "2 "+data.getExtras().getString("merchantOrderId"));
        if (str.equalsIgnoreCase("success")) {
            msg = "支付成功！";
            Toaster.showShort(PayConfirmActivity.this,msg);
            showDialog();
        } else if (str.equalsIgnoreCase("fail")) {
            msg = "支付失败！";
            Toaster.showShort(PayConfirmActivity.this,msg);
        } else if (str.equalsIgnoreCase("cancel")) {
            msg = "用户取消了支付";
            Toaster.showShort(PayConfirmActivity.this,msg);
        }
        //支付完成,处理自己的业务逻辑!
    }

}
