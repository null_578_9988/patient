package com.wonhx.patient.app.model;

/**
 * Created by nsd on 2017/5/19.
 */

public class SearchHospital {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
