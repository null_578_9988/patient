package com.wonhx.patient.app.model;

/**
 * Created by Administrator on 2017/4/5.
 */

public class SupurDoctor {
    String id;
    String name;
    String dept_name;
    String hos_name;
    String good_subjects;
    String title;
    String logo_img_path;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }

    public String getGood_subjects() {
        return good_subjects;
    }

    public void setGood_subjects(String good_subjects) {
        this.good_subjects = good_subjects;
    }

    public String getLogo_img_path() {
        return logo_img_path;
    }

    public void setLogo_img_path(String logo_img_path) {
        this.logo_img_path = logo_img_path;
    }
}
