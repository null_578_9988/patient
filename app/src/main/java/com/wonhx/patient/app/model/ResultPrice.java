package com.wonhx.patient.app.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Created by nsd on 2017/5/27.
 *
 */

public class ResultPrice implements Serializable {
    public String birthday;
    public String address;
    public String good_subjects;
    public String city;
    public String province;
    public String name;
    public String phone;
    public String status;
    public String sex;
    public String title;
    public String introduction;
    public String deptName;
    public String hostipalName;

    public List<Serviceinfo> serviceinfo = Collections.EMPTY_LIST;
    public List<Schedule> schedule = Collections.EMPTY_LIST;
    public List<FamilyPrice> familyPrice = Collections.EMPTY_LIST;

    public List<FamilyPrice> getFamilyPrice() {
        return familyPrice;
    }

    public void setFamilyPrice(List<FamilyPrice> familyPrice) {
        this.familyPrice = familyPrice;
    }

    public List<Serviceinfo> getServiceinfo() {
        return serviceinfo;
    }

    public void setServiceinfo(List<Serviceinfo> serviceinfo) {
        this.serviceinfo = serviceinfo;
    }

    public List<Schedule> getSchedule() {

        return schedule;
    }

    public void setSchedule(List<Schedule> schedule) {
        this.schedule = schedule;
    }


    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGood_subjects() {
        return good_subjects;
    }

    public void setGood_subjects(String good_subjects) {
        this.good_subjects = good_subjects;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getHostipalName() {
        return hostipalName;
    }

    public void setHostipalName(String hostipalName) {
        this.hostipalName = hostipalName;
    }

    public class Schedule implements Serializable{
        public String startTime;
        public String endTime;
        public String id;
        public String status;
        public String serviceType;

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }
    }
    public class Serviceinfo implements Serializable{
        public String id;
        public String ServiceId;
        public String price;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getServiceId() {
            return ServiceId;
        }

        public void setServiceId(String serviceId) {
            ServiceId = serviceId;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }
    public class FamilyPrice implements Serializable{
        String service_price;
        String price_type_id;

        public String getService_price() {
            return service_price;
        }

        public void setService_price(String service_price) {
            this.service_price = service_price;
        }

        public String getPrice_type_id() {
            return price_type_id;
        }

        public void setPrice_type_id(String price_type_id) {
            this.price_type_id = price_type_id;
        }
    }

}
