package com.wonhx.patient.app.manager.servicehhistory;

import com.wonhx.patient.app.AppException;
import com.wonhx.patient.app.manager.ServiceHistoryManger;
import com.wonhx.patient.app.model.Chat_list;
import com.wonhx.patient.app.model.Evaluate;
import com.wonhx.patient.app.model.HistoryDetial;
import com.wonhx.patient.app.model.HistoryRecord;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.MedicalBook;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.http.Rest;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2017/4/13.
 *
 */

public class ServiceHistoryMangerImal implements ServiceHistoryManger {
    @Override
    public Subscription getServicelist(final String member_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<HistoryRecord>>(){
            @Override
            public void call(Subscriber<? super ListProResult<HistoryRecord>> subscriber) {
                try {
                    ListProResult<HistoryRecord> result=restServer.getservicehistory(member_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                        subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getServicelistdetial(final String req_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<HistoryDetial>>(){
            @Override
            public void call(Subscriber<? super ProResult<HistoryDetial>> subscriber) {
                try {
                    ProResult<HistoryDetial> result=restServer.getservicehistorydetial(req_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getmedicalbook(final String request_id, final Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<MedicalBook>>(){
            @Override
            public void call(Subscriber<? super ListProResult<MedicalBook>> subscriber) {
                try {
                    ListProResult<MedicalBook> result=restServer.getmedicalbook(request_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }
    @Override
    public Subscription getchatlist(final String req_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<Chat_list>>(){
            @Override
            public void call(Subscriber<? super ListProResult<Chat_list>> subscriber) {
                try {
                    ListProResult<Chat_list> result=restServer.getchatlist(req_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription evaluateDoctor(final String request_id, final String content, final String scroe, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>(){
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result=restServer.evaluateDoctor(request_id,content,scroe);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getEvaluateDetail(final String consultation_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<Evaluate>>(){
            @Override
            public void call(Subscriber<? super ProResult<Evaluate>> subscriber) {
                try {
                    ProResult<Evaluate> result=restServer.getealuatedtail(consultation_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription postVideoStatus(final String schedule_id, final String v_status, Subscriber subscriber) {
        return  Observable.create(new Observable.OnSubscribe<Result>(){
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result=restServer.postVideoStatus(schedule_id,v_status);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription postVideoInfo(final String schedule_id, final String member_id, final String name, final String req_id, Subscriber subscriber) {
        return  Observable.create(new Observable.OnSubscribe<Result>(){
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result=restServer.postVideoInfo(schedule_id,member_id,name,req_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription tuikuan(final String order_id, Subscriber subscriber) {
        return  Observable.create(new Observable.OnSubscribe<Result>(){
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result=restServer.tuikuan(order_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }
    ServiceHistoryMangerImal.RestServer restServer= Rest.create(ServiceHistoryMangerImal.RestServer.class);
    interface  RestServer{
        /**
         * 获取服务记录
         * @param member_id
         * @return
         */
        @GET("/consultation_service/patient_service_record")
        ListProResult<HistoryRecord> getservicehistory(@Query("member_id") String member_id);
        /**
         * 获取服务记录详情
         * @param req_id
         * @return
         */
        @GET("/consultation_service/get_patient_info")
        ProResult<HistoryDetial> getservicehistorydetial(@Query("req_id") String req_id);
        /**
         * 获取病例文档
         * @param request_id
         * @return
         */
        @GET("/consultation_service/case_document")
        ListProResult<MedicalBook>getmedicalbook(@Query("request_id") String request_id);
        /**
         * 获取聊天记录
         * @param req_id
         * @return
         */
        @GET("/consultation_service/remote_record")
        ListProResult<Chat_list>getchatlist(@Query("req_id") String req_id);

        /**
         * 评价医生
         * @param request_id
         * @param content
         * @param scroe
         * @return
         */
        @FormUrlEncoded
        @POST("/patient_concern/patient_service_appraise")
        Result evaluateDoctor(@Field("request_id") String request_id,@Field("content") String content,@Field("scroe") String scroe);

        /**
         * 获取评价详情
         * @param consultation_id
         * @return
         */
        @GET("/consultation_service/get_service_appraisal")
        ProResult<Evaluate> getealuatedtail(@Query("consultation_id") String consultation_id);

        /**
         * 提交视频通话状态
         * @param schedule_id
         * @param v_status
         * @return
         */
        @GET("/timed_task/video_status")
        Result postVideoStatus(@Query("schedule_id") String schedule_id,@Query("v_status") String v_status);

        /**
         * 提交视频通话信息
         * @param schedule_id
         * @param member_id
         * @param name
         * @param req_id
         * @return
         */
        @GET("/timed_task/video_rows")
        Result postVideoInfo(@Query("schedule_id") String schedule_id,@Query("member_id") String member_id,@Query("name") String name,@Query("req_id") String req_id);

        /**
         * \退款
         * @param order_id
         * @return
         */
        @FormUrlEncoded
        @POST("/timed_task/video_full_refund")
        Result tuikuan(@Field("order_id")String order_id);
    }
}
