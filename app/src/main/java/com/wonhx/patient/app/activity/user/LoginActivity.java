package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.LoginResult;
import com.wonhx.patient.app.model.UserInfo;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/23.
 * 登录页面
 */
public class LoginActivity extends BaseActivity {
    @InjectView(R.id.phoneNum)
    EditText mPhoneNum;
    @InjectView(R.id.pwd)
    EditText mPwd;
    @InjectView(R.id.clear_phone)
    ImageView mClearPhone;
    @InjectView(R.id.clear_pwd)
    ImageView mClearPwd;
    @InjectView(R.id.login_btn)
    Button mLoginBtn;
    UserManager userManager = new UserManagerImpl();
    UserInfo userDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mPhoneNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(mPhoneNum.getText().toString().trim())) {
                    mClearPhone.setVisibility(View.VISIBLE);
                } else {
                    mClearPhone.setVisibility(View.GONE);
                }
            }
        });
        mPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(mPwd.getText().toString().trim())) {
                    mClearPwd.setVisibility(View.VISIBLE);
                } else {
                    mClearPwd.setVisibility(View.GONE);
                }
            }
        });
    }

    @OnClick(R.id.login_close_btn)
    void close() {
        finish();
    }

    @OnClick(R.id.clear_pwd)
    void clearPwd() {
        mPwd.setText("");
    }

    @OnClick(R.id.clear_phone)
    void clearPhone() {
        mPhoneNum.setText("");
    }

    @OnClick(R.id.login_btn)
    void doLogin() {
        if (StrKit.isBlank(mPhoneNum.getText().toString().trim())) {
            Toaster.showShort(this, "请输入手机号！");
            return;
        }
        if (!StrKit.isMobileNO(mPhoneNum.getText().toString().trim())) {
            Toaster.showShort(this, "请正确输入手机号码！");
            return;
        }
        if (StrKit.isBlank(mPwd.getText().toString().trim())) {
            Toaster.showShort(this, "请输入密码！");
            return;
        }
        userManager.login(mPhoneNum.getText().toString().trim(), mPwd.getText().toString().trim(),
                "1", "2", AbSharedUtil.getString(LoginActivity.this,"device_token"), new SubscriberAdapter<LoginResult>() {
                    @Override
                    public void success(LoginResult loginResult) {
                        super.success(loginResult);
                        //健康档案那里用到的字段
                        AbSharedUtil.putString(LoginActivity.this,"helthId",null);
                        AbSharedUtil.putString(LoginActivity.this,"userId",loginResult.getMember_id());
                        //初始化数据库
                        appContext.setIsCallDoctor(false);
                        appContext.login();
                        userDao = new UserInfo();
                        userDao.setMember_id(loginResult.getMember_id());
                        userDao.setUsername(loginResult.getPatient_name());
                        userDao.setPhone(loginResult.getPhone());
                        userDao.setHuanxin_username(loginResult.getHuanxin_username());
                        userDao.setHuanxin_pw(loginResult.getHuanxin_pw());
                        userDao.setAvater(loginResult.getLogo_img_path());
                        userDao.saveOrUpdate();
                        signIn(loginResult.getHuanxin_username(), loginResult.getHuanxin_pw());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                });
    }
    @OnClick(R.id.regist_login_btn)
    void goRegister() {
        UIKit.open(LoginActivity.this, RegisterActivity.class);
        finish();
    }

    @OnClick(R.id.forget_pwd_btn)
    void editPwd() {
        UIKit.open(LoginActivity.this, ForgetPwdfActivity.class);
    }

    /**
     * 登录方法
     */
    private void signIn(String username, String password) {
        EMClient.getInstance().login(username, password, new EMCallBack() {
            /**
             * 登陆成功的回调
             */
            @Override
            public void onSuccess() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mDialog.dismiss();
                        // 加载所有会话到内存
                        EMClient.getInstance().chatManager().loadAllConversations();
                        // 加载所有群组到内存，如果使用了群组的话
                        // EMClient.getInstance().groupManager().loadAllGroups();
                        //更新当前用户的在苹果 APNS 推送的昵称
                        EMClient.getInstance().updateCurrentUserNick(userDao.getUsername());
                        //登录成功后  刷新 服务记录与我的医生
                        Intent intent = new Intent();
                        intent.setAction("REFRESH");
                        intent.putExtra("key","2");
                        sendBroadcast(intent);
                        Toaster.showShort(LoginActivity.this, "会员登录成功！");
                        finish();
                    }
                });
            }

            /**
             * 登陆错误的回调
             * @param i
             * @param s
             */
            @Override
            public void onError(final int i, final String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mDialog.dismiss();
                        Log.d("lzan13", "登录失败 Error code:" + i + ", message:" + s);
                        /**
                         * 关于错误码可以参考官方api详细说明
                         * http://www.easemob.com/apidoc/android/chat3.0/classcom_1_1hyphenate_1_1_e_m_error.html
                         */
                        switch (i) {
                            // 网络异常 2
                            case EMError.NETWORK_ERROR:
                                Toast.makeText(LoginActivity.this, "网络错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无效的用户名 101
                            case EMError.INVALID_USER_NAME:
                                Toast.makeText(LoginActivity.this, "无效的用户名 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无效的密码 102
                            case EMError.INVALID_PASSWORD:
                                Toast.makeText(LoginActivity.this, "无效的密码 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 用户认证失败，用户名或密码错误 202
                            case EMError.USER_AUTHENTICATION_FAILED:
                                Toast.makeText(LoginActivity.this, "用户认证失败，用户名或密码错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 用户不存在 204
                            case EMError.USER_NOT_FOUND:
                                Toast.makeText(LoginActivity.this, "用户不存在 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无法访问到服务器 300
                            case EMError.SERVER_NOT_REACHABLE:
                                Toast.makeText(LoginActivity.this, "无法访问到服务器 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 等待服务器响应超时 301
                            case EMError.SERVER_TIMEOUT:
                                Toast.makeText(LoginActivity.this, "等待服务器响应超时 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 服务器繁忙 302
                            case EMError.SERVER_BUSY:
                                Toast.makeText(LoginActivity.this, "服务器繁忙 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 未知 Server 异常 303 一般断网会出现这个错误
                            case EMError.SERVER_UNKNOWN_ERROR:
                                Toast.makeText(LoginActivity.this, "未知的服务器异常 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            default:
                                // mDialog.dismiss();
                                // 加载所有会话到内存
                                EMClient.getInstance().chatManager().loadAllConversations();
                                // 加载所有群组到内存，如果使用了群组的话
                                // EMClient.getInstance().groupManager().loadAllGroups();
                                //更新当前用户的在苹果 APNS 推送的昵称
                                EMClient.getInstance().updateCurrentUserNick(userDao.getUsername());
                                //登录成功后  刷新 服务记录与我的医生
                                Intent intent = new Intent();
                                intent.setAction("REFRESH");
                                intent.putExtra("key",userDao.getMember_id());
                                sendBroadcast(intent);
                                Toaster.showShort(LoginActivity.this, "会员登录成功！");
                                finish();
                                //Toast.makeText(LoginActivity.this, "ml_sign_in_failed code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

            @Override
            public void onProgress(int i, String s) {

            }
        });
    }
}
