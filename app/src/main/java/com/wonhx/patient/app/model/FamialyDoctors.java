package com.wonhx.patient.app.model;

/**
 * Created by Administrator on 2017/6/24.
 */

public class FamialyDoctors {
    private  String doctor_id;
    private  String member_id;
    private  String doctor_name;
    private  String logo_img_path;
    private  String good_subjects;
    private  String title;
    private  String dept_name;
    private  String hospital_name;
    private  String price;
    private  String end_time;
    private  String order_id;
    private  String times;
    private  String service_id;
    private  String sys_time;

    public String getSys_time() {
        return sys_time;
    }

    public void setSys_time(String sys_time) {
        this.sys_time = sys_time;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getLogo_img_path() {
        return logo_img_path;
    }

    public void setLogo_img_path(String logo_img_path) {
        this.logo_img_path = logo_img_path;
    }

    public String getGood_subjects() {
        return good_subjects;
    }

    public void setGood_subjects(String good_subjects) {
        this.good_subjects = good_subjects;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }
}
