package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.model.PushMessage;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MessageDetialActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.titles)
    TextView titles;
    @InjectView(R.id.time)
    TextView time;
    @InjectView(R.id.alert)
    TextView alert;
    String ti="",tim="",isrea="",aler="";
    PushMessage pushMessage=new PushMessage();
    List<PushMessage>pushMessages=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_detial);
        ButterKnife.inject(this);
        pushMessages=pushMessage.findAll();
        Intent intent=getIntent();
        ti=intent.getStringExtra("title");
        tim=intent.getStringExtra("time");
        aler=intent.getStringExtra("alert");
        title.setText("消息详情");
        titles.setText(ti);
        time.setText(tim);
        alert.setText(aler);
    }

    @OnClick(R.id.left_btn)
    public void onViewClicked() {
        finish();
    }
}
