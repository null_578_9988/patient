package com.wonhx.patient.app.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wonhx.patient.app.fragment.main.DoctorTabFragment;
import com.wonhx.patient.app.fragment.main.MyTabFragment;
import com.wonhx.patient.app.fragment.main.ServiceHistoryFragment;
import com.wonhx.patient.app.fragment.main.ServiceTabFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 17/3/23.
 * 主页viewpager适配器
 */
public class MainPagerAdapter extends FragmentPagerAdapter {
    Context mContext;
    List<Fragment> fragments = new ArrayList<Fragment>();
    public MainPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        fragments.add(Fragment.instantiate(mContext, ServiceTabFragment.class.getName()));
        fragments.add(Fragment.instantiate(mContext, ServiceHistoryFragment.class.getName()));
        fragments.add(Fragment.instantiate(mContext, DoctorTabFragment.class.getName()));
        fragments.add(Fragment.instantiate(mContext, MyTabFragment.class.getName()));
    }
    @Override
    public Fragment getItem(int arg0) {
        return fragments.get(arg0);
    }
    @Override
    public int getCount() {
        return fragments.size();
    }
}