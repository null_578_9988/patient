package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.adapter.PriceSelectAdapter;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.ResultPrice;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.MGridView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class HomeDoctorDetialActivity extends BaseActivity {
    String service_price = "",  doctor_id = "",service_id="";
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    ArrayList<ResultPrice.FamilyPrice> mfamilyinfo = new ArrayList<ResultPrice.FamilyPrice>();
    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.grid_view)
    GridView gridView;
    PriceSelectAdapter priceSelectAdapter;
    @InjectView(R.id.tv_price)
    TextView tv_price;
    MyReceiver myReceiver;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            unregisterReceiver(myReceiver);
        }
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        title.setText("家庭医生");
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("GOTO_HISTORY");
        intentFilter.addAction("QUIT");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String str) {
                finish();
            }
        });
    }
    @Override
    protected void onInitData() {
        super.onInitData();
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            mfamilyinfo = (ArrayList<ResultPrice.FamilyPrice>) bundle.getSerializable("mfamilyinfo");
            doctor_id=bundle.getString("doctor_id");
            priceSelectAdapter=new PriceSelectAdapter(this,mfamilyinfo);
            gridView.setAdapter(priceSelectAdapter);
            service_price=mfamilyinfo.get(0).getService_price();
            tv_price.setText("￥"+mfamilyinfo.get(0).getService_price());
            service_id=mfamilyinfo.get(0).getPrice_type_id();
        }
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                priceSelectAdapter.changeSelected(i);
                service_price=mfamilyinfo.get(i).getService_price();
                tv_price.setText("￥"+mfamilyinfo.get(i).getService_price());
                service_id=mfamilyinfo.get(i).getPrice_type_id();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_doctor_detial);
        ButterKnife.inject(this);
    }

    //结算
    @OnClick(R.id.tv_jiesuan)
    void jiesuan() {
          firstPagerMager.getFamilyDoctor_oeder(service_price, service_id, AbSharedUtil.getString(this, "userId"), doctor_id, new SubscriberAdapter<Result>() {
              @Override
              public void success(Result result) {
                  super.success(result);
                  Intent intent = new Intent(HomeDoctorDetialActivity.this, FamilyPayActivity.class);
                  intent.putExtra("price", service_price);
                  intent.putExtra("orderid", result.getData());
                  startActivity(intent);
              }
          });
    }

    @OnClick(R.id.left_btn)
    void back() {
        finish();
    }
}
