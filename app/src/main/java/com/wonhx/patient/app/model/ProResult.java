package com.wonhx.patient.app.model;

/**
 * Created by apple on 17/3/29.
 *
 */
public class ProResult<T> {
    public String code;
    public String msg;
    public T data;

    public boolean getCode() {
        return code.equals("0");
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
