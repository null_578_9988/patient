package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.FamilyDoctorSearchList;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.kit.Toaster;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class Family_Doctor_SearchActivity extends BaseActivity {

    @InjectView(R.id.lv)
    ListView lv;
    @InjectView(R.id.keyword)
    EditText mKeyWord;
    String keyword="";
    FirstPagerMager firstPagerMager=new FirstPagerMangerImal();
    List<FamilyDoctorSearchList>list=new ArrayList<>();
    private QuickAdapter<FamilyDoctorSearchList>adapter;
    @Override
    protected void onInitView() {
        super.onInitView();
        adapter=new QuickAdapter<FamilyDoctorSearchList>(this,R.layout.item_family_search) {
            @Override
            protected void convert(BaseAdapterHelper helper, FamilyDoctorSearchList item) {
                helper.setText(R.id.doctor,item.getDoctor_name()!=null&&!item.getDoctor_name().equals("")?item.getDoctor_name():"");
            }
        };
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent in=new Intent(Family_Doctor_SearchActivity.this, DoctorDetialActivity.class);
                in.putExtra("member_id",list.get(i).getMember_id());
                startActivity(in);
            }
        });
        mKeyWord.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == android.view.KeyEvent.KEYCODE_ENTER && event.getAction() == android.view.KeyEvent.ACTION_UP) {
                    // 先隐藏键盘
                    ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                            .hideSoftInputFromWindow(Family_Doctor_SearchActivity.this.getCurrentFocus()
                                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    //进行搜索操作的方法，在该方法中可以加入mEditSearchUser的非空判断
                    keyword=mKeyWord.getText().toString().trim();
                    getlist(keyword);
                }
                return false;
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family__doctor__search);
        ButterKnife.inject(this);
    }
    @OnClick({R.id.back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }

    private void getlist(String content) {
        firstPagerMager.getFamilyDoctor_searchList(content, new SubscriberAdapter<ListProResult<FamilyDoctorSearchList>>(){
            @Override
            public void onError(Throwable e) {
                dismissLoadingDialog();
                list.clear();
                adapter.notifyDataSetChanged();
                Toaster.showShort(Family_Doctor_SearchActivity.this, e.getMessage());
            }
            @Override
            public void success(ListProResult<FamilyDoctorSearchList> familyDoctorSearchListListProResult) {
                super.success(familyDoctorSearchListListProResult);
                    list.clear();
                    list.addAll(familyDoctorSearchListListProResult.getData());
                    if (list.size() > 0) {
                        adapter.addAll(list);
                    }
            }
        });
    }
}
