package com.wonhx.patient.app.fragment.main;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.firstpage.BannerDetailActivity;
import com.wonhx.patient.app.activity.user.AccountActivity;
import com.wonhx.patient.app.activity.user.BuyMedicineActivity;
import com.wonhx.patient.app.activity.user.ChangePwdActivity;
import com.wonhx.patient.app.activity.user.HealthyFileActivity;
import com.wonhx.patient.app.activity.user.LoginActivity;
import com.wonhx.patient.app.activity.user.MyFamilyDoctorActivity;
import com.wonhx.patient.app.activity.user.MySuggestionActivity;
import com.wonhx.patient.app.activity.user.UserInfoActivty;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.UserInfo;
import com.wonhx.patient.app.sweep.CaptureActivity;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.FileKit;
import com.wonhx.patient.kit.ImageKit;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.view.LoginDialog;
import com.wonhx.patient.view.QuiteDialog;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

/**
 * Created by apple on 17/3/22.
 * 个人中心（我的）
 */
public class MyTabFragment extends BaseFragment {
    private final static int CROP = 200;
    private final static String FILE_SAVEPATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Patient/Portrait/";
    @InjectView(R.id.myHead)
    SimpleDraweeView mUserHead;
    @InjectView(R.id.myName)
    TextView mUserName;
    @InjectView(R.id.quit_login_btn)
    Button mLoginOrQuickBtn;
    UserInfo userDao = new UserInfo();
    String mMemberId;
    private Bitmap protraitBitmap;
    private File protraitFile;
    private String protraitPath;
    private Uri cropUri;
    private Uri origUri;
    LoginDialog dialog;
    UserManager userManager = new UserManagerImpl();
    QuiteDialog quitedialog = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_tab_mycenter, container, false);
        ButterKnife.inject(this, viewGroup);
        return viewGroup;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mMemberId = AbSharedUtil.getString(getActivity(), "userId");
        Log.e("mMemberId", mMemberId + "aaaaaaa");
        if (mMemberId != null) {
            userDao = userDao.findById(Integer.parseInt(mMemberId));
            if (userDao != null) {
                Uri imgurl = Uri.parse(Constants.REST_ORGIN + "/emedicine" + userDao.getAvater());
                Log.e("hahah","嘿嘿嘿");
                mUserHead.setImageURI(imgurl);
                mUserName.setText(userDao.getUsername());
                mLoginOrQuickBtn.setVisibility(View.VISIBLE);
                mLoginOrQuickBtn.setText("退出登录");
            }
        } else {
            //清空头像跟电话
            mUserName.setText("请点击登录");
            mUserHead.setImageResource(R.mipmap.offhead);
            mLoginOrQuickBtn.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.li_tiaozhuan)
    void goTologin() {
        if (mMemberId != null) {
            imageChooseItem();
        } else {
            UIKit.open(getActivity(), LoginActivity.class);
        }
    }

    @OnClick(R.id.quit_login_btn)
    void loginOrQuick() {
        if (mMemberId != null) {
            //退出
            quitedialog();
        } else {
            //登录，跳转到登录页面
            UIKit.open(getActivity(), LoginActivity.class);
        }
    }

    private void quitedialog() {
        quitedialog = new QuiteDialog(getActivity());
        quitedialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quitedialog.dismiss();
                quit();
            }
        });
        quitedialog.setOnNegativeListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                quitedialog.dismiss();
            }
        });
        quitedialog.show();
    }

    private void quit() {
        userManager.quit(mMemberId, new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                EMClient.getInstance().logout(true, new EMCallBack() {
                    @Override
                    public void onSuccess() {
                        //退出登录，清空用户信息
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AbSharedUtil.putString(getActivity(), "userId", null);
                                mMemberId = AbSharedUtil.getString(getActivity(), "userId");
                                mUserName.setText("请点击登录");
                                mUserHead.setImageResource(R.mipmap.offhead);
                                mLoginOrQuickBtn.setVisibility(View.GONE);
                                appContext.setUserInfo(null);
                                //退出成功后  刷新 服务记录与我的医生
                                Intent intent = new Intent();
                                intent.putExtra("key", "1");
                                intent.setAction("REFRESH");
                                getActivity().sendBroadcast(intent);
                            }
                        });
                    }

                    @Override
                    public void onError(int i, String s) {
                        Toaster.showShort(getActivity(), "退出登录失败！");
                    }

                    @Override
                    public void onProgress(int i, String s) {

                    }
                });
            }
        });
    }

    //个人信息
    @OnClick(R.id.persondata)
    void conversation() {
        if (mMemberId != null) {
            UIKit.open(getActivity(), UserInfoActivty.class);
        } else {
            dialog();
        }
    }
    //我的家庭医生
    @OnClick(R.id.heart)
    void myheart(){
        if (mMemberId != null) {
            UIKit.open(getActivity(), MyFamilyDoctorActivity.class);
        } else {
            dialog();
        }
    }

    //购药清单
    @OnClick(R.id.buymedicine)
    void buymedicine() {
        if (mMemberId != null) {
            UIKit.open(getActivity(), BuyMedicineActivity.class);
        } else {
            dialog();
        }
    }

    //声明
    @OnClick(R.id.linear_shengming)
    void shengming() {
        if (mMemberId != null) {
            Intent intent = new Intent(getActivity(), BannerDetailActivity.class);
            intent.putExtra("url", Constants.MIANZE);
            intent.putExtra("title", "");
            startActivity(intent);
        } else {
            dialog();
        }
    }

    //意见反馈
    @OnClick(R.id.mysuggestion)
    void suggestion() {
        if (mMemberId != null) {
            UIKit.open(getActivity(), MySuggestionActivity.class);
        } else {
            dialog();
        }
    }

    //帮助中心
    @OnClick(R.id.li_help)
    void help_center() {
        if (mMemberId != null) {
            Intent intent = new Intent(getActivity(), BannerDetailActivity.class);
            intent.putExtra("url", Constants.HELP_CENTER);
            intent.putExtra("title", "");
            startActivity(intent);
        } else {
            dialog();
        }
    }

    //密码修改
    @OnClick(R.id.mysetting)
    void change_pwd() {
        if (mMemberId != null) {
            UIKit.open(getActivity(), ChangePwdActivity.class);
        } else {
            dialog();
        }
    }

    //个人账户
    @OnClick(R.id.account)
    void acount() {
        if (mMemberId != null) {
            UIKit.open(getActivity(), AccountActivity.class);
        } else {
            dialog();
        }
    }

    /**
     * 扫码关注
     */
    @OnClick(R.id.scanner)
    void capture() {
        if (mMemberId != null) {
            UIKit.open(getActivity(), CaptureActivity.class);
        } else {
            dialog();
        }
    }

    /**
     * 健康档案
     */
    @OnClick(R.id.healdangan)
    void healthyFile() {
        if (mMemberId != null) {
            UIKit.open(getActivity(), HealthyFileActivity.class);
        } else {
            dialog();
        }
    }

    /**
     * 选择头像
     */
    private void imageChooseItem() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.select_dialog, null);
        final Dialog dialog = new AlertDialog.Builder(getActivity()).create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.mydialogstyle);   //添加动画
        window.setBackgroundDrawable(new ColorDrawable());
        dialog.show();
        window.setContentView(view);
        TextView camera = (TextView) view.findViewById(R.id.camera);
        TextView picfile = (TextView) view.findViewById(R.id.picfile);
        TextView cancle = (TextView) view.findViewById(R.id.cancle);
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, new PermissionsResultAction() {
                    @Override
                    public void onGranted() {
                        //拍照
                        dialog.dismiss();
                        startActionCamera();
                    }

                    @Override
                    public void onDenied(String permission) {
                        //没有权限
                        Toast.makeText(getActivity(), "没有拍照权限！", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });
        picfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, new PermissionsResultAction() {
                    @Override
                    public void onGranted() {
                        //相册
                        dialog.dismiss();
                        startImagePick();
                    }

                    @Override
                    public void onDenied(String permission) {
                        //没有权限
                        Toast.makeText(getActivity(), "没有选择照片权限！", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }


    /**
     * 相机拍照
     */
    private void startActionCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, this.getCameraTempFile());
        startActivityForResult(intent, ImageKit.REQUEST_CODE_GETIMAGE_BYCAMERA);
    }

    /**
     * 选择图片
     */
    private void startImagePick() {
        Intent intent = new Intent(getActivity(), MultiImageSelectorActivity.class);
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, false);
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, MultiImageSelectorActivity.MODE_SINGLE);
        startActivityForResult(intent, ImageKit.REQUEST_CODE_GETIMAGE_BYCROP);
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        intent.setType("image/*");
//        startActivityForResult(Intent.createChooser(intent, "选择图片"), ImageKit.REQUEST_CODE_GETIMAGE_BYCROP);
    }

    /**
     * 选图后裁剪
     *
     * @param data 原始图片
     */
    private void startActionCrop(Uri data) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(Uri.fromFile(new File(data.toString())), "image/*");
        intent.putExtra("output", getUploadTempFile(data));
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);// 裁剪框比例
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", CROP);// 输出图片大小
        intent.putExtra("outputY", CROP);
        intent.putExtra("scale", true);// 去黑边
        intent.putExtra("scaleUpIfNeeded", true);// 去黑边
        intent.putExtra("return-data", true);
        startActivityForResult(intent, ImageKit.REQUEST_CODE_GETIMAGE_BYSDCARD);
    }
    /**
     * 拍照后裁剪
     *
     * @param data 原始图片
     */
    private void startActionCrop1(Uri data) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(data, "image/*");
        intent.putExtra("output", getUploadTempFile(data));
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);// 裁剪框比例
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", CROP);// 输出图片大小
        intent.putExtra("outputY", CROP);
        intent.putExtra("scale", true);// 去黑边
        intent.putExtra("scaleUpIfNeeded", true);// 去黑边
        intent.putExtra("return-data", true);
        startActivityForResult(intent, ImageKit.REQUEST_CODE_GETIMAGE_BYSDCARD);
    }

    // 拍照保存的绝对路径
    private Uri getCameraTempFile() {
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            File savedir = new File(FILE_SAVEPATH);
            if (!savedir.exists()) {
                savedir.mkdirs();
            }
        } else {
            Toaster.showShort(getActivity(), "无法保存，请检查SD卡是否挂载");
            return null;
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        // 照片命名
        String cropFileName = "osc_camera" + ".jpg";
        // 裁剪头像的绝对路径
        protraitPath = FILE_SAVEPATH + cropFileName;
        protraitFile = new File(protraitPath);
        cropUri = Uri.fromFile(protraitFile);
        this.origUri = this.cropUri;
        return this.cropUri;
    }

    // 裁剪头像的绝对路径
    private Uri getUploadTempFile(Uri uri) {
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            File savedir = new File(FILE_SAVEPATH);
            if (!savedir.exists()) {
                savedir.mkdirs();
            }
        } else {
            Toaster.showShort(getActivity(), "无法保存，请检查SD卡是否挂载");
            return null;
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String thePath = ImageKit.getAbsolutePathFromNoStandardUri(uri);
        // 如果是标准Uri
        if (StrKit.isBlank(thePath)) {
            thePath = ImageKit.getAbsoluteImagePath(getActivity(), uri);
        }
        String ext = FileKit.getFileFormat(thePath);
        ext = StrKit.isBlank(ext) ? "jpg" : ext;
        // 照片命名
        String cropFileName = "osc_crop_" + timeStamp + "." + ext;
        // 裁剪头像的绝对路径
        protraitPath = FILE_SAVEPATH + cropFileName;
        protraitFile = new File(protraitPath);
        cropUri = Uri.fromFile(protraitFile);
        return this.cropUri;
    }

    /**
     * 上传新照片
     */
    private void uploadNewPhoto() {
        // 获取头像缩略图
          if (!StrKit.isBlank(protraitPath) && protraitFile.exists()) {
            protraitBitmap = ImageKit.loadImgThumbnail(protraitPath, 200, 200);
        } else {
            Toaster.showShort(getActivity(), "图像不存在，上传失败·");
            return;
        }
        if (protraitBitmap == null) {
            Toaster.showShort(getActivity(), "图像不存在，上传失败·");
            return;
        }
        // mUserHead.setImageBitmap(protraitBitmap);
        String imageContent = "";
        try {
            imageContent = FileKit.encodeBase64File(protraitPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        userManager.updateUserHead(imageContent, AbSharedUtil.getString(getActivity(), "userId"), new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
               // Toaster.showShort(getActivity(), result.getMsg() + result.getData());
                userDao.setAvater(result.getData());
                userDao.saveOrUpdate();
                Uri imgurl = Uri.parse(Constants.REST_ORGIN + "/emedicine" + result.getData());
                // 清除Fresco对这条验证码的缓存
                ImagePipeline imagePipeline = Fresco.getImagePipeline();
//              imagePipeline.evictFromMemoryCache(imgurl);
//              imagePipeline.evictFromDiskCache(imgurl);
                // combines above two lines
                imagePipeline.evictFromCache(imgurl);
                Log.e("hahah","哈哈哈");
                mUserHead.setImageURI(imgurl);
                // 获取头像缩略图
                if (!StrKit.isBlank(protraitPath) && protraitFile.exists()) {
                   protraitFile.delete();
                }
            }
        });
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK)
            return;
        switch (requestCode) {
            case ImageKit.REQUEST_CODE_GETIMAGE_BYCAMERA:
                startActionCrop1(origUri);// 拍照后裁剪
                break;
            case ImageKit.REQUEST_CODE_GETIMAGE_BYCROP:
                // Get the result list of select image paths
                List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                Uri pathUrl = Uri.parse(path.get(0));
                startActionCrop(pathUrl);// 选图后裁剪
                break;
            case ImageKit.REQUEST_CODE_GETIMAGE_BYSDCARD:
                uploadNewPhoto();// 上传新照片
                break;
        }
    }

    // 弹窗
    private void dialog() {
        dialog = new LoginDialog(getActivity());
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setClass(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });
        dialog.show();
    }
}
