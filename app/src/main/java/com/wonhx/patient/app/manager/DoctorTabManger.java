package com.wonhx.patient.app.manager;

import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Administrator on 2017/4/13.
 */

public interface DoctorTabManger {
    /**
     * 获取患者关注医生列表
     * @param member_id
     * @param subscriber
     * @return
     */
    public Subscription getServicelist(String member_id, Subscriber subscriber);
}
