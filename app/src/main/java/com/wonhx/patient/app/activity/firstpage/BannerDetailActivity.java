package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;

import butterknife.InjectView;
import butterknife.OnClick;

public class BannerDetailActivity extends BaseActivity {
    @InjectView(R.id.my_web_view)
    WebView webView;
    @InjectView(R.id.title)
    TextView tv_title;
    @InjectView(R.id.right_btn)
    TextView tv_right;
    String url="",title="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner_detail);
        tv_right.setText("");
        Intent intent=getIntent();
        if (intent!=null){
            url=intent.getStringExtra("url");
            title=intent.getStringExtra("title");
            tv_title.setText(title);
            Log.e("url",url);
            showLoadingDialog();
            setWebview();
        }
    }

    private void setWebview() {
        webView.loadUrl(url);
        dismissLoadingDialog();
        // 覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // TODO Auto-generated method stub
                // 返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                view.loadUrl(url);
                return true;
            }
        });
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        finish();
        return super.onKeyDown(keyCode, event);
    }
    @OnClick(R.id.left_btn)
    void back(){
        finish();
    }
}
