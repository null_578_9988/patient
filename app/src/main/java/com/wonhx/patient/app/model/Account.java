package com.wonhx.patient.app.model;

/**
 * Created by Administrator on 2017/4/7.
 */

public class Account {
    String id;
    String account_balance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount_balance() {
        return account_balance;
    }

    public void setAccount_balance(String account_balance) {
        this.account_balance = account_balance;
    }
}
