package com.wonhx.patient.app.activity.calldoctor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.hyphenate.EMCallBack;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;
import com.wonhx.patient.R;
import com.wonhx.patient.app.activity.main.MainActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.CallDoctorManager;
import com.wonhx.patient.app.manager.calldoctor.CallDoctorManagerImpl;
import com.wonhx.patient.app.model.CallUser;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.TabEntity;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.view.HJkeshiPopupWindow;
import com.wonhx.patient.view.HJloginDialog;

import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/31.
 * 呼叫主页
 */
public class IndexActivity extends BaseActivity implements HJkeshiPopupWindow.KeShiCallback {
    HJloginDialog dialog1;
    protected BroadcastReceiver broadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
                if (intent.getStringExtra("flag").equals("1")) {
                    showDialog(intent.getStringExtra("phone"));
                }else {
                    if (intent.getStringExtra("msgType").equals("2")) {
                        showDialog(intent.getStringExtra("video"));
                    }
                }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction("phoneexit");
        intentFilter.addAction("videoexit");
        this.registerReceiver(this.broadcastReceiver,intentFilter);
    }
    private void showDialog(String message) {
        dialog1 = new HJloginDialog(IndexActivity.this);
        dialog1.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
                startActivity(new Intent(IndexActivity.this,CallRecordsActivity.class));
            }
        });
        dialog1.setTitle("提示");
        dialog1.setContentMessage(message);
        dialog1.setPositiveMessage("确定");
        dialog1.setCancelVisibility(false);
        dialog1.setPositiveColor(this.getResources().getColor(R.color.hj_txt_red));
        dialog1.show();
    }
    @InjectView(R.id.llayout_phone)
    LinearLayout llayout_phone;
    @InjectView(R.id.llayout_video)
    LinearLayout ll_video;
    @InjectView(R.id.txt_name)
    TextView tv_name;
    @InjectView(R.id.llayout_family)
    LinearLayout ll_keshi;
    @InjectView(R.id.txt_family)
    TextView tv_keshi;
    @InjectView(R.id.txt_number)
    EditText ed_phonenum;
    @InjectView(R.id.btn_submit)
    Button btn_commit;
    @InjectView(R.id.llayout_phone_info)
    LinearLayout llayout_phone_info;
    @InjectView(R.id.menutab)
    CommonTabLayout mTabMenu;
    @InjectView(R.id.img_phone)
    ImageView mImgPhone;
    @InjectView(R.id.txt_phone)
    TextView mTxtPhone;
    @InjectView(R.id.img_video)
    ImageView mImgVideo;
    @InjectView(R.id.txt_video)
    TextView mTxtVideo;
    HJloginDialog dialog;
    CallUser calluser = new CallUser();
    String keshi_id = "all", type = "phone";
    Intent intent;
    CallDoctorManager calldoctor = new CallDoctorManagerImpl();
    private String[] mTitles = {"0", "1"};
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        boolean isFromSplash = getIntent().getBooleanExtra("isFromSplash",false);
        if (isFromSplash){
            //初始化数据库
            appContext.setIsCallDoctor(true);
            appContext.login();
            calluser = calluser.findById(AbSharedUtil.getInt(this, "callUserid"));
            tv_name.setText(calluser.getUsername());
            ed_phonenum.setText(calluser.getPhone());
            if (!EMClient.getInstance().isConnected()){
                signIn(calluser.getHuanxin_user(),calluser.getHuanxin_pass());
            }
        }else {
            calluser = calluser.findById(AbSharedUtil.getInt(this, "callUserid"));
            tv_name.setText(calluser.getUsername());
            ed_phonenum.setText(calluser.getPhone());
        }
        calluser = calluser.findById(AbSharedUtil.getInt(this, "callUserid"));
        tv_name.setText(calluser.getUsername());
        ed_phonenum.setText(calluser.getPhone());
    }


    @Override
    protected void onInitView() {
        super.onInitView();
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i]));
        }
        mTabMenu.setTabData(mTabEntities);
        mTabMenu.setCurrentTab(0);
        mImgPhone.setImageResource(R.mipmap.hj_index_phone_on);
        mTxtPhone.setTextColor(getResources().getColor(R.color.hj_txt_red));
        mImgVideo.setImageResource(R.mipmap.hj_index_video_off);
        mTxtVideo.setTextColor(getResources().getColor(R.color.gray_pressed));
    }

    //电话咨询
    @OnClick(R.id.llayout_phone)
    void llayout_phone() {
        type = "phone";
        mTabMenu.setCurrentTab(0);
        llayout_phone_info.setVisibility(View.VISIBLE);
        mImgPhone.setImageResource(R.mipmap.hj_index_phone_on);
        mTxtPhone.setTextColor(getResources().getColor(R.color.hj_txt_red));
        mImgVideo.setImageResource(R.mipmap.hj_index_video_off);
        mTxtVideo.setTextColor(getResources().getColor(R.color.gray_pressed));
        llayout_phone_info.setVisibility(View.VISIBLE);
    }

    //视频咨询
    @OnClick(R.id.llayout_video)
    void ll_video() {
        type = "video";
        mTabMenu.setCurrentTab(1);
        llayout_phone_info.setVisibility(View.GONE);
        mImgPhone.setImageResource(R.mipmap.hj_index_phone_off);
        mTxtPhone.setTextColor(getResources().getColor(R.color.gray_pressed));
        mImgVideo.setImageResource(R.mipmap.hj_index_video_on);
        mTxtVideo.setTextColor(getResources().getColor(R.color.hj_txt_red));
    }
    //选择科室
    @OnClick(R.id.llayout_family)
    void keshi() {
        showKeshiPopup();
    }

    //退出
    @OnClick(R.id.img_return)
    void turnout() {
        exitDialog();
    }


    @OnClick(R.id.img_history)
    void goToHistory(){
        UIKit.open(IndexActivity.this,CallRecordsActivity.class);
    }
    //提交
    @OnClick(R.id.btn_submit)
    void commit() {
        if (type.equals("phone")) {
            if (StrKit.isBlank(ed_phonenum.getText().toString())){
                Toaster.showShort(IndexActivity.this, "接听号码不能为空！");
                return;
            }
            //电话咨询
            phoneserver();
        } else if (type.equals("video")) {
            //视频咨询
            videoserver();
        }
    }

    /**
     * 视频咨询
     */
    private void videoserver() {
        calldoctor.videoserver(calluser.getUser_id(),keshi_id, calluser.getUsername(), new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                Toast.makeText(IndexActivity.this, result.getMsg(), Toast.LENGTH_LONG).show();
                intent = new Intent(IndexActivity.this, VideoServerActivity.class);
                intent.putExtra("phone", calluser.getUsername());
                startActivity(intent);
            }
        });
    }

    /**
     * 电话咨询
     */
    private void phoneserver() {
        calldoctor.phoneserver(keshi_id, ed_phonenum.getText().toString(), calluser.getUser_id(), new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                Toast.makeText(IndexActivity.this, result.getMsg(), Toast.LENGTH_LONG).show();
                Bundle bundle = new Bundle();
                bundle.putString("phone", ed_phonenum.getText().toString());
                UIKit.open(IndexActivity.this, PhoneServerActivity.class, bundle);
            }
        });
    }
    /**
     * 退出弹窗
     */
    private void exitDialog() {
        dialog = new HJloginDialog(this);
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //退出
                quit();
                dialog.dismiss();
            }
        });
        dialog.setOnCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setTitle("确定退出系统？");
        dialog.setContentMessage("退出后下次进入需要重新登录");
        dialog.setCancelMessage("取消");
        dialog.setPositiveMessage("确认");
        dialog.setPositiveColor(this.getResources()
                .getColor(R.color.hj_txt_red));
        dialog.show();
    }
    /**
     * 退出接口
     */
    private void quit() {
        calldoctor.quit(calluser.getUser_id(), new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                AbSharedUtil.putInt(IndexActivity.this,"callUserid",0);
                appContext.setUserInfo(null);
                UIKit.open(IndexActivity.this, MainActivity.class);
                finish();
            }
        });
    }

    public void showKeshiPopup() {
        HJkeshiPopupWindow keshiPopup = new HJkeshiPopupWindow(IndexActivity.this, this);
        keshiPopup.setSoftInputMode(PopupWindow.INPUT_METHOD_NEEDED);
        keshiPopup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        keshiPopup.showAtLocation(tv_name, Gravity.RIGHT, 0, 0);
    }
    @Override
    public void click(View v, String keshi_id, String keshi_name) {
        this.keshi_id = keshi_id;
        tv_keshi.setText(keshi_name);
    }
    @Override
    public void onBackPressed() {
        //实现Home键效果
        //super.onBackPressed();这句话一定要注掉,不然又去调用默认的back处理方式了
        Intent i= new Intent(Intent.ACTION_MAIN);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addCategory(Intent.CATEGORY_HOME);
        startActivity(i);
    }
    /**
     * 登录方法
     */
    private void signIn(String username, String password) {
        EMClient.getInstance().login(username, password, new EMCallBack() {
            /**
             * 登陆成功的回调
             */
            @Override
            public void onSuccess() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mDialog.dismiss();
                        // 加载所有会话到内存
                        EMClient.getInstance().chatManager().loadAllConversations();
                        // 加载所有群组到内存，如果使用了群组的话
                        // EMClient.getInstance().groupManager().loadAllGroups();
                        //更新当前用户的在苹果 APNS 推送的昵称
                        EMClient.getInstance().updateCurrentUserNick(calluser.getUsername());
                    }
                });
            }

            /**
             * 登陆错误的回调
             * @param i
             * @param s
             */
            @Override
            public void onError(final int i, final String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mDialog.dismiss();
                        Log.d("lzan13", "登录失败 Error code:" + i + ", message:" + s);
                        /**
                         * 关于错误码可以参考官方api详细说明
                         * http://www.easemob.com/apidoc/android/chat3.0/classcom_1_1hyphenate_1_1_e_m_error.html
                         */
                        switch (i) {
                            // 网络异常 2
                            case EMError.NETWORK_ERROR:
                                Toast.makeText(IndexActivity.this, "网络错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无效的用户名 101
                            case EMError.INVALID_USER_NAME:
                                Toast.makeText(IndexActivity.this, "无效的用户名 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无效的密码 102
                            case EMError.INVALID_PASSWORD:
                                Toast.makeText(IndexActivity.this, "无效的密码 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 用户认证失败，用户名或密码错误 202
                            case EMError.USER_AUTHENTICATION_FAILED:
                                Toast.makeText(IndexActivity.this, "用户认证失败，用户名或密码错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 用户不存在 204
                            case EMError.USER_NOT_FOUND:
                                Toast.makeText(IndexActivity.this, "用户不存在 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无法访问到服务器 300
                            case EMError.SERVER_NOT_REACHABLE:
                                Toast.makeText(IndexActivity.this, "无法访问到服务器 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 等待服务器响应超时 301
                            case EMError.SERVER_TIMEOUT:
                                Toast.makeText(IndexActivity.this, "等待服务器响应超时 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 服务器繁忙 302
                            case EMError.SERVER_BUSY:
                                Toast.makeText(IndexActivity.this, "服务器繁忙 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 未知 Server 异常 303 一般断网会出现这个错误
                            case EMError.SERVER_UNKNOWN_ERROR:
                                Toast.makeText(IndexActivity.this, "未知的服务器异常 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(IndexActivity.this, "ml_sign_in_failed code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

            @Override
            public void onProgress(int i, String s) {
            }
        });
    }

}
