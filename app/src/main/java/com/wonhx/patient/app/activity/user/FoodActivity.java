package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FoodActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.left_btn)
    ImageView leftBtn;
    @InjectView(R.id.right_btn)
    TextView rightBtn;
    @InjectView(R.id.search_layout)
    RelativeLayout searchLayout;
    @InjectView(R.id.radio0)
    CheckBox radio0;
    @InjectView(R.id.radio1)
    CheckBox radio1;
    @InjectView(R.id.radio2)
    CheckBox radio2;
    @InjectView(R.id.radio3)
    CheckBox radio3;
    @InjectView(R.id.radio4)
    CheckBox radio4;
    @InjectView(R.id.radio5)
    CheckBox radio5;
    @InjectView(R.id.button1)
    Button button1;
    String msg = "";
    @InjectView(R.id.LinearLayout1)
    LinearLayout LinearLayout1;
    String msga="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);
        ButterKnife.inject(this);
        Intent in=getIntent();
        msga=in.getStringExtra("msg");
        if (msga!=null&&!msga.equals("")){
            if (msga.contains(getResources().getString(R.string.hunsu))){
                radio0.setChecked(true);
            }
            if (msga.contains(getResources().getString(R.string.sushi))){
                radio1.setChecked(true);
            }
            if (msga.contains(getResources().getString(R.string.hunshi))){
                radio2.setChecked(true);
            }
            if (msga.contains(getResources().getString(R.string.shiyan))){
                radio3.setChecked(true);
            }
            if (msga.contains(getResources().getString(R.string.shiyou))){
                radio4.setChecked(true);
            }
            if (msga.contains(getResources().getString(R.string.shitang))){
                radio5.setChecked(true);
            }
        }

    }
    @OnClick(R.id.button1)
    public void onViewClicked() {
        if (radio0.isChecked()) {
            msg = msg + "  "+radio0.getText().toString();
        }
        if (radio1.isChecked()) {
            msg = msg + "  "+radio1.getText().toString();
        }
        if (radio2.isChecked()) {
            msg = msg + "  "+radio2.getText().toString();
        }
        if (radio3.isChecked()) {
            msg = msg + "  "+radio3.getText().toString();
        }
        if (radio4.isChecked()) {
            msg = msg +"  "+ radio4.getText().toString();
        }
        if (radio5.isChecked()) {
            msg = msg + radio5.getText().toString();
        }
        Intent intent=new Intent();
        intent.putExtra("msg",msg);
        setResult(102,intent);
        finish();

    }
    @OnClick(R.id.left_btn)
    public void back() {
        finish();
    }
}
