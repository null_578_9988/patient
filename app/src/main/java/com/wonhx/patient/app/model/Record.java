package com.wonhx.patient.app.model;

import java.io.Serializable;

/**
 * Created by apple on 17/4/5.
 *  呼叫记录
 */
public class Record implements Serializable{
    private String dept_name;
    private String dept_id;
    private String logo_img_path;
    private String name;
    private String id;						//服务id
    private String type;					 //订单类型  1电话服务 2视频服务
    private String status;					//1 通话中 2 未接通 3 呼叫成功 4 呼叫失败 5 医生已解答 6  医生未解答 患者已评价  7 医生已解答  患者已评价
    private String update_date;
    private String phone;
    private String call_times;
    private String order_id;
    private String title;
    private String user_name;
    private String answer;
    private String voice_answer_path;
    private String reply;
    private String reply_time;
    private String score;
    private String create_date;

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getReply_time() {
        return reply_time;
    }

    public void setReply_time(String reply_time) {
        this.reply_time = reply_time;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getVoice_answer_path() {
        return voice_answer_path;
    }

    public void setVoice_answer_path(String voice_answer_path) {
        this.voice_answer_path = voice_answer_path;
    }

    public String getCall_times() {
        return call_times;
    }

    public void setCall_times(String call_times) {
        this.call_times = call_times;
    }

    public String getDept_id() {
        return dept_id;
    }

    public void setDept_id(String dept_id) {
        this.dept_id = dept_id;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogo_img_path() {
        return logo_img_path;
    }

    public void setLogo_img_path(String logo_img_path) {
        this.logo_img_path = logo_img_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdate_date() {
        return update_date;
    }
    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }

}
