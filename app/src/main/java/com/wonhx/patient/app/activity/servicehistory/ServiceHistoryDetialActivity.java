package com.wonhx.patient.app.activity.servicehistory;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.drawee.view.SimpleDraweeView;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.hedgehog.ratingbar.RatingBar;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.firstpage.DoctorDetialActivity;
import com.wonhx.patient.app.activity.firstpage.ImagePagerActivity;
import com.wonhx.patient.app.activity.user.LoginActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.ease.ChartFragment;
import com.wonhx.patient.app.ease.ConversationActivity;
import com.wonhx.patient.app.ease.HistoryChartFragment;
import com.wonhx.patient.app.ease.VideoCallActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.manager.ServiceHistoryManger;
import com.wonhx.patient.app.manager.servicehhistory.ServiceHistoryMangerImal;
import com.wonhx.patient.app.model.Chat_list;
import com.wonhx.patient.app.model.HistoryDetial;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.MedicalBook;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.TabEntity;
import com.wonhx.patient.kit.ChattingRecordsUtils;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.view.LoginDialog;
import com.wonhx.patient.view.VoiceButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

public class ServiceHistoryDetialActivity extends BaseActivity implements OnTabSelectListener {
    @InjectView(R.id.title)
    TextView tv_title;
    ServiceHistoryManger serviceHistoryManger=new ServiceHistoryMangerImal();
    @InjectView(R.id.doctorHead)
    SimpleDraweeView simpleDraweeView;
    @InjectView(R.id.doctor_name)
    TextView tv_name;
    @InjectView(R.id.titles)
    TextView tv_titles;
    @InjectView(R.id.dept_name)
    TextView tv_dept_name;
    @InjectView(R.id.servicetype)
    TextView tv_servicetype;
    @InjectView(R.id.startTime)
    TextView tv_startTime;
    @InjectView(R.id.new_consult)
    TextView tv_new_consult;
    @InjectView(R.id.menutab)
    CommonTabLayout mMenuTab;
    @InjectView(R.id.tab1)
    LinearLayout tab1;
    @InjectView(R.id.voice)
    RelativeLayout rl_voice;
    @InjectView(R.id.zixunhuanzhe)
    TextView zixunhuanzhe;
    @InjectView(R.id.nianling)
    TextView tv_nianling;
    @InjectView(R.id.huanbingshijian)
    TextView tv_huanbingshijian;
    @InjectView(R.id.suohuanjibiing)
    TextView tv_suohuanjibing;
    @InjectView(R.id.bingqingmiaoshu)
    TextView tv_bingqingmiaoshu;
    @InjectView(R.id.jiuyiqingkuang)
    TextView tv_jiuyiqingkuang;
    @InjectView(R.id.yishengjida)
    TextView tv_yishengjieda;
    @InjectView(R.id.yishengweijieda)
    VoiceButton tv_yisheweijieda;
    @InjectView(R.id.ll_pingjia)
    LinearLayout ll_pingjia;
    @InjectView(R.id.patient_name)
    TextView tv_patient_name;
    @InjectView(R.id.tv_pingjia)
    TextView tv_pingjia;
    @InjectView(R.id.pingjiatime)
    TextView tv_pingjiashijian;
    @InjectView(R.id.tab2)
    LinearLayout tab2;
    @InjectView(R.id.tab3)
    LinearLayout tab3;
    @InjectView(R.id.logoa)
    SimpleDraweeView patient_logo;
    @InjectView(R.id.ratingbar1)
    RatingBar ratingBar;
    @InjectView(R.id.gridview)
    GridView gridView_wendang;
    QuickAdapter<MedicalBook>adapter_book;
    List<MedicalBook>lise_book=new ArrayList<>();
    LoginDialog dialog;
    @InjectView(R.id.rl_no)
    RelativeLayout rl_wendang;
    @InjectView(R.id.rl_no_chaat)
    RelativeLayout rl_nochat;
    private String[] mTitles = {"患者信息", "互动详情", "病历文档"};
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    ArrayList<String> imageUrls = new ArrayList<>();
    String age="",content="",conslutionId="",serviceType="",scheduleId="";
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    @Override
    protected void onInitView() {
        super.onInitView();
        conslutionId = getIntent().getStringExtra("orderId");
        content = getIntent().getStringExtra("content");
        serviceType = getIntent().getStringExtra("serviceType");
        scheduleId = getIntent().getStringExtra("scheduleId");
        tv_title.setText("咨询详情");
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i]));
        }
        mMenuTab.setTabData(mTabEntities);
        mMenuTab.setOnTabSelectListener(this);
        mMenuTab.setCurrentTab(0);
        adapter_book=new QuickAdapter<MedicalBook>(this,R.layout.medical_book_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, MedicalBook item) {
                ImageView iv=  helper.getView(R.id.iv);
                if (item.getPath()!=null&&!item.getPath().equals("")) {
                    Glide.with(ServiceHistoryDetialActivity.this).load(Constants.REST_ORGIN + "/emedicine" + item.getPath()).centerCrop().into(iv);
                }
            }
        };
        gridView_wendang.setAdapter(adapter_book);
    }
    @Override
    protected void onInitData() {
        super.onInitData();
        //获取详情
        getdetial();
        //获取病例文档
        getmedicalbook();
        //获取聊天记录
        getchatlist();
    }

    private void getchatlist() {
        serviceHistoryManger.getchatlist(conslutionId,new SubscriberAdapter<ListProResult<Chat_list>>(){
            @Override
            public void success(ListProResult<Chat_list> chat_list) {
                super.success(chat_list);
                rl_nochat.setVisibility(View.GONE);
                findViewById(R.id.cartcontainer).setVisibility(View.VISIBLE);
                List<Chat_list> listDatas = new ArrayList<Chat_list>();
                listDatas.addAll(chat_list.getData());
                ArrayList<EMMessage> mMessageList = new ArrayList<EMMessage>();
                if (listDatas.size()>0){
                    for (Chat_list chatMsg : listDatas){
                        mMessageList.add(ChattingRecordsUtils.chattingRecordsData(chatMsg));
                    }
                }
                //反序
                Collections.reverse(mMessageList);
                //聊天人或群id
                String toChatUsername = getIntent().getStringExtra(EaseConstant.EXTRA_USER_ID);
                HistoryChartFragment chatFragment = new HistoryChartFragment();
                //传入参数
                Bundle args = new Bundle();
                args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                if (mMessageList.get(0).getFrom().equals(EMClient.getInstance().getCurrentUser())){
                    args.putString(EaseConstant.EXTRA_USER_ID,mMessageList.get(0).getTo());
                }else{
                    args.putString(EaseConstant.EXTRA_USER_ID,mMessageList.get(0).getFrom());
                }
                args.putString("DT_RowId", conslutionId);
                args.putString("DOC_NAME","医生");
                args.putParcelableArrayList("MESSAGES",mMessageList);
                args.putBoolean("HISTORY",true);
                chatFragment.setArguments(args);
                getSupportFragmentManager().beginTransaction().add(R.id.cartcontainer, chatFragment).commit();
            }

            @Override
            public void onError(Throwable e) {
                //super.onError(e);
                dismissLoadingDialog();
                findViewById(R.id.cartcontainer).setVisibility(View.GONE);
                rl_nochat.setVisibility(View.VISIBLE);

            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_history_detial);
       // Log.e("id",getIntent().getStringExtra("id"));
    }
    @OnClick(R.id.left_btn)
    void  back(){
        finish();
    }
    @Override
    public void onTabSelect(int position) {
        if (position==0){
            tab1.setVisibility(View.VISIBLE);
            tab2.setVisibility(View.GONE);
            tab3.setVisibility(View.GONE);
        }else if (position==1){
            tab1.setVisibility(View.GONE);
            tab2.setVisibility(View.VISIBLE);
            tab3.setVisibility(View.GONE);
        }else if (position==2){
            tab1.setVisibility(View.GONE);
            tab2.setVisibility(View.GONE);
            tab3.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onTabReselect(int position) {
    }
    private void getdetial() {
        serviceHistoryManger.getServicelistdetial(conslutionId,new SubscriberAdapter<ProResult<HistoryDetial>>(){
            @Override
            public void success(final ProResult<HistoryDetial> historyDetialProResult) {
                super.success(historyDetialProResult);
                if (historyDetialProResult.getData().getLogo_img_path()!=null&&!historyDetialProResult.getData().getLogo_img_path().equals("")){
                    simpleDraweeView.setImageURI(Uri.parse(Constants.REST_ORGIN+"/emedicine"+historyDetialProResult.getData().getLogo_img_path()));
                }
                tv_name.setText(historyDetialProResult.getData().getName());
                tv_titles.setText(historyDetialProResult.getData().getTitle());
                tv_dept_name.setText(historyDetialProResult.getData().getDept_name());
                tv_servicetype.setText(historyDetialProResult.getData().getServiceType());
                tv_startTime.setText(historyDetialProResult.getData().getStartTime());
                if(content.equals("查看详情")){
                    tv_new_consult.setText("再次咨询");
                }else {
                    tv_new_consult.setText(content);
                }
                if (historyDetialProResult.getData().getStatus().equals("已购买")){
                    ll_pingjia.setVisibility(View.GONE);
                }else if (historyDetialProResult.getData().getStatus().equals("已解答")){
                    ll_pingjia.setVisibility(View.GONE);
                }else if (historyDetialProResult.getData().getStatus().equals("已结束")||historyDetialProResult.getData().getStatus().equals("已退款")||historyDetialProResult.getData().getStatus().equals("已超时")){
                    ll_pingjia.setVisibility(View.VISIBLE);
                }
                //立即咨询 查看详情 立即评价点击事件
                tv_new_consult.setOnClickListener(new View.OnClickListener() {
                    //服务记录咨询状态 1待付费 2已购买 3已解答 4已结束 5 已退款 6 已超时 7 已取消
                    @Override
                    public void onClick(View view) {
                              firstPagerMager.getDoctorEMCName(historyDetialProResult.getData().getDoctorMemberId(),new SubscriberAdapter<Result>(){
                                  @Override
                                  public void success(Result result) {
                                      super.success(result);
                                      //图文的时候的立即咨询
                                      if (content.equals("立即咨询")){
                                          if (serviceType.equals("2")) {
                                              Bundle args = new Bundle();
                                              args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                                              args.putString(EaseConstant.EXTRA_USER_ID, result.getHuanxin_username());
                                              args.putString("DT_RowId", conslutionId);
                                              args.putString("DOC_NAME", historyDetialProResult.getData().getName());
                                              UIKit.open(ServiceHistoryDetialActivity.this, ConversationActivity.class, args);
                                              //电话的时候的立即咨询
                                          }else if (serviceType.equals("2")){

                                              //视频的时候的立即咨询
                                          }else if (serviceType.equals("4")){
                                              Bundle bundle = new Bundle();
                                              bundle.putString("username", result.getHuanxin_username());
                                              bundle.putBoolean("isComingCall", false);
                                              bundle.putString("req_id",conslutionId);
                                              bundle.putString("member_id",historyDetialProResult.getData().getDoctorMemberId());
                                              bundle.putString("schedule_id",scheduleId);
                                              UIKit.open(ServiceHistoryDetialActivity.this, VideoCallActivity.class,bundle);
                                          }
                                      }else if (content.equals("去评价")){
                                          Bundle bundle = new Bundle();
                                          bundle.putString("service_id", conslutionId);
                                          UIKit.open(ServiceHistoryDetialActivity.this, EvaluateDoctorActivity.class, bundle);
                                      }else if (content.equals("等待解答")){
                                          dialog(2);
                                      }else if (content.equals("等待咨询")){
                                          //电话的时候的等待咨询
                                          if (serviceType.equals("3")){
                                              dialog(7);
                                              //视频的时候的等待咨询
                                          }else if (serviceType.equals("4")){
                                              dialog(3);
                                          }
                                      }else if (content.equals("已退款")){
                                          //电话的时候的退款
                                          if (serviceType.equals("3")){
                                              dialog(8);
                                              //视频的时候的退款
                                          }else if (serviceType.equals("4")){
                                              dialog(4);
                                          }
                                      }else if (content.equals("已超时")){
                                          dialog(5);
                                      }else if (content.equals("已取消")){
                                          dialog(6);
                                      }else if (content.equals("再次咨询")||content.equals("查看详情")){
                                          //继续咨询
                                          Intent in=new Intent(ServiceHistoryDetialActivity.this, DoctorDetialActivity.class);
                                          in.putExtra("member_id",historyDetialProResult.getData().getDoctorMemberId());
                                          startActivity(in);
                                      }else if (content.equals("点击退款")){
                                          serviceHistoryManger.tuikuan(conslutionId,new SubscriberAdapter<Result>(){
                                              @Override
                                              public void success(Result result) {
                                                  super.success(result);
                                                  Toaster.showShort(ServiceHistoryDetialActivity.this,result.getMsg());
                                                  tv_new_consult.setText("再次咨询");
                                              }
                                          });
                                      }
                                  }
                              });
                    }
                });
                if (historyDetialProResult.getData().getBirthday()!=null&&!historyDetialProResult.getData().getBirthday().equals("")) {
                    age = historyDetialProResult.getData().getBirthday().substring(0, 4);
                    Calendar a=Calendar.getInstance();
                    int ages=a.get(Calendar.YEAR);
                    int agea=ages-Integer.parseInt(age);
                    if (agea==ages){
                        tv_nianling.setText("  年龄              "+"您还未设置出生日期");
                    }else if (agea>=0){
                        tv_nianling.setText("  年龄              "+agea);
                    }else  if (agea<0){
                        tv_nianling.setText("  年龄              "+"您设置出生日期有误！");
                    }
                }else {
                       tv_nianling.setText("   年龄             您还未设置出生日期");
                }
                zixunhuanzhe.setText("  咨询患者      "+historyDetialProResult.getData().getPatientName());
                tv_huanbingshijian.setText("  患病时间      "+historyDetialProResult.getData().getHistoryTime());
                tv_suohuanjibing.setText("  所患疾病      "+historyDetialProResult.getData().getMedical());
                tv_bingqingmiaoshu.setText("  病情描述      "+historyDetialProResult.getData().getContent());
                tv_jiuyiqingkuang.setText("  就医情况      "+historyDetialProResult.getData().getHospitalHistory());
                tv_yishengjieda.setText("  医生解答      " + historyDetialProResult.getData().getReply());
                tv_patient_name.setText(historyDetialProResult.getData().getPatientName());
                tv_pingjia.setText(historyDetialProResult.getData().getReplyAppraise());
                tv_pingjiashijian.setText(historyDetialProResult.getData().getReplyDate());
                patient_logo.setImageResource(R.mipmap.offhead);
                ratingBar.setStar(Float.parseFloat(historyDetialProResult.getData().getReplyScore()));
                //显示语音的控件
                if (historyDetialProResult.getData().getAudioPath()!=null&&!historyDetialProResult.getData().getAudioPath().equals("")){
                    tv_yisheweijieda.setPlayPath(Constants.REST_ORGIN+"/emedicine"+historyDetialProResult.getData().getAudioPath());
                    rl_voice.setVisibility(View.VISIBLE);

                }else {
                    rl_voice.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void getmedicalbook() {
        serviceHistoryManger.getmedicalbook(conslutionId,new SubscriberAdapter<ListProResult<MedicalBook>>(){
            @Override
            public void onError(Throwable e) {
               // super.onError(e);
                rl_wendang.setVisibility(View.VISIBLE);
                gridView_wendang.setVisibility(View.GONE);
                dismissLoadingDialog();
            }

            @Override
            public void success(ListProResult<MedicalBook> medicalBookListProResult) {
                super.success(medicalBookListProResult);
                gridView_wendang.setVisibility(View.VISIBLE);
                rl_wendang.setVisibility(View.GONE);
                lise_book.addAll(medicalBookListProResult.getData());
                for (int i = 0; i <lise_book.size() ; i++) {
                    imageUrls.add(Constants.REST_ORGIN+"/emedicine"+lise_book.get(i).getPath());
                }
                if (lise_book.size()>0){
                    adapter_book.addAll(lise_book);
                }
                gridView_wendang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        //查看大图
                        Bundle bundle = new Bundle();
                        bundle.putStringArrayList("image_urls", imageUrls);
                        bundle.putInt("image_index", i);
                        UIKit.open(ServiceHistoryDetialActivity.this, ImagePagerActivity.class, bundle);
                    }
                });
            }
        });
    }
    // 弹窗
    private void dialog(final int a) {
        dialog= new LoginDialog(ServiceHistoryDetialActivity.this);
         if (a==2){
            dialog.setContent("本次咨询服务已结束，请您耐心等待解答，谢谢使用！");
        }else if (a==3){
            dialog.setContent("还未到预约时间，请耐心等待！");
        }else if (a==4){
            dialog.setContent("在视频咨询预约时间内您未进行咨询，系统将会自动退回部分咨询款项到您的账户中，请耐心等待。");
        }else if (a==5){
            dialog.setContent("此服务已超时");
        }else if (a==8){
            dialog.setContent("由于您在本次电话咨询服务中未接听咨询电话，系统将会扣除部分款项，剩余部分将会退回您的账户中，谢谢使用。");
        }else if (a==6){
            dialog.setContent("此服务已取消");
        }else if (a==7){
            dialog.setContent("还未到您与医生预约的时间，请耐心等待！到达预约时间后请注意接听号码010- 88888888（号码需要固定） 的来电，接听此号码后请耐心等待医生接听！");
        }
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    dialog.dismiss();
            }
        });
        dialog.show();
    }

}
