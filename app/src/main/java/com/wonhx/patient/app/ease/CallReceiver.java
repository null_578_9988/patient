package com.wonhx.patient.app.ease;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by apple on 17/3/27.
 * 呼入监听
 */
public class CallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // 拨打方username
        String from = intent.getStringExtra("from");
        // call type
        String type = intent.getStringExtra("type");
        //跳转到通话页面
        intent = new Intent();
        intent.setClass(context.getApplicationContext(), VideoCallActivity.class);
        intent.putExtra("username", from);
        intent.putExtra("isComingCall", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(intent);
    }
}
