package com.wonhx.patient.app.activity.firstpage;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.fragment.doctorandhospital.FamousDoctorFragment;
import com.wonhx.patient.app.fragment.doctorandhospital.FamousHospitalFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class Meet_doctorActivity extends BaseActivity {
    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.tv_mingyi)
    TextView tvMingyi;
    @InjectView(R.id.iv_mingyi)
    ImageView ivMingyi;
    @InjectView(R.id.ll_mingyi)
    LinearLayout llMingyi;
    @InjectView(R.id.tv_mingyuan)
    TextView tvMingyuan;
    @InjectView(R.id.iv_mingyuan)
    ImageView ivMingyuan;
    @InjectView(R.id.ll_mingyuan)
    LinearLayout llMingyuan;
    @InjectView(R.id.vp)
    ViewPager vp;
    List<Fragment>list=new ArrayList<>();
    FamousHospitalFragment famousHospitalFragment;
    FamousDoctorFragment famousDoctorFragment;
    FragmentManager fa;
    myFragmentPagerAdapter myFragmentPagerAdapter;

    @Override
    protected void onInitView() {
        super.onInitView();
        title.setText("名医");
        tvMingyi.setTextColor(getResources().getColor(R.color.colorAccent));
        ivMingyi.setVisibility(View.VISIBLE);
        tvMingyuan.setTextColor(Color.BLACK);
        ivMingyuan.setVisibility(View.INVISIBLE);
        famousDoctorFragment=new FamousDoctorFragment();
        famousHospitalFragment=new FamousHospitalFragment();
        list.add(famousDoctorFragment);
        list.add(famousHospitalFragment);
        fa=getSupportFragmentManager();
        myFragmentPagerAdapter=new myFragmentPagerAdapter(fa,list);
        vp.setAdapter(myFragmentPagerAdapter);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meet_doctor);
        ButterKnife.inject(this);
    }

    @OnClick({R.id.left_btn, R.id.ll_mingyi, R.id.ll_mingyuan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.ll_mingyi:
                title.setText("名医");
                vp.setCurrentItem(0);
                tvMingyi.setTextColor(getResources().getColor(R.color.colorAccent));
                ivMingyi.setVisibility(View.VISIBLE);
                tvMingyuan.setTextColor(Color.BLACK);
                ivMingyuan.setVisibility(View.INVISIBLE);
                break;
            case R.id.ll_mingyuan:
                title.setText("名院");
                vp.setCurrentItem(1);
                tvMingyuan.setTextColor(getResources().getColor(R.color.colorAccent));
                ivMingyuan.setVisibility(View.VISIBLE);
                tvMingyi.setTextColor(Color.BLACK);
                ivMingyi.setVisibility(View.INVISIBLE);
                break;
        }
    }
   class myFragmentPagerAdapter extends FragmentPagerAdapter {

       private FragmentManager fragmetnmanager;  //创建FragmentManager
       private List<Fragment> listfragment; //创建一个List<Fragment>

       public myFragmentPagerAdapter(FragmentManager fm, List<Fragment> list) {
           super(fm);
           this.fragmetnmanager = fm;
           this.listfragment = list;
       }

       @Override
       public Fragment getItem(int arg0) {
           // TODO Auto-generated method stub
           return listfragment.get(arg0); //返回第几个fragment
       }

       @Override
       public int getCount() {
           // TODO Auto-generated method stub
           return listfragment.size(); //总共有多少个fragment
       }
   }

    }

