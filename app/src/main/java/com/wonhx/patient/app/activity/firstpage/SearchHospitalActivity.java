package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.FamilyDoctorSearchList;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.MoveHospital;
import com.wonhx.patient.app.model.SearchHospital;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by nsd on 2017/5/19.
 *
 */

public class SearchHospitalActivity extends BaseActivity {
    @InjectView(R.id.listview)
    ListView mListView;
    @InjectView(R.id.keyword)
    EditText mKeyWord;
    QuickAdapter<SearchHospital> mListAdapter;
    List<SearchHospital> mListData = new ArrayList<>();
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    int start=0;
    String length="10";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_hospital);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mListAdapter = new QuickAdapter<SearchHospital>(this, R.layout.listitem_select) {
            @Override
            protected void convert(BaseAdapterHelper helper, SearchHospital item) {
                helper.setText(R.id.content, item.getName());
            }
        };
        mListView.setAdapter(mListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SearchHospital moveHospital = mListAdapter.getItem(i);
                Bundle bundle = new Bundle();
                bundle.putString("CLINICID", moveHospital.getId());
                UIKit.open(SearchHospitalActivity.this, HospitalDetailActivity.class, bundle);
            }
        });
        mKeyWord.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
                    // 先隐藏键盘
                    ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                            .hideSoftInputFromWindow(SearchHospitalActivity.this.getCurrentFocus()
                                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    //进行搜索操作的方法，在该方法中可以加入mEditSearchUser的非空判断
                    searchHospital(mKeyWord.getText().toString());
                }
                return false;
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SearchHospital hospital = mListAdapter.getItem(position);
                Bundle bundle = new Bundle();
                bundle.putString("CLINICID",hospital.getId());
                UIKit.open(SearchHospitalActivity.this,HospitalDetailActivity.class,bundle);
            }
        });
    }

    /**
     * 搜索医院
     *
     * @param keyword
     */
    private void searchHospital(String keyword) {
        firstPagerMager.searchClinic(keyword, new SubscriberAdapter<ListProResult<SearchHospital>>() {
            @Override
            public void onError(Throwable e) {
                super.onError(e);
                dismissLoadingDialog();
                mListData.clear();
                mListAdapter.notifyDataSetChanged();
                Toaster.showShort(SearchHospitalActivity.this, e.getMessage());
            }

            @Override
            public void success(ListProResult<SearchHospital> result) {
                super.success(result);
                    mListData.clear();
                    mListData.addAll(result.getData());
                    if (mListData.size() > 0) {
                        mListAdapter.addAll(mListData);
                    }
            }
        });
    }

    @OnClick(R.id.back)
    void back() {
        finish();
    }
}
