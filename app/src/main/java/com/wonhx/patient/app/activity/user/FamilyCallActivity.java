package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.Toaster;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FamilyCallActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.right_btn)
    TextView rightBtn;
    @InjectView(R.id.et_message)
    EditText etMessage;
    UserManager userManager=new UserManagerImpl();
String orderid="",member_id="";
    @Override
    protected void onInitData() {
        super.onInitData();
        title.setText("电话回拨");
        rightBtn.setText("申请");
        Intent intent=getIntent();
        orderid=intent.getStringExtra("orderid");
        member_id=intent.getStringExtra("member_id");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_call);
        ButterKnife.inject(this);
    }

    @OnClick({R.id.left_btn, R.id.right_btn, R.id.search_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.right_btn:
               userManager.familycallcontent(member_id,orderid,etMessage.getText().toString().trim(),new SubscriberAdapter<Result>(){
                   @Override
                   public void success(Result result) {
                       super.success(result);
                       Toaster.showShort(FamilyCallActivity.this,result.getMsg());
                       finish();
                   }
               });
                break;

        }
    }
}
