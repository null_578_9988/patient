package com.wonhx.patient.app.model;

import com.wonhx.patient.db.annotation.Table;

/**
 * 用户信息
 */
@Table(version = 1)
public class UserInfo extends BaseModel<UserInfo> {
    /**
     * 用户ID
     */
    public String  member_id;
    /**
     *  用户名称
     */
    public String  username;
    /**
     * 用户电话
     */
    public String  phone;
    /**
     * 环信用户名
     */
    public String  huanxin_username;
    /**
     * 环信用户密码
     */
    public String  huanxin_pw;
    /**
     * 用户头像
     */
    public String  avater;
    /**
     * 用户地址
     */
    public String address;
    /**
     * 性别
     */
    public String sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvater() {
        return avater;
    }

    public void setAvater(String avater) {
        this.avater = avater;
    }

    public String getHuanxin_pw() {
        return huanxin_pw;
    }

    public void setHuanxin_pw(String huanxin_pw) {
        this.huanxin_pw = huanxin_pw;
    }

    public String getHuanxin_username() {
        return huanxin_username;
    }

    public void setHuanxin_username(String huanxin_username) {
        this.huanxin_username = huanxin_username;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
        setId(Integer.parseInt(member_id));
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
