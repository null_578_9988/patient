package com.wonhx.patient.app.activity.calldoctor;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.CallDoctorManager;
import com.wonhx.patient.app.manager.calldoctor.CallDoctorManagerImpl;
import com.wonhx.patient.app.model.CodeData;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/4/6.
 * 忘记密码页面
 */
public class CallForgetActivity extends BaseActivity {
    @InjectView(R.id.edit_user)
    EditText mUserName;
    @InjectView(R.id.edit_verification)
    EditText mUserCode;
    @InjectView(R.id.btn_next)
    Button mNext;
    @InjectView(R.id.llayout_forget1)
    LinearLayout mCodeLayout;
    @InjectView(R.id.llayout_forget2)
    LinearLayout mPwdLayout;
    @InjectView(R.id.llayout_user_cancel)
    LinearLayout mUserCancle;
    @InjectView(R.id.llayout_pass_cancel)
    LinearLayout mPwdCancle;
    @InjectView(R.id.txt_get_verification)
    TextView mGetCode;
    @InjectView(R.id.edit_pass)
    EditText mNewPwd;
    @InjectView(R.id.btn_submit)
    Button mSubmit;
    CallDoctorManager callDoctorManager = new CallDoctorManagerImpl();
    TimeCount time;
    CodeData codeData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_forget);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (StrKit.isBlank(mUserName.getText().toString().trim())) {
                    mUserCancle.setVisibility(View.GONE);
                } else {
                    mUserCancle.setVisibility(View.VISIBLE);
                }
                if (!StrKit.isBlank(mUserName.getText().toString().trim()) && !StrKit.isBlank(mUserCode.getText().toString().trim())) {
                    mNext.setTextColor(CallForgetActivity.this.getResources().getColor(R.color.hj_txt_white));
                    mNext.setEnabled(true);
                } else {
                    mNext.setTextColor(CallForgetActivity.this.getResources().getColor(R.color.hj_txt_white33));
                    mNext.setEnabled(false);
                }
            }
        });
        mUserCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(mUserName.getText().toString().trim()) && !StrKit.isBlank(mUserCode.getText().toString().trim())) {
                    mNext.setTextColor(CallForgetActivity.this.getResources().getColor(R.color.hj_txt_white));
                    mNext.setEnabled(true);
                } else {
                    mNext.setTextColor(CallForgetActivity.this.getResources().getColor(R.color.hj_txt_white33));
                    mNext.setEnabled(false);
                }
            }
        });
        mNewPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (StrKit.isBlank(mNewPwd.getText().toString().trim())) {
                    mSubmit.setTextColor(CallForgetActivity.this.getResources().getColor(R.color.hj_txt_white33));
                    mPwdCancle.setVisibility(View.GONE);
                    mSubmit.setEnabled(false);
                } else {
                    mSubmit.setTextColor(CallForgetActivity.this.getResources().getColor(R.color.hj_txt_white));
                    mPwdCancle.setVisibility(View.VISIBLE);
                    mSubmit.setEnabled(true);
                }
            }
        });
    }

    @OnClick(R.id.txt_return)
    void back() {
        finish();
    }

    @OnClick(R.id.llayout_user_cancel)
    void clearUser() {
        mUserName.setText("");
    }

    @OnClick(R.id.llayout_pass_cancel)
    void clearPass() {
        mNewPwd.setText("");
    }

    @OnClick(R.id.txt_get_verification)
    void getCode() {
        if (StrKit.isBlank(mUserName.getText().toString().trim())) {
            Toaster.showShort(CallForgetActivity.this, "帐号不能为空！");
            return;
        }
        callDoctorManager.getPhoneCode(mUserName.getText().toString().trim(), new SubscriberAdapter<CodeData>() {
            @Override
            public void success(CodeData codeDataProResult) {
                super.success(codeDataProResult);
                codeData = codeDataProResult;
                time = new TimeCount(60000, 1000);
                time.start();
                Toaster.showShort(CallForgetActivity.this, codeDataProResult.getMsg());
            }
        });
    }

    @OnClick(R.id.btn_next)
    void next() {
        if (StrKit.isBlank(mUserName.getText().toString().trim())) {
            Toaster.showShort(CallForgetActivity.this, "帐号不能为空！");
            return;
        }
        if (StrKit.isBlank(mUserCode.getText().toString().trim())) {
            Toaster.showShort(CallForgetActivity.this, "验证码不能为空！");
            return;
        }
        if (!mUserCode.getText().toString().trim().equals(codeData.getContent())) {
            Toaster.showShort(CallForgetActivity.this, "验证码不正确！");
            return;
        } else {
            mCodeLayout.setVisibility(View.GONE);
            mPwdLayout.setVisibility(View.VISIBLE);
        }
    }
    @OnClick(R.id.btn_submit)
    void submit(){
        if (StrKit.isBlank(mNewPwd.getText().toString().trim())) {
            Toaster.showShort(CallForgetActivity.this, "密码不能为空！");
            return;
        }
        callDoctorManager.newPwd(mUserName.getText().toString().trim(),codeData.getContent(),mNewPwd.getText().toString().trim(),codeData.getPhone(),new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                Toaster.showShort(CallForgetActivity.this, result.getMsg());
                finish();
            }
        });

    }

    //倒计时
    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            mGetCode.setClickable(false);
            mGetCode.setText(String.valueOf(millisUntilFinished / 1000));
            mGetCode.setTextColor(getResources().getColor(R.color.text_line_one));
        }

        @Override
        public void onFinish() {
            mGetCode.setText("获取验证码");
            mGetCode.setClickable(true);
            mGetCode.setTextColor(getResources().getColor(R.color.hj_txt_reddb));
        }
    }
}
