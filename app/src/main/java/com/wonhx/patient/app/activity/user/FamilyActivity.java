package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.kit.Toaster;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FamilyActivity extends BaseActivity {
    @InjectView(R.id.radio0)
    CheckBox radio0;
    @InjectView(R.id.radio1)
    CheckBox radio1;
    @InjectView(R.id.radio2)
    CheckBox radio2;
    @InjectView(R.id.radio3)
    CheckBox radio3;
    @InjectView(R.id.radio4)
    CheckBox radio4;
    @InjectView(R.id.radio5)
    CheckBox radio5;
    @InjectView(R.id.radio6)
    CheckBox radio6;
    @InjectView(R.id.radio7)
    CheckBox radio7;
    @InjectView(R.id.radio8)
    CheckBox radio8;
    @InjectView(R.id.radio9)
    CheckBox radio9;
    @InjectView(R.id.radio10)
    CheckBox radio10;
    @InjectView(R.id.radio12)
    CheckBox radio12;
   String msga;
    List<String >list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);
        ButterKnife.inject(this);
        Intent in=getIntent();
        msga=in.getStringExtra("msg");
        if (msga!=null&&!msga.equals("")){
            if (msga.contains(getResources().getString(R.string.wubingshi))){
                radio0.setChecked(true);
                list.add(getResources().getString(R.string.wubingshi));
            }
            if (msga.contains(getResources().getString(R.string.gaoxueya))){
                radio1.setChecked(true);
                list.add(getResources().getString(R.string.gaoxueya));
            }
            if (msga.contains(getResources().getString(R.string.tangniaobing))){
                radio2.setChecked(true);
                list.add(getResources().getString(R.string.tangniaobing));
            }
            if (msga.contains(getResources().getString(R.string.guanxinbing))){
                radio3.setChecked(true);
                list.add(getResources().getString(R.string.guanxinbing));
            }
            if (msga.contains(getResources().getString(R.string.manxing))){
                radio4.setChecked(true);
                list.add(getResources().getString(R.string.manxing));
            }
            if (msga.contains(getResources().getString(R.string.exin))){
                radio5.setChecked(true);
                list.add(getResources().getString(R.string.exin));
            }
            if (msga.contains(getResources().getString(R.string.naozu))){
                radio6.setChecked(true);
                list.add(getResources().getString(R.string.naozu));
            }
            if (msga.contains(getResources().getString(R.string.zhongxing))){
                radio7.setChecked(true);
                list.add(getResources().getString(R.string.zhongxing));
            }
            if (msga.contains(getResources().getString(R.string.jiehe))){
                radio8.setChecked(true);
                list.add(getResources().getString(R.string.jiehe));
            }
            if (msga.contains(getResources().getString(R.string.ganyan))){
                radio9.setChecked(true);
                list.add(getResources().getString(R.string.ganyan));
            }
            if (msga.contains(getResources().getString(R.string.jixing))){
                radio10.setChecked(true);
                list.add(getResources().getString(R.string.jixing));
            }
            if (msga.contains(getResources().getString(R.string.qitaa))){
                radio12.setChecked(true);
                list.add(getResources().getString(R.string.qitaa));
            }

        }
        radio0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio4.setChecked(false);
                    radio3.setChecked(false);
                    radio2.setChecked(false);
                    radio1.setChecked(false);
                    radio5.setChecked(false);
                    radio6.setChecked(false);
                    radio7.setChecked(false);
                    radio8.setChecked(false);
                    radio9.setChecked(false);
                    radio10.setChecked(false);
                    radio12.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).contains(radio0.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio0.getText().toString());
                }else {
                    list.remove(radio0.getText().toString());
                }
            }
        });
        radio1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio1.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio1.getText().toString());
                }else {
                    list.remove(radio1.getText().toString());
                }
            }
        });
        radio2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio2.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio2.getText().toString());
                }else {
                    list.remove(radio2.getText().toString());
                }
            }
        });
        radio3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio3.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio3.getText().toString());
                }else {
                    list.remove(radio3.getText().toString());
                }
            }
        });
        radio4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio4.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio4.getText().toString());
                }else {
                    list.remove(radio4.getText().toString());
                }
            }
        });
        radio5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio5.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio5.getText().toString());
                }else {
                    list.remove(radio5.getText().toString());
                }
            }
        });
        radio6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio6.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio6.getText().toString());
                }else {
                    list.remove(radio6.getText().toString());
                }
            }
        });
        radio7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio7.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio7.getText().toString());
                }else {
                    list.remove(radio7.getText().toString());
                }
            }
        });
        radio8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio8.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio8.getText().toString());
                }else {
                    list.remove(radio8.getText().toString());
                }
            }
        });
        radio9.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio9.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio9.getText().toString());
                }else {
                    list.remove(radio9.getText().toString());
                }
            }
        });
        radio10.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio10.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio10.getText().toString());
                }else {
                    list.remove(radio10.getText().toString());
                }
            }
        });
        radio12.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无病史")||list.get(i).contains(radio12.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio12.getText().toString());
                }else {
                    list.remove(radio12.getText().toString());
                }
            }
        });
    }

    @OnClick({R.id.left_btn, R.id.button1})
    public void onViewClicked(View view) {
        String msg="";
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.button1:
                if (list.size()>0) {
                    StringBuffer sb = new StringBuffer();
                    for (String item : list) {
                        sb.append("  "+item);
                    }
                    msg = sb.toString();
                    Intent intent=new Intent();
                    intent.putExtra("msg",msg);
                    setResult(304,intent);
                    finish();
                }else {
                    Toaster.showShort(FamilyActivity.this,"选择内容为空");
                }

                break;
        }
    }
}
