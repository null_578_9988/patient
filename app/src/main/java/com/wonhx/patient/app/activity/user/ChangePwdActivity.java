package com.wonhx.patient.app.activity.user;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.Toaster;

import butterknife.InjectView;
import butterknife.OnClick;

public class ChangePwdActivity extends BaseActivity {
    @InjectView(R.id.title)
    TextView tv_title;
    @InjectView(R.id.oldPwd)
    EditText ed_old;
    @InjectView(R.id.newPwdText)
    EditText ed_new;
    @InjectView(R.id.newPwdConfirmText)
    EditText ed_confim;
    String oldpassword,password,confirmPwd;
    UserManager userManager=new UserManagerImpl();

    @Override
    protected void onInitView() {
        super.onInitView();
        tv_title.setText("修改密码");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pwd);
    }
    @OnClick(R.id.left_btn)
    void back(){
        finish();
    }
    @OnClick(R.id.changePwd_btn)
    void commit(){
        oldpassword = ed_old.getText().toString().trim();
        password = ed_new.getText().toString().trim();
        confirmPwd = ed_confim.getText().toString().trim();
        if (oldpassword == null || oldpassword.length() <= 0) {
            Toaster.showShort(ChangePwdActivity.this, "旧密码不能为空");
            return;
        }
        if (password == null || password.length() < 6) {
            Toaster.showShort(ChangePwdActivity.this, "新密码至少为6位");
            return;
        }
        if (confirmPwd == null || confirmPwd.length() < 6) {
            Toaster.showShort(ChangePwdActivity.this, "确认密码至少为6位");
            return;
        }
        if (!confirmPwd.equals(password)) {
            // TODO Auto-generated method stub
            Toaster.showShort(ChangePwdActivity.this, "请确认两次新密码必须相同");
            return;

        }
        userManager.change_pwd(AbSharedUtil.getString(ChangePwdActivity.this, "userId"),oldpassword,password,new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                if (result.getCode()){
                    Toaster.showShort(ChangePwdActivity.this,result.getMsg());
                    finish();
                }else {
                    Toaster.showShort(ChangePwdActivity.this,result.getMsg());
                }
            }
        });
    }
}
