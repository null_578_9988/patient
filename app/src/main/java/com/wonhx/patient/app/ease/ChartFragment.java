package com.wonhx.patient.app.ease;

import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;

import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.adapter.ChatRowCard;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.hyphenate.easeui.ui.EaseChatFragment.EaseChatFragmentHelper;
import com.hyphenate.easeui.widget.chatrow.EaseChatRow;
import com.hyphenate.easeui.widget.chatrow.EaseCustomChatRowProvider;
import com.hyphenate.exceptions.HyphenateException;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.firstpage.DoctorDetialActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.EaseCard;
import com.wonhx.patient.app.model.UserInfo;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.DateKit;
import com.wonhx.patient.kit.GsonKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.HttpMethod;
import org.xutils.http.RequestParams;
import org.xutils.x;

/**
 * Created by nsd on 2017/4/20
 *
 */

public class ChartFragment extends EaseChatFragment implements EaseChatFragmentHelper {
    private String conslutionId; // 订单id 普通咨询服务
    private String clinicId;//移动诊所的服务
    private String homeOrderId;//家庭医生的服务
    private String endTime;//家庭医生的到期时间
    JSONObject extJson;
    private static final int MINGPIAN = 92;
    private static final int REQUEST_CODE_SELECT_MINGPIAN = 99;
    private static final int MESSAGE_TYPE_RECV_MINGPIAN_CALL = 5;
    private static final int MESSAGE_TYPE_SEND_MINGPIAN_CALL = 6;
    FirstPagerMager firstPagerMager=new FirstPagerMangerImal();
    //重写发送消息的方法
    protected void send(EMMessage message){
        if (message == null) {
            return;
        }
        if(chatFragmentHelper != null){
            //set extension
            chatFragmentHelper.onSetMessageAttributes(message);
        }
        if (chatType == EaseConstant.CHATTYPE_GROUP){
            message.setChatType(EMMessage.ChatType.GroupChat);
        }else if(chatType == EaseConstant.CHATTYPE_CHATROOM){
            message.setChatType(EMMessage.ChatType.ChatRoom);
        }
        //send message
        EMClient.getInstance().chatManager().sendMessage(message);
        //refresh ui
        if(true) {
            messageList.refreshSelectLast();
        }
    }

    @Override
    protected void sendMessage(final EMMessage message) {
        if (clinicId!=null){
                super.sendMessage(message);
        }else if (conslutionId!=null){
            RequestParams params=new RequestParams(Constants.STATUS);
            params.addBodyParameter("request_id",conslutionId);
            x.http().request(HttpMethod.POST, params, new Callback.CommonCallback<String>() {
                @Override
                public void onSuccess(String result) {
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equals("0")) {
                            String date = jsonObject.getString("data");
                            if (Integer.parseInt(date) > 2) {
                                Toaster.showShort(getActivity(), "此服务已到期，不能继续咨询！");
                            } else {
                                //继续咨询
                                send(message);
                            }
                        }else {
                            Toaster.showShort(getActivity(), jsonObject.getString("msg"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(Throwable ex, boolean isOnCallback) {

                }

                @Override
                public void onCancelled(CancelledException cex) {

                }

                @Override
                public void onFinished() {

                }
            });

        }
        else if (homeOrderId!=null&&endTime!=null){
            RequestParams params=new RequestParams(Constants.SYS_TIME);
            x.http().get(params, new Callback.CacheCallback<String>() {
                @Override
                public void onSuccess(String result) {
                    try {
                        JSONObject jsonObject=new JSONObject(result);
                        String date=jsonObject.getString("data");
                        if (DateKit.compare_date(date,endTime)>0){
                            Toaster.showShort(getActivity(),"此服务已到期，不能继续咨询！");
                        }else {
                            //继续咨询
                            send(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(Throwable ex, boolean isOnCallback) {

                }

                @Override
                public void onCancelled(CancelledException cex) {

                }

                @Override
                public void onFinished() {

                }

                @Override
                public boolean onCache(String result) {
                    return false;
                }
            });


        }
    }

    private static final int ITEM_RED_PACKET = 16;
    //end of red packet code
    @Override
    protected void setUpView() {
        setChatFragmentListener(this);
     // setListItemClickListener();
        super.setUpView();
       // preferenceUtil = new SharedPreferencesUtil(getActivity());
        fragmentArgs = getArguments();
        homeOrderId=fragmentArgs.getString("homeOrderId");
        conslutionId = fragmentArgs.getString("DT_RowId");
        clinicId = fragmentArgs.getString("CLINIC_ID");
        endTime = fragmentArgs.getString("endTime");
        titleBar.setTitle(fragmentArgs.getString("DOC_NAME"));
        try {
            String mMemberId = AbSharedUtil.getString(getActivity(),"userId");
            UserInfo userDao = new UserInfo();
            userDao = userDao.findById(mMemberId);
            extJson = putJson(conslutionId,homeOrderId,clinicId, mMemberId, userDao.getUsername());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        messageList.init(conslutionId, homeOrderId,clinicId,toChatUsername, chatType, chatFragmentHelper != null ? chatFragmentHelper.onSetCustomChatRowProvider() : null);
        setListItemClickListener();
    }
    private JSONObject putJson(String dT_RowId,String homeOrderId, String mClinicId,String memberId, String myName) throws JSONException {
        JSONObject jsonObject = new JSONObject();
      if (clinicId!=null&&homeOrderId==null&&conslutionId==null){
            jsonObject.put("clinicId", mClinicId);
            jsonObject.put("memberId", memberId);
            jsonObject.put("name", myName);
        }else if (homeOrderId!=null&&clinicId==null&&conslutionId==null){
            jsonObject.put("homeOrderId", homeOrderId);
            jsonObject.put("memberId", memberId);
            jsonObject.put("name", myName);
            jsonObject.put("endTime", endTime);
        } else if (conslutionId!=null&&homeOrderId==null&&clinicId==null){
            jsonObject.put("conslutionId", dT_RowId);
            jsonObject.put("memberId", memberId);
            jsonObject.put("name", myName);
        }
            return jsonObject;
    }
    @Override
    public void onSetMessageAttributes(EMMessage message) {
        // 设置消息扩展属性
        if (clinicId==null&&homeOrderId==null&&conslutionId!=null){
            message.setAttribute("conslutionId", conslutionId);
            message.setAttribute("em_apns_ext", extJson);
        }else if (clinicId!=null&&homeOrderId==null&&conslutionId==null){
            message.setAttribute("clinicId", clinicId);
            message.setAttribute("em_apns_ext", extJson);
        }else if (homeOrderId!=null&&clinicId==null&&conslutionId==null){
            message.setAttribute("homeOrderId", homeOrderId);
            message.setAttribute("endTime", endTime);
            message.setAttribute("em_apns_ext", extJson);
        }
    }
    /////////
    @Override
    protected void registerExtendMenuItem() {
        //use the menu in base class
        super.registerExtendMenuItem();
        //extend menu items
    }

    @Override
    public void onEnterToChatDetails() {
        // 进入会话详情
    }

    @Override
    public void onAvatarClick(String username) {
        // 用户头像点击事件

    }

    @Override
    public void onAvatarLongClick(String username) {

    }

    @Override
    public boolean onMessageBubbleClick(EMMessage message) {
        // 消息气泡框点击事件
        try {
            if (message.getStringAttribute(EaseCard.myExtType).equals("card")){
                JSONObject jsonObject = message.getJSONObjectAttribute("doctor");
                Doctor doctor = GsonKit.fromJson(jsonObject.toString(),Doctor.class);
                Bundle bundle = new Bundle();
                bundle.putString("member_id",doctor.getMemberId());
                UIKit.open(getActivity(), DoctorDetialActivity.class,bundle);
                return true;
            }
        } catch (HyphenateException e) {
            e.printStackTrace();
        }
        return false;
    }
    @Override
    public void onMessageBubbleLongClick(EMMessage message) {
        // 消息气泡框长按事件
    }

    @Override
    public boolean onExtendMenuItemClick(int itemId, View view) {
        // 扩展输入栏item点击事件,如果要覆盖EaseChatFragment已有的点击事件，return true
//        switch (itemId) {
//            //red packet code : 进入发红包页面
//            case ITEM_RED_PACKET:
//                EMMessage emMessage = EMMessage.createTxtSendMessage("wode mingpian","u39");
//                emMessage.setAttribute(EaseCard.myExtType,"card");
//                EMClient.getInstance().chatManager().sendMessage(emMessage);
//                break;
//        }
        return false;
    }

    @Override
    public EaseCustomChatRowProvider onSetCustomChatRowProvider() {
        // 设置自定义chatrow提供者
        return new CustomChatRowProvider();
    }


    //////////////////////////////


    /**
     * chat row provider
     *
     */
    private final class CustomChatRowProvider implements EaseCustomChatRowProvider {
        @Override
        public int getCustomChatRowTypeCount() {
            //here the number is the message type in EMMessage::Type
            //which is used to count the number of different chat row
            return 10;
        }

        @Override
        public int getCustomChatRowType(EMMessage message) {
            if(message.getType() == EMMessage.Type.TXT){
                try {
                    if (message.getStringAttribute(EaseCard.myExtType).equals("card")) {
                         return message.direct() == EMMessage.Direct.RECEIVE ? MESSAGE_TYPE_RECV_MINGPIAN_CALL : MESSAGE_TYPE_SEND_MINGPIAN_CALL;
                     }
                } catch (HyphenateException e) {
                    e.printStackTrace();
                }
                //end of red packet code
            }
            return 0;
        }

        @Override
        public EaseChatRow getCustomChatRow(EMMessage message, int position, BaseAdapter adapter) {
            if(message.getType() == EMMessage.Type.TXT){
                try {
                    if (message.getStringAttribute(EaseCard.myExtType).equals("card")) {
                        return new ChatRowCard(getActivity(), message, position, adapter);
                    }
                } catch (HyphenateException e) {
                    e.printStackTrace();
                }
                //end of red packet code
            }
            return null;
        }

    }
}
