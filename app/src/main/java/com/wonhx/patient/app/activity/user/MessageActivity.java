package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.utils.EaseSmileUtils;
import com.hyphenate.util.DateUtils;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.firstpage.ClinicMessageActivity;
import com.wonhx.patient.app.activity.firstpage.HomeDoctorMessageActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.ease.ConversationActivity;
import com.wonhx.patient.app.ease.Message;
import com.wonhx.patient.app.model.BaseModel;
import com.wonhx.patient.app.model.PushMessage;
import com.wonhx.patient.db.ModelBuilder;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.ListViewForScrollView;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.InjectView;
import butterknife.OnClick;

public class MessageActivity extends BaseActivity {
    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.tv_num)
    TextView tvNum;
    @InjectView(R.id.clinic_num)
    TextView mClinicUnCountNum;
    @InjectView(R.id.home_num)
    TextView home_num;
    @InjectView(R.id.right_btn)
    ImageView tv_right;
    @InjectView(R.id.listview)
    ListViewForScrollView mListView;
    QuickAdapter<Message> mListAdapter;
    Message dao = new Message();
    List<Message> mDatas = new ArrayList<>();
    List<Message> mListDatas = new ArrayList<>();
    List<PushMessage> listisread = new ArrayList<>();
    MyReceiver myReceiver = null;
    @Override
    protected void onInitView() {
        super.onInitView();
        tv_right.setBackgroundResource(R.mipmap.sandian);
        //消息
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("UPDATE_MSG_COUNT");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String str) {
                getDatas();
            }
        });
        mListAdapter = new QuickAdapter<Message>(this, R.layout.listitem_message) {
            @Override
            protected void convert(BaseAdapterHelper helper, Message item) {
                SimpleDraweeView headView = helper.getView(R.id.avatar);
                Log.e("head", Constants.REST_ORGIN + "/emedicine/pub/member_logo/" + item.getMemberId() + "/" + item.getMemberId() + ".png");
                headView.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine/pub/member_logo/" + item.getMemberId() + "/" + item.getMemberId() + ".png"));
                TextView countText = helper.getView(R.id.unread_msg_number);
                helper.setText(R.id.name,  item.getNickName());
                int count = item.getUnReadCount();
                if (count == 0) {
                    countText.setVisibility(View.GONE);
                } else {
                    countText.setVisibility(View.VISIBLE);
                    countText.setText(String.valueOf(count));
                }
                TextView content = helper.getView(R.id.message);
                helper.setText(R.id.time, DateUtils.getTimestampString(new Date(item.getTime())));
                if (item.getType() != null) {
                    switch (item.getType()) {
                        case "TXT":
                            content.setText(EaseSmileUtils.getSmiledText(MessageActivity.this, item.getContent()), TextView.BufferType.SPANNABLE);
                            break;
                        case "IMAGE":
                            content.setText("图片");
                            break;
                        case "VOICE":
                            content.setText("语音");
                            break;
                        default:
                            content.setText(EaseSmileUtils.getSmiledText(MessageActivity.this, item.getContent()), TextView.BufferType.SPANNABLE);
                            break;
                    }
                }
            }
        };
        mListView.setAdapter(mListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Message message = mListAdapter.getItem(position);
                //设置该订单所有消息为已读
                for (Message message1 : mDatas) {
                    if (message1.getOrderId().equals(message.getOrderId())) {
                        message1.setRead(true);
                        message1.saveOrUpdate();
                    }
                }
                Bundle args = new Bundle();
                args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                args.putString(EaseConstant.EXTRA_USER_ID, message.getDoctor());
                if (!message.getOrderId().equals("0")) {
                    args.putString("DT_RowId", message.getOrderId());
                }
                args.putString("DOC_NAME", message.getNickName());
                UIKit.open(MessageActivity.this, ConversationActivity.class, args);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getDatas();
    }

    private void getDatas() {
        listisread.clear();
        PushMessage pu = new PushMessage();
        listisread = pu.find(String.format("select * from %s where isDelete=0 and isread = '1' order by createTime desc", ModelBuilder.getTableName(PushMessage.class)));
        if (listisread.size() > 0) {
            tvNum.setVisibility(View.VISIBLE);
        } else {
            tvNum.setVisibility(View.GONE);
        }
        mListAdapter.clear();
        mDatas.clear();
        mListDatas.clear();
        mDatas = dao.find(String.format("select * from %s where isDelete=0 and orderId <> 'null' and orderId <> '0' order by createTime desc", ModelBuilder.getTableName(Message.class)));
        List<List<Message>> messages = new ArrayList<>();
        List<String> consultionIds = new ArrayList<>();
        for (Message message : mDatas) {
            consultionIds.add(message.getOrderId());
        }
        removeDuplicate(consultionIds);
        for (String id : consultionIds) {
            List<Message> messagesList = new ArrayList<>();
            for (Message message : mDatas) {
                if (message.getOrderId().equals(id)) {
                    messagesList.add(message);
                }
            }
            messages.add(messagesList);
        }
        /////////////////
        //诊所里边立即咨询收到的消息
        List<Message> clinicMsgs = new ArrayList<>();
        clinicMsgs.addAll(dao.find(String.format("select * from %s where isDelete=0 and clinicId  <> 'null' order by createTime desc", ModelBuilder.getTableName(Message.class))));
        int unCountClinic = 0;
        for (Message message : clinicMsgs) {
            if (!message.isRead()) {
                unCountClinic++;
            }
        }
        if (unCountClinic > 0) {
            mClinicUnCountNum.setVisibility(View.VISIBLE);
        } else {
            mClinicUnCountNum.setVisibility(View.GONE);
        }
        //家庭医生收到的消息
        List<Message> homedoctorMsgs = new ArrayList<>();
        homedoctorMsgs.addAll(dao.find(String.format("select * from %s where isDelete=0 and homeOrderId   <> 'null' order by createTime desc", ModelBuilder.getTableName(Message.class))));
        int unCountHome = 0;
        for (Message message : homedoctorMsgs) {
            if (!message.isRead()) {
                unCountHome++;
            }
        }
        if (unCountHome > 0) {
            home_num.setVisibility(View.VISIBLE);
        } else {
            home_num.setVisibility(View.GONE);
        }
        //////////////所有消息
        for (List<Message> messageList : messages) {
            Message message = new Message();
            int unReadCount = 0;
            for (int i = 0; i < messageList.size(); i++) {
                if (!messageList.get(i).isRead()) {
                    unReadCount++;
                }
            }
            message.setEndTime(messageList.get(messageList.size()-1).getEndTime());
            message.setUnReadCount(unReadCount);
            message.setContent(messageList.get(messageList.size() - 1).getContent());
            message.setRead(false);
            message.setDoctor(messageList.get(messageList.size() - 1).getDoctor());
            message.setOrderId(messageList.get(messageList.size() - 1).getOrderId());
            message.setTime(messageList.get(messageList.size() - 1).getTime());
            message.setType(messageList.get(messageList.size() - 1).getType());
            message.setNickName(messageList.get(messageList.size() - 1).getNickName());
            message.setMemberId(messageList.get(messageList.size() - 1).getMemberId());
            mListDatas.add(message);
        }
        mListAdapter.replaceAll(mListDatas);
    }

    /**
     * 去重
     *
     * @param list
     * @return
     */
    public static List<String> removeDuplicate(List<String> list) {
        Set set = new LinkedHashSet<String>();
        set.addAll(list);
        list.clear();
        list.addAll(set);
        return list;
    }

    @OnClick(R.id.left_btn)
    public void back() {
        finish();
    }

    @OnClick(R.id.right_btn)
    void message() {
        shoupopwindow();
    }

    /**
     * 家庭医生消息
     */
    @OnClick(R.id.homeMsg)
    void homeMsg(){
        UIKit.open(MessageActivity.this, HomeDoctorMessageActivity.class);
    }

    /**
     * 移动诊所
     */
    @OnClick(R.id.clinicMsg)
    void goToClinicMsg() {
        UIKit.open(MessageActivity.this, ClinicMessageActivity.class);
    }

    private void shoupopwindow() {
        View view = LayoutInflater.from(MessageActivity.this).inflate(R.layout.pop, null);
        final PopupWindow popupWindow = new PopupWindow(view,
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setTouchable(true);
        TextView tv_yidu = (TextView) view.findViewById(R.id.tv_yidu);
        TextView tv_shanchu = (TextView) view.findViewById(R.id.tv_shanchu);
        tv_yidu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isread();
                getDatas();
                popupWindow.dismiss();
            }


        });
        tv_shanchu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delect();
                getDatas();
                popupWindow.dismiss();
            }


        });
        popupWindow.showAsDropDown(tv_right);

    }

    private void isread() {
        for (Message message1 : mDatas) {
            message1.setRead(true);
            message1.saveOrUpdate();
        }
        for (PushMessage message : listisread) {
            message.setIsread("0");
            message.saveOrUpdate();
        }
    }

    private void delect() {
        for (Message message1 : mDatas) {
            message1.delete();
        }
        for (PushMessage pushMessage : listisread) {
            pushMessage.delete();
        }
    }

    @OnClick(R.id.head)
    public void list() {
        startActivity(new Intent(MessageActivity.this, MessageListActivity.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myReceiver != null) {
            unregisterReceiver(myReceiver);
        }
    }
}
