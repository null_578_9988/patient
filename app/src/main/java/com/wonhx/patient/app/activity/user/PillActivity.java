package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.kit.Toaster;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class PillActivity extends BaseActivity {

    @InjectView(R.id.left_btn)
    ImageView leftBtn;
    @InjectView(R.id.radio0)
    CheckBox radio0;
    @InjectView(R.id.radio1)
    CheckBox radio1;
    @InjectView(R.id.radio2)
    CheckBox radio2;
    @InjectView(R.id.radio3)
    CheckBox radio3;
    @InjectView(R.id.radio4)
    CheckBox radio4;
     String msga="";
    List<String>list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pill);
        ButterKnife.inject(this);
        Intent in=getIntent();
        msga=in.getStringExtra("msg");
        if (msga!=null&&!msga.equals("")) {
            if (msga.contains(getResources().getString(R.string.wu))) {
                radio0.setChecked(true);
                list.add("无");
            }
            if (msga.contains(getResources().getString(R.string.qingmeisu))) {
                radio1.setChecked(true);
                list.add("青霉素");
            }
            if (msga.contains(getResources().getString(R.string.huangan))) {
                radio2.setChecked(true);
                list.add("磺胺");
            }
            if (msga.contains(getResources().getString(R.string.lianmei))) {
                radio3.setChecked(true);
                list.add("链霉素");
            }
            if (msga.contains("其他药物")) {
                radio4.setChecked(true);
                list.add("无");
            }
        }
        radio0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio4.setChecked(false);
                    radio3.setChecked(false);
                    radio2.setChecked(false);
                    radio1.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).contains(radio0.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio0.getText().toString());
                }else {
                    list.remove(radio0.getText().toString());
                }
            }
        });
        radio1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无")||list.get(i).contains(radio1.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio1.getText().toString());
                }else {
                    list.remove(radio1.getText().toString());
                }
            }
        });
        radio2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无")||list.get(i).contains(radio2.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio2.getText().toString());
                }else {
                    list.remove(radio2.getText().toString());
                }
            }
        });
        radio3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无")||list.get(i).contains(radio3.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio3.getText().toString());
                }else {
                    list.remove(radio3.getText().toString());
                }
            }
        });
        radio4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    radio0.setChecked(false);
                    if (list.size()>0){
                        for (int i = 0; i <list.size() ; i++) {
                            if (list.get(i).equals("无")||list.get(i).contains(radio4.getText().toString())){
                                list.remove(i);
                            }
                        }
                    }
                    list.add(radio4.getText().toString());
                }else {
                    list.remove(radio4.getText().toString());
                }
            }
        });

    }

    @OnClick({R.id.left_btn, R.id.button1})
    public void onViewClicked(View view) {
        String msg="";
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.button1:
                if (list.size()>0) {
                    StringBuffer sb = new StringBuffer();
                    for (String item : list) {
                        sb.append("  "+item);
                    }
                    msg = sb.toString();
                    Intent intent = new Intent();
                    intent.putExtra("msg", msg);
                    setResult(104, intent);
                    finish();
                }else {
                    Toaster.showShort(PillActivity.this,"选择内容为空");
                }
        }
    }
}
