package com.wonhx.patient.app.manager;

import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Administrator on 2017/4/13.
 */

public interface ServiceHistoryManger {
  /**
   * 获取服务列表
   * @param member_id
   * @return
     */
  public Subscription getServicelist(String member_id,Subscriber subscriber);

  /**
   * 获取服务记录详情
   * @param req_id
   * @param subscriber
   * @return
     */
  public Subscription getServicelistdetial(String req_id, Subscriber subscriber);

  /**
   * 获取病例文档
   * @param request_id
   * @param subscriber
   * @return
     */
  public Subscription getmedicalbook(String request_id, Subscriber subscriber);

  /**
   * 获取聊天记录
   * @param subscriber
   * @return
     */
  public Subscription getchatlist(String req_id, Subscriber subscriber);

  /**
   * 评价医生
   * @param request_id
   * @param content
   * @param scroe
   * @param subscriber
   * @return
   */
  public Subscription evaluateDoctor(String request_id, String content,String scroe,Subscriber subscriber);

  /**
   * 获取评价详情
   * @param consultation_id
   * @param subscriber
   * @return
   */
  public Subscription getEvaluateDetail(String consultation_id,Subscriber subscriber);

  /**
   * 提交视频通话状态
   * @param schedule_id
   * @param v_status
   * @param subscriber
   * @return
   */
  public Subscription postVideoStatus(String schedule_id,String v_status,Subscriber subscriber);

  /**
   * 视频通话接通后  提交通话信息
   * @param schedule_id
   * @param member_id
   * @param name
   * @param req_id
   * @param subscriber
   * @return
   */
  public Subscription postVideoInfo(String schedule_id,String member_id, String name,String req_id,Subscriber subscriber);

  /**
   * 视频退款
   * @param order_id
   * @param subscriber
   * @return
   */
  public Subscription tuikuan(String order_id,Subscriber subscriber);
}
