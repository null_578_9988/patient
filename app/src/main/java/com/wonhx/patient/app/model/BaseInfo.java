package com.wonhx.patient.app.model;

/**
 * Created by apple on 2017/4/8.
 * 健康档案：基本信息
 */

public class BaseInfo {
    private String patientId;
    private String health_id;
    private String name;
    private String sex;
    private String age;
    private String birthday;
    private String id_card;
    private String phone;
    private String address;
    private String profession;
    private String is_married;
    private String contectperson;
    private String contectphone;
    private String person;
    private String person_time;

    public String getPerson_time() {
        return person_time;
    }

    public void setPerson_time(String person_time) {
        this.person_time = person_time;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getContectperson() {
        return contectperson;
    }

    public void setContectperson(String contectperson) {
        this.contectperson = contectperson;
    }

    public String getContectphone() {
        return contectphone;
    }

    public void setContectphone(String contectphone) {
        this.contectphone = contectphone;
    }

    public String getHealth_id() {
        return health_id;
    }

    public void setHealth_id(String health_id) {
        this.health_id = health_id;
    }

    public String getId_card() {
        return id_card;
    }

    public void setId_card(String id_card) {
        this.id_card = id_card;
    }

    public String getIs_married() {
        return is_married;
    }

    public void setIs_married(String is_married) {
        this.is_married = is_married;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
