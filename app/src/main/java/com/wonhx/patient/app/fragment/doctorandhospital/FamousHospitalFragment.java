package com.wonhx.patient.app.fragment.doctorandhospital;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.firstpage.FamousDetialActivity;
import com.wonhx.patient.app.activity.firstpage.Meet_doctorActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.SupurDoctor;
import com.wonhx.patient.app.model.SupurHosptial;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.view.XListView;

import org.xutils.x;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2017/7/19.
 */

public class FamousHospitalFragment extends BaseFragment implements XListView.IXListViewListener {
    View view;
    @InjectView(R.id.lv)
    XListView lv;
    QuickAdapter<SupurHosptial>mHosptialAdapter;
    Handler handler=new Handler();
    FirstPagerMager firstPagerMager=new FirstPagerMangerImal();
    List<SupurHosptial>list=new ArrayList<>();
    int start=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.hospital, null);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        lv.setPullLoadEnable(false);
        mHosptialAdapter = new QuickAdapter<SupurHosptial>(getActivity(), R.layout.famous_hospital_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, SupurHosptial item) {
                String ligo=item.getLogo_img_path();
                String name=item.getName();
                if (name!=null&&!name.equals("")) {
                    helper.setText(R.id.doctor_name,name);
                }
                String level=item.getLevel();
                if (level!=null&&!level.equals("")) {
                    helper.setText(R.id.doctor_hospitalName,level);
                }
                String dept=item.getDept();
                if (dept!=null&&!dept.equals("")) {
                    helper.setText(R.id.doctor_goodSubjects,dept);
                }
                String add=item.getAddress();
                if (add!=null&&!add.equals("")) {
                    helper.setText(R.id.doctor_goodSubjectss,add);
                }
                ImageView simpleDraweeView=helper.getView(R.id.logo);
                if (ligo!=null&&!ligo.equals("")){
                    x.image().bind(simpleDraweeView,Constants.REST_ORGIN+"/emedicine"+ligo);
                }
            }
        };
        lv.setAdapter(mHosptialAdapter);
        lv.setXListViewListener(this);
        handler=new Handler();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent(getActivity(),FamousDetialActivity.class);
                intent.putExtra("doctor_id","");
                intent.putExtra("hos_id", list.get(i-1).getId());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        getsuperDoctor();
    }

    /**
     * 获取名医
     */
    private void getsuperDoctor() {
        firstPagerMager.getSuperhospital(start+"","10",new SubscriberAdapter<ListProResult<SupurHosptial>>(){
            @Override
            public void onError(Throwable e) {
                //super.onError(e);
                dissmissProgressDialog();
                Toaster.showShort(getActivity(),"没有更多医院");
            }

            @Override
            public void success(ListProResult<SupurHosptial> supurDoctorListProResult) {
                super.success(supurDoctorListProResult);
                if (supurDoctorListProResult.getCode()){
                    list.addAll(supurDoctorListProResult.getData());
                    if (list.size()>0){
                        mHosptialAdapter.replaceAll(list);
                        if (list.size()>=10){
                            lv.setPullLoadEnable(true);
                        }else {
                            lv.setPullLoadEnable(false);
                        }
                    }
                }
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start=0;
                list.clear();
                getsuperDoctor();
                onLoad();
            }
        },1000);

    }
    private void onLoad() {
        lv.stopRefresh();
        lv.stopLoadMore();
        lv.setRefreshTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }
    @Override
    public void onLoadMore() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start=start+10;
                getsuperDoctor();
                onLoad();
            }
        },1000);

    }
}
