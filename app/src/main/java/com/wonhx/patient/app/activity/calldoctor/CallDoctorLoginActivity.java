package com.wonhx.patient.app.activity.calldoctor;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.CallDoctorManager;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.calldoctor.CallDoctorManagerImpl;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.CallUser;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.view.HJloginDialog;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/30.
 * 呼叫医生登录页面
 */
public class CallDoctorLoginActivity extends BaseActivity {
    @InjectView(R.id.edit_user)
    EditText mEditUser;
    @InjectView(R.id.edit_pass)
    EditText mPassWord;
    @InjectView(R.id.img_user_cancel)
    ImageView mUserCancle;
    @InjectView(R.id.img_pass_cancel)
    ImageView mPassCancle;
    @InjectView(R.id.img_user)
    ImageView mImgUser;
    @InjectView(R.id.img_pass)
    ImageView mImgPass;
    @InjectView(R.id.btn_login)
    Button mBtnLogin;
    String memberId;
    HJloginDialog dialog;
    CallUser callUser;
    CallDoctorManager callDoctorManager = new CallDoctorManagerImpl();
    String username,password;
    UserManager userManager = new UserManagerImpl();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 隐藏状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_call_doctor);
        showDialog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // 隐藏状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     * 初始化视图
     */
    @Override
    protected void onInitView() {
        super.onInitView();
        mEditUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(mEditUser.getText().toString().trim())) {
                    mUserCancle.setVisibility(View.VISIBLE);
                    mImgUser.setImageResource(R.mipmap.hj_login_user_on);
                } else {
                    mUserCancle.setVisibility(View.GONE);
                    mImgUser.setImageResource(R.mipmap.hj_login_user_off);
                }
                if (!StrKit.isBlank(mEditUser.getText().toString().trim()) && !StrKit.isBlank(mPassWord.getText().toString().trim())) {
                    mBtnLogin.setTextColor(CallDoctorLoginActivity.this.getResources().getColor(R.color.hj_txt_white));
                } else {
                    mBtnLogin.setTextColor(CallDoctorLoginActivity.this.getResources().getColor(R.color.hj_txt_white26));
                }

            }
        });
        mPassWord.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(mPassWord.getText().toString().trim())) {
                    mPassCancle.setVisibility(View.VISIBLE);
                    mImgPass.setImageResource(R.mipmap.hj_login_pass_on);
                } else {
                    mPassCancle.setVisibility(View.GONE);
                    mImgPass.setImageResource(R.mipmap.hj_login_pass_off);
                }
                if (!StrKit.isBlank(mEditUser.getText().toString().trim()) && !StrKit.isBlank(mPassWord.getText().toString().trim())) {
                    mBtnLogin.setTextColor(CallDoctorLoginActivity.this.getResources().getColor(R.color.hj_txt_white));
                } else {
                    mBtnLogin.setTextColor(CallDoctorLoginActivity.this.getResources().getColor(R.color.hj_txt_white26));
                }
            }
        });
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        memberId = AbSharedUtil.getString(this, "userId");
    }

    @OnClick(R.id.img_return)
    void back() {
        finish();
    }

    @OnClick(R.id.txt_forget)
    void forgetPwd() {
        //忘记密码页面
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        UIKit.open(CallDoctorLoginActivity.this,CallForgetActivity.class);
    }

    @OnClick(R.id.img_user_cancel)
    void clearUser() {
        mEditUser.setText("");
    }

    @OnClick(R.id.img_pass_cancel)
    void clearPass() {
        mPassWord.setText("");
    }

    @OnClick(R.id.btn_login)
    void submit() {
        if (StrKit.isBlank(mEditUser.getText().toString())) {
            Toaster.showShort(CallDoctorLoginActivity.this, "用户名不能为空！");
            return;
        }
        if (StrKit.isBlank(mPassWord.getText().toString())) {
            Toaster.showShort(CallDoctorLoginActivity.this, "密码不能为空！");
            return;
        }
        username = mEditUser.getText().toString();
        password = mPassWord.getText().toString();
        if (memberId != null) {
                //已经登录,退出普通用户
                quit();
            } else {
                //直接登录
                login();
            }
    }
    private void quit() {
        userManager.quit(memberId, new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                //退出环信
                loginOutEmc();
            }
        });
    }
    /**
     * 退出环信
     */
    private void loginOutEmc() {
        EMClient.getInstance().logout(true, new EMCallBack() {
            @Override
            public void onSuccess() {
              runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                      appContext.setUserInfo(null);
                      login();
                  }
              });
            }
            @Override
            public void onError(int i, String s) {
            }

            @Override
            public void onProgress(int i, String s) {

            }
        });
    }

    /**
     * 登录
     */
    private void login() {
        callDoctorManager.login(username, password, "2",AbSharedUtil.getString(CallDoctorLoginActivity.this,"device_token"),new SubscriberAdapter<ProResult<CallUser>>() {
            @Override
            public void success(ProResult<CallUser> callUserProResult) {
                super.success(callUserProResult);
                Toaster.showShort(CallDoctorLoginActivity.this, callUserProResult.getMsg());
                callUser = callUserProResult.getData();
                if(callUser!=null){
                    AbSharedUtil.putInt(CallDoctorLoginActivity.this, "callUserid", Integer.parseInt(callUser.getUser_id()));
                    AbSharedUtil.putString(CallDoctorLoginActivity.this,"userId",null);
                    //初始化数据库
                    appContext.setIsCallDoctor(true);
                    appContext.login();
                    callUser.setHuanxin_pass(callUser.getHuanxin_pass());
                    callUser.setHuanxin_user(callUser.getHuanxin_user());
                    callUser.setPhone(callUser.getPhone());
                    callUser.setUser_id(callUser.getUser_id());
                    callUser.setUsername(callUser.getUsername());
                    callUser.setUser_status(callUser.getUser_status());
                    callUser.saveOrUpdate();
                    signIn(callUser.getHuanxin_user(), callUser.getHuanxin_pass());
                }
            }
        });
    }

    /**
     * 提示对话框
     */
    private void showDialog() {
        dialog = new HJloginDialog(CallDoctorLoginActivity.this);
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                dialog.dismiss();
            }
        });
        dialog.setTitle("温馨提示");
        dialog.setContentMessage("本入口为中铁列车端专用登录入口，限备案列车专用。");
        dialog.setCancelMessage("取消");
        dialog.setPositiveMessage("继续");
        dialog.setPositiveColor(this.getResources().getColor(R.color.hj_txt_red));
        dialog.show();
    }
    /** 5人 {@link com.wonhx.patient.app.manager.FirstPagerMager           }
     * 登录方法
     */
    private void signIn(String username, String password) {
        EMClient.getInstance().login(username, password, new EMCallBack() {
            /**
             * 登陆成功的回调
             */
            @Override
            public void onSuccess() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mDialog.dismiss();
                        // 加载所有会话到内存
                        EMClient.getInstance().chatManager().loadAllConversations();
                        // 加载所有群组到内存，如果使用了群组的话
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                        UIKit.open(CallDoctorLoginActivity.this, IndexActivity.class);
                        finish();
                    }
                });
            }

            /**
             * 登陆错误的回调
             * @param i
             * @param s
             */
            @Override
            public void onError(final int i, final String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mDialog.dismiss();
                        /**
                         * 关于错误码可以参考官方api详细说明
                         * http://www.easemob.com/apidoc/android/chat3.0/classcom_1_1hyphenate_1_1_e_m_error.html
                         */
                        switch (i) {
                            // 网络异常 2
                            case EMError.NETWORK_ERROR:
                                Toast.makeText(CallDoctorLoginActivity.this, "网络错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无效的用户名 101
                            case EMError.INVALID_USER_NAME:
                                Toast.makeText(CallDoctorLoginActivity.this, "无效的用户名 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无效的密码 102
                            case EMError.INVALID_PASSWORD:
                                Toast.makeText(CallDoctorLoginActivity.this, "无效的密码 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 用户认证失败，用户名或密码错误 202
                            case EMError.USER_AUTHENTICATION_FAILED:
                                Toast.makeText(CallDoctorLoginActivity.this, "用户认证失败，用户名或密码错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 用户不存在 204
                            case EMError.USER_NOT_FOUND:
                                Toast.makeText(CallDoctorLoginActivity.this, "用户不存在 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无法访问到服务器 300
                            case EMError.SERVER_NOT_REACHABLE:
                                Toast.makeText(CallDoctorLoginActivity.this, "无法访问到服务器 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 等待服务器响应超时 301
                            case EMError.SERVER_TIMEOUT:
                                Toast.makeText(CallDoctorLoginActivity.this, "等待服务器响应超时 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 服务器繁忙 302
                            case EMError.SERVER_BUSY:
                                Toast.makeText(CallDoctorLoginActivity.this, "服务器繁忙 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 未知 Server 异常 303 一般断网会出现这个错误
                            case EMError.SERVER_UNKNOWN_ERROR:
                                Toast.makeText(CallDoctorLoginActivity.this, "未知的服务器异常 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            default:
                                EMClient.getInstance( ).chatManager().loadAllConversations();
                                // 加载所有群组到内存，如果使用了群组的话
                                // EMClient.getInstance().groupManager().loadAllGroups();
                                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                                UIKit.open(CallDoctorLoginActivity.this, IndexActivity.class);
                                finish();
                               // Toast.makeText(CallDoctorLoginActivity.this, "ml_sign_in_failed code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

            @Override
            public void onProgress(int i, String s) {
            }
        });
    }


}
