package com.wonhx.patient.app.model;


import java.util.List;

/**
 * Created by Administrator on 2017/4/18.
 */

public class ListProResultNormal<T> {


    /**
     * Created by apple on 17/4/1.
     */
        public String code;
        public String msg;
        public List<T> data;

        public boolean getCode() {
            return code.equals("0");
        }

        public void setCode(String code) {
            this.code = code;
        }

    public List<T> getDoctorList() {
        return data;
    }

    public void setDoctorList(List<T> doctorList) {
        this.data = doctorList;
    }

    public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }


}
