package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.ta.utdid2.android.utils.StringUtils;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.InjectView;
import butterknife.OnClick;
public class FamousDetialActivity extends BaseActivity {
    @InjectView(R.id.title)
    TextView tv_title;
    @InjectView(R.id.name)
    EditText ed_name;
    @InjectView(R.id.birthday)
    TextView ed_birth;
    @InjectView(R.id.phone)
    EditText ed_num;
    @InjectView(R.id.detiala)
    EditText ed_detial;
    @InjectView(R.id.male)
    RadioButton rb_boy;
    @InjectView(R.id.female)
    RadioButton rb_girl;
    String sex="";
    FirstPagerMager firstPagerMager=new FirstPagerMangerImal();
    String  doctor_id="",hos_id ="",type="";
    TimePickerView pvTime;
    @Override
    protected void onInitView() {
        super.onInitView();
        tv_title.setText("提交信息");
        Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
        pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                ed_birth.setText(getTime(date));
            }
        })
                .setType(TimePickerView.Type.YEAR_MONTH_DAY)//默认全部显示
                .setCancelText("取消")//取消按钮文字
                .gravity(Gravity.CENTER)
                .setSubmitText("确定")//确认按钮文字
                .setContentSize(18)//滚轮文字大小
                .setTitleSize(20)//标题文字大小
                .setTitleText("")//标题文字
                .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(true)//是否循环滚动
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setSubmitColor(Color.BLUE)//确定按钮文字颜色
                .setCancelColor(Color.BLUE)//取消按钮文字颜色
                .setTitleBgColor(getResources().getColor(R.color.line_grey))//标题背景颜色 Night mode
                .setBgColor(Color.WHITE)//滚轮背景颜色 Night mode
                .setRange(c.get(Calendar.YEAR) -100, c.get(Calendar.YEAR) )//默认是1900-2100年
                // .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
               // .setRangDate(c.get(Calendar.YEAR) -100,year+month+date)//起始终止年月日设定
                .setLabel("年","月","日","","","")
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                // .isDialog(true)//是否显示为对话框样式
                .build();
    }
    private String getTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_famous_detial);
        Intent intent=getIntent();
        if (intent!=null){
            doctor_id=intent.getStringExtra("doctor_id");
            hos_id=intent.getStringExtra("hos_id");
            if (!StrKit.isBlank(doctor_id)){
                type="8";
            }else {
                type="9";
            }
        }
    }

    @OnClick(R.id.left_btn)
    void back() {
        finish();
    }
    @OnClick(R.id.tv_commit)
    void commit(){
        if (rb_boy.isChecked()){
            sex="男";
        }else if (rb_girl.isChecked()){
            sex="女";
        }
        firstPagerMager.postyuyue(ed_name.getText().toString().trim(),ed_birth.getText().toString().trim(),sex,ed_num.getText().toString(),ed_detial.getText().toString().trim(),AbSharedUtil.getString(this, "userId"),type,doctor_id,hos_id,new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                if (result.getCode()){
                    Toaster.showShort(FamousDetialActivity.this,result.getMsg());
                    finish();
                }else {
                    Toaster.showShort(FamousDetialActivity.this,result.getMsg());
                }
            }
        });
    }
    @OnClick(R.id.birthday)
    void birthday_rl(){
        pvTime.show();
    }
}
