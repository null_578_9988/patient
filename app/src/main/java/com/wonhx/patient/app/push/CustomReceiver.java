package com.wonhx.patient.app.push;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.avos.avoscloud.AVOSCloud;
import com.avos.avospush.notification.NotificationCompat;
import com.wonhx.patient.app.activity.user.BuyMedicineActivity;
import com.wonhx.patient.app.activity.user.LoginActivity;
import com.wonhx.patient.app.activity.user.MessageActivity;
import com.wonhx.patient.app.model.PushMessage;
import com.wonhx.patient.kit.AbSharedUtil;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CustomReceiver extends BroadcastReceiver {


	@Override
	public void onReceive(Context context, Intent intent) {

		try {
			if (intent.getAction().equals("com.shunxi.phone.action")) {
				JSONObject json = new JSONObject(intent.getExtras().getString("com.avos.avoscloud.Data"));
				Log.e("铁路电话-接收推送", "成功" + json.toString());
				Intent intent1= new Intent();
				intent1.putExtra("phone",json.getString("title"));
				intent1.putExtra("flag","1");
				intent1.setAction("phoneexit");
				context.sendBroadcast(intent1);
			}
			if (intent.getAction().equals("com.shunxi.video.action")) {
				JSONObject json = new JSONObject(intent.getExtras().getString("com.avos.avoscloud.Data"));
				Log.e("铁路视频-接收推送", "成功" + json.toString());
				Intent intent1= new Intent();
				intent1.putExtra("video",json.getString("title"));
				intent1.putExtra("flag","2");
				intent1.putExtra("msgType",json.getString("msgType"));
				intent1.setAction("videoexit");
				context.sendBroadcast(intent1);
			}
			if (intent.getAction().equals("com.shunxi_message.action")){
				JSONObject json = new JSONObject(intent.getExtras().getString("com.avos.avoscloud.Data"));
				Log.e("其他信息推送接收推送", "成功" + json.toString());
				String alert = json.getString("alert");
				String title = json.getString("title");
				String messageType = json.getString("messageType"); // 消息类型--6 购药清单 2 系统消息
				String timestamp = json.getString("timestamp"); // 消息类型--6 购药清单 2 系统消息
				PushMessage pushMessage=new PushMessage();
				pushMessage.setTitle(title);
				pushMessage.setAlert(alert);
				pushMessage.setIsread("1");
				pushMessage.setId(Integer.parseInt(timestamp));
				pushMessage.setMessageType(messageType);
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				long now = System.currentTimeMillis();
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(now);
				pushMessage.setTimestamp(formatter.format(calendar.getTime()));
				pushMessage.saveOrUpdate();
				//收到消息
				//更新消息数量
				Intent intent2 = new Intent();
				intent2.putExtra("key", "UPDATE_MSG_COUNT");
				intent2.setAction("UPDATE_MSG_COUNT");
				context.sendBroadcast(intent2);
				if (messageType.equals("6")) {
						Intent resultIntent = new Intent(AVOSCloud.applicationContext, BuyMedicineActivity.class);
						PendingIntent pendingIntent = PendingIntent.getActivity(AVOSCloud.applicationContext, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
						NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(AVOSCloud.applicationContext).setSmallIcon(context.getApplicationInfo().icon).setContentTitle(title).setContentText(alert).setTicker(alert);
						mBuilder.setContentIntent(pendingIntent);
						mBuilder.setAutoCancel(true);
						mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
						int mNotificationId = 10086;
						NotificationManager mNotifyMgr = (NotificationManager) AVOSCloud.applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
						mNotifyMgr.notify(mNotificationId, mBuilder.build());
					    AVOSCloud.applicationContext.startActivity(intent);
				}else if (messageType.equals("2")){
					Intent resultIntent = new Intent(AVOSCloud.applicationContext, MessageActivity.class);
					PendingIntent pendingIntent = PendingIntent.getActivity(AVOSCloud.applicationContext, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
					NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(AVOSCloud.applicationContext).setSmallIcon(context.getApplicationInfo().icon).setContentTitle(title).setContentText(alert).setTicker(alert);
					mBuilder.setContentIntent(pendingIntent);
					mBuilder.setAutoCancel(true);
					mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
					int mNotificationId = 10086;
					NotificationManager mNotifyMgr = (NotificationManager) AVOSCloud.applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
					mNotifyMgr.notify(mNotificationId, mBuilder.build());
					AVOSCloud.applicationContext.startActivity(intent);
				}
			}
		} catch (Exception e) {

		}

	}

}
