package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class WinActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.left_btn)
    ImageView leftBtn;
    @InjectView(R.id.right_btn)
    TextView rightBtn;
    @InjectView(R.id.radio0)
    CheckBox radio0;
    @InjectView(R.id.radio1)
    CheckBox radio1;
    @InjectView(R.id.radio2)
    CheckBox radio2;
    @InjectView(R.id.radio3)
    CheckBox radio3;
    @InjectView(R.id.radio4)
    CheckBox radio4;
String msga="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win);
        ButterKnife.inject(this);
        Intent in=getIntent();
        msga=in.getStringExtra("msg");
        if (msga!=null&&!msga.equals("")){
            if (msga.contains(getResources().getString(R.string.baijiu))){
                radio0.setChecked(true);
            } if (msga.contains(getResources().getString(R.string.pijiu))){
                radio1.setChecked(true);
            }
            if (msga.contains(getResources().getString(R.string.honghjiu))){
                radio2.setChecked(true);
            }
            if (msga.contains(getResources().getString(R.string.huangjiu))){
                radio3.setChecked(true);
            }
            if (msga.contains(getResources().getString(R.string.qitaa))){
                radio4.setChecked(true);
            }
        }

    }

    @OnClick({R.id.left_btn, R.id.button1})
    public void onViewClicked(View view) {
        String msg="";
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.button1:
                if (radio0.isChecked()) {
                    msg = msg + "  "+radio0.getText().toString();
                }
                if (radio1.isChecked()) {
                    msg = msg + "  "+radio1.getText().toString();
                }
                if (radio2.isChecked()) {
                    msg = msg + "  "+radio2.getText().toString();
                }
                if (radio3.isChecked()) {
                    msg = msg + "  "+radio3.getText().toString();
                }
                if (radio4.isChecked()) {
                    msg = msg +"  "+ radio4.getText().toString();
                }
                Intent intent=new Intent();
                intent.putExtra("msg",msg);
                setResult(202,intent);
                finish();
                break;
        }
    }
}
