package com.wonhx.patient.app.activity.calldoctor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;

import butterknife.InjectView;
import butterknife.OnClick;
/**
 * Created by apple on 17/4/5
 * 电话呼叫页面
 */
public class PhoneServerActivity extends BaseActivity {
    protected BroadcastReceiver broadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction("phoneexit");
        this.registerReceiver(this.broadcastReceiver,intentFilter);
    }
    @InjectView(R.id.txt_return)
    TextView tv_back;
    @InjectView(R.id.txt_phone1_phone)
    TextView tv_phone;
    @InjectView(R.id.img_dian_anim1)
    ImageView iv_animal;
    String phone_num="";
    AnimationDrawable animatedDrawable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_server);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        Intent intent=getIntent();
        if (intent!=null) {
            phone_num = intent.getStringExtra("phone");
        }
        tv_phone.setText(phone_num);
        animatedDrawable = (AnimationDrawable) iv_animal.getBackground();
        animatedDrawable.setOneShot(false);
        animatedDrawable.start();//启动
    }
    @OnClick(R.id.txt_return)
    void turn_back(){
        startActivity(new Intent(PhoneServerActivity.this,CallRecordsActivity.class));
        finish();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
      //  animatedDrawable.stop();
       // startActivity(new Intent(PhoneServerActivity.this,CallRecordsActivity.class));
        this.unregisterReceiver(broadcastReceiver);
       animatedDrawable.stop();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            startActivity(new Intent(PhoneServerActivity.this, CallRecordsActivity.class));
            finish();
            return false;
        }else {
            return super.onKeyDown(keyCode, event);
        }
    }
}
