package com.wonhx.patient.app.activity.firstpage;

import android.content.IntentFilter;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.utils.EaseSmileUtils;
import com.hyphenate.util.DateUtils;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.ease.ConversationActivity;
import com.wonhx.patient.app.ease.Message;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.db.DbKit;
import com.wonhx.patient.db.ModelBuilder;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.kit.UpdateUIListenner;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by nsd on 2017/6/7.
 * 移动诊所消息页面
 */

public class ClinicMessageActivity extends BaseActivity {
    @InjectView(R.id.listview)
    ListView mListView;
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.right_btn)
    ImageView tv_right;
    QuickAdapter<Message> mListAdapter;
    Message dao = new Message();
    List<Message> mDatas = new ArrayList<>();
    List<Message> mListDatas = new ArrayList<>();
    MyReceiver myReceiver = null;
    FirstPagerMager firstPagerMager=new FirstPagerMangerImal();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_message);
    }
    @Override
    protected void onStart() {
        super.onStart();
        getDatas();
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("移动诊所消息");
        //消息
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("UPDATE_MSG_COUNT");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String str) {
                getDatas();
            }
        });
        mListAdapter = new QuickAdapter<Message>(this, R.layout.listitem_message) {
            @Override
            protected void convert(BaseAdapterHelper helper, Message item) {
                SimpleDraweeView headView = helper.getView(R.id.avatar);
                headView.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine/pub/member_logo/" + item.getMemberId() + "/" + item.getMemberId() + ".png"));
                TextView countText = helper.getView(R.id.unread_msg_number);
                helper.setText(R.id.name, item.getNickName());
                int count = item.getUnReadCount();
                if (count == 0) {
                    countText.setVisibility(View.GONE);
                } else {
                    countText.setVisibility(View.VISIBLE);
                    countText.setText(String.valueOf(count));
                }
                TextView content = helper.getView(R.id.message);
                helper.setText(R.id.time, DateUtils.getTimestampString(new Date(item.getTime())));
                if (item.getType() != null) {
                    switch (item.getType()) {
                        case "TXT":
                            content.setText(EaseSmileUtils.getSmiledText(ClinicMessageActivity.this, item.getContent()), TextView.BufferType.SPANNABLE);
                            break;
                        case "IMAGE":
                            content.setText("图片");
                            break;
                        case "VOICE":
                            content.setText("语音");
                            break;
                        default:
                            content.setText(EaseSmileUtils.getSmiledText(ClinicMessageActivity.this, item.getContent()), TextView.BufferType.SPANNABLE);
                            break;
                    }
                }
            }
        };
        mListView.setAdapter(mListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Message message = mListAdapter.getItem(position);
                //设置该订单所有消息为已读
                for (Message message1 : mDatas) {
                    if (message1.getDoctor().equals(message.getDoctor())) {
                        message1.setRead(true);
                        message1.saveOrUpdate();
                    }
                }
                //如果有值班医生
                firstPagerMager.getNowTimeDoctor(message.getClinicId(),new SubscriberAdapter<Result>(){
                    //没有值班医生
                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        dismissLoadingDialog();
                        Toaster.showShort(ClinicMessageActivity.this,"没有值班医生");
                    }
                    @Override
                    public void success(Result result) {
                        super.success(result);
                        if (message.getMemberId().equals(result.getData())){
                            Bundle args = new Bundle();
                            args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                            args.putString(EaseConstant.EXTRA_USER_ID, message.getDoctor());
                            args.putString("CLINIC_ID", message.getClinicId());
                            args.putString("DOC_NAME", message.getNickName());
                            UIKit.open(ClinicMessageActivity.this, ConversationActivity.class, args);
                        }else {
                            //有值班医生但不是这个人
                            Toaster.showShort(ClinicMessageActivity.this,"目前值班医生不是他");
                        }
                    }
                });

            }
        });
    }

    /**
     * 从数据库获取数据
     */
    private void getDatas() {
        mListAdapter.clear();
        mDatas.clear();
        mListDatas.clear();
        List<String> gggg = new ArrayList<>();
        mDatas =dao.find(String.format("select * from %s where isDelete=0 and clinicId  <> 'null' order by createTime desc", ModelBuilder.getTableName(Message.class)));
        List<List<Message>> messages = new ArrayList<>();
        List<String> docNames = new ArrayList<>();
        for (Message message : mDatas) {
            if (message.getDoctor() != null) {
                docNames.add(message.getDoctor());
            }
        }
        removeDuplicate(docNames);
        for (String name : docNames) {
            List<Message> msgs = new ArrayList<>();
            for (Message message : mDatas) {
                if (message.getDoctor().equals(name)) {
                    msgs.add(message);
                }
            }
            messages.add(msgs);
        }
        for (List<Message> messageList : messages) {
            Message message = new Message();
            int unReadCount = 0;
            for (int i = 0; i < messageList.size(); i++) {
                if (!messageList.get(i).isRead()) {
                    unReadCount++;
                }
            }
            message.setUnReadCount(unReadCount);
            message.setContent(messageList.get(messageList.size() - 1).getContent());
            message.setRead(false);
            message.setDoctor(messageList.get(messageList.size() - 1).getDoctor());
            message.setOrderId(messageList.get(messageList.size() - 1).getOrderId());
            message.setTime(messageList.get(messageList.size() - 1).getTime());
            message.setType(messageList.get(messageList.size() - 1).getType());
            message.setNickName(messageList.get(messageList.size() - 1).getNickName());
            message.setMemberId(messageList.get(messageList.size() - 1).getMemberId());
            message.setClinicId(messageList.get(messageList.size()-1).getClinicId());
            mListDatas.add(message);
        }
        mListAdapter.replaceAll(mListDatas);
    }

    /**
     * 去重
     *
     * @param list
     * @return
     */
    public static List<String> removeDuplicate(List<String> list) {
        Set set = new LinkedHashSet<String>();
        set.addAll(list);
        list.clear();
        list.addAll(set);
        return list;
    }

    private void shoupopwindow() {
        View view = LayoutInflater.from(ClinicMessageActivity.this).inflate(R.layout.pop, null);
        final PopupWindow popupWindow = new PopupWindow(view,
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setTouchable(true);
        TextView tv_yidu = (TextView) view.findViewById(R.id.tv_yidu);
        TextView tv_shanchu = (TextView) view.findViewById(R.id.tv_shanchu);
        tv_yidu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isread();
                getDatas();
                popupWindow.dismiss();
            }


        });
        tv_shanchu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delect();
                getDatas();
                popupWindow.dismiss();
            }


        });
        popupWindow.showAsDropDown(tv_right);

    }

    @OnClick(R.id.left_btn)
    void back() {
        finish();
    }

    @OnClick(R.id.right_btn)
    void message() {
        shoupopwindow();
    }


    /**
     * 全部已读
     */
    private void isread() {
        for (Message message1 : mDatas) {
            message1.setRead(true);
            message1.saveOrUpdate();
        }
    }

    /**
     * 全部删除
     */
    private void delect() {
        for (Message message1 : mDatas) {
            message1.delete();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myReceiver != null) {
            unregisterReceiver(myReceiver);
        }
    }
}
