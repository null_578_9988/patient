package com.wonhx.patient.app.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wonhx.patient.app.fragment.user.BaseInfoFragment;
import com.wonhx.patient.app.fragment.user.CheckProgressFragment;
import com.wonhx.patient.app.fragment.user.HealthyInfoFragment;
import com.wonhx.patient.app.fragment.user.LifeinfoFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 2017/4/7.
 * 健康档案适配器
 */

public class HealthyFilePagerAdapter extends FragmentPagerAdapter {
    Context mContext;
    List<Fragment> fragments = new ArrayList<Fragment>();
    public HealthyFilePagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        fragments.add(Fragment.instantiate(mContext, BaseInfoFragment.class.getName()));
        fragments.add(Fragment.instantiate(mContext, HealthyInfoFragment.class.getName()));
        fragments.add(Fragment.instantiate(mContext, LifeinfoFragment.class.getName()));
        fragments.add(Fragment.instantiate(mContext, CheckProgressFragment.class.getName()));
    }
    @Override
    public Fragment getItem(int arg0) {
        return fragments.get(arg0);
    }
    @Override
    public int getCount() {
        return fragments.size();
    }
}