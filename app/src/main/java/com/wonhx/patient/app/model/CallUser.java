package com.wonhx.patient.app.model;

/**
 * Created by apple on 17/3/30.
 * 呼叫医生模块用户实体
 */
public class CallUser extends BaseModel<CallUser> {
    private String user_id;
    private String username;
    private String user_status;
    private String phone;
    private String huanxin_user;
    private String huanxin_pass;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHuanxin_user() {
        return huanxin_user;
    }

    public void setHuanxin_user(String huanxin_user) {
        this.huanxin_user = huanxin_user;
    }

    public String getHuanxin_pass() {
        return huanxin_pass;
    }

    public void setHuanxin_pass(String huanxin_pass) {
        this.huanxin_pass = huanxin_pass;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
        setId(Integer.parseInt(user_id));
    }


    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
