package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class PayFinishActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.price)
    TextView price;
    @InjectView(R.id.image)
    ImageView image;
    @InjectView(R.id.payjieguo)
    TextView payjieguo;
    @InjectView(R.id.finish)
    TextView finish;
    String prices="",flag="",msg="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_finish);
        ButterKnife.inject(this);
        title.setText("支付");
        Intent intent=getIntent();
        prices=intent.getStringExtra("price");
        if (prices.equals("")){
            price.setVisibility(View.GONE);
        }else {
            price.setText("￥"+prices);
        }
        flag=intent.getStringExtra("flag");
        msg=intent.getStringExtra("msg");
        payjieguo.setText(msg);
        if (flag.equals("1")){
            image.setImageResource(R.mipmap.oks);
            finish.setText("完成");
        }else {
            image.setImageResource(R.mipmap.xxxx);
            finish.setText("重新支付");
        }

    }

    @OnClick({R.id.left_btn, R.id.finish})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                myExit();
                break;
            case R.id.finish:
                if (flag.equals("1")){
                    myExit();
                }else if (flag.equals("0")){
                   finish();
                }
                break;
        }
    }
    public void myExit() {
        Intent intent = new Intent();
        intent.setAction("ExitApp");
        sendBroadcast(intent);
        finish();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            myExit();
            return false;
        }else {
            return super.onKeyDown(keyCode, event);
        }

    }
}
