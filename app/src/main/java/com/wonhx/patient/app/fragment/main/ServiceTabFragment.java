package com.wonhx.patient.app.fragment.main;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.baidu.mapapi.map.BaiduMap;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.calldoctor.CallDoctorLoginActivity;
import com.wonhx.patient.app.activity.firstpage.BannerDetailActivity;
import com.wonhx.patient.app.activity.firstpage.DoctorDetialActivity;
import com.wonhx.patient.app.activity.firstpage.HomeDoctorActivity;
import com.wonhx.patient.app.activity.firstpage.Meet_doctorActivity;
import com.wonhx.patient.app.activity.firstpage.MoveHospitalActivity;
import com.wonhx.patient.app.activity.firstpage.NowConstantActivity;
import com.wonhx.patient.app.activity.firstpage.TuijiankeshiDetialActivity;
import com.wonhx.patient.app.activity.user.HealthyFileActivity;
import com.wonhx.patient.app.activity.user.LoginActivity;
import com.wonhx.patient.app.activity.user.MessageActivity;
import com.wonhx.patient.app.adapter.TuijianDoctorAbstratorInfo;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.ease.Message;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.BannerList;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.PushMessage;
import com.wonhx.patient.app.model.TuijianDoctor;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.FrescoImgLoder;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.LoginDialog;
import com.wonhx.patient.view.ServiceListView;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/22.
 *   首页
 */
public class ServiceTabFragment extends BaseFragment{
    @InjectView(R.id.banner)
    Banner mBanner;
    @InjectView(R.id.listview)
    ServiceListView mListView;
    @InjectView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @InjectView(R.id.red_point)
    TextView tv_red_point;
    UserManager userManager=new UserManagerImpl();
    FirstPagerMager firstPagerMager=new FirstPagerMangerImal();
    List<BannerList>banner=new ArrayList<>();
    List<String>images=new ArrayList<>();
    List<TuijianDoctor>  datas = new ArrayList<TuijianDoctor>();
    TuijianDoctorAbstratorInfo tuijianDoctorAdapter;
    LoginDialog dialog;
    @InjectView(R.id.notice_layout)
    FrameLayout notice_layout;
    MyReceiver myReceiver;
    List<PushMessage>all_message=new ArrayList<>();
    List<PushMessage>noread_message=new ArrayList<>();
    PushMessage pushMessage=new PushMessage();
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            getActivity(). unregisterReceiver(myReceiver);
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        int noread=0;
        int unReadCount = 0;
        String memberId= AbSharedUtil.getString(getActivity(),"userId");
        if (memberId!=null&&!memberId.equals("")) {
            all_message = pushMessage.findAll();
            for (int i = 0; i < all_message.size(); i++) {
                if (all_message.get(i).getIsread().equals("1")) {
                    noread++;
                }
            }
            Message msgDao = new Message();
            List<Message> mAllDatas = new ArrayList<>();
            mAllDatas.addAll(msgDao.findAll());
            for (Message msg : mAllDatas){
                if (!msg.isRead()){
                    unReadCount++;
                }
            }
            noread =noread+  unReadCount;
            tv_red_point.setText(noread+"");
            if (noread > 0) {
                tv_red_point.setVisibility(View.VISIBLE);
            } else {
                tv_red_point.setVisibility(View.GONE);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup)inflater.inflate(R.layout.fragment_tab_service,container,false);
        ButterKnife.inject(this,viewGroup);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light, android.R.color.holo_green_light, android.R.color.holo_blue_bright, android.R.color.holo_orange_light);
        mListView.setFocusable(false);
        return viewGroup;
    }
    @OnClick(R.id.notice_layout)
    void notice_layout(){
        String memberId= AbSharedUtil.getString(getActivity(),"userId");
        if (memberId!=null) {
            UIKit.open(getActivity(), MessageActivity.class);
        }else {
            dialog();
        }
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //消息
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("UPDATE_MSG_COUNT");
        intentFilter.addAction("REFRESH");
        getActivity(). registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String str) {
                if (str.equals("UPDATE_MSG_COUNT")) {
                    noread_message.clear();
                    all_message = pushMessage.findAll();
                    //更新消息数量
                    int unreadMsgCountTotal = 0;
                    //获取环信未读消息数量
                    for (int i = 0; i < all_message.size(); i++) {
                        if (all_message.get(i).getIsread().equals("1")) {
                            noread_message.add(all_message.get(i));
                        }
                    }
                    Message msgDao = new Message();
                    List<Message> mAllDatas = new ArrayList<>();
                    mAllDatas.addAll(msgDao.findAll());
                    for (Message msg : mAllDatas){
                        if (!msg.isRead()){
                            unreadMsgCountTotal++;
                        }
                    }
                    unreadMsgCountTotal = unreadMsgCountTotal + noread_message.size();
                    tv_red_point.setText(unreadMsgCountTotal+"");
                    if (unreadMsgCountTotal > 0) {
                        tv_red_point.setVisibility(View.VISIBLE);
                    } else {
                        tv_red_point.setVisibility(View.GONE);
                    }
                }else{
                    tv_red_point.setVisibility(View.GONE);
                }
            }
        });
    }
    /**
     * 初始化视图
     */
    @Override
    protected void onInitView() {
        super.onInitView();
        firstPagerMager.getBanner(new SubscriberAdapter<ListProResult<BannerList>>(){
            @Override
            public void success(ListProResult<BannerList> bannerListProResult) {
                super.success(bannerListProResult);
                banner.clear();
                images.clear();
                banner.addAll(bannerListProResult.getData());
                for (int i = 0; i <banner.size() ; i++) {
                    images.add(banner.get(i).getPath());
                }
                mBanner.setDelayTime(4000);
                mBanner.setImages(images)
                        .setImageLoader(new FrescoImgLoder())
                        .start();
                mBanner.setOnBannerListener(new OnBannerListener() {
                    @Override
                    public void OnBannerClick(int position) {
                        Intent intent=new Intent(getActivity(),BannerDetailActivity.class);
                        intent.putExtra("url",banner.get(position).getUrl());
                        intent.putExtra("title","");
                        startActivity(intent);
                    }
                });
            }
        });
        tuijianDoctorAdapter=new TuijianDoctorAbstratorInfo(getActivity(),R.layout.tuijian_doctor_item,datas);
        mListView.setAdapter(tuijianDoctorAdapter);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                userManager.getDoctor(new SubscriberAdapter<ListProResult<TuijianDoctor>>(){
                    @Override
                    public void success(ListProResult<TuijianDoctor> tuijianDoctorProResult) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        super.success(tuijianDoctorProResult);
                        datas.clear();
                        datas .addAll(tuijianDoctorProResult.getData());
                        if (datas.size()>0){
                            tuijianDoctorAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in=new Intent(getActivity(), DoctorDetialActivity.class);
                in.putExtra("member_id",datas.get(position).getMemberId());
                startActivity(in);
            }
        });
    }
    /**
     * 初始化数据
     */
    @Override
    protected void onInitData() {
        super.onInitData();
        userManager.getDoctor(new SubscriberAdapter<ListProResult<TuijianDoctor>>(){
            @Override
            public void onError(Throwable e) {
               // super.onError(e);
                dissmissProgressDialog();
            }

            @Override
            public void success(ListProResult<TuijianDoctor> tuijianDoctorProResult) {
                super.success(tuijianDoctorProResult);
                datas.clear();
                datas .addAll(tuijianDoctorProResult.getData());
                if (datas.size()>0){
                    tuijianDoctorAdapter.notifyDataSetChanged();
                }
            }
        });

    }
    //呼叫医生
    @OnClick(R.id.rel_call_doc)
    void callDoctor(){
        UIKit.open(getActivity(), CallDoctorLoginActivity.class);
    }
    //开始咨询
    @OnClick(R.id.li_consult)
    void start_quest(){
        UIKit.open(getActivity(), NowConstantActivity.class);
    }
    //名医预约
    @OnClick(R.id.medicle)
    void meet_doctor(){
        String memberId= AbSharedUtil.getString(getActivity(),"userId");
        if (memberId!=null&&!memberId.equals("")) {
        UIKit.open(getActivity(), Meet_doctorActivity.class);}
        else {
            dialog();
        }
    }
    //家庭医生
    @OnClick(R.id.home_dortor)
    void home_doctor(){
            String memberId= AbSharedUtil.getString(getActivity(),"userId");
            if (memberId!=null&&!memberId.equals("")) {
                UIKit.open(getActivity(), HomeDoctorActivity.class);
            }else {
                dialog();
            }

    }
    //健康档案
    @OnClick(R.id.health_records_one)
    void helath_recode(){
        String memberId= AbSharedUtil.getString(getActivity(),"userId");
        if (memberId!=null&&!memberId.equals("")) {
            UIKit.open(getActivity(), HealthyFileActivity.class);
        }else {
            dialog();
        }
    }
    //移动诊所
    @OnClick(R.id.family_doctor)
    void family_doctor(){
       // Toaster.showShort(getActivity(),"暂未开放 敬请期待！");
        String memberId= AbSharedUtil.getString(getActivity(),"userId");
        if (memberId!=null&&!memberId.equals("")) {
            UIKit.open(getActivity(), MoveHospitalActivity.class);
        }else {
            dialog();
        }
    }
    //中医体质
    @OnClick(R.id.tcm_constitution)
    void china_health(){
        String memberId= AbSharedUtil.getString(getActivity(),"userId");
        if (memberId!=null&&!memberId.equals("")) {
            Intent intent = new Intent(getActivity(), BannerDetailActivity.class);
            intent.putExtra("url", Constants.CHINA_HEATH);
            intent.putExtra("title", "中医体质");
            startActivity(intent);
        }else {
            dialog();
        }
    }
    @OnClick(R.id.serc)
    void search(){
        Intent in=new Intent(getActivity(),TuijiankeshiDetialActivity.class);
        in.putExtra("deptid","");
        startActivity(in);
    }

    // 弹窗
    private void dialog() {
        dialog = new LoginDialog(getActivity());
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setClass(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });
        dialog.show();
    }
}
