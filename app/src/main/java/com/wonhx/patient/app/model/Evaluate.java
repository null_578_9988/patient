package com.wonhx.patient.app.model;

/**
 * Created by nsd on 2017/4/26.
 */

public class Evaluate {
    private String conId;
    private String content;
    private String patientId;
    private String sickTime;
    private String medical;
    private String medical_description;
    private String end_time;
    private String conreqId;
    private String medicine_id;
    private String reply_appraise;
    private String reply;
    private String reply_date;
    private String reply_appraise_date;
    private String reply_score;
    private String start_time;
    private String status;
    private String replypath;
    private String audiotime;
    private String serviceType;
    private String patientName;
    private String sex;
    private String patientMemberId;
    private String birthday;
    private String doctorName;
    private String doctorMemberId;
    private String patient_huanxin_username;

    public String getConId() {
        return conId;
    }

    public void setConId(String conId) {
        this.conId = conId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getSickTime() {
        return sickTime;
    }

    public void setSickTime(String sickTime) {
        this.sickTime = sickTime;
    }

    public String getMedical() {
        return medical;
    }

    public void setMedical(String medical) {
        this.medical = medical;
    }

    public String getMedical_description() {
        return medical_description;
    }

    public void setMedical_description(String medical_description) {
        this.medical_description = medical_description;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getConreqId() {
        return conreqId;
    }

    public void setConreqId(String conreqId) {
        this.conreqId = conreqId;
    }

    public String getMedicine_id() {
        return medicine_id;
    }

    public void setMedicine_id(String medicine_id) {
        this.medicine_id = medicine_id;
    }

    public String getReply_appraise() {
        return reply_appraise;
    }

    public void setReply_appraise(String reply_appraise) {
        this.reply_appraise = reply_appraise;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getReply_date() {
        return reply_date;
    }

    public void setReply_date(String reply_date) {
        this.reply_date = reply_date;
    }

    public String getReply_appraise_date() {
        return reply_appraise_date;
    }

    public void setReply_appraise_date(String reply_appraise_date) {
        this.reply_appraise_date = reply_appraise_date;
    }

    public String getReply_score() {
        return reply_score;
    }

    public void setReply_score(String reply_score) {
        this.reply_score = reply_score;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReplypath() {
        return replypath;
    }

    public void setReplypath(String replypath) {
        this.replypath = replypath;
    }

    public String getAudiotime() {
        return audiotime;
    }

    public void setAudiotime(String audiotime) {
        this.audiotime = audiotime;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPatientMemberId() {
        return patientMemberId;
    }

    public void setPatientMemberId(String patientMemberId) {
        this.patientMemberId = patientMemberId;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorMemberId() {
        return doctorMemberId;
    }

    public void setDoctorMemberId(String doctorMemberId) {
        this.doctorMemberId = doctorMemberId;
    }

    public String getPatient_huanxin_username() {
        return patient_huanxin_username;
    }

    public void setPatient_huanxin_username(String patient_huanxin_username) {
        this.patient_huanxin_username = patient_huanxin_username;
    }
}
