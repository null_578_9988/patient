package com.wonhx.patient.app.activity.calldoctor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/4/5.
 * 视频呼叫页面
 */
public class VideoServerActivity extends BaseActivity {
    protected BroadcastReceiver broadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
           if (intent.getStringExtra("msgType").equals("1")){
               mHint.setText(intent.getStringExtra("video"));
            }else {
               finish();
           }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction("videoexit");
        this.registerReceiver(this.broadcastReceiver,intentFilter);
    }
    @InjectView(R.id.txt_return)
    TextView tv_back;
    @InjectView(R.id.txt_phone1_phone)
    TextView tv_phone;
    @InjectView(R.id.img_dian_anim1)
    ImageView iv_animal;
    @InjectView(R.id.txt_hint)
    TextView mHint;
    @InjectView(R.id.txt_title)
    TextView mTitle;
    @InjectView(R.id.img_set)
    ImageView mImgSet;
    @InjectView(R.id.icon)
    ImageView mIcon;
    String phone_num = "";
    AnimationDrawable animatedDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_server);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("视频呼叫");
        mHint.setText("统正在为您分配医生，请耐心等待");
        mIcon.setImageResource(R.mipmap.hj_video_icon);
        mImgSet.setImageResource(R.mipmap.hj_video_icon);
        Intent intent = getIntent();
        if (intent != null) {
            phone_num = intent.getStringExtra("phone");
        }
        tv_phone.setText(phone_num);
        animatedDrawable = (AnimationDrawable) iv_animal.getBackground();
        //是否仅仅启动一次？
        animatedDrawable.setOneShot(false);
        animatedDrawable.start();

    }

    @OnClick(R.id.txt_return)
    void turn_back() {
        startActivity(new Intent(VideoServerActivity.this,CallRecordsActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        animatedDrawable.stop();
        this.unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            startActivity(new Intent(VideoServerActivity.this, CallRecordsActivity.class));
            finish();
            return false;
        }else {
            return super.onKeyDown(keyCode, event);
        }
    }
}
