package com.wonhx.patient.app.manager.user;

import android.util.Log;

import com.wonhx.patient.app.AppException;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.model.Account;
import com.wonhx.patient.app.model.AccountDetial;
import com.wonhx.patient.app.model.BaseInfo;
import com.wonhx.patient.app.model.BuyMedicineList;
import com.wonhx.patient.app.model.CheckProgress;
import com.wonhx.patient.app.model.HealthyInfo;
import com.wonhx.patient.app.model.LifeInfo;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.LoginResult;
import com.wonhx.patient.app.model.OrderDetial;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.Resultt;
import com.wonhx.patient.app.model.SUserInfo;
import com.wonhx.patient.app.model.TuijianDoctor;
import com.wonhx.patient.app.model.WeXinBean;
import com.wonhx.patient.http.Rest;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by apple
 * 用户模块实现
 */
public class UserManagerImpl implements UserManager {

    @Override
    public Subscription login(final String username, final String password, final String member_type, final String device_type, final String device_token, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<LoginResult>() {
            @Override
            public void call(Subscriber<? super LoginResult> subscriber) {
                try {
                    LoginResult result = rest.login(username, password, member_type, device_type, device_token);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getRegisterCode(final String phone, final String ident_code, final String member_type, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.getRegisterCode(phone, ident_code, member_type);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription register(final String phone, final String password, final String member_type, final String sms_code, final String device_type, final String device_token, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<LoginResult>() {
            @Override
            public void call(Subscriber<? super LoginResult> subscriber) {
                try {
                    LoginResult result = rest.register(phone, password, member_type, sms_code, device_type, device_token);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getForgetCode(final String phone, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.getForgetCode(phone,"1");
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription modifyPwd(final String phone, final String password, final String member_type, final String sms_code, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.modifyPwd(phone, password, member_type, sms_code);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription updateUserHead(final String head_image, final String memberId, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.updateUserHead(head_image, memberId);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getUserInfo(final String memberId, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<SUserInfo>>() {
            @Override
            public void call(Subscriber<? super ProResult<SUserInfo>> subscriber) {
                try {
                    ProResult<SUserInfo> result = rest.getUserInfo(memberId);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription submitUserInfo(final String member_id, final String name, final String birthday, final String sex, final String address, final String phone, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.submitUserInfo(member_id,name,birthday,sex,address,phone);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription quit(final String member_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                Result re=rest.quit(member_id);
                if (re.getCode()){
                    subscriber.onNext(re);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }


    @Override
    public Subscription getDoctor(Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<TuijianDoctor>>() {
            @Override
            public void call(Subscriber<? super ListProResult<TuijianDoctor>> subscriber) {
                try {
                    ListProResult<TuijianDoctor> result = rest.tuijianDoctor();
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription suggestion(final String member_id, final String content, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.suggestion(member_id,content);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription change_pwd(final String member_id, final String old_password, final String new_password, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.change_pwd(member_id,old_password,new_password);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getBaseInfo(final String patient_member_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<BaseInfo>>() {
            @Override
            public void call(Subscriber<? super ProResult<BaseInfo>> subscriber) {
                try {
                    ProResult<BaseInfo> result = rest.getBaseInfo(patient_member_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getHealthyInfo(final String health_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<HealthyInfo>>() {
            @Override
            public void call(Subscriber<? super ProResult<HealthyInfo>> subscriber) {
                try {
                    ProResult<HealthyInfo> result = rest.getHealthyInfo(health_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getLifeInfo(final String health_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<LifeInfo>>() {
            @Override
            public void call(Subscriber<? super ProResult<LifeInfo>> subscriber) {
                try {
                    ProResult<LifeInfo> result = rest.getLifeInfo(health_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getCheckProgress(final String health_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<CheckProgress>>() {
            @Override
            public void call(Subscriber<? super ListProResult<CheckProgress>> subscriber) {
                try {
                    ListProResult<CheckProgress> result = rest.getCheckProgress(health_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription submitBaseInfo(final String member_id, final String msg, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.submitBaseInfo(member_id,msg);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription submitHealthyInfo(final String health_id, final String msg, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.submitHealthyInfo(health_id,msg);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription submitLifeinfo(final String health_id, final String msg, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.submitLifeInfo(health_id,msg);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription updateCheckImg(final String member_id, final String record_id, final String health_img, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.updateCheckImg(member_id, record_id, health_img);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }
    @Override
    public Subscription submitCheckName(final String member_id, final String health_id, final String msg, final String record_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.submitCheckName(member_id,health_id,msg,record_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }
        @Override
    public Subscription getaccount(final String member_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ProResult<Account>>() {
            @Override
            public void call(Subscriber<? super ProResult<Account>> subscriber) {
                try {
                    ProResult<Account> result = rest.getaccount(member_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }
    @Override
    public Subscription getaccountdetial(final String member_id, final String start, final String length, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<AccountDetial>>() {
            @Override
            public void call(Subscriber<? super ListProResult<AccountDetial>> subscriber) {
                try {
                    ListProResult<AccountDetial> result = rest.getaccountdetial(member_id,start,length);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription send_money(final String member_id, final String price, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.send_money(member_id,price);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription zhifubao(final String out_trade_no, final String business_type, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.zhifubao(out_trade_no,business_type);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription yinlian(final String orderId, final String business_type, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.yinlian(orderId,business_type);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription weixin(final String out_trade_no, final String business_type, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<WeXinBean>() {
            @Override
            public void call(Subscriber<? super WeXinBean> subscriber) {
                try {
                    WeXinBean result = rest.weixinpay(out_trade_no,business_type);
                    if (!result.getCode().equals("0"))
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription buymedicinelist(final String member_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<BuyMedicineList>>() {
            @Override
            public void call(Subscriber<? super ListProResult<BuyMedicineList>> subscriber) {
                try {
                    ListProResult<BuyMedicineList> result = rest.buymedince(member_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription delectmedicinelist(final String order_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.delect(order_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription orderdetial(final String order_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<ListProResult<OrderDetial>>() {
            @Override
            public void call(Subscriber<? super ListProResult<OrderDetial>> subscriber) {
                try {
                    ListProResult<OrderDetial> result = rest.orderdetial(order_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription BanlancePay(final String order_id, final String member_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.banlancepay(order_id,member_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription commitAdd(final String order_id, final String address, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.commitAdd(order_id,address);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription getUserInfoByEaseName(final String huanxin_username, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.getUserInfoByEaseName(huanxin_username);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    Log.e("hahahhahaha成功","hhhhh");
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription familycallcontent(final String member_id, final String order_id, final String cont, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.familycallcontent(member_id,order_id,cont);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription familycallon(final String order_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.familycallon(order_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    @Override
    public Subscription delectfamilylist(final String family_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe<Result>() {
            @Override
            public void call(Subscriber<? super Result> subscriber) {
                try {
                    Result result = rest.delectfamilylist(family_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }






    /**
     * REST Service
     */
    RestService rest = Rest.create(RestService.class);

    interface RestService {
        /**
         * 用户登录
         *
         * @param username
         * @param password
         * @param member_type
         * @param device_type
         * @param device_token
         * @return
         */
        @FormUrlEncoded
        @POST("/member_login/login")
        LoginResult login(@Field("username") String username
                , @Field("password") String password
                , @Field("member_type") String member_type
                , @Field("device_type") String device_type
                , @Field("device_token") String device_token);

        /**
         * 获取注册手机验证码
         *
         * @param phone
         * @param ident_code
         * @return
         */
        @FormUrlEncoded
        @POST("/sms_code/sms_indent_code")
        Result getRegisterCode(@Field("phone") String phone
                , @Field("ident_code") String ident_code, @Field("member_type") String member_type);

        /**
         * 注册
         *
         * @param phone
         * @param password
         * @param member_type
         * @param sms_code
         * @param device_type
         * @param device_token
         * @return
         */
        @FormUrlEncoded
        @POST("/register_member/reg_message")
        LoginResult register(@Field("phone") String phone
                , @Field("password") String password
                , @Field("member_type") String member_type
                , @Field("sms_code") String sms_code
                , @Field("device_type") String device_type
                , @Field("device_token") String device_token);

        /**
         * 获取忘记密码短信验证码
         *
         * @param phone
         * @return
         */
        @FormUrlEncoded
        @POST("/member_login/password_smscode")
        Result getForgetCode(@Field("phone") String phone,@Field("member_type")String member_type);

        /**
         * 修改密码
         *
         * @param phone
         * @param password
         * @param member_type
         * @param sms_code
         * @return
         */
        @FormUrlEncoded
        @POST("/member_login/modify_password")
        Result modifyPwd(@Field("phone") String phone
                , @Field("password") String password
                , @Field("member_type") String member_type
                , @Field("sms_code") String sms_code);
        /**
         * 上传用户头像
         *
         * @param head_image
         * @param memberId
         * @return
         */
        @FormUrlEncoded
        @POST("/patient_concern/patient_head_portrait")
        Result updateUserHead(@Field("head_image") String head_image
                , @Field("member_id") String memberId);

        /**
         * 获取个人信息
         *
         * @param memberId
         * @return
         */
        @GET("/patient_concern/get_patient_info")
        ProResult<SUserInfo> getUserInfo(@Query("member_id") String memberId);

        /**
         * 提交用户信息
         *
         * @param member_id
         * @param name
         * @param birthday
         * @param sex
         * @param address
         * @param phone
         * @return
         */
        @FormUrlEncoded
        @POST("/patient_concern/patient_info")
        Result submitUserInfo(@Field("member_id") String member_id
                , @Field("name") String name
                , @Field("birthday") String birthday
                , @Field("sex") String sex
                , @Field("address") String address
                , @Field("phone") String phone);

        /**
         * 普通用户退出
         * @param member_id
         * @return
         */
        @FormUrlEncoded
        @POST("/member_login/patient_login_out")
        Result quit(@Field("member_id")String member_id);

        /**
         * 获取推荐医生列表
         * @return
         */
        @GET("/patient_concern/recommend_doctor")
        ListProResult<TuijianDoctor> tuijianDoctor();

        /**
         * 提交反馈意见
         * @param member_id
         * @param content
         * @return
         */
        @FormUrlEncoded
        @POST("/patient_concern/submit_feedback")
        Result suggestion(@Field("member_id")String member_id,@Field("content")String content);

        /**
         * 修改密码
         * @param member_idpas
         * @param old_sword
         * @param new_password
         * @return
         */
        @FormUrlEncoded
        @POST("/member_login/personal_passwd")
        Result change_pwd(@Field("member_id")String member_idpas,@Field("old_password")String old_sword,@Field("new_password")String new_password);
        /**
         * 健康档案：获取基本信息
         * @param patient_member_id
         * @return
         */
        @GET("/health_file/get_patient_info")
        ProResult<BaseInfo> getBaseInfo(@Query("patient_member_id") String patient_member_id);

        /**
         * 健康档案：获取健康信息
         * @param health_id
         * @return
         */
        @GET("/health_file/get_health")
        ProResult<HealthyInfo> getHealthyInfo(@Query("health_id") String health_id);

        /**
         *  健康档案：获取生活信息
         * @param health_id
         * @return
         */
        @GET("/health_file/get_habits")
        ProResult<LifeInfo> getLifeInfo(@Query("health_id") String health_id);

        /**
         * 将康档案：获取体检流水
         * @param health_id
         * @return
         */
        @GET("/health_file/get_physical_flow")
        ListProResult<CheckProgress> getCheckProgress(@Query("health_id") String health_id);

        /**
         * 健康档案：提交基本信息
         * @param member_id
         * @param msg
         * @return
         */
        @FormUrlEncoded
        @POST("/health_file/modify_message")
        Result submitBaseInfo(@Field("member_id") String member_id,@Field("msg") String msg);
        /**
         * 健康档案：提交健康信息
         * @param health_id
         * @param msg
         * @return
         */
        @FormUrlEncoded
        @POST("/health_file/modify_health_msg")
        Result submitHealthyInfo(@Field("health_id") String health_id,@Field("msg") String msg);
        /**
         * 健康档案：提交生活信息
         * @param health_id
         * @param msg
         * @return
         */
        @FormUrlEncoded
        @POST("/health_file/modify_habits_msg")
        Result submitLifeInfo(@Field("health_id") String health_id,@Field("msg") String msg);

        /**
         * 健康档案；上传体检流程图片
         * @param member_id
         * @param record_id
         * @param health_img
         * @return
         */
        @FormUrlEncoded
        @POST("/health_file/upload_health_flow")
        Result updateCheckImg(@Field("member_id") String member_id,@Field("record_id") String record_id,@Field("health_img") String health_img);

        /**
         * 健康档案：提交体检流程名称
         * @param member_id
         * @param health_id
         * @return
         */
        @FormUrlEncoded
        @POST("/health_file/modify_health_flow")
        Result submitCheckName(@Field("member_id") String member_id,@Field("health_id") String health_id,@Field("msg")String msg,@Field("record_id")String record_id);


        /**
         * 获取个人账户金额
         * @param member_id
         * @return
         */
        @FormUrlEncoded
        @POST("/patient_concern/patient_balance")
        ProResult<Account>getaccount(@Field("member_id")String member_id);

        /**
         * 获取账单明细
         * @param member_id
         * @return
         */
        @GET("/patient_concern/expenditure_details")
        ListProResult<AccountDetial>getaccountdetial(@Query("member_id") String member_id,@Query("start") String start,@Query("length") String length);

        /**
         * 充值
         * @param member_id
         * @param price
         * @return
         */
        @FormUrlEncoded
        @POST("/consultation_service/recharge_order_submit")
        Result send_money(@Field("member_id")String member_id,@Field("price")String price);

        /**
         * 支付宝支付
         * @param out_trade_no
         * @param business_type
         * @return
         */
        @FormUrlEncoded
        @POST("/pay/alipay")
        Result zhifubao(@Field("out_trade_no")String out_trade_no,@Field("business_type")String business_type);

        /**
         * 银联支付
         * @param orderId
         * @param business_type
         * @return
         */
        @FormUrlEncoded
        @POST("/pay/unionpay")
        Result yinlian(@Field("orderId")String orderId,@Field("business_type")String business_type);

        /**
         * 获取购药清单列表
         * @param member_id
         * @return
         */
        @GET("/medicine_msg/patient_medicine_list")
        ListProResult<BuyMedicineList>buymedince(@Query("member_id")String member_id);

        /**
         * 删除购药清单
         * @param order_id
         * @return
         */
        @GET("/medicine_msg/update_order_pay_status")
        Result delect(@Query("order_id")String order_id);

        /**
         * 获取订单详情
         * @param order_id
         * @return
         */
        @GET("/medicine_msg/medicine_order_details?order_id")
        ListProResult<OrderDetial>orderdetial(@Query("order_id")String order_id);
        /**
         * 余额支付药品
         * @param order_id
         * @param member_id
         * @return
         */
        @FormUrlEncoded
        @POST("/pay/balance_payment/")
        Result banlancepay(@Field("order_id")String order_id ,@Field("member_id")String member_id );

        /**
         * 药品支付的时候先提交地址
         * @param order_id
         * @param address
         * @return
         */
        @GET("/medicine_msg/patient_submit_addr")
        Result commitAdd(@Query("order_id") String order_id ,@Query("address")String address );

        /**
         * 微信支付
         * @param business_type
         * @param out_trade_no
         * @return
         */
        @FormUrlEncoded
        @POST("/pay/wx_patt_pay")
        WeXinBean weixinpay( @Field("out_trade_no") String out_trade_no ,@Field("business_type") String business_type );

        /**
         * 根据环信用户名称获取用户信息
         * @param huanxin_username
         * @return
         */
        @FormUrlEncoded
        @POST("mysql_api/member_login/doctor_message")
        Result getUserInfoByEaseName(@Field("huanxin_username") String huanxin_username);

        /**
         * 提交家庭医生回拨申请
         * @param member_id
         * @param order_id
         * @param cont
         * @return
         */
        @FormUrlEncoded
        @POST("/family_doctor/audit_callbake")
        Result familycallcontent(@Field("member_id") String member_id,@Field("order_id") String order_id,@Field("cont") String cont);

        /**
         * 紧急呼叫
         * @param order_id
         * @return
         */
        @FormUrlEncoded
        @POST("/family_doctor/urgency_call")
        Result familycallon(@Field("order_id") String order_id);

        /**
         * 删除家庭医生列表
         * @param family_id
         * @return
         */
        @FormUrlEncoded
        @POST("/family_doctor/update_family_status")
        Result delectfamilylist(@Field("family_id") String family_id);


    }
}
