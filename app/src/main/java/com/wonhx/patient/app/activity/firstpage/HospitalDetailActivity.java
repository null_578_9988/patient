package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hyphenate.easeui.EaseConstant;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.user.MessageActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.ease.ConversationActivity;
import com.wonhx.patient.app.ease.Message;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.MoveHospital;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.PushMessage;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.Resultt;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.DateKit;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.kit.UpdateUIListenner;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.netopen.hotbitmapgg.library.view.RingProgressBar;

/**
 * Created by nsd on 2017/5/18.
 * 诊所详情
 */

public class HospitalDetailActivity extends BaseActivity {
    boolean haveDoctor = false;//有无值班医生
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.clinicHead)
    SimpleDraweeView mClinicHeadImg;
    @InjectView(R.id.logo)
    SimpleDraweeView mDocLogo;
    @InjectView(R.id.brief)
    TextView mBrief;
    @InjectView(R.id.doctor_name)
    TextView mDocName;
    @InjectView(R.id.doctor_dept)
    TextView mDocDept;
    @InjectView(R.id.doctor_hospitalName)
    TextView mHospitalName;
    @InjectView(R.id.count)
    TextView mCount;
    @InjectView(R.id.content)
    TextView mContent;
    @InjectView(R.id.circle_progress_bar1)
    RingProgressBar mCircleProgress1;
    @InjectView(R.id.circle_progress_bar2)
    RingProgressBar mCircleProgress2;
    @InjectView(R.id.circle_progress_bar3)
    RingProgressBar mCircleProgress3;
    @InjectView(R.id.circle_progress_bar4)
    RingProgressBar mCircleProgress4;
    @InjectView(R.id.submit)
    TextView mSubmit;
    @InjectView(R.id.doctor_title)
    TextView doctor_title;
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    String mClinicId;
    MoveHospital mMoveHospital;
    ViewGroup view;
    TextView textView;
    MyReceiver myReceiver;
    List<PushMessage> all_message = new ArrayList<>();
    List<PushMessage> noread_message = new ArrayList<>();
    PushMessage pushMessage = new PushMessage();
    @InjectView(R.id.red_point)
    TextView tv_red_point;
    @InjectView(R.id.tv_notice)
    TextView tvNotice;
    @InjectView(R.id.tv_time)
    TextView tvTime;
    @InjectView(R.id.tv_add)
    TextView tvAdd;
    @InjectView(R.id.notice)
    RelativeLayout notice;
    @InjectView(R.id.time)
    RelativeLayout time;
    @InjectView(R.id.address)
    RelativeLayout address;
    String la="";
    String ln="";
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (myReceiver != null) {
            unregisterReceiver(myReceiver);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        int noread = 0;
        int unReadCount = 0;
        String memberId = AbSharedUtil.getString(HospitalDetailActivity.this, "userId");
        if (memberId != null && !memberId.equals("")) {
            all_message = pushMessage.findAll();
            for (int i = 0; i < all_message.size(); i++) {
                if (all_message.get(i).getIsread().equals("1")) {
                    noread++;
                }
            }
            Message msgDao = new Message();
            List<Message> mAllDatas = new ArrayList<>();
            mAllDatas.addAll(msgDao.findAll());
            for (Message msg : mAllDatas) {
                if (!msg.isRead()) {
                    unReadCount++;
                }
            }
            noread = noread + unReadCount;
            tv_red_point.setText(noread + "");
            if (noread > 0) {
                tv_red_point.setVisibility(View.VISIBLE);
            } else {
                tv_red_point.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > 18) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_clinic_detail);
        ButterKnife.inject(this);
        // 创建TextView
        textView = new TextView(this);
        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, UIKit.getStatusBarHeight(this));
        textView.setBackgroundColor(Color.parseColor("#5b90ff"));
        textView.setLayoutParams(lParams);
        // 获得根视图并把TextView加进去。
        view = (ViewGroup) getWindow().getDecorView();
        view.addView(textView);


        //消息
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("UPDATE_MSG_COUNT");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String str) {
                if (str.equals("UPDATE_MSG_COUNT")) {
                    noread_message.clear();
                    all_message = pushMessage.findAll();
                    //更新消息数量
                    int unreadMsgCountTotal = 0;
                    //获取环信未读消息数量
                    for (int i = 0; i < all_message.size(); i++) {
                        if (all_message.get(i).getIsread().equals("1")) {
                            noread_message.add(all_message.get(i));
                        }
                    }
                    Message msgDao = new Message();
                    List<Message> mAllDatas = new ArrayList<>();
                    mAllDatas.addAll(msgDao.findAll());
                    for (Message msg : mAllDatas) {
                        if (!msg.isRead()) {
                            unreadMsgCountTotal++;
                        }
                    }
                    unreadMsgCountTotal = unreadMsgCountTotal + noread_message.size();
                    tv_red_point.setText(unreadMsgCountTotal + "");
                    if (unreadMsgCountTotal > 0) {
                        tv_red_point.setVisibility(View.VISIBLE);
                    } else {
                        tv_red_point.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @OnClick(R.id.notice_layout)
    void notice_layout() {
        UIKit.open(HospitalDetailActivity.this, MessageActivity.class);
    }

    /**
     * 初始化数据
     */
    @Override
    protected void onInitData() {
        super.onInitData();
        mClinicId = getIntent().getStringExtra("CLINICID");
        if (mClinicId != null) {
            firstPagerMager.getClinicDetail(mClinicId, new SubscriberAdapter<ProResult<MoveHospital>>() {
                @Override
                public void success(ProResult<MoveHospital> result) {
                    super.success(result);
                    mMoveHospital = result.getData();
                    if (mMoveHospital != null) {
                        if (mMoveHospital.getBulletin() != null && !mMoveHospital.getBulletin().equals("")) {
                            notice.setVisibility(View.VISIBLE);
                            tvNotice.setText(mMoveHospital.getBulletin());
                        } else {
                            notice.setVisibility(View.GONE);
                        }
                        if (mMoveHospital.getAddress()!=null&&!mMoveHospital.getAddress().equals("")){
                            address.setVisibility(View.VISIBLE);
                            tvAdd.setText(mMoveHospital.getAddress());
                        }else {
                            address.setVisibility(View.GONE);
                        }
                        if (mMoveHospital.getBusiness_hours()!=null&&!mMoveHospital.getBusiness_hours().equals("")){
                            time.setVisibility(View.VISIBLE);
                            tvTime.setText(mMoveHospital.getBusiness_hours());
                        }else {
                            time.setVisibility(View.GONE);
                        }
                        String title="";
                        if (mMoveHospital.getTitle().equals("1")){
                            title="主任医师";
                        }else if (mMoveHospital.getTitle().equals("2")){
                            title="副主任医师";
                        }else  if (mMoveHospital.getTitle().equals("3")){
                            title="主治医师";
                        }else  if (mMoveHospital.getTitle().equals("1")){
                            title="住院医师";
                        }
                        la=mMoveHospital.getLat();
                        ln=mMoveHospital.getLng();
                        address.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (la!=null&&ln!=null&&!la.equals("")&&!ln.equals("")) {
                                    Intent in = new Intent(HospitalDetailActivity.this, ClinicLocationActivity.class);
                                    in.putExtra("la", la);
                                    in.putExtra("ln", ln);
                                    in.putExtra("address", mMoveHospital.getAddress());
                                    startActivity(in);
                                }
                            }
                        });
                        doctor_title.setText(title);
                        mTitle.setText(mMoveHospital.getClinic_name());
                        mClinicHeadImg.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine" + mMoveHospital.getLogo_img_path()));
                        mBrief.setText("主治："+mMoveHospital.getGood_subjects());
                        mContent.setText(mMoveHospital.getInfo());
                        mDocLogo.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine" + mMoveHospital.getMedicine_logo_img()));
                        mDocName.setText(mMoveHospital.getDoc_name());
                        mDocDept.setText(mMoveHospital.getDept_name());
                        mHospitalName.setText("医院："+ mMoveHospital.getHospital_name());
                        mCount.setText("查看全部" + mMoveHospital.getMedicine_num().get(0).getResult() + "人");
                        if (mMoveHospital.getMedicine_num() != null) {
                            switch (mMoveHospital.getMedicine_num().size()) {
                                case 1:
                                    mCount.setText("查看全部" + mMoveHospital.getMedicine_num().get(0).getResult() + "人");
                                    switch (Integer.parseInt(mMoveHospital.getMedicine_num().get(0).getTitle())) {
                                        case 1:
                                            mCircleProgress1.setProgress(100);
                                            break;
                                        case 2:
                                            mCircleProgress2.setProgress(100);
                                            break;
                                        case 3:
                                            mCircleProgress3.setProgress(100);
                                            break;
                                        case 4:
                                            mCircleProgress4.setProgress(100);
                                            break;
                                    }
                                    break;
                                case 2:
                                    int count = Integer.parseInt(mMoveHospital.getMedicine_num().get(0).getResult())
                                            + Integer.parseInt(mMoveHospital.getMedicine_num().get(1).getResult());
                                    mCount.setText("查看全部" + String.valueOf(count) + "人");
                                    for (MoveHospital.Medicine medicine : mMoveHospital.getMedicine_num()) {
                                        DecimalFormat df = new DecimalFormat("0.00");
                                        String s = df.format((float) (Integer.parseInt(medicine.getResult())) / count);
                                        double a = Double.parseDouble(s);
                                        switch (Integer.parseInt(medicine.getTitle())) {
                                            case 1:
                                                mCircleProgress1.setProgress((int) (a * 100));
                                                break;
                                            case 2:
                                                mCircleProgress2.setProgress((int) (a * 100));
                                                break;
                                            case 3:
                                                mCircleProgress3.setProgress((int) (a * 100));
                                                break;
                                            case 4:
                                                mCircleProgress4.setProgress((int) (a * 100));
                                                break;
                                        }
                                    }

                                    break;
                                case 3:
                                    int count1 = Integer.parseInt(mMoveHospital.getMedicine_num().get(0).getResult())
                                            + Integer.parseInt(mMoveHospital.getMedicine_num().get(1).getResult())
                                            + Integer.parseInt(mMoveHospital.getMedicine_num().get(2).getResult());
                                    mCount.setText("查看全部" + String.valueOf(count1) + "人");
                                    for (MoveHospital.Medicine medicine : mMoveHospital.getMedicine_num()) {
                                        DecimalFormat df = new DecimalFormat("0.00");
                                        String s = df.format((float) (Integer.parseInt(medicine.getResult())) / count1);
                                        double a = Double.parseDouble(s);
                                        switch (Integer.parseInt(medicine.getTitle())) {
                                            case 1:
                                                mCircleProgress1.setProgress((int) (a * 100));
                                                break;
                                            case 2:
                                                mCircleProgress2.setProgress((int) (a * 100));
                                                break;
                                            case 3:
                                                mCircleProgress3.setProgress((int) (a * 100));
                                                break;
                                            case 4:
                                                mCircleProgress4.setProgress((int) (a * 100));
                                                break;
                                        }
                                    }
                                    break;
                                case 4:
                                    int count2 = Integer.parseInt(mMoveHospital.getMedicine_num().get(0).getResult())
                                            + Integer.parseInt(mMoveHospital.getMedicine_num().get(1).getResult())
                                            + Integer.parseInt(mMoveHospital.getMedicine_num().get(2).getResult())
                                            + Integer.parseInt(mMoveHospital.getMedicine_num().get(3).getResult());
                                    mCount.setText("查看全部" + String.valueOf(count2) + "人");
                                    for (MoveHospital.Medicine medicine : mMoveHospital.getMedicine_num()) {
                                        DecimalFormat df = new DecimalFormat("0.00");
                                        String s = df.format((float) (Integer.parseInt(medicine.getResult())) / count2);
                                        double a = Double.parseDouble(s);
                                        switch (Integer.parseInt(medicine.getTitle())) {
                                            case 1:
                                                mCircleProgress1.setProgress((int) (a * 100));
                                                break;
                                            case 2:
                                                mCircleProgress2.setProgress((int) (a * 100));
                                                break;
                                            case 3:
                                                mCircleProgress3.setProgress((int) (a * 100));
                                                break;
                                            case 4:
                                                mCircleProgress4.setProgress((int) (a * 100));
                                                break;
                                        }
                                    }
                                    break;
                            }
                        }

                    }
                }
            });
        }
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mCircleProgress1.setProgress(0);
        mCircleProgress2.setProgress(0);
        mCircleProgress3.setProgress(0);
        mCircleProgress4.setProgress(0);
    }

    @OnClick(R.id.left_btn)
    void back() {
        finish();
    }

    @OnClick(R.id.contentRela)
    void goToBrief() {
        Bundle bundle = new Bundle();
        bundle.putString("CLINICID", mClinicId);
        bundle.putString("BRIEF", mMoveHospital.getInfo());
        UIKit.open(HospitalDetailActivity.this, HospitalBriefActivity.class, bundle);
    }

    @OnClick(R.id.members)
    void members() {
        Bundle bundle = new Bundle();
        bundle.putString("CLINICID", mClinicId);
        UIKit.open(HospitalDetailActivity.this, HospitalMembersActivity.class, bundle);
    }

    /**
     * 立即咨询
     */
    @OnClick(R.id.submit)
    void submit() {
        List<MoveHospital.DoctorTime> doctors = new ArrayList<>();
        doctors.addAll(mMoveHospital.getMedicine_rota());
        if (doctors.size()==0){
            Toaster.showShort(HospitalDetailActivity.this, "暂无值班医生！");
        }else {
            for (final MoveHospital.DoctorTime doctorTime : doctors) {
                firstPagerMager.getSystime(new SubscriberAdapter<Resultt>() {
                    @Override
                    public void success(Resultt resultt) {
                        super.success(resultt);
                        String data = resultt.getData();
                        Date date = DateKit.parse(data);
                        if (DateKit.isInDate(date, doctorTime.getStart_time(), doctorTime.getEnd_time())) {
                            haveDoctor = true;
                            //获取环信用户名
                            firstPagerMager.getDoctorEMCName(doctorTime.getMember_id(), new SubscriberAdapter<Result>() {
                                @Override
                                public void success(Result result) {
                                    super.success(result);
                                    Bundle args = new Bundle();
                                    args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                                    args.putString(EaseConstant.EXTRA_USER_ID, result.getHuanxin_username());
                                    args.putString("CLINIC_ID", mMoveHospital.getId());
                                    args.putString("DOC_NAME", doctorTime.getDoc_name());
                                    UIKit.open(HospitalDetailActivity.this, ConversationActivity.class, args);
                                }
                            });
                        }
                        if (!haveDoctor) {
                            Toaster.showShort(HospitalDetailActivity.this, "暂无值班医生！");
                        }
                    }
                });

            }
        }

    }


}
