package com.wonhx.patient.app.manager.doctortab;

import com.wonhx.patient.app.AppException;
import com.wonhx.patient.app.manager.DoctorTabManger;
import com.wonhx.patient.app.model.ListProResultNormal;
import com.wonhx.patient.app.model.LookDoctor;
import com.wonhx.patient.http.Rest;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2017/4/13.
 */

public class DoctorTabMangerImal implements DoctorTabManger {
    @Override
    public Subscription getServicelist(final String member_id, Subscriber subscriber) {
        return Observable.create(new Observable.OnSubscribe< ListProResultNormal<LookDoctor>>(){
            @Override
            public void call(Subscriber<? super  ListProResultNormal<LookDoctor>> subscriber) {
                try {
                    ListProResultNormal<LookDoctor> result=restServer.getlookhistory(member_id);
                    if (!result.getCode())
                        throw AppException.custom(result.getMsg());
                    subscriber.onNext(result);
                } catch (AppException e) {
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }
    DoctorTabMangerImal.RestServer restServer= Rest.create(DoctorTabMangerImal.RestServer.class);
    interface  RestServer{
        /**
         * 获取患者关注医生的列表
         * @param member_id
         * @return
         */
        @FormUrlEncoded
        @POST("/patient_concern/doctor_list")
        ListProResultNormal<LookDoctor> getlookhistory(@Field("member_id") String member_id);
    }
}
