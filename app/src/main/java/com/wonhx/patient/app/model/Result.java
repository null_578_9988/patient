package com.wonhx.patient.app.model;

/**
 * Created by apple on 17/3/21.
 *
 */
public class Result {
    public String code;
    public String msg;
    public String content;
    public String data;
    public String huanxin_username;
    public String member_id;
    public String doc_name;

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getHuanxin_username() {
        return huanxin_username;
    }

    public void setHuanxin_username(String huanxin_username) {
        this.huanxin_username = huanxin_username;
    }
    public String tn;

    public String getTn() {
        return tn;
    }

    public void setTn(String tn) {
        this.tn = tn;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean getCode() {
        return code.equals("0");
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
