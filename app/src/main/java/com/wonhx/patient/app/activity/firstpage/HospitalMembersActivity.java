package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.base.BaseActivity;
import com.facebook.drawee.view.SimpleDraweeView;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.ClinicMember;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.kit.UIKit;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by nsd on 2017/5/19.
 * 诊所团队成员
 */

public class HospitalMembersActivity extends BaseActivity {
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.listview)
    ListView mListView;
    QuickAdapter<ClinicMember> mListAdapter;
    String mClinicId;
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    List<ClinicMember> mListDatas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_members);
    }
    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("诊所成员");
        mListAdapter = new QuickAdapter<ClinicMember>(this,R.layout.listitem_clinic_member) {
            @Override
            protected void convert(BaseAdapterHelper helper, ClinicMember item) {
                SimpleDraweeView view = helper.getView(R.id.logo);
                view.setImageURI(Uri.parse(Constants.REST_ORGIN+"/emedicine"+item.getMedicine_logo_img()));
                helper.setText(R.id.doctor_name,item.getDoc_name())
                        .setText(R.id.doctor_dept,item.getDept_name())
                        .setText(R.id.doctor_hospitalName,item.getHospital_name())
                        .setText(R.id.doctor_title,item.getTitle())
                .setText(R.id.doctor_goodSubjects,item.getGood_subjects()) ;
                if (item.getRota_time()!=null&&!item.getRota_time().equals("")){
                   helper. setText(R.id.tv_time,"值班时间："+item.getRota_time());
                }else {
                    helper. setText(R.id.tv_time,"值班时间：还未安排值班时间");
                }

            }
        };
        mListView.setAdapter(mListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ClinicMember member = mListAdapter.getItem(position);
                Bundle bundle = new Bundle();
                bundle.putString("member_id",member.getMember_id());
                UIKit.open(HospitalMembersActivity.this,DoctorDetialActivity.class,bundle);
            }
        });
    }

    /**
     * 初始化数据
     */
    @Override
    protected void onInitData() {
        super.onInitData();
        mClinicId = getIntent().getStringExtra("CLINICID");
        if (mClinicId!=null){
            firstPagerMager.getClinicMembers(mClinicId,new SubscriberAdapter<ListProResult<ClinicMember>>(){
                @Override
                public void success(ListProResult<ClinicMember> result) {
                    super.success(result);
                    mListDatas.addAll(result.getData());
                    mListAdapter.replaceAll(mListDatas);
                }
            });
        }
    }

    @OnClick(R.id.left_btn)
    void back(){
        finish();
    }
}
