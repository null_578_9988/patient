package com.wonhx.patient.app.activity.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;

import com.tencent.mm.opensdk.constants.Build;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.unionpay.UPPayAssistEx;
import com.unionpay.uppay.PayActivity;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Account;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.WeXinBean;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.GYcityPopupWindow;
import com.wonhx.patient.view.PayResult;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class OrderPayActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.jine)
    TextView jine;
    @InjectView(R.id.diqu)
    TextView diqu;
    @InjectView(R.id.ed)
    EditText ed;
    int payType=0;
    String keyong="",orderId="",totalPrice="",zhifubaopayInfo="",yinlianpayinfo="",member_id="";
    private String mMode = "00";//设置测试模式:01为测试 00为正式环境
    private String tn="";
    private IWXAPI api;
    UserManager userManager=new UserManagerImpl();
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Intent intent=new Intent(OrderPayActivity.this,PayFinishActivity.class);
                        intent.putExtra("price",totalPrice);
                        intent.putExtra("flag","1");
                        intent.putExtra("msg","支付成功");
                        startActivity(intent);
                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(OrderPayActivity.this, "支付结果确认中", Toast.LENGTH_SHORT).show();
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            Intent intent=new Intent(OrderPayActivity.this,PayFinishActivity.class);
                            intent.putExtra("price",totalPrice);
                            intent.putExtra("flag","0");
                            intent.putExtra("msg","支付失败");
                            startActivity(intent);
                        }
                    }
                    break;
                }
                case  2:{
                    if (msg.obj == null || ((String) msg.obj).length() == 0) {
                        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(OrderPayActivity.this);
                        builder.setTitle("错误提示");
                        builder.setMessage("网络连接失败,请重试!");
                        builder.setNegativeButton("确定",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.create().show();
                    } else {
                        tn = (String) msg.obj;
                        doStartUnionPayPlugin(OrderPayActivity.this, tn, mMode);
                    }
                }
                default:
                    break;
            }

        }
    };

    @Override
    protected void onInitView() {
        super.onInitView();
        title.setText("购药清单结算");
        title.setTextColor(getResources().getColor(R.color.colorAccent));
    }
    public void doStartUnionPayPlugin(Activity activity, String tn, String mode) {
        UPPayAssistEx.startPayByJAR(activity, PayActivity.class, null, null,
                tn, mode);
    }
    MyReceiver myReceiver;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            unregisterReceiver(myReceiver);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_pay);
        ButterKnife.inject(this);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("ExitApp");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String memberId) {
                finish();
            }
        });
        getAccount();
        Intent intent=getIntent();
        totalPrice=intent.getStringExtra("totalPrice");
        orderId=intent.getStringExtra("orderId");
        member_id=intent.getStringExtra("member_id");
        jine.setText("￥"+totalPrice);
        api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);
        api.registerApp(Constants.APP_ID);
    }
    @OnClick({R.id.left_btn, R.id.jiesuan,R.id.rl_diqu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.jiesuan:
                commitAdd();
                break;
            case R.id.rl_diqu:
                new GYcityPopupWindow(OrderPayActivity.this,diqu).showAtLocation(view,Gravity.BOTTOM,0,0);
                break;
        }
    }
    private void commitAdd() {
        if (!TextUtils.isEmpty(diqu.getText().toString())&&!TextUtils.isEmpty(ed.getText().toString())) {
            userManager.commitAdd( orderId, diqu.getText().toString() +ed.getText().toString(), new SubscriberAdapter<Result>() {
                @Override
                public void success(Result result) {
                    super.success(result);
                    pay_type();
                }
            });
        }else {
            Toaster.showShort(OrderPayActivity.this,"地区与详细地址都不能为空！");
        }
    }

    private void getAccount() {
        userManager.getaccount(AbSharedUtil.getString(OrderPayActivity.this,"userId"),new SubscriberAdapter<ProResult<Account>>(){
            @Override
            public void success(ProResult<Account> accountProResult) {
                super.success(accountProResult);
                keyong=accountProResult.getData().getAccount_balance();
            }
        });
    }

    private void pay_type() {
        View view = LayoutInflater.from(OrderPayActivity.this).inflate(R.layout.order_pay, null);
        final Dialog dialog = new AlertDialog.Builder(this ).create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.mydialogstyle);  //添加动画
        window.setBackgroundDrawable(new ColorDrawable());
        dialog.show();
        window.setContentView(view);
        ImageView iv_cancle= (ImageView) view.findViewById(R.id.cancle);
        iv_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        LinearLayout ll_yue= (LinearLayout) view.findViewById(R.id.four_pay);
        LinearLayout ll_zhifubao= (LinearLayout) view.findViewById(R.id.one_pay);
        LinearLayout ll_weixin= (LinearLayout) view.findViewById(R.id.second_pay);
        LinearLayout ll_yinhangka= (LinearLayout) view.findViewById(R.id.third_pay);
        final TextView yue= (TextView) view.findViewById(R.id.yueeaa);
        final TextView zhifubao= (TextView) view.findViewById(R.id.zhifubaos);
        final TextView weixin= (TextView) view.findViewById(R.id.tv_weixins);
        final TextView yinhangka= (TextView) view.findViewById(R.id.yinlian);
        final TextView keyongyue= (TextView) view.findViewById(R.id.keyongyue);
        keyongyue.setText("可用余额："+keyong);
        ll_yue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payType=0;
                yue.setVisibility(View.VISIBLE);
                zhifubao.setVisibility(View.GONE);
                weixin.setVisibility(View.GONE);
                yinhangka.setVisibility(View.GONE);
            }
        });
        ll_zhifubao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payType=1;
                yue.setVisibility(View.GONE);
                zhifubao.setVisibility(View.VISIBLE);
                weixin.setVisibility(View.GONE);
                yinhangka.setVisibility(View.GONE);
            }
        });
        ll_weixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payType=2;
                yue.setVisibility(View.GONE);
                zhifubao.setVisibility(View.GONE);
                weixin.setVisibility(View.VISIBLE);
                yinhangka.setVisibility(View.GONE);
            }
        });
        ll_yinhangka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payType=3;
                yue.setVisibility(View.GONE);
                zhifubao.setVisibility(View.GONE);
                weixin.setVisibility(View.GONE);
                yinhangka.setVisibility(View.VISIBLE);
            }
        });
        TextView tv_commit = (TextView) view.findViewById(R.id.commit);
        tv_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //支付宝
                if (payType==1){
                    userManager.zhifubao(orderId,"B",new SubscriberAdapter<Result>(){
                        @Override
                        public void success(Result result) {
                            super.success(result);
                            zhifubaopayInfo=result.getData();
                            Runnable runnable=new Runnable() {
                                @Override
                                public void run() {
                                    PayTask alipay = new PayTask(OrderPayActivity.this);
                                    // 调用支付接口，获取支付结果
                                    String result = alipay.pay(zhifubaopayInfo, true);
                                    Message msg = new Message();
                                    msg.what = 1;
                                    msg.obj = result;
                                    handler.sendMessage(msg);
                                }
                            };
                            Thread payThread = new Thread(runnable);
                            payThread.start();
                        }
                    });
                    //微信
                }else  if (payType==2){
                    boolean sIsWXAppInstalledAndSupported   = api.isWXAppInstalled()
                            && api.isWXAppSupportAPI();
                    if (sIsWXAppInstalledAndSupported ) {
                        boolean isPaySupported = api.getWXAppSupportAPI() >= Build.PAY_SUPPORTED_SDK_INT;
                        if (isPaySupported) {
                            Log.e("ssss", orderId + "");
                            //Toaster.showShort(OrderPayActivity.this,"暂时未开通！");
                            userManager.weixin(orderId, "B2", new SubscriberAdapter<WeXinBean>() {
                                @Override
                                public void success(WeXinBean result) {
                                    super.success(result);
                                    weixinpay(result);
                                }
                            });
                        }else {
                            Toaster.showShort(OrderPayActivity.this,"此版本还不支持微信支付");
                        }
                    }else {
                        Toaster.showShort(OrderPayActivity.this,"还未安装微信");
                    }
                    //银行卡
                }else if (payType==3){
                    userManager.yinlian(orderId,"B1",new SubscriberAdapter<Result>(){
                        @Override
                        public void success(Result result) {
                            super.success(result);
                            yinlianpayinfo=result.getTn();
                            Runnable runnable=new Runnable() {
                                @Override
                                public void run() {
                                    // 调用支付接口，获取支付结果
                                    Message msg = new Message();
                                    msg.what = 2;
                                    msg.obj = yinlianpayinfo;
                                    handler.sendMessage(msg);
                                }
                            };
                            Thread payThread = new Thread(runnable);
                            payThread.start();

                        }
                    });
                    //余额支付
                }else if (payType==0){
                    userManager.BanlancePay(orderId,AbSharedUtil.getString(OrderPayActivity.this, "userId"),new SubscriberAdapter<Result>(){
                        @Override
                        public void onError(Throwable e) {
                            super.onError(e);
                            Intent intent=new Intent(OrderPayActivity.this,PayFinishActivity.class);
                            intent.putExtra("price",totalPrice);
                            intent.putExtra("flag","0");
                            intent.putExtra("msg","支付失败");
                            startActivity(intent);
                        }
                        @Override
                        public void success(Result result) {
                            super.success(result);
                            Intent intent=new Intent(OrderPayActivity.this,PayFinishActivity.class);
                            intent.putExtra("price",totalPrice);
                            intent.putExtra("flag","1");
                            intent.putExtra("msg","支付成功");
                            startActivity(intent);
                        }
                    });
                }

            }
        });


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        String msg = "";
        /*
         * 支付控件返回字符串:success、fail、cancel 分别代表支付成功，支付失败，支付取消
         */
        String str = data.getExtras().getString("pay_result");
        Log.e("zftphone", "2 "+data.getExtras().getString("merchantOrderId"));
        if (str.equalsIgnoreCase("success")) {
            msg = "支付成功！";
            Intent intent=new Intent(OrderPayActivity.this,PayFinishActivity.class);
            intent.putExtra("price",totalPrice);
            intent.putExtra("flag","1");
            intent.putExtra("msg",msg);
            startActivity(intent);
        } else if (str.equalsIgnoreCase("fail")) {
            msg = "支付失败！";
            Intent intent=new Intent(OrderPayActivity.this,PayFinishActivity.class);
            intent.putExtra("price",totalPrice);
            intent.putExtra("flag","0");
            intent.putExtra("msg",msg);
            startActivity(intent);
        } else if (str.equalsIgnoreCase("cancel")) {
            msg = "用户取消了支付";
            Intent intent=new Intent(OrderPayActivity.this,PayFinishActivity.class);
            intent.putExtra("price",totalPrice);
            intent.putExtra("flag","0");
            intent.putExtra("msg",msg);
            startActivity(intent);
        }
        //支付完成,处理自己的业务逻辑!
    }
    public void weixinpay(WeXinBean result) {
        PayReq payreq=new PayReq();
        payreq.appId=result.getAppid();
        payreq.partnerId=result.getPartnerid();
        payreq.prepayId=result.getPrepayid();
        payreq.nonceStr=result.getNoncestr();
        payreq.timeStamp=result.getTimestamp();
        payreq.packageValue="Sign=WXPay";
        payreq.sign=result.getSign();
        Constants.flag=2;
        api.sendReq(payreq);
    }

}
