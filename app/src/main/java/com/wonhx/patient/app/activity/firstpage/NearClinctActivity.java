package com.wonhx.patient.app.activity.firstpage;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.UiSettings;
import com.baidu.mapapi.model.LatLng;
import com.facebook.drawee.view.SimpleDraweeView;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.baidu.map.BikeInfo;
import com.wonhx.patient.app.baidu.map.MyOrientationListener;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.NearClinct;
import com.wonhx.patient.kit.UIKit;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;



public class NearClinctActivity extends BaseActivity {

    private List<NearClinct>nearClincts=new ArrayList<>();
    @InjectView(R.id.mapview)
    MapView mMapView;
    public static NearHomeDoctorActivity instance = null;
    LocationClient mLocClient;
    public MyLocationListenner myListener = new MyLocationListenner();
    static BDLocation lastLocation = null;
    ProgressDialog progressDialog;
    @InjectView(R.id.logo)
    SimpleDraweeView logo;
    @InjectView(R.id.doctor_name)
    TextView doctorName;
    @InjectView(R.id.doctor_add)
    TextView doctorAdd;
    @InjectView(R.id.rl_info)
    RelativeLayout rlInfo;
    private BaiduMap mBaiduMap;
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();

    @OnClick(R.id.rl_info)
    public void info() {
        Bundle bundle = new Bundle();
        bundle.putString("CLINICID", bikeInfo.getId());
        UIKit.open(this, HospitalDetailActivity.class, bundle);
    }
    //点击定位按钮重新定位
    @OnClick(R.id.dingewi)
    void dingwei(){
        mBaiduMap.setMyLocationEnabled(true);
        mLocClient.start();
    }
    public class BaiduSDKReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            String s = intent.getAction();
            String st1 = getResources().getString(com.hyphenate.easeui.R.string.Network_error);
            if (s.equals(SDKInitializer.SDK_BROADTCAST_ACTION_STRING_PERMISSION_CHECK_ERROR)) {
                String st2 = getResources().getString(com.hyphenate.easeui.R.string.please_check);
                Toast.makeText(instance, st2, Toast.LENGTH_SHORT).show();
            } else if (s.equals(SDKInitializer.SDK_BROADCAST_ACTION_STRING_NETWORK_ERROR)) {
                Toast.makeText(instance, st1, Toast.LENGTH_SHORT).show();
            }
        }
    }
    @InjectView(R.id.zoomin)
    Button zoomInBtn;
    @InjectView(R.id.zoomout)
    Button zoomOutBtn;
    @OnClick(R.id.zoomin)
    void fangda(){
        isfirst=false;
        currentZoomLevel = mBaiduMap.getMapStatus().zoom;
        if(currentZoomLevel<=22){
            mBaiduMap.setMapStatus(MapStatusUpdateFactory.zoomIn());
            zoomOutBtn.setEnabled(true);
        }else{
            Toast.makeText(this, "已经放大的最大了", Toast.LENGTH_SHORT).show();
            zoomInBtn.setEnabled(false);
        }
    }
    @OnClick(R.id.zoomout)
    void suoxiao(){
        isfirst=false;
        currentZoomLevel = mBaiduMap.getMapStatus().zoom;
        if(currentZoomLevel>4){
            mBaiduMap.setMapStatus(MapStatusUpdateFactory.zoomOut());
            zoomInBtn.setEnabled(true);
        }else{
            zoomOutBtn.setEnabled(false);
            Toast.makeText(this, "已经缩小的最小了", Toast.LENGTH_SHORT).show();
        }
    }

    private BaiduSDKReceiver mBaiduReceiver;
    NearClinct bikeInfo;
    //自定义图标
    private BitmapDescriptor doctorimage,locationimage;
    private UiSettings uiSettings;
    private float currentZoomLevel;
    private float maxZoomLevel;
    private float minZoomLevel;
    boolean isfirst=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SDKInitializer.initialize(getApplicationContext());
        setContentView(R.layout.activity_near_clinct);
        // 不显示地图上比例尺
        mMapView.showScaleControl(false);
        // 不显示地图缩放控件（按钮控制栏）
        mMapView.showZoomControls(false);
        // 隐藏百度的LOGO
        View child = mMapView.getChildAt(1);
        if (child != null && (child instanceof ImageView || child instanceof ZoomControls)) {
            child.setVisibility(View.INVISIBLE);
        }
        MyLocationConfiguration.LocationMode mCurrentMode = MyLocationConfiguration.LocationMode.FOLLOWING;
        mBaiduMap = mMapView.getMap();
        uiSettings=mBaiduMap.getUiSettings();
        uiSettings.setZoomGesturesEnabled(true);
        uiSettings.setScrollGesturesEnabled(true);
        mMapView.setLongClickable(true);
        // 开启定位图层
        mBaiduMap.setMyLocationEnabled(true);
        mBaiduMap.setMyLocationConfigeration(new MyLocationConfiguration(mCurrentMode, true, null));
        showMapWithLocationClient();
        doctorimage = BitmapDescriptorFactory.fromResource(R.mipmap.doctorshome);
        locationimage = BitmapDescriptorFactory.fromResource(R.mipmap.dingewi);
        IntentFilter iFilter = new IntentFilter();
        iFilter.addAction(SDKInitializer.SDK_BROADTCAST_ACTION_STRING_PERMISSION_CHECK_ERROR);
        iFilter.addAction(SDKInitializer.SDK_BROADCAST_ACTION_STRING_NETWORK_ERROR);
        mBaiduReceiver = new BaiduSDKReceiver();
        registerReceiver(mBaiduReceiver, iFilter);
        initMarkerClickEvent();
        //地图拖拽的时候监听
        mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {

            @Override
            public void onMapStatusChangeStart(MapStatus mapStatus) {

            }

            @Override
            public void onMapStatusChange(MapStatus arg0) {
                currentZoomLevel = arg0.zoom;
                if(currentZoomLevel>22){
                    zoomInBtn.setEnabled(false);
                }
                else if(currentZoomLevel<3.5){
                    zoomOutBtn.setEnabled(false);
                }
                else{
                    zoomInBtn.setEnabled(true);
                    zoomOutBtn.setEnabled(true);
                }
                maxZoomLevel = mBaiduMap.getMaxZoomLevel();
                minZoomLevel = mBaiduMap.getMinZoomLevel();

                currentZoomLevel = arg0.zoom;

                if (currentZoomLevel>=maxZoomLevel) {
                    currentZoomLevel = maxZoomLevel;
                }
                else if(currentZoomLevel<=minZoomLevel){
                    currentZoomLevel = minZoomLevel;
                }


            }

            @Override
            public void onMapStatusChangeFinish(MapStatus mapStatus) {
                mBaiduMap.clear();
                LatLng _latLng = mapStatus.target;
//                //定义地图状态
                if (isfirst){
                    MapStatus mMapStatus = new MapStatus.Builder()
                            .target(_latLng)
                            .zoom(18)
                            .build();
                    //定义MapStatusUpdate对象，以便描述地图状态将要发生的变化
                    MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mMapStatus);
                    //改变地图状态
                    mBaiduMap.setMapStatus(mMapStatusUpdate);
                }else {
                    MapStatus mMapStatus = new MapStatus.Builder()
                            .target(_latLng)
                            .zoom((int)currentZoomLevel)
                            .build();
                    //定义MapStatusUpdate对象，以便描述地图状态将要发生的变化
                    MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mMapStatus);
                    //改变地图状态
                    mBaiduMap.setMapStatus(mMapStatusUpdate);

                }
                //构建MarkerOption，用于在地图上添加Marker
                MarkerOptions options = new MarkerOptions().position(_latLng)
                        .zIndex(18)
                        .icon(locationimage);
                // 在地图上添加Marker，并显示
                mBaiduMap.addOverlay(options);
                firstPagerMager.getNearClic(_latLng.longitude + "", _latLng.latitude + "", new SubscriberAdapter<ListProResult<NearClinct>>() {
                    @Override
                    public void success(ListProResult<NearClinct> bikeInfoListProResult) {
                        super.success(bikeInfoListProResult);
                        nearClincts.clear();
                        nearClincts.addAll(bikeInfoListProResult.getData());
                        addInfosOverlay(nearClincts);
                    }
                });

            }
        });


    }

    private void initMarkerClickEvent() {
        // 对Marker的点击
        mBaiduMap.setOnMarkerClickListener(new BaiduMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                rlInfo.setVisibility(View.VISIBLE);
                // 获得marker中的数据
                if (marker != null && marker.getExtraInfo() != null) {
                    NearClinct bikeInfo = (NearClinct) marker.getExtraInfo().get("info");
                    if (bikeInfo != null)
                        updateBikeInfo(bikeInfo);
                }
                return true;
            }
        });
    }

    private void updateBikeInfo(NearClinct bikeInfo) {
        Log.e("bikeinfo", bikeInfo.toString());
        doctorName.setText(bikeInfo.getName());
        doctorAdd.setText(bikeInfo.getAddress());
        logo.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine" + bikeInfo.getLogo_img_path()));
        rlInfo.setVisibility(View.VISIBLE);
        this.bikeInfo=bikeInfo;

    }

    private void showMapWithLocationClient() {
        String str1 = getResources().getString(com.hyphenate.easeui.R.string.Making_sure_your_location);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(str1);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                finish();
            }
        });
        progressDialog.show();
        mLocClient = new LocationClient(this);
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true); // 打开gps
        option.setCoorType("bd09ll"); // 设置坐标类型
        option.setScanSpan(5000);//设置onReceiveLocation()获取位置的频率
        option.setIsNeedAddress(true);//如想获得具体位置就需要设置为true
        mLocClient.setLocOption(option);
        mLocClient.registerLocationListener(myListener);
    }


    public void addInfosOverlay(List<NearClinct> infos) {
        LatLng latLng = null;
        OverlayOptions overlayOptions = null;
        Marker marker = null;
        for (NearClinct info : infos) {
            // 位置
            latLng = new LatLng(Double.parseDouble(info.getLat()), Double.parseDouble(info.getLng()));
            // 图标
            overlayOptions = new MarkerOptions().position(latLng)
                    .icon(doctorimage).zIndex(5);
            marker = (Marker) (mBaiduMap.addOverlay(overlayOptions));
            Bundle bundle = new Bundle();
            bundle.putSerializable("info", info);
            marker.setExtraInfo(bundle);
        }
    }

    private void addOverLayout(String _latitude, String _longitude) {
        firstPagerMager.getNearClic(_longitude + "", _latitude + "", new SubscriberAdapter<ListProResult<NearClinct>>() {
            @Override
            public void success(ListProResult<NearClinct> bikeInfoListProResult) {
                super.success(bikeInfoListProResult);
                nearClincts.clear();
                nearClincts.addAll(bikeInfoListProResult.getData());
                addInfosOverlay(nearClincts);

            }
        });


    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        if (mLocClient != null) {
            mLocClient.stop();
        }
        super.onPause();
        lastLocation = null;
    }

    @Override
    protected void onResume() {
        mMapView.onResume();
        if (mLocClient != null) {
            mLocClient.start();
        }
        super.onResume();
    }


    /**
     * format new location to string and show on screen
     */
    public class MyLocationListenner implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            if (location == null) {
                return;
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }

            //添加方向信息
            MyLocationData locData = new MyLocationData.Builder()
                    .accuracy(location.getRadius())
                    .latitude(location.getLatitude())
                    .longitude(location.getLongitude()).build();
            mBaiduMap.setMyLocationData(locData);
            lastLocation = location;
            addOverLayout(lastLocation.getLatitude() + "", lastLocation.getLongitude() + "");
        }

        public void onReceivePoi(BDLocation poiLocation) {
            if (poiLocation == null) {
                return;
            }
        }
    }

    @OnClick(R.id.back)
    void back() {
        finish();
    }
}
