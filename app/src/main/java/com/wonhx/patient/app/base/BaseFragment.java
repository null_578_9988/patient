package com.wonhx.patient.app.base;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.orhanobut.logger.Logger;
import com.wonhx.patient.app.AppContext;
import com.wonhx.patient.app.AppException;
import com.wonhx.patient.kit.Toaster;

import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nsd on 2016/3/15.
 * 基Fragment
 */
public class BaseFragment extends Fragment {

    /**
     * 全局Context
     */
    protected AppContext appContext;
    /**
     * 传入参数
     */
    protected Bundle mExtras;
    /**
     * 碎片管理器
     */
    protected FragmentManager mFragmentManager;
    private CompositeSubscription mSubscriptions;
    /**
     * 请求progressDialog
     */
    private Dialog progressDialog = null;
    /**
     * 是否显示Dialog
     */
    public boolean showDialog = true;


    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 获取全局Context
        appContext = (AppContext) getActivity().getApplication();
        mFragmentManager = getFragmentManager();
        mExtras = new Bundle();
        Bundle fragment = getArguments();
        Activity activittest = getActivity();
        Bundle activity = getActivity().getIntent().getExtras();
        if (activity != null) // 先获取Activity参数
            mExtras.putAll(activity);
        if (fragment != null) // 再获取Fragment参数
            mExtras.putAll(fragment);
        onInitArgs(); // onInitArgs
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSubscriptions = new CompositeSubscription();
        onInitView(); // onInitView
        onInitData(); // onInitData
    }


    /**
     * InitArgs
     */
    protected void onInitArgs() {
    }

    /**
     * InitView
     */
    protected void onInitView() {
    }

    /**
     * InitData
     */
    protected void onInitData() {
    }

    @Override
    public void onDestroyView() {
        if (mSubscriptions != null)
            mSubscriptions.unsubscribe();
        super.onDestroyView();
    }



    /**
     * RxSubscriberAdapter
     *
     * @param <T>
     */
    protected class SubscriberAdapter<T> extends Subscriber<T> {
        public SubscriberAdapter() {
            if (mSubscriptions == null)
                mSubscriptions = new CompositeSubscription();
            mSubscriptions.add(this);
        }

        public void success(T t) {
        }

        @Override
        public void onNext(T t) {
            if (showDialog)
                dissmissProgressDialog();
            success(t);
        }

        @Override
        public void onStart() {
            if (showDialog)
                showProgressDialog();
            // showLoadingDialog("正在加载中");
        }

        @Override
        public void onError(Throwable e) {
            if (showDialog)
                dissmissProgressDialog();
//            Logger.e((Exception) e);
            if (e instanceof AppException) {
                AppException ex = (AppException) e;
                if (ex.getType() == AppException.TYPE_CUSTOM)
                    //showAlertDialog("提示", e.getMessage());
                    Toaster.showShort(getActivity(), e.getMessage());
                else
                    ex.makeToast(getActivity());
                return;
            }
        }

        @Override
        public void onCompleted() {
        }
    }

    /**
     * 显示进度框
     */
    public void showProgressDialog() {
//        if (progressDialog == null) {
//            progressDialog = new Dialog(getActivity(), R.style.progress_dialog);
//            progressDialog.setContentView(R.layout.dialog_dialog);
//            progressDialog.setCancelable(true);
//            Window dialogWindow = progressDialog.getWindow();
//            dialogWindow.setBackgroundDrawableResource(android.R.color.transparent);
//            dialogWindow.setGravity(Gravity.TOP);
//            progressDialog.show();
//        } else if (!progressDialog.isShowing()) {
//            progressDialog.show();
//        }
    }

    /**
     * 隐藏进度框
     */
    public void dissmissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /**
     * 设置 是否显示dialog
     *
     * @param showDialog
     */
    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
