package com.wonhx.patient.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wonhx.patient.R;

import java.util.List;


public class Zhensuoitema extends BaseAdapter {

    private List<String> data;
    int mSelect = 0;   //选中项
    /**
     * LayoutInflater 类是代码实现中获取布局文件的主要形式 LayoutInflater layoutInflater =
     * LayoutInflater.from(context); View convertView =
     * layoutInflater.inflate();
     * LayoutInflater的使用,在实际开发种LayoutInflater这个类还是非常有用的,它的作用类似于 findViewById(),
     * 不同点是LayoutInflater是用来找layout下xml布局文件，并且实例化！ 而findViewById()是找具体xml下的具体
     * widget控件(如:Button,TextView等)。
     */
    private LayoutInflater layoutInflater;
    private Context context;
    public Zhensuoitema(Context context,
                        List<String > data) {
        this.context = context;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        if (data != null && data.size() != 0) {
            return data.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (data != null && data!= null) {
            return data.get(position);
        } else {
            return null;
        }
    }
    public void changeSelected(int positon){ //刷新方法
        if(positon != mSelect){
            mSelect = positon;
            notifyDataSetChanged();
        }
    }
    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        // 组装数据
        if (convertView == null) {
            convertView = this.layoutInflater.inflate(R.layout.doctor_item, null);
            holder.tv_name= (TextView) convertView.findViewById(R.id.doctor);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(mSelect==position){
            holder.tv_name.setBackgroundResource(R.color.white);  //选中项背景
        }else{
            holder.tv_name.setBackgroundResource(R.color.bgs);  //其他项背景
        }
        holder.tv_name.setText(data.get(position));
        return convertView;
    }

    public class ViewHolder {
        TextView tv_name; // 名字

    }
}