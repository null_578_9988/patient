package com.wonhx.patient.app.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.model.Tuijiankeshi;

import java.util.List;


public class TuijianKeshiAbstratorInfo extends BaseAdapter {

    private List<Tuijiankeshi> data;
    /**
     * LayoutInflater 类是代码实现中获取布局文件的主要形式 LayoutInflater layoutInflater =
     * LayoutInflater.from(context); View convertView =
     * layoutInflater.inflate();
     * LayoutInflater的使用,在实际开发种LayoutInflater这个类还是非常有用的,它的作用类似于 findViewById(),
     * 不同点是LayoutInflater是用来找layout下xml布局文件，并且实例化！ 而findViewById()是找具体xml下的具体
     * widget控件(如:Button,TextView等)。
     */
    private LayoutInflater layoutInflater;
    private Context context;
    // 适配显示的图片数组
    private int layoutId;

    public TuijianKeshiAbstratorInfo(Context context) {

        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public TuijianKeshiAbstratorInfo(Context context, List<Tuijiankeshi > data) {

        this.context = context;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public TuijianKeshiAbstratorInfo(Context context, int layoutId,
                                     List<Tuijiankeshi > data) {
        this.context = context;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
        this.layoutId = layoutId;
    }

    public void setData( List<Tuijiankeshi > data) {
        this.data = data;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (data != null && data.size() != 0) {
            return data.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (data != null && data!= null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        // 组装数据
        if (convertView == null) {
            convertView = this.layoutInflater.inflate(this.layoutId, null);
            holder.tv_name= (TextView) convertView.findViewById(R.id.name);
            holder.iv_head= (SimpleDraweeView) convertView.findViewById(R.id.image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tv_name.setText(data.get(position).getDeptname());
        holder.iv_head.setImageURI(Uri.parse(Constants.REST_ORGIN+"/emedicine/pub/dept/"+data.get(position).getDeptid()+".png"));
        return convertView;
    }

    public class ViewHolder {
        TextView tv_name; // 名字
        SimpleDraweeView iv_head;

    }
}