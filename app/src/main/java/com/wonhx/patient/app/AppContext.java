package com.wonhx.patient.app;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.SaveCallback;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.orhanobut.logger.LogLevel;
import com.orhanobut.logger.Logger;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.wonhx.patient.R;
import com.wonhx.patient.app.ease.EaseHelper;
import com.wonhx.patient.app.model.UserInfo;
import com.wonhx.patient.db.DbKit;
import com.wonhx.patient.http.Config;
import com.wonhx.patient.http.RestKit;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.update.lib.UpdateConfig;
import com.wonhx.patient.update.lib.callback.UpdateDownloadCB;
import com.wonhx.patient.update.lib.model.CheckEntity;
import com.wonhx.patient.update.lib.model.HttpMethod;
import com.wonhx.patient.update.lib.model.Update;
import com.wonhx.patient.update.lib.model.UpdateParser;
import com.wonhx.patient.update.lib.strategy.UpdateStrategy;

import org.json.JSONObject;
import org.xutils.x;

import java.io.File;
import java.util.List;

import rx.subjects.BehaviorSubject;

/**
 * 全局应用程序类：用于保存和调用全局应用配置及访问网络数据
 */
public class AppContext extends MultiDexApplication {
    public static Context context;
    // IWXAPI 是第三方app和微信通信的openapi接口
    private IWXAPI api;
    // 网络类型
    public static final int NETTYPE_WIFI = 0x01;
    public static final int NETTYPE_CMWAP = 0x02;
    public static final int NETTYPE_CMNET = 0x03;
    /**
     * 用户信息
     */
    private UserInfo userInfo;
    /**
     * 呼叫页面
     */
    public boolean isCallDoctor = false;
    public Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        //更新
        initUpdater();
        // 注册App异常崩溃处理器
        Thread.setDefaultUncaughtExceptionHandler(AppException.getAppExceptionHandler());
        // Init Logger
        Logger.init("Android-Retrofit").setLogLevel(LogLevel.FULL).setMethodCount(5);
        // UserInfo中使用了Rest，所以Init Rest需要在UserInfo初始化前
        RestKit.init(new Config(this, Constants.REST_ENDPOINT));
        //initEasemob();
        //环信接收消息的监听
        EaseHelper.getInstance().init(this);
        //EaseHelper.getInstance().init(this);
        Fresco.initialize(this);
        //Db Init
        this.login();
        MultiDex.install(this);
        //推送配置
        AVOSCloud.initialize(this, Constants.PUSH_ID, Constants.PUSH_SERECT);
        AVOSCloud.setLastModifyEnabled(true);
        AVOSCloud.setDebugLogEnabled(true);
        AVInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                String installationId = AVInstallation.getCurrentInstallation().getInstallationId();
                AbSharedUtil.putString(AppContext.this, "device_token", installationId);
            }
        });
        x.Ext.init(this);
        // 通过WXAPIFactory工厂，获取IWXAPI的实例
        api = WXAPIFactory.createWXAPI(this, Constants.APP_ID, false);
        api.registerApp(Constants.APP_ID);
    }
    public boolean isCallDoctor() {
        return isCallDoctor;
    }
    public void setIsCallDoctor(boolean isCallDoctor) {
        this.isCallDoctor = isCallDoctor;
    }
    /**
     * 登录
     */
    public void login() {
        if (getUserInfo() == null)
            return;
        // 添加该用户的数据库并设为默认
        String configName = DbKit.USER_CONFIG_NAME;
        String dbName = configName + "_" + getUserId() + ".db";
        DbKit.init(this, dbName, 2);
        isLoginRx.onNext(getUserId() > 0);
    }
    private void initUpdater() {
        // UpdateConfig为全局配置。当在其他页面中。使用UpdateBuilder进行检查更新时。
        // 对于没传的参数，会默认使用UpdateConfig中的全局配置
        UpdateConfig.getConfig()
                // 必填：数据更新接口,url与checkEntity两种方式任选一种填写
                .checkEntity(new CheckEntity().setMethod(HttpMethod.POST).setUrl(Constants.UPDATE))
                // 必填：用于从数据更新接口获取的数据response中。解析出Update实例。以便框架内部处理
                .jsonParser(new UpdateParser() {
                    @Override
                    public Update parse(String response) {
                        /* 此处根据上面url或者checkEntity设置的检查更新接口的返回数据response解析出
                         * 一个update对象返回即可。更新启动时框架内部即可根据update对象的数据进行处理
                         */
                        // 此处模拟一个Update对象
                        Update update = new Update(response);
                        try {
                            JSONObject json = new JSONObject(update.getOriginal());
                            JSONObject result = json.getJSONObject("data");
                            // 此apk包的更新时间
                            update.setUpdateTime(System.currentTimeMillis());
                            // 此apk包的下载地址
                            String updateUrl = result.getString("path");
                            if (updateUrl != null && updateUrl.contains("https:")) {
                                //部分手机上https不能下载
                                updateUrl = updateUrl.replace("https:", "http:");
                            }
                            update.setUpdateUrl(updateUrl);
                            // 此apk包的版本号
                            update.setVersionCode(result.getInt("version_code"));
                            // 此apk包的版本名称
                            update.setVersionName(result.getString("version_name"));
                            // 此apk包的更新内容
                            update.setUpdateContent(result.getString("content"));
                            // 此apk包是否为强制更新
                            update.setForced(result.getBoolean("is_update"));
                            // 是否显示忽略此次版本更新按钮
                            update.setIgnore(false);
                        } catch (Throwable e) {
                        }
                        return update;
                    }
                })
                // apk下载的回调
                .downloadCB(new UpdateDownloadCB() {
                    private NotificationManager nm = (NotificationManager) getSystemService(AppContext.NOTIFICATION_SERVICE);
                    private NotificationCompat.Builder builder = new NotificationCompat.Builder(getBaseContext())
                            .setProgress(100, 0, false)
                            .setDefaults(PendingIntent.FLAG_UPDATE_CURRENT)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentInfo("下载中...")
                            .setContentTitle("正在下载");
                    private long updateTime = System.currentTimeMillis();

                    @Override
                    public void onUpdateStart() {
                        Toast.makeText(AppContext.this, "下载开始", Toast.LENGTH_SHORT).show();
                        nm.notify(0, builder.build());
                    }

                    @Override
                    public void onUpdateComplete(File file) {
                        if (file == null) return;
                        //下载完成后点击安装
                        Intent it = new Intent(Intent.ACTION_VIEW);
                        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        it.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "application/vnd.android.package-archive");
                        it.setDataAndType(Uri.parse("file://" + file.getPath()), "application/vnd.android.package-archive");
                        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, it, PendingIntent.FLAG_UPDATE_CURRENT);
                        builder.setContentTitle("下载完成")
                                .setContentText("点击安装")
                                .setContentInfo("100%")
                                .setProgress(100, 100, false)
                                .setDefaults(0)
                                .setContentIntent(pendingIntent);
                        nm.notify(0, builder.build());
                    }

                    @Override
                    public void onUpdateProgress(long current, long total) {
                        if (System.currentTimeMillis() - updateTime > 500L) {
                            builder.setProgress(100, (int) (current * 100 / total), false)
                                    .setContentInfo((int) (current * 100 / total) + "%");
                            nm.notify(0, builder.build());
                            updateTime = System.currentTimeMillis();
                        }
                    }

                    @Override
                    public void onUpdateError(int code, String errorMsg) {
                        builder.setContentTitle("下载失败")
                                .setContentInfo("(T＿T)")
                                .setContentText("网络不给力~~~");
                        nm.notify(0, builder.build());
                    }
                })
                // 自定义更新策略，默认WIFI下自动下载更新
                .strategy(new UpdateStrategy() {
                    @Override
                    public boolean isShowUpdateDialog(Update update) {
                        // 是否在检查到有新版本更新时展示Dialog。
                        return true;
                    }

                    @Override
                    public boolean isAutoInstall() {
                        // 是否自动更新。此属性与是否isShowInstallDialog互斥
                        return true;
                    }

                    @Override
                    public boolean isShowDownloadDialog() {
                        // 在APK下载时。是否显示下载进度的Dialog
                        return false;
                    }
                });
    }

    /**
     * 当前登录用户ID
     *
     * @return
     */
    public int getUserId() {
        return getUserInfo() == null ? 0 : getUserInfo().getId();
    }

    public UserInfo getUserInfo() {
        if (userInfo == null) {
            String userId = "";
            if (isCallDoctor) {
                userId = String.valueOf(AbSharedUtil.getInt(this, "callUserid"));
            } else {
                userId = AbSharedUtil.getString(this, "userId");
            }
            if (userId != null) {
                userInfo = new UserInfo();
                userInfo.setId(Integer.parseInt(userId));
                userInfo.setMember_id(userId);
                return userInfo;
            }
        }
        return userInfo;
    }

    public String getProperty(String key) {
        return AppConfig.getAppConfig(this).get(key);
    }

    /**
     * 用户登录（非游客）
     */
    public BehaviorSubject<Boolean> isLoginRx = BehaviorSubject.create(!isGuest());

    /**
     * 游客登录
     *
     * @return
     */
    public boolean isGuest() {
        return userInfo != null && userInfo.getId() == 0;
    }

    /**
     * 应用是否在后台运行
     *
     * @return
     */
    public boolean isAppBackground() {
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(getPackageName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 检测网络是否可用
     *
     * @return
     */
    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnectedOrConnecting();
    }

    /**
     * 获取当前网络类型
     *
     * @return 0：没有网络   1：WIFI网络   2：WAP网络    3：NET网络
     */
    public int getNetworkType() {
        int netType = 0;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            return netType;
        }
        int nType = networkInfo.getType();
        if (nType == ConnectivityManager.TYPE_MOBILE) {
            String extraInfo = networkInfo.getExtraInfo();
            if (!StrKit.isBlank(extraInfo)) {
                if (extraInfo.toLowerCase().equals("cmnet")) {
                    netType = NETTYPE_CMNET;
                } else {
                    netType = NETTYPE_CMWAP;
                }
            }
        } else if (nType == ConnectivityManager.TYPE_WIFI) {
            netType = NETTYPE_WIFI;
        }
        return netType;
    }

    /**
     * 判断当前版本是否兼容目标版本的方法
     *
     * @param VersionCode
     * @return
     */
    public static boolean isMethodsCompat(int VersionCode) {
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        return currentVersion >= VersionCode;
    }

    /**
     * 获取App安装包信息
     *
     * @return
     */
    public PackageInfo getPackageInfo() {
        PackageInfo info = null;
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace(System.err);
        }
        if (info == null) info = new PackageInfo();
        return info;
    }

    /**
     * 返回app运行状态
     * 1:程序在前台运行
     * 2:程序在后台运行
     * 3:程序未启动
     * 注意：需要配置权限<uses-permission android:name="android.permission.GET_TASKS" />
     */
    public int getAppSatus() {
        String pageName = getPackageName();
        ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(20);
        //判断程序是否在栈顶
        if (list.get(0).topActivity.getPackageName().equals(pageName)) {
            return 1;
        } else {
            //判断程序是否在栈里
            for (ActivityManager.RunningTaskInfo info : list) {
                if (info.topActivity.getPackageName().equals(pageName)) {
                    return 2;
                }
            }
            return 3;//栈里找不到，返回3
        }
    }

    /**
     * 判断app是否启动
     * @return
     */
    public boolean checkApkExist() {
        String packageName = getPackageName();
        if (packageName == null || "".equals(packageName))
            return false;
        try {
            ApplicationInfo info = this.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}


