package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;

import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by nsd on 2017/4/21.
 * 选择患病时间
 */

public class SelectITimeActivity extends BaseActivity {
    @InjectView(R.id.listview)
    ListView mListView;
    @InjectView(R.id.title)
    TextView mTitle;
    ArrayList<String> mIllTimes = new ArrayList<>();
    QuickAdapter<String> mListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_maketime);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("选择患病时间");
        mListAdapter = new QuickAdapter<String>(this,R.layout.listitem_select) {
            @Override
            protected void convert(BaseAdapterHelper helper, String item) {
                helper.setText(R.id.content,item);
            }
        };
        mListView.setAdapter(mListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String schedule = mListAdapter.getItem(position);
                Intent intent = new Intent();
                intent.putExtra("ill_time",schedule);
                setResult(RESULT_OK,intent);
                finish();
            }
        });
    }


    @Override
    protected void onInitData() {
        super.onInitData();
        mIllTimes.add("1天");
        mIllTimes.add("3天");
        mIllTimes.add("5天");
        mIllTimes.add("10天");
        mIllTimes.add("半月");
        mIllTimes.add("一月");
        mIllTimes.add("两月");
        mIllTimes.add("半年");
        mIllTimes.add("一年");
        mIllTimes.add("两年");
        mIllTimes.add("五年");
        mIllTimes.add("很久");
        mListAdapter.addAll(mIllTimes);
    }
    @OnClick(R.id.left_btn)
    void back(){
        finish();
    }
}
