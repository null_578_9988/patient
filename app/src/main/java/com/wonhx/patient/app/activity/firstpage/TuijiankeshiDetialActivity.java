package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.adapter.TuijianDoctorAbstratorInfo;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.TuijianDoctor;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.view.XListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class TuijiankeshiDetialActivity extends BaseActivity implements XListView.IXListViewListener {
    @InjectView(R.id.lv)
    XListView lv;
    String deptid = "";
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    List<TuijianDoctor> datas = new ArrayList<>();
    String keywor="";
    @InjectView(R.id.keyword)
    EditText mKeyWord;
    Handler handler;
    int start=0;
    boolean flag;
    QuickAdapter<TuijianDoctor>adapter;
    @Override
    protected void onInitView() {
        super.onInitView();
        lv.setPullLoadEnable(false);
        adapter=new QuickAdapter<TuijianDoctor>(this,R.layout.tuijian_doctor_item) {
            @Override
            protected void convert(BaseAdapterHelper holder, TuijianDoctor item) {
                holder.setText(R.id.doctor_name,item.getName()!=null&&!item.getName().equals("")?item.getName():"")
                        .setText(R.id.doctor_hospitalName,item.getHospitalName()!=null&&!item.getHospitalName().equals("")?item.getHospitalName():"")
                        .setText(R.id.doctor_dept,item.getDept()!=null&&!item.getDept().equals("")?item.getDept():"")
                        .setText(R.id.doctor_goodSubjects,item.getGoodSubjects()!=null&&!item.getGoodSubjects().equals("")?item.getGoodSubjects():"")
                        .setText(R.id.doctor_title,item.getTitle()!=null&&!item.getTitle().equals("")?item.getTitle():"");
                SimpleDraweeView logo=holder.getView(R.id.logo);
                logo.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine/pub/member_logo/" + item.getMemberId()+"/"+item.getMemberId()+".png"));
            }
        };
        lv.setAdapter(adapter);
        lv.setXListViewListener(this);
        handler=new Handler();
        Intent in = getIntent();
        if (in.getStringExtra("deptid") != null && !in.getStringExtra("deptid").equals("")) {
             deptid = in.getStringExtra("deptid");
             mKeyWord.setText(in.getStringExtra("deptName"));
             flag=true;
             getTuijianyishen();
        }
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent in = new Intent(TuijiankeshiDetialActivity.this, DoctorDetialActivity.class);
                in.putExtra("member_id",datas.get(position-1).getMemberId());
                startActivity(in);
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tuijiankeshi_detial);
        ButterKnife.inject(this);
        mKeyWord.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == android.view.KeyEvent.KEYCODE_ENTER && event.getAction() == android.view.KeyEvent.ACTION_UP) {
                    // 先隐藏键盘
                    ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                            .hideSoftInputFromWindow(TuijiankeshiDetialActivity.this.getCurrentFocus()
                                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    //进行搜索操作的方法，在该方法中可以加入mEditSearchUser的非空判断
                    keywor=mKeyWord.getText().toString().trim();
                    start=0;
                    datas.clear();
                    adapter.clear();
                    flag=false;
                    getsearch();
                }
                return false;
            }
        });

    }
    private void getTuijianyishen() {
        firstPagerMager.getDoctor(deptid,start+"","10", new SubscriberAdapter<ListProResult<TuijianDoctor>>() {
            @Override
            public void onError(Throwable e) {
               // super.onError(e);
                dismissLoadingDialog();
                Toaster.showShort(TuijiankeshiDetialActivity.this,"没有更多医生");
            }

            @Override
            public void success(ListProResult<TuijianDoctor> tuijianDoctorListProResult) {
                super.success(tuijianDoctorListProResult);
                lv.setVisibility(View.VISIBLE);
                datas.addAll(tuijianDoctorListProResult.getData());
                if (datas.size() > 0) {
                    adapter.replaceAll(datas);
                    if (datas.size()>=10){
                        lv.setPullLoadEnable(true);
                    }else {
                        lv.setPullLoadEnable(false);
                    }
                }
            }
        });
    }

    @OnClick({R.id.back,})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;

        }
    }

    private void getsearch() {
        firstPagerMager.search(keywor,start+"","10",new SubscriberAdapter<ListProResult<TuijianDoctor>>(){
            @Override
            public void onError(Throwable e) {
                //super.onError(e);
                dismissLoadingDialog();
                Toaster.showShort(TuijiankeshiDetialActivity.this,"没有更多医生");
            }
            @Override
            public void success(ListProResult<TuijianDoctor> tuijiankeshiListProResult) {
                super.success(tuijiankeshiListProResult);
                lv.setVisibility(View.VISIBLE);
                datas.addAll(tuijiankeshiListProResult.getData());
                if (datas.size()>0){
                    adapter.replaceAll(datas);
                    if (datas.size()>=10){
                        lv.setPullLoadEnable(true);
                    }else {
                        lv.setPullLoadEnable(false);
                    }
                }
            }
        });
    }
    private void onLoad() {
        lv.stopRefresh();
        lv.stopLoadMore();
        lv.setRefreshTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }
    @Override
    public void onRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                datas.clear();
                start=0;
                if (flag){
                    getTuijianyishen();
                }else {
                    getsearch();
                }
                onLoad();
            }
        },1000);

    }

    @Override
    public void onLoadMore() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start=start+10;
                if (flag){
                    getTuijianyishen();
                }else {
                    getsearch();
                }
                onLoad();
            }
        },1000);
    }
}
