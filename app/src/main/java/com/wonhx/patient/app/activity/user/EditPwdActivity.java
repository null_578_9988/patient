package com.wonhx.patient.app.activity.user;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/25.
 *  修改密码页
 */
public class EditPwdActivity extends BaseActivity {
    @InjectView(R.id.et_newpwd)
    EditText mEditPwd;
    @InjectView(R.id.et_confirmpwd)
    EditText mConfirmPwd;
    @InjectView(R.id.iv_clear)
    ImageView mClearPwd;
    @InjectView(R.id.iv_clearn)
    ImageView mClearConfirmPwd;
    String phoneNum,code;
    UserManager userManager = new UserManagerImpl();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pwd);
    }
    /**
     * 初始化视图
     */
    @Override
    protected void onInitView() {
        super.onInitView();
        mEditPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(mEditPwd.getText().toString().trim())) {
                    mClearPwd.setVisibility(View.VISIBLE);
                } else {
                    mClearPwd.setVisibility(View.GONE);
                }
            }
        });
        mConfirmPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(mConfirmPwd.getText().toString().trim())){
                    mClearConfirmPwd.setVisibility(View.VISIBLE);
                }else{
                    mClearConfirmPwd.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 初始化数据
     */
    @Override
    protected void onInitData() {
        super.onInitData();
        phoneNum = getIntent().getStringExtra("phoneNum");
        code = getIntent().getStringExtra("sms_code");
    }

    /**
     * 提交修改
     */
    @OnClick(R.id.btn_resetpwd)
    void resetPwd(){
        if (StrKit.isBlank(mEditPwd.getText().toString().trim())) {
            Toaster.showShort(EditPwdActivity.this,"用户密码不能为空！");
            return;
        }
        if (StrKit.isBlank(mConfirmPwd.getText().toString().trim())){
            Toaster.showShort(EditPwdActivity.this,"确认密码不能为空！");
            return;
        }
        if (!mEditPwd.getText().toString().trim().equals(mConfirmPwd.getText().toString().trim())){
            Toaster.showShort(EditPwdActivity.this,"密码与确认密码不一致！");
            return;
        }
        userManager.modifyPwd(phoneNum,mConfirmPwd.getText().toString().trim(),"1",code,new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                Toaster.showShort(EditPwdActivity.this,result.getMsg());
                finish();
            }
        });

    }
    @OnClick(R.id.tv_back)
    void back(){
        finish();
    }
}
