package com.wonhx.patient.app.model;

import java.util.List;

/**
 * Created by Administrator on 2017/5/22.
 */

public class Zhnsuo {
      String parent_dept_name;
    public List<Sub_dept_name> sub_dept_name;
    public String code;
    public String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getParent_dept_name() {
        return parent_dept_name;
    }

    public void setParent_dept_name(String parent_dept_name) {
        this.parent_dept_name = parent_dept_name;
    }

    public List<Sub_dept_name> getSub_dept_name() {
        return sub_dept_name;
    }

    public   void setSub_dept_name(List<Sub_dept_name> sub_dept_name) {
        this.sub_dept_name = sub_dept_name;
    }
        public static class Sub_dept_name {
        private String id;
        private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

}
