package com.wonhx.patient.app.baidu.map;

import com.wonhx.patient.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gaolei on 16/12/19.
 */

public class BikeInfo implements Serializable {

String doctor_id;
String member_id;
String doctor_name;
String address;
String lng;
String lat;
String logo_img_path;
String title;
String dept_name;
    int iamgeid= R.mipmap.doctorshome;
    public static List<BikeInfo> bInfo = new ArrayList<BikeInfo>();

    @Override
    public String toString() {
        return "BikeInfo{" +
                "doctor_id='" + doctor_id + '\'' +
                ", member_id='" + member_id + '\'' +
                ", name='" + doctor_name + '\'' +
                ", address='" + address + '\'' +
                ", lng='" + lng + '\'' +
                ", lat='" + lat + '\'' +
                ", logo_img_path='" + logo_img_path + '\'' +
                ", title='" + title + '\'' +
                ", dept_name='" + dept_name + '\'' +
                ", iamgeid='" + iamgeid + '\'' +
                '}';
    }

    public BikeInfo(String doctor_id, String member_id, String doctor_name, String address, String lng, String lat, String logo_img_path, String title, String dept_name,int iamgeid) {
        this.doctor_id = doctor_id;
        this.member_id = member_id;
        this.doctor_name = doctor_name;
        this.address = address;
        this.lng = lng;
        this.lat = lat;
        this.logo_img_path = logo_img_path;
        this.title = title;
        this.dept_name = dept_name;
        this.iamgeid=iamgeid;
    }

    public int getIamgeid() {
        return iamgeid;
    }

    public void setIamgeid(int iamgeid) {
        this.iamgeid = iamgeid;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public static List<BikeInfo> getbInfo() {
        return bInfo;
    }

    public static void setbInfo(List<BikeInfo> bInfo) {
        BikeInfo.bInfo = bInfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLogo_img_path() {
        return logo_img_path;
    }

    public void setLogo_img_path(String logo_img_path) {
        this.logo_img_path = logo_img_path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }


}
