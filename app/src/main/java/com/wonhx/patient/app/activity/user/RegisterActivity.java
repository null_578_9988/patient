package com.wonhx.patient.app.activity.user;

import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.view.CustomDialog;

import java.util.Date;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/23.
 * 注册页面
 */
public class RegisterActivity extends BaseActivity {
    @InjectView(R.id.phoneNum)
    EditText mPhoneNum;
    @InjectView(R.id.clear_phone)
    ImageView mClearPhone;
    UserManager userManager = new UserManagerImpl();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mPhoneNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(mPhoneNum.getText().toString().trim())) {
                    mClearPhone.setVisibility(View.VISIBLE);
                } else {
                    mClearPhone.setVisibility(View.GONE);
                }
            }
        });
    }
    @OnClick(R.id.regist_close_btn)
    void back(){
        finish();
    }
    @OnClick(R.id.clear_phone)
    void clearPhone(){
        mPhoneNum.setText("");
    }
    @OnClick(R.id.btn_yanzheng)
    void doLogin(){
        if (StrKit.isBlank(mPhoneNum.getText().toString().trim())){
            Toaster.showShort(this, "请输入手机号！");
            return;
        }
        if (!StrKit.isMobileNO(mPhoneNum.getText().toString().trim())){
            Toaster.showShort(this,"请正确输入手机号码！");
            return;
        }
        dialog();
    }

    /**
     * |图片验证码弹窗
     */
    private void dialog() {
        final CustomDialog  dialog = new CustomDialog(RegisterActivity.this);
        final EditText editText = (EditText) dialog.getEditText();//方法在CustomDialog中实现
        final SimpleDraweeView imgCodeView=(SimpleDraweeView)dialog.getImageView();
        final ImageView iv_clears=(ImageView)dialog.getImageClear();
        iv_clears.setVisibility(View.GONE);
        //只用下面这一行弹出对话框时需要点击输入框才能弹出软键盘
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        //加上下面这一行弹出对话框时软键盘随之弹出
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        imgCodeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValidateCode(imgCodeView);
            }
        });
        iv_clears.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                editText.setText("");
            }
        });
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(editText.getText().toString().trim())) {
                    iv_clears.setVisibility(View.VISIBLE);
                } else {
                    iv_clears.setVisibility(View.GONE);
                }

            }
        });
        getValidateCode(imgCodeView);
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StrKit.isBlank(editText.getText().toString().trim())){
                    Toaster.showShort(RegisterActivity.this,"请输入验证码！");
                }else{
                    getRegistCode(editText.getText().toString().trim());
                }

            }
        });
        dialog.setOnNegativeListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * 获取注册手机验证码
     * @param code
     */
    private void getRegistCode(final String code){
        userManager.getRegisterCode(mPhoneNum.getText().toString().trim(),code,"1",new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                Toaster.showShort(RegisterActivity.this, result.getMsg());
                Bundle bundle = new Bundle();
                bundle.putString("phoneNum",mPhoneNum.getText().toString().trim());
                bundle.putString("code",code);
                UIKit.open(RegisterActivity.this,InputRegisterCodeActivity.class,bundle);
                finish();
            }
        });
    }
    /**
     * 获取图片验证码
     * @param view
     */
    public void getValidateCode(final SimpleDraweeView view) {
        new Thread() {
            @Override
            public void run() {
                super.run();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        view.setImageURI(Uri.parse(Constants.REST_ENDPOINT+"/Ident_code?current_time=" + (new Date()).getTime()));
                    }
                });
            }
        }.start();
    }
}
