package com.wonhx.patient.app.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/8/8.
 */

public class NearClinct implements Serializable {
    String id;
    String name;
    String logo_img_path;
    String address;
    String lng;
    String lat;
    String dept_name;
    List<NearClinct>nearClincts=new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo_img_path() {
        return logo_img_path;
    }

    public void setLogo_img_path(String logo_img_path) {
        this.logo_img_path = logo_img_path;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public NearClinct(String id, String name, String logo_img_path, String address, String lng, String lat, String dept_name) {
        this.id = id;
        this.name = name;
        this.logo_img_path = logo_img_path;
        this.address = address;
        this.lng = lng;
        this.lat = lat;
        this.dept_name = dept_name;

    }

    @Override
    public String toString() {
        return "NearClinct{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", logo_img_path='" + logo_img_path + '\'' +
                ", address='" + address + '\'' +
                ", lng='" + lng + '\'' +
                ", lat='" + lat + '\'' +
                ", dept_name='" + dept_name + '\'' +
                '}';
    }
}
