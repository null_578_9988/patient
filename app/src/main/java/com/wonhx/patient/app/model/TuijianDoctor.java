package com.wonhx.patient.app.model;

/**
 * Created by Administrator on 2017/3/31 0031.
 *
 */

public class TuijianDoctor {
    private String level;
    private String sex;
    private String doctorId;
    private String goodSubjects;
    private String logoImgPath;
    private String memberId;
    private String name;
    private String title;
    private String dept;
    private String hospitalName;
    private String hospitalId;
    private String logo;
    private String onLineStatus;

    public String getOnLineStatus() {
        return onLineStatus;
    }

    public void setOnLineStatus(String onLineStatus) {
        this.onLineStatus = onLineStatus;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getGoodSubjects() {
        return goodSubjects;
    }

    public void setGoodSubjects(String goodSubjects) {
        this.goodSubjects = goodSubjects;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getLogoImgPath() {
        return logoImgPath;
    }

    public void setLogoImgPath(String logoImgPath) {
        this.logoImgPath = logoImgPath;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }
    public String getDept() {
        return dept;
    }



}
