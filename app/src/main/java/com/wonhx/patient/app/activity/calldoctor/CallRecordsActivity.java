package com.wonhx.patient.app.activity.calldoctor;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.CallDoctorManager;
import com.wonhx.patient.app.manager.calldoctor.CallDoctorManagerImpl;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.Record;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.TabEntity;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/31.
 * 呼叫记录
 */
public class CallRecordsActivity extends BaseActivity implements OnTabSelectListener {
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.right_btn)
    TextView mRightBtn;
    @InjectView(R.id.menutab)
    CommonTabLayout mMenuTab;
    @InjectView(R.id.listview)
    ListView mListView;
    @InjectView(R.id.left_btn)
    ImageView mImgBack;
    String[] mTitles = {"全部", "待评价"};
    int position1 = 0;
    @InjectView(R.id.rl_no)
    RelativeLayout rl_no;
    ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    /**
     * 全部数据
     */
    List<Record> mAllListDatas = new ArrayList<>();
    /**
     * 待评价数据
     */
    List<Record> mWaitCommitListDatas = new ArrayList<>();
    QuickAdapter<Record> mListAdapter;
    CallDoctorManager callDoctorManager = new CallDoctorManagerImpl();
    int mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callrecord);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mImgBack.setImageResource(R.mipmap.hj_phone_arrows_l);
        mTitle.setText("呼叫记录");
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i]));
        }
        mMenuTab.setTabData(mTabEntities);
        mMenuTab.setOnTabSelectListener(this);
        mListAdapter = new QuickAdapter<Record>(this, R.layout.listitem_record) {
            @Override
            protected void convert(BaseAdapterHelper helper, final Record item) {
                SimpleDraweeView imgType = helper.getView(R.id.img_consult_type);
                SimpleDraweeView imgStatus = helper.getView(R.id.img_status);
                SimpleDraweeView imghead = helper.getView(R.id.img_photo);
                Button btnEvaluate = helper.getView(R.id.btn_evaluate);
                Button btnNewCall = helper.getView(R.id.btn_anew_call);
                TextView statusTxt = helper.getView(R.id.txt_status);
                Button btnDetail = helper.getView(R.id.btn_details);
                if (item.getLogo_img_path() != null) {
                    imghead.setImageURI(Uri.parse(Constants.REST_ORGIN +"/emedicine"+ item.getLogo_img_path()));
                }else {
                    imghead.setImageResource(R.mipmap.hj_index_photo);
                }
                TextView tv_name=helper.getView(R.id.txt_doctor_name);
                if (item.getName()!=null&&!item.getName().equals("")){
                    tv_name.setText(item.getName());
                }else {
                    tv_name.setText("暂时没有医生接单");
                }
                helper.setText(R.id.txt_consult_time, item.getCreate_date()).setText(R.id.txt_doctor_keshi, item.getDept_name());
                btnEvaluate.setTag(item.getId());
                if (item.getType().equals("1")) {
                    imgType.setImageResource(R.mipmap.hj_record_type_phone);
                    helper.setText(R.id.txt_consult_type, "电话咨询");
                } else {
                    imgType.setImageResource(R.mipmap.hj_record_type_video);
                    helper.setText(R.id.txt_consult_type, "视频咨询");
                }
                if (item.getStatus() != null) {
                    btnDetail.setTag(item);
                    switch (Integer.parseInt(item.getStatus())) {
                        case 1:
                            statusTxt.setText("通话中");
                            imgStatus.setVisibility(View.VISIBLE);
                            btnEvaluate.setVisibility(View.GONE);
                            btnNewCall.setVisibility(View.GONE);
                            break;
                        case 2:
                            statusTxt.setText("未接通");
                            imgStatus.setVisibility(View.GONE);
                            btnEvaluate.setVisibility(View.GONE);
                            btnNewCall.setVisibility(View.VISIBLE);
                            break;
                        case 3:
                            statusTxt.setText("呼叫成功");
                            imgStatus.setVisibility(View.GONE);
                            btnEvaluate.setVisibility(View.VISIBLE);
                            btnNewCall.setVisibility(View.GONE);
                            break;
                        case 4:
                            statusTxt.setText("呼叫失败");
                            imgStatus.setVisibility(View.GONE);
                            btnEvaluate.setVisibility(View.GONE);
                            btnNewCall.setVisibility(View.VISIBLE);
                            break;
                        case 5:
                            statusTxt.setText("呼叫成功");
                            imgStatus.setVisibility(View.GONE);
                            btnEvaluate.setVisibility(View.VISIBLE);
                            btnNewCall.setVisibility(View.GONE);
                            break;
                        case 6:
                            statusTxt.setText("呼叫成功");
                            imgStatus.setVisibility(View.GONE);
                            btnEvaluate.setVisibility(View.GONE);
                            btnNewCall.setVisibility(View.GONE);
                            break;
                        case 7:
                            statusTxt.setText("呼叫成功");
                            imgStatus.setVisibility(View.GONE);
                            btnEvaluate.setVisibility(View.GONE);
                            btnNewCall.setVisibility(View.GONE);
                            break;
                    }

                    //查看详情
                    btnDetail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mMenuTab.getCurrentTab() == 0) {
                                position1 = 0;
                            } else if (mMenuTab.getCurrentTab() == 1) {
                                position1 = 1;
                            }
                            Record record = (Record) v.getTag();
                            switch (Integer.parseInt(record.getStatus())) {
                                case 1:
                                    if (item.getType().equals("1")) {
                                        Intent intent = new Intent(CallRecordsActivity.this, PhoneServerActivity.class);
                                        //传的参数跟视频的可能不一样
                                        intent.putExtra("phone", record.getPhone());
                                        startActivityForResult(intent, 1001);
                                    } else {
                                        Intent intent = new Intent(CallRecordsActivity.this, VideoServerActivity.class);
                                        intent.putExtra("phone", record.getPhone());
                                        startActivityForResult(intent, 1001);
                                    }
                                    break;
                                default:
                                    Intent intent = new Intent(CallRecordsActivity.this, RecordDetailActivity.class);
                                    intent.putExtra("record", record);
                                    startActivityForResult(intent, 1001);
                                    break;
                            }
                        }
                    });
                    //重新拨打
                    btnNewCall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String dept_id="";
                            if (item.getDept_id()!=null&&!item.getDept_id().equals("")){
                                dept_id=item.getDept_id();
                            }else {
                                dept_id="all";
                            }
                            if (mMenuTab.getCurrentTab() == 0) {
                                position1 = 0;
                            } else if (mMenuTab.getCurrentTab() == 1) {
                                position1 = 1;
                            }
                            if (item.getType().equals("1")) {
                                callDoctorManager.reCallPhone(dept_id, item.getOrder_id(), item.getPhone(), new SubscriberAdapter<Result>() {
                                    @Override
                                    public void success(Result result) {
                                        super.success(result);
                                        Toaster.showShort(CallRecordsActivity.this, result.getMsg());
                                        Bundle bundle = new Bundle();
                                        bundle.putString("phone", item.getPhone());
                                        UIKit.open(CallRecordsActivity.this, PhoneServerActivity.class, bundle);
                                        finish();
                                    }
                                });
                            } else {
                                callDoctorManager.reCallVideo(dept_id, item.getId(), item.getOrder_id(), new SubscriberAdapter<Result>() {
                                    @Override
                                    public void success(Result result) {
                                        super.success(result);
                                        Toaster.showShort(CallRecordsActivity.this, result.getMsg());
                                        Bundle bundle = new Bundle();
                                        bundle.putString("phone", item.getUser_name());
                                        UIKit.open(CallRecordsActivity.this, VideoServerActivity.class, bundle);
                                        finish();
                                    }
                                });
                            }
                        }
                    });
                    //评价
                    btnEvaluate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mMenuTab.getCurrentTab() == 0) {
                                position1 = 0;
                            } else if (mMenuTab.getCurrentTab() == 1) {
                                position1 = 1;
                            }
                            Intent intent = new Intent(CallRecordsActivity.this, CallEvaluateActivity.class);
                            intent.putExtra("service_id", v.getTag().toString());
                            startActivityForResult(intent, 1001);
                        }
                    });
                }
            }

        };
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        mUserId = AbSharedUtil.getInt(this, "callUserid");
        if (mUserId != 0) {
            //获取列表数据
            getRecordsDatas();
        }
    }

    @OnClick(R.id.left_btn)
    void back() {
        finish();
    }

    /**
     * menu切换
     *
     * @param position
     */
    @Override
    public void onTabSelect(int position) {
        if (position == 0) {
            position1 = 0;
            if (mAllListDatas.size()>0) {
                mListView.setVisibility(View.VISIBLE);
                rl_no.setVisibility(View.GONE);
                //显示全部数据
                mListAdapter.clear();
                mListAdapter.addAll(mAllListDatas);
            }else {
                mListView.setVisibility(View.GONE);
                rl_no.setVisibility(View.VISIBLE);
            }
        } else {
            //显示待评价数据
            position1 = 1;
            if (mWaitCommitListDatas.size()>0) {
                mListView.setVisibility(View.VISIBLE);
                rl_no.setVisibility(View.GONE);
                mListAdapter.clear();
                mListAdapter.addAll(mWaitCommitListDatas);
            }else {
                mListView.setVisibility(View.GONE);
                rl_no.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onTabReselect(int position) {
    }

    /**
     * 获取列表数据
     */
    private void getRecordsDatas() {
        callDoctorManager.getRecords(String.valueOf(mUserId), new SubscriberAdapter<ListProResult<Record>>() {
            @Override
            public void success(ListProResult<Record> recordListProResult) {
                super.success(recordListProResult);
                if (recordListProResult.getData() != null && recordListProResult.getData().size() > 0) {
                    mAllListDatas.clear();
                    mWaitCommitListDatas.clear();
                    mAllListDatas.addAll(recordListProResult.getData());
                    for (Record record : mAllListDatas) {
                        if (record.getStatus().equals("3") || record.getStatus().equals("5") && !record.getStatus().equals("6") && !record.getStatus().equals("7")) {
                            mWaitCommitListDatas.add(record);
                        }
                    }
                    if (position1 == 0) {
                        mMenuTab.setCurrentTab(0);
                        if (mAllListDatas.size()>0) {
                            mListView.setVisibility(View.VISIBLE);
                            rl_no.setVisibility(View.GONE);
                            mListAdapter.clear();
                            mListAdapter.addAll(mAllListDatas);
                        }else {
                            mListView.setVisibility(View.GONE);
                            rl_no.setVisibility(View.VISIBLE);
                        }
                    } else if (position1 == 1) {
                        mMenuTab.setCurrentTab(1);
                        if (mWaitCommitListDatas.size()>0) {
                            mListView.setVisibility(View.VISIBLE);
                            rl_no.setVisibility(View.GONE);
                            mListAdapter.clear();
                            mListAdapter.addAll(mWaitCommitListDatas);
                        }else {
                            mListView.setVisibility(View.GONE);
                            rl_no.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    Toaster.showShort(CallRecordsActivity.this, "暂无数据");
                }
            }
        });
        mListView.setAdapter(mListAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != 1001)
            return;
        getRecordsDatas();
    }
}
