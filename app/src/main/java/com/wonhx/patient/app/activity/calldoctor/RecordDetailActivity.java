package com.wonhx.patient.app.activity.calldoctor;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hedgehog.ratingbar.RatingBar;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.CallDoctorManager;
import com.wonhx.patient.app.manager.calldoctor.CallDoctorManagerImpl;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Record;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/4/5.
 * 服务记录详情
 */
public class RecordDetailActivity extends BaseActivity {
    @InjectView(R.id.txt_state)
    TextView mStatus;
    @InjectView(R.id.txt_serve_doctor)
    TextView mDoctor;
    @InjectView(R.id.txt_doctor_grade)
    TextView mNickName;
    @InjectView(R.id.txt_keshi)
    TextView mKeShi;
    @InjectView(R.id.txt_consult_type)
    TextView mConsultType;
    @InjectView(R.id.txt_consult_time)
    TextView mConsultTime;
    @InjectView(R.id.txt_report_content)
    TextView mReportContent;
    @InjectView(R.id.img_photo)
    SimpleDraweeView mHeadView;
    @InjectView(R.id.llayout_my_evaluate)
    LinearLayout mEvaluateLayout;
    @InjectView(R.id.img_state)
    ImageView mImageStatus;
    @InjectView(R.id.txt_state_info)
    TextView mStatusInfo;
    @InjectView(R.id.llayout_doctor_report)
    LinearLayout mReportlayout;
    @InjectView(R.id.btn_anew_call)
    Button mNewCall;
    @InjectView(R.id.txt_user)
    TextView mTrainUser;
    @InjectView(R.id.img_train)
    SimpleDraweeView mTrainImg;
    @InjectView(R.id.ratingbar)
    RatingBar mTrainRatingBar;
    @InjectView(R.id.txt_evaluate)
    TextView mEvaluateContent;
    @InjectView(R.id.txt_type)
    TextView mEvaluateType;
    @InjectView(R.id.txt_time)
            TextView mEvaluateTime;
    Record mRecord;
    CallDoctorManager callDoctorManager = new CallDoctorManagerImpl();
    int tag=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_detail);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        mRecord = (Record) getIntent().getSerializableExtra("record");
        callDoctorManager.getRecordInfo(mRecord.getId(), new SubscriberAdapter<ProResult<Record>>() {
            @Override
            public void success(ProResult<Record> recordProResult) {
                super.success(recordProResult);
                if (recordProResult != null) {
                    mRecord = recordProResult.getData();
                    if (mRecord.getName()!=null) {
                        mDoctor.setText(mRecord.getName());
                    }else {
                        mDoctor.setText("没有医生接单，请尝试重新拨打");
                    }
                    mNickName.setText(mRecord.getTitle());
                    if (mRecord.getDept_name()!=null) {
                        mKeShi.setText(mRecord.getDept_name());
                    }else {
                        mKeShi.setText("暂无");
                    }
                    mTrainUser.setText(mRecord.getUser_name());
                    mTrainRatingBar.setStar(Float.parseFloat(mRecord.getScore()));
                    mEvaluateContent.setText(mRecord.getReply());
                    mEvaluateTime.setText(mRecord.getReply_time());
                    if (mRecord.getType().equals("1")) {
                        mConsultType.setText("电话咨询");
                        mEvaluateType.setText("电话咨询评价");
                    } else {
                        mConsultType.setText("视频咨询");
                        mEvaluateType.setText("视频咨询评价");
                    }
                    mConsultTime.setText(mRecord.getCreate_date());
                    mReportContent.setText(mRecord.getAnswer());
                    if (mRecord.getLogo_img_path() != null) {
                        mHeadView.setImageURI(Uri.parse(Constants.REST_ORGIN +"/emedicine"+ mRecord.getLogo_img_path()));
                    }
                    if (mRecord.getAnswer() != null && !mRecord.getAnswer().equals("")) {
                        mReportlayout.setVisibility(View.VISIBLE);
                    } else {
                        mReportlayout.setVisibility(View.GONE);
                    }
                    if (mRecord.getStatus() != null) {
                        //1 通话中 2 未接通 3 呼叫成功 4 呼叫失败 5 医生已解答 6  医生未解答 患者已评价  7 医生已解答  患者已评价
                        switch (Integer.parseInt(mRecord.getStatus())) {
                            case 1:
                                mImageStatus.setImageResource(R.mipmap.hj_index_hint);
                                mStatusInfo.setVisibility(View.VISIBLE);
                                mStatusInfo.setText("请稍后");
                                mStatus.setText("通话中");
                                mNewCall.setVisibility(View.GONE);
                                mEvaluateLayout.setVisibility(View.GONE);
                                tag=1;
                                break;
                            case 2:
                                mImageStatus.setImageResource(R.mipmap.hj_index_hint);
                                mStatusInfo.setVisibility(View.VISIBLE);
                                mStatusInfo.setText("请稍候再次尝试接通");
                                mStatus.setText("暂未接通");
                                mNewCall.setVisibility(View.VISIBLE);
                                mEvaluateLayout.setVisibility(View.GONE);
                                tag=2;
                                break;
                            case 3:
                                mImageStatus.setImageResource(R.mipmap.hj_record_details_succeed);
                                mStatusInfo.setVisibility(View.VISIBLE);
                                mStatusInfo.setText("请对我们的服务进行评价");
                                mStatus.setText("呼叫成功");
                                mNewCall.setVisibility(View.GONE);
                                mEvaluateLayout.setVisibility(View.GONE);
                                break;
                            case 4:
                                mImageStatus.setImageResource(R.mipmap.hj_record_details_failed);
                                mStatusInfo.setVisibility(View.VISIBLE);
                                mStatusInfo.setText("请稍候再次尝试接通");
                                mStatus.setText("呼叫失败");
                                mNewCall.setVisibility(View.VISIBLE);
                                mEvaluateLayout.setVisibility(View.GONE);
                                tag=2;
                                break;
                            case 5:
                                mImageStatus.setImageResource(R.mipmap.hj_record_details_succeed);
                                mStatusInfo.setVisibility(View.VISIBLE);
                                mStatusInfo.setText("请对我们的服务进行评价");
                                mStatus.setText("呼叫成功");
                                mNewCall.setVisibility(View.GONE);
                                mEvaluateLayout.setVisibility(View.GONE);
                                break;
                            case 6:
                                mImageStatus.setImageResource(R.mipmap.hj_record_details_succeed);
                                mStatusInfo.setVisibility(View.GONE);
                                mStatus.setText("呼叫成功");
                                mNewCall.setVisibility(View.GONE);
                                mEvaluateLayout.setVisibility(View.VISIBLE);
                                break;
                            case 7:
                                mImageStatus.setImageResource(R.mipmap.hj_record_details_succeed);
                                mStatusInfo.setVisibility(View.GONE);
                                mStatus.setText("呼叫成功");
                                mNewCall.setVisibility(View.GONE);
                                mEvaluateLayout.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                }
            }
        });
    }

    @OnClick(R.id.txt_return)
    void turn_back() {
        finish();
    }
    @OnClick(R.id.btn_anew_call)
    void reNewCall(){
        String dept_id="";
        if (mRecord.getDept_id()!=null&&!mRecord.getDept_id().equals("")){
            dept_id=mRecord.getDept_id();
        }else {
            dept_id="all";
        }
        if (mRecord.getType().equals("1")) {
            if (tag==1){
                Bundle bundle = new Bundle();
                bundle.putString("phone",mRecord.getPhone());
                UIKit.open(RecordDetailActivity.this, PhoneServerActivity.class, bundle);
                finish();
            }else if (tag==2){
                callDoctorManager.reCallPhone(dept_id, mRecord.getOrder_id(), mRecord.getPhone(), new SubscriberAdapter<Result>() {
                    @Override
                    public void success(Result result) {
                        super.success(result);
                        Toaster.showShort(RecordDetailActivity.this, result.getMsg());
                        Bundle bundle = new Bundle();
                        bundle.putString("phone",mRecord.getPhone());
                        UIKit.open(RecordDetailActivity.this, PhoneServerActivity.class, bundle);
                        finish();
                    }
                });
            }

        } else {
            if (tag==1){
                Bundle bundle = new Bundle();
                bundle.putString("phone", mRecord.getPhone());
                UIKit.open(RecordDetailActivity.this, VideoServerActivity.class, bundle);
                finish();
            }else if (tag==2) {
                callDoctorManager.reCallVideo(dept_id, mRecord.getId(), mRecord.getOrder_id(), new SubscriberAdapter<Result>() {
                    @Override
                    public void success(Result result) {
                        super.success(result);
                        Toaster.showShort(RecordDetailActivity.this, result.getMsg());
                        Bundle bundle = new Bundle();
                        bundle.putString("phone", mRecord.getUser_name());
                        UIKit.open(RecordDetailActivity.this, VideoServerActivity.class, bundle);
                        finish();
                    }
                });
            }
        }
    }

}
