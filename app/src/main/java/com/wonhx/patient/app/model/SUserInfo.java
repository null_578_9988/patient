package com.wonhx.patient.app.model;

/**
 * Created by apple on 17/3/29.
 * 个人信息
 */
public class SUserInfo {
    private String id;
    private String name;
    private String sex;
    private String birthday;
    private String address;
    private String phone;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex.equals("1") ? "男" : "女";
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
