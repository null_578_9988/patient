package com.wonhx.patient.app.model;

/**
 * Created by Administrator on 2017/4/13.
 */

public class HistoryDetial {
    String consultationId;
    String content;
    String patient_id;
    String medical;
    String historyTime;
    String medicalDescription;
    String hospitalHistory;
    String consultationreqId;
    String doctorId;
    String startTime;
    String emdTime;
    String reply;
    String replyAppraise;
    String replyAppraiseDate;
    String audioTime;
    String replyScore;
    String replyDate;
    String audioPath;
    String servicePrice;
    String orderId;
    String name;
    String title;
    String good_subjects;
    String logo_img_path;
    String birthday;
    String patientName;
    String patientSex;
    String sex;
    String doctorMemberId;
    String status;
    String serviceType;
    String dept_name;
    String hos_name;

    public String getAudioTime() {
        return audioTime;
    }

    public void setAudioTime(String audioTime) {
        this.audioTime = audioTime;
    }

    public String getConsultationId() {
        return consultationId;
    }

    public void setConsultationId(String consultationId) {
        this.consultationId = consultationId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getMedical() {
        return medical;
    }

    public void setMedical(String medical) {
        this.medical = medical;
    }

    public String getHistoryTime() {
        return historyTime;
    }

    public void setHistoryTime(String historyTime) {
        this.historyTime = historyTime;
    }

    public String getMedicalDescription() {
        return medicalDescription;
    }

    public void setMedicalDescription(String medicalDescription) {
        this.medicalDescription = medicalDescription;
    }

    public String getHospitalHistory() {
        return hospitalHistory;
    }

    public void setHospitalHistory(String hospitalHistory) {
        this.hospitalHistory = hospitalHistory;
    }

    public String getConsultationreqId() {
        return consultationreqId;
    }

    public void setConsultationreqId(String consultationreqId) {
        this.consultationreqId = consultationreqId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEmdTime() {
        return emdTime;
    }

    public void setEmdTime(String emdTime) {
        this.emdTime = emdTime;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getReplyAppraise() {
        return replyAppraise;
    }

    public void setReplyAppraise(String replyAppraise) {
        this.replyAppraise = replyAppraise;
    }

    public String getReplyAppraiseDate() {
        return replyAppraiseDate;
    }

    public void setReplyAppraiseDate(String replyAppraiseDate) {
        this.replyAppraiseDate = replyAppraiseDate;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }

    public String getReplyScore() {
        return replyScore;
    }

    public void setReplyScore(String replyScore) {
        this.replyScore = replyScore;
    }

    public String getReplyDate() {
        return replyDate;
    }

    public void setReplyDate(String replyDate) {
        this.replyDate = replyDate;
    }

    public String getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(String servicePrice) {
        this.servicePrice = servicePrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGood_subjects() {
        return good_subjects;
    }

    public void setGood_subjects(String good_subjects) {
        this.good_subjects = good_subjects;
    }

    public String getLogo_img_path() {
        return logo_img_path;
    }

    public void setLogo_img_path(String logo_img_path) {
        this.logo_img_path = logo_img_path;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientSex() {
        return patientSex;
    }

    public void setPatientSex(String patientSex) {
        this.patientSex = patientSex;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDoctorMemberId() {
        return doctorMemberId;
    }

    public void setDoctorMemberId(String doctorMemberId) {
        this.doctorMemberId = doctorMemberId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }
}
