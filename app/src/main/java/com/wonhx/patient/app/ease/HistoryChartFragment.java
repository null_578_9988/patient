package com.wonhx.patient.app.ease;

import android.view.View;

import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.hyphenate.easeui.widget.chatrow.EaseCustomChatRowProvider;

import java.util.ArrayList;

/**
 * Created by nsd on 2017/4/25.
 *
 */

public class HistoryChartFragment extends EaseChatFragment implements EaseChatFragment.EaseChatFragmentHelper {
    private String conslutionId; // 订单id
    private String homeOrderId; // 订单id
    private String clinicId; // 订单id
    ArrayList<EMMessage> mMessageList = new ArrayList<>();
    @Override
    protected void setUpView() {
        super.setUpView();
        // preferenceUtil = new SharedPreferencesUtil(getActivity());
        setChatFragmentListener(this);
        titleBar.setVisibility(View.GONE);
        fragmentArgs = getArguments();
        conslutionId = fragmentArgs.getString("DT_RowId");
        homeOrderId = fragmentArgs.getString("homeOrderId");
        clinicId = fragmentArgs.getString("CLINIC_ID");
        mMessageList = fragmentArgs.getParcelableArrayList("MESSAGES");
        messageList.init(mMessageList,conslutionId,homeOrderId,clinicId, toChatUsername, chatType, chatFragmentHelper != null ? chatFragmentHelper.onSetCustomChatRowProvider() : null);
        setListItemClickListener();
    }


    @Override
    public void onSetMessageAttributes(EMMessage message) {
        // 设置消息扩展属性
        if (conslutionId!=null&&clinicId==null&&homeOrderId==null) {
            message.setAttribute("conslutionId", conslutionId);
        }else  if (conslutionId==null&&clinicId==null&&homeOrderId!=null) {
            message.setAttribute("homeOrderId", homeOrderId);
        }else if (conslutionId==null&&clinicId!=null&&homeOrderId==null) {
            message.setAttribute("clinicId", clinicId);
        }
        //message.setAttribute("em_apns_ext", extJson);
    }

    @Override
    public void onEnterToChatDetails() {
        // 进入会话详情

    }

    @Override
    public void onAvatarClick(String username) {
        // 用户头像点击事件

    }

    @Override
    public void onAvatarLongClick(String username) {

    }

    @Override
    public boolean onMessageBubbleClick(EMMessage message) {
        // 消息气泡框点击事件
        return false;
    }

    @Override
    public void onMessageBubbleLongClick(EMMessage message) {
        // 消息气泡框长按事件

    }

    @Override
    public boolean onExtendMenuItemClick(int itemId, View view) {
        // 扩展输入栏item点击事件,如果要覆盖EaseChatFragment已有的点击事件，return true
        return false;
    }

    @Override
    public EaseCustomChatRowProvider onSetCustomChatRowProvider() {
        // 设置自定义chatrow提供者
        return null;
    }
}
