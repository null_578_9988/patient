package com.wonhx.patient.app.activity.user;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.Toaster;

import butterknife.InjectView;
import butterknife.OnClick;

public class MySuggestionActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView tv_title;
    @InjectView(R.id.et_message)
    EditText ed;
    UserManager userManager=new UserManagerImpl();

    @Override
    protected void onInitView() {
        super.onInitView();
        tv_title.setText("意见反馈");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_suggestion);
    }
    @OnClick(R.id.left_btn)
    void back(){
        finish();
    }
    @OnClick(R.id.sendmessage)
    void commit(){
        if (TextUtils.isEmpty(ed.getText().toString())){
            Toaster.showShort(MySuggestionActivity.this,"反馈内容不能为空");
        }else {
            userManager.suggestion(AbSharedUtil.getString(this, "userId"),ed.getText().toString().trim(),new SubscriberAdapter<Result>(){
                @Override
                public void success(Result result) {
                    super.success(result);
                    if (result.getCode()){
                        Toaster.showShort(MySuggestionActivity.this,result.getMsg());
                        finish();
                    }else {
                        Toaster.showShort(MySuggestionActivity.this,result.getMsg());
                    }
                }
            });
        }
    }
}
