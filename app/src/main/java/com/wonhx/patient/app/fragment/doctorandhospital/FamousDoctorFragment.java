package com.wonhx.patient.app.fragment.doctorandhospital;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.firstpage.FamousDetialActivity;
import com.wonhx.patient.app.activity.firstpage.Meet_doctorActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.SupurDoctor;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.view.XListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2017/7/19.
 */

public class FamousDoctorFragment extends BaseFragment implements XListView.IXListViewListener {
    View view;
    @InjectView(R.id.lv)
    XListView lv;
    QuickAdapter<SupurDoctor>mDoctorAdapter;
    Handler handler=new Handler();
    FirstPagerMager firstPagerMager=new FirstPagerMangerImal();
    List<SupurDoctor>list=new ArrayList<>();
    int start=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.doctor, null);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        lv.setPullLoadEnable(false);
        mDoctorAdapter = new QuickAdapter<SupurDoctor>(getActivity(), R.layout.supter_doctor) {
            @Override
            protected void convert(BaseAdapterHelper helper, SupurDoctor item) {
                String name=item.getName();
                String hospitalName=item.getHos_name();
                String dept=item.getDept_name();
                String goodSubjects=item.getGood_subjects();
                String title=item.getTitle();
                String url=item.getLogo_img_path();
                SimpleDraweeView simpleDraweeView=helper.getView(R.id.logo);
                if (url!=null&&!url.equals("")){
                    simpleDraweeView.setImageURI(Uri.parse(Constants.REST_ORGIN+"/emedicine"+url));
                }
                if (name!=null&&!name.equals("")){
                    helper.setText(R.id.doctor_name,name);
                }
                if (hospitalName!=null&&!hospitalName.equals("")){
                    helper.setText(R.id.doctor_hospitalName,hospitalName);
                }
                if (dept!=null&&!dept.equals("")){
                    helper.setText(R.id.doctor_dept,dept);
                }
                if (goodSubjects!=null&&!goodSubjects.equals("")){
                    helper.setText(R.id.doctor_goodSubjects,goodSubjects);
                }
                if (title!=null&&!title.equals("")){
                    helper.setText(R.id.doctor_status,title);
                }
            }
        };
        lv.setAdapter(mDoctorAdapter);
        lv.setXListViewListener(this);
        handler=new Handler();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent(getActivity(),FamousDetialActivity.class);
                intent.putExtra("doctor_id","");
                intent.putExtra("hos_id", list.get(i-1).getId());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        getsuperDoctor();
    }

    /**
     * 获取名医
     */
    private void getsuperDoctor() {
        firstPagerMager.getSuperDoctor(start+"","10",new SubscriberAdapter<ListProResult<SupurDoctor>>(){
            @Override
            public void onError(Throwable e) {
                //super.onError(e);
                dissmissProgressDialog();
                Toaster.showShort(getActivity(),"没有更多医生");
            }

            @Override
            public void success(ListProResult<SupurDoctor> supurDoctorListProResult) {
                super.success(supurDoctorListProResult);
                if (supurDoctorListProResult.getCode()){
                    list.addAll(supurDoctorListProResult.getData());
                    if (list.size()>0){
                        mDoctorAdapter.replaceAll(list);
                        if (list.size()>=10){
                            lv.setPullLoadEnable(true);
                        }else {
                            lv.setPullLoadEnable(false);
                        }
                    }
                }
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start=0;
                list.clear();
                getsuperDoctor();
                onLoad();
            }
        },1000);

    }
    private void onLoad() {
        lv.stopRefresh();
        lv.stopLoadMore();
        lv.setRefreshTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }
    @Override
    public void onLoadMore() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start=start+10;
                getsuperDoctor();
                onLoad();
            }
        },1000);

    }
}
