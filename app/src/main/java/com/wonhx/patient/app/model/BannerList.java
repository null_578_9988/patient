package com.wonhx.patient.app.model;

/**
 * Created by Administrator on 2017/4/1 0001.
 */

public class BannerList {
    String id;
    String path;
    String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
