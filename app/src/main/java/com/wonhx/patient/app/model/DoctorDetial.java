package com.wonhx.patient.app.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 2017/4/12.
 * 医生详情
 */

public class DoctorDetial implements Serializable{
    String doctorId;
    String sex;
    String birthday;
    String name;
    String log_img_path;
    String phone;
    String title;
    String good_subjects;
    String status;
    String address;
    String reason;
    String deptName;
    String hospitalName;
    String cityString;
    String provinceName;
    String dept_tel;
    String followed_status;
    String memberId;
    List<Schedule> schedule = Collections.EMPTY_LIST;

    public List<Schedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<Schedule> schedule) {
        this.schedule = schedule;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    private List<Price> price = Collections.EMPTY_LIST;
    public String getFollowed_status() {
        return followed_status;
    }

    public void setFollowed_status(String followed_status) {
        this.followed_status = followed_status;
    }


    public String getGood_subjects() {
        return good_subjects;
    }

    public void setGood_subjects(String good_subjects) {
        this.good_subjects = good_subjects;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLog_img_path() {
        return log_img_path;
    }

    public void setLog_img_path(String log_img_path) {
        this.log_img_path = log_img_path;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getCityString() {
        return cityString;
    }

    public void setCityString(String cityString) {
        this.cityString = cityString;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDept_tel() {
        return dept_tel;
    }

    public void setDept_tel(String dept_tel) {
        this.dept_tel = dept_tel;
    }

    public List<Price> getPrice() {
        return price;
    }

    public void setPrice(List<Price> price) {
        this.price = price;
    }
    public   class Price implements Serializable{
        String service_type;
        String service_price;

        public String getService_type() {
            return service_type;
        }

        public void setService_type(String service_type) {
            this.service_type = service_type;
        }

        public String getService_price() {
            return service_price;
        }

        public void setService_price(String service_price) {
            this.service_price = service_price;
        }
    }
    public class Schedule implements Serializable{
        String id;
        String start_time;
        String service_type;
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getService_type() {
            return service_type;
        }

        public void setService_type(String service_type) {
            this.service_type = service_type;
        }
    }

}
