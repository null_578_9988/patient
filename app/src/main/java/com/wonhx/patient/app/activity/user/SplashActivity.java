package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.hyphenate.chat.EMMessage;
import com.wonhx.patient.R;
import com.wonhx.patient.app.activity.calldoctor.IndexActivity;
import com.wonhx.patient.app.activity.main.MainActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.UIKit;

/**
 * Created by apple on 17/3/21.
 * note:启动页
 */
public class SplashActivity extends BaseActivity {
    EMMessage mMessage = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 隐藏状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        mMessage = getIntent().getParcelableExtra("message");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                int calluserId = AbSharedUtil.getInt(SplashActivity.this, "callUserid");
                if ( calluserId!= 0) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFromSplash",true);
                    UIKit.open(SplashActivity.this, IndexActivity.class,bundle);
                    finish();
                } else {
                    if (mMessage!=null){
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("message",mMessage);
                        UIKit.open(SplashActivity.this, MainActivity.class,bundle);
                    }else{
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        //overridePendingTransition(R.anim.enter,R.anim.exit);
                        finish();
                    }

                }

            }
        }, 1000);
    }
}
