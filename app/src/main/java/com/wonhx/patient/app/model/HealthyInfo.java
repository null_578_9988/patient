package com.wonhx.patient.app.model;

/**
 * Created by apple on 2017/4/8.
 * 健康档案：健康信息
 */

public class HealthyInfo {
    private String height;
    private String weight;
    private String bloodtype;
    private String bmi;
    private String temperature;
    private String pr;
    private String br;
    private String waistline;
    private String bp_left;
    private String bp_right;
    private String allergic_history;
    private String medical_history;
    private String family_medical_history;
    private String disability;
    private String is_operation;
    private String operation;
    private String is_tourims;
    private String is_transfusion;
    private String first_operation;
    private String first_ortime;
    private String first_tourims;
    private String first_tortime;
    private String first_transfusion;
    private String first_trantime;

    public String getBmi() {
        return bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getPr() {
        return pr;
    }

    public void setPr(String pr) {
        this.pr = pr;
    }

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br;
    }

    public String getWaistline() {
        return waistline;
    }

    public void setWaistline(String waistline) {
        this.waistline = waistline;
    }

    public String getBp_left() {
        return bp_left;
    }

    public void setBp_left(String bp_left) {
        this.bp_left = bp_left;
    }

    public String getBp_right() {
        return bp_right;
    }

    public void setBp_right(String bp_right) {
        this.bp_right = bp_right;
    }

    public String getAllergic_history() {
        return allergic_history;
    }

    public void setAllergic_history(String allergic_history) {
        this.allergic_history = allergic_history;
    }

    public String getMedical_history() {
        return medical_history;
    }

    public void setMedical_history(String medical_history) {
        this.medical_history = medical_history;
    }

    public String getFamily_medical_history() {
        return family_medical_history;
    }

    public void setFamily_medical_history(String family_medical_history) {
        this.family_medical_history = family_medical_history;
    }

    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
    }

    public String getIs_operation() {
        return is_operation;
    }

    public void setIs_operation(String is_operation) {
        this.is_operation = is_operation;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getIs_tourims() {
        return is_tourims;
    }

    public void setIs_tourims(String is_tourims) {
        this.is_tourims = is_tourims;
    }

    public String getIs_transfusion() {
        return is_transfusion;
    }

    public void setIs_transfusion(String is_transfusion) {
        this.is_transfusion = is_transfusion;
    }

    public String getFirst_operation() {
        return first_operation;
    }

    public void setFirst_operation(String first_operation) {
        this.first_operation = first_operation;
    }

    public String getFirst_ortime() {
        return first_ortime;
    }

    public void setFirst_ortime(String first_ortime) {
        this.first_ortime = first_ortime;
    }

    public String getFirst_tourims() {
        return first_tourims;
    }

    public void setFirst_tourims(String first_tourims) {
        this.first_tourims = first_tourims;
    }

    public String getFirst_tortime() {
        return first_tortime;
    }

    public void setFirst_tortime(String first_tortime) {
        this.first_tortime = first_tortime;
    }

    public String getFirst_transfusion() {
        return first_transfusion;
    }

    public void setFirst_transfusion(String first_transfusion) {
        this.first_transfusion = first_transfusion;
    }

    public String getFirst_trantime() {
        return first_trantime;
    }

    public void setFirst_trantime(String first_trantime) {
        this.first_trantime = first_trantime;
    }

    public String getBloodtype() {
        return bloodtype;
    }

    public void setBloodtype(String bloodtype) {
        this.bloodtype = bloodtype;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
