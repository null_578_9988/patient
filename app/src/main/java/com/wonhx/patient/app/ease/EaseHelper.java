package com.wonhx.patient.app.ease;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMError;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMOptions;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.controller.EaseUI;
import com.hyphenate.easeui.domain.EaseUser;
import com.hyphenate.easeui.model.EaseAtMessageHelper;
import com.hyphenate.easeui.model.EaseNotifier;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.hyphenate.exceptions.HyphenateException;
import com.hyphenate.util.NetUtils;
import com.wonhx.patient.R;
import com.wonhx.patient.app.AppContext;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.main.MainActivity;
import com.wonhx.patient.app.activity.user.SplashActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.UserInfo;
import com.wonhx.patient.kit.AbSharedUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by apple on 17/3/27.
 * 环信helper
 */
public class EaseHelper implements EMMessageListener, EMConnectionListener, EaseUI.EaseUserProfileProvider,
        EaseNotifier.EaseNotificationInfoProvider ,EaseUI.EaseSettingsProvider{
    private AppContext appContext;
    /**
     * 来电监听
     */
    private CallReceiver callReceiver;
    /**
     * EMEventListener
     */

    JSONObject json = null;
    String memberid ="";
    String nickName ="";
    protected EMMessageListener messageListener = null;
    private static EaseHelper instance = null;
    public boolean isVideoCalling;
    // 记录是否已经初始化
    private boolean isInit = false;
    UserManager userManager = new UserManagerImpl();

    private EaseHelper() {
    }

    public synchronized static EaseHelper getInstance() {
        if (instance == null) {
            instance = new EaseHelper();
        }
        return instance;
    }

    public void init(AppContext mContext) {
        // demoModel = new DemoModel(context);
        appContext = mContext;
        EMOptions options = initChatOptions();
        setGlobalListeners();
    }

    /**
     * set global listener
     */
    protected void setGlobalListeners() {
        //注册来点监听
        IntentFilter callFilter = new IntentFilter(EMClient.getInstance().callManager().getIncomingCallBroadcastAction());
        if (callReceiver == null) {
            callReceiver = new CallReceiver();
        }
        appContext.registerReceiver(callReceiver, callFilter);

        // Offline call push
       // EMClient.getInstance().callManager().getCallOptions().setIsSendPushIfOffline(getModel().isPushCall());
        //消息监听
        EMClient.getInstance().chatManager().addMessageListener(this);
        //连接状态监听
        EMClient.getInstance().addConnectionListener(this);
        //用户信息提供者
        EaseUI.getInstance().setUserProfileProvider(this);
        EaseUI.getInstance().setSettingsProvider(this);
        EaseUI.getInstance().getNotifier().setNotificationInfoProvider(this);

    }

    private EMOptions initChatOptions() {
        // 获取当前进程 id 并取得进程名
        int pid = android.os.Process.myPid();
        String processAppName = getAppName(pid);
        /**
         * 如果app启用了远程的service，此application:onCreate会被调用2次
         * 为了防止环信SDK被初始化2次，加此判断会保证SDK被初始化1次
         * 默认的app会在以包名为默认的process name下运行，如果查到的process name不是app的process name就立即返回
         */
        if (processAppName == null || !processAppName.equalsIgnoreCase(appContext.getPackageName())) {
            // 则此application的onCreate 是被service 调用的，直接返回
            return null;
        }
        if (isInit) {
            return null;
        }
        /**
         * SDK初始化的一些配置
         * 关于 EMOptions 可以参考官方的 API 文档
         * http://www.easemob.com/apidoc/android/chat3.0/classcom_1_1hyphenate_1_1chat_1_1_e_m_options.html
         */
        EMOptions options = new EMOptions();
        // 设置Appkey，如果配置文件已经配置，这里可以不用设置
        // options.setAppKey("guaju");
        // 设置自动登录
        options.setAutoLogin(false);
        // 设置是否需要发送已读回执
        options.setRequireAck(true);
        // 设置是否需要发送回执，TODO 这个暂时有bug，上层收不到发送回执
        options.setRequireDeliveryAck(true);
        // 设置是否需要服务器收到消息确认
        //options.setRequireServerAck(true);
        // 收到好友申请是否自动同意，如果是自动同意就不会收到好友请求的回调，因为sdk会自动处理，默认为true
        options.setAcceptInvitationAlways(false);
        // 设置是否自动接收加群邀请，如果设置了当收到群邀请会自动同意加入
        options.setAutoAcceptGroupInvitation(false);
        // 设置（主动或被动）退出群组时，是否删除群聊聊天记录
        options.setDeleteMessagesAsExitGroup(false);
        // 设置是否允许聊天室的Owner 离开并删除聊天室的会话
        options.allowChatroomOwnerLeave(true);
        // 设置google GCM推送id，国内可以不用设置
        // options.setGCMNumber(MLConstants.ML_GCM_NUMBER);
        // 设置集成小米推送的appid和appkey
        // options.setMipushConfig(MLConstants.ML_MI_APP_ID, MLConstants.ML_MI_APP_KEY);
        // 初始化UI组件
        EaseUI.getInstance().init(appContext, options);
        // 设置初始化已经完成
        isInit = true;
        return options;
    }

    /**
     * 根据Pid获取当前进程的名字，一般就是当前app的包名
     *
     * @param pid 进程的id
     * @return 返回进程的名字
     */
    private String getAppName(int pid) {
        String processName = null;
        ActivityManager activityManager = (ActivityManager) appContext.getSystemService(Context.ACTIVITY_SERVICE);
        List list = activityManager.getRunningAppProcesses();
        Iterator i = list.iterator();
        while (i.hasNext()) {
            ActivityManager.RunningAppProcessInfo info = (ActivityManager.RunningAppProcessInfo) (i.next());
            try {
                if (info.pid == pid) {
                    // 根据进程的信息获取当前进程的名字
                    processName = info.processName;
                    // 返回当前进程名
                    return processName;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // 没有匹配的项，返回为null
        return null;
    }

    /**
     * 消息监听
     *
     * @param list
     */
    @Override
    public void onMessageReceived(List<EMMessage> list) {
        for (EMMessage message : list) {
            if(!EaseUI.getInstance().hasForegroundActivies()){
                EaseUI.getInstance().getNotifier().onNewMsg(message);
            }
            Message message1 = new Message();
            String memberid = "";
            String nickName = "";
            String endTime = "";
            JSONObject json = null;
            String conslutionId = null;
            String clinicId = null;
            String homeOrderId  = null;
            try {
                json = message.getJSONObjectAttribute("em_apns_ext");
            } catch (HyphenateException e) {
                e.printStackTrace();
            }
            try {
                try {
                    conslutionId = message.getStringAttribute("conslutionId");
                    memberid = json.getString("memberId");
                    nickName = json.getString("name");
                } catch (HyphenateException e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                memberid = json.getString("memberId");
                nickName = json.getString("name");
                clinicId = json.getString("clinicId");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                memberid = json.getString("memberId");
                nickName = json.getString("name");
                homeOrderId  = json.getString("homeOrderId");
                endTime  = json.getString("endTime");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (conslutionId != null ) {
                message1.setOrderId(conslutionId);
                message1.setEndTime(null);
                message1.setHomeOrderId(null);
                message1.setClinicId(null);
            }
            if (clinicId!=null){
                message1.setClinicId(clinicId);
                message1.setEndTime(null);
                message1.setOrderId(null);
                message1.setHomeOrderId(null);
            }
            if (homeOrderId !=null){
                message1.setHomeOrderId(homeOrderId);
                message1.setEndTime(endTime);
                message1.setClinicId(null);
                message1.setOrderId(null);
            }
            message1.setId((int) message.getMsgTime());
            message1.setTime(message.getMsgTime());
            message1.setDoctor(message.getUserName());
            message1.setRead(false);
            message1.setMemberId(memberid);
            message1.setNickName(nickName);
            message1.setType(message.getType().name());
            if (message.getType().name().equals("TXT")) {
                EMTextMessageBody txtBody = (EMTextMessageBody) message.getBody();
                message1.setContent(txtBody.getMessage());
            } else {
                message1.setContent(message.getBody().toString());
            }
            message1.save();
        }
        //收到消息
        //更新消息数量
        Intent intent = new Intent();
        intent.putExtra("key", "UPDATE_MSG_COUNT");
        intent.setAction("UPDATE_MSG_COUNT");
        appContext.sendBroadcast(intent);
    }


    @Override
    public void onCmdMessageReceived(List<EMMessage> list) {
        //收到透传消息
    }

    @Override
    public void onMessageRead(List<EMMessage> list) {
        //收到已读回执
    }

    @Override
    public void onMessageDelivered(List<EMMessage> list) {
        //收到已送达回执
    }
    @Override
    public void onMessageChanged(EMMessage emMessage, Object o) {
        //消息状态变动
    }

    /**
     * 连接状态监听
     */
    @Override
    public void onConnected() {
        //连接
    }

    @Override
    public void onDisconnected(int error) {
        //断开
        if (error == EMError.USER_REMOVED) {
            // 显示帐号已经被移除
        } else if (error == EMError.USER_LOGIN_ANOTHER_DEVICE) {
            // 显示帐号在其他设备登录
            // 发送广播弹窗提示
            // Toaster.showShort(appContext,"您的正好在其他设备登录！");
            AbSharedUtil.putString(appContext.getActivity(), "userId", null);
            AbSharedUtil.putInt(appContext.getActivity(), "callUserid", 0);
            appContext.setUserInfo(null);
            Intent intent = new Intent();
            intent.setClass(appContext.getActivity(), MainActivity.class);
            intent.putExtra("otherLogin", true);
            appContext.getActivity().startActivity(intent);
            Log.e("其他设备登录", "其他设备登录");
        } else {
            if (NetUtils.hasNetwork(appContext.getActivity())) {
                //连接不到聊天服务器
            } else {
                //当前网络不可用，请检查网络设置
            }
        }
        EMClient.getInstance().chatManager().addMessageListener(messageListener);
    }

    /**
     * 用户信息提供者
     *
     * @param username
     * @return
     */
    @Override
    public EaseUser getUser(String username) {
        //首先网络获取用户 头像，username是环信用户名
        EaseUser user = new EaseUser(username);
        UserInfo userDao = new UserInfo();
        List<UserInfo> userInfoList = new ArrayList<>();
        userInfoList.addAll(userDao.findAll());
        for (UserInfo user1 : userInfoList) {
            if (user1.getHuanxin_username().equals(username)) {
                userDao = user1;
            }
        }
        if (userDao != null && userDao.getMember_id() != null) {
            String head = Constants.REST_ORGIN + "/emedicine/pub/member_logo/" + userDao.getMember_id() + "/" + userDao.getMember_id() + ".png";
            user.setAvatar(head);
            user.setNickname(userDao.getUsername());
            return user;
        }
//        }else {
//            getUserInfo(username);
//        }
        return user;
    }

    /**
     * 通知
     *
     * @param message
     * @return
     */
    @Override
    public String getDisplayedText(EMMessage message) {
        // 设置状态栏的消息提示，可以根据message的类型做相应提示
        String ticker = EaseCommonUtils.getMessageDigest(message, appContext);
        if(message.getType() == EMMessage.Type.TXT) {
            ticker = ticker.replaceAll("\\[.{2,3}\\]", "[表情]");
        }
        try {
            json = message.getJSONObjectAttribute("em_apns_ext");
            memberid = json.getString("memberId");
            nickName = json.getString("name");
        } catch (HyphenateException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(nickName != null){
            return nickName+ ": " + ticker;
        }else{
            return message.getFrom() + ": " + ticker;
        }
    }

    @Override
    public String getLatestText(EMMessage message, int fromUsersNum, int messageNum) {
        return null;
    }

    @Override
    public String getTitle(EMMessage message) {
        return nickName;
    }

    @Override
    public int getSmallIcon(EMMessage message) {
        return R.mipmap.ic_launcher;
    }

    @Override
    public Intent getLaunchIntent(EMMessage message) {
        //点击通知栏跳转事件
        // you can set what activity you want display when user click the notification
        Intent intent = new Intent(appContext, ConversationActivity.class);
        if (appContext.checkApkExist()){
            // open calling activity if there is call
            if(isVideoCalling){
                intent = new Intent(appContext, VideoCallActivity.class);
                intent.putExtra("username", message.getFrom());
                intent.putExtra("isComingCall", true);
            }else{
                JSONObject json = null;
                String conslutionId = null;
                String nickName = null;
                String memberid = null;
                String clinicId = null;
                String homeOrderId  = null;
                String endTime  = null;
                try {
                    json = message.getJSONObjectAttribute("em_apns_ext");
                } catch (HyphenateException e) {
                    e.printStackTrace();
                }
                try {
                    try {
                        conslutionId = message.getStringAttribute("conslutionId");
                        memberid = json.getString("memberId");
                        nickName = json.getString("name");
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    memberid = json.getString("memberId");
                    nickName = json.getString("name");
                    clinicId = json.getString("clinicId");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    memberid = json.getString("memberId");
                    nickName = json.getString("name");
                    homeOrderId  = json.getString("homeOrderId");
                    endTime  = json.getString("endTime");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (clinicId!=null){
                    Bundle args = new Bundle();
                    args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                    args.putString(EaseConstant.EXTRA_USER_ID, message.getFrom());
                    args.putString("DOC_NAME", nickName);
                    args.putString("CLINIC_ID",clinicId);
                    intent.putExtras(args);
                }else if (homeOrderId!=null){
                    Bundle args = new Bundle();
                    args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                    args.putString(EaseConstant.EXTRA_USER_ID, message.getFrom());
                    args.putString("DOC_NAME", nickName);
                    args.putString("homeOrderId",homeOrderId);
                    args.putString("endTime",endTime);
                    intent.putExtras(args);
                }else if(conslutionId!= null &&!conslutionId.equals("")&&!conslutionId.equals("0")){
                    Bundle args = new Bundle();
                    args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                    args.putString(EaseConstant.EXTRA_USER_ID, message.getFrom());
                    args.putString("DT_RowId", conslutionId);
                    args.putString("DOC_NAME", nickName);
                    intent.putExtras(args);
                }
            }
        }else{
            //如果程序没有启动  点击跳转
            intent = new Intent(appContext, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            intent.putExtra("message",message);
        }
        return intent;
    }

    @Override
    public boolean isMsgNotifyAllowed(EMMessage message) {
        return true;
    }

    @Override
    public boolean isMsgSoundAllowed(EMMessage message) {
        return true;
    }

    @Override
    public boolean isMsgVibrateAllowed(EMMessage message) {
        return true;
    }

    @Override
    public boolean isSpeakerOpened() {
        return true;
    }





    
}
