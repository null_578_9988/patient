package com.wonhx.patient.app.model;

/**
 * Created by Administrator on 2017/6/26.
 */

public class FamilyDoctorSearchList {
    String doctor_id;
    String member_id;
    String doctor_name;

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }
}
