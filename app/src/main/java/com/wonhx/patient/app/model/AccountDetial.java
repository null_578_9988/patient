package com.wonhx.patient.app.model;

/**
 * Created by Administrator on 2017/4/7.
 */

public class AccountDetial {
    String type;
    String flow_time;
    String amount;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFlow_time() {
        return flow_time;
    }

    public void setFlow_time(String flow_time) {
        this.flow_time = flow_time;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
