package com.wonhx.patient.app.activity.firstpage;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hyphenate.easeui.ui.EaseBaiduMapActivity;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.adapter.PriceAdapter;
import com.wonhx.patient.app.adapter.Zhensuoitem;
import com.wonhx.patient.app.adapter.Zhensuoitema;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.FamialyDoctors;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.MoveHospital;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.ProvinceCity;
import com.wonhx.patient.app.model.Zhnsuo;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.XListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class HomeDoctorActivity extends BaseActivity implements XListView.IXListViewListener {
    @InjectView(R.id.tv_city)
    TextView tvCity;
    @InjectView(R.id.iv_city)
    ImageView ivCity;
    @InjectView(R.id.rl_all_city)
    RelativeLayout rlAllCity;
    @InjectView(R.id.tv_keshi)
    TextView tvKeshi;
    @InjectView(R.id.iv_keshi)
    ImageView ivKeshi;
    @InjectView(R.id.rl_all_keshi)
    RelativeLayout rlAllKeshi;
    @InjectView(R.id.tv_price)
    TextView tvPrice;
    @InjectView(R.id.iv_price)
    ImageView ivPrice;
    @InjectView(R.id.rl_all_price)
    RelativeLayout rlAllPrice;
    @InjectView(R.id.lll)
    LinearLayout lll;
    @InjectView(R.id.lv_provice)
    ListView lvProvice;
    @InjectView(R.id.lv_city)
    ListView lvCity;
    @InjectView(R.id.ll_diqu)
    LinearLayout llDiqu;
    @InjectView(R.id.lv_fu)
    ListView lvFukeshi;
    @InjectView(R.id.lv_zi)
    ListView lvZikeshi;
    @InjectView(R.id.ll_zhensuo)
    LinearLayout llZhensuo;
    @InjectView(R.id.lv_price)
    ListView lvPrice;
    @InjectView(R.id.ll_price)
    LinearLayout llPrice;
    @InjectView(R.id.load_more_list)
    XListView loadMoreList;
    FirstPagerMager firstpagermager = new FirstPagerMangerImal();
    String city_id = "0", dept_id = "0", length = "10", sort = "", city_name = "";
    int star = 0;
    QuickAdapter<FamialyDoctors> adapter_all;
    List<FamialyDoctors> all_list = new ArrayList<>();
    Zhensuoitema adapter_p;
    QuickAdapter<ProvinceCity.City> adapter_c;
    QuickAdapter<Zhnsuo.Sub_dept_name> adapter_zi;
    List<String> fuzhensuo_list = new ArrayList<>();
    List<Zhnsuo.Sub_dept_name> zizhensuo_list = new ArrayList<>();
    Zhensuoitem adapter_fu;
    List<ProvinceCity.City> city = new ArrayList<>();
    List<String> provience = new ArrayList<>();
    boolean diqu = true, zhensuos = true,price=true;
    List<String>list_price=new ArrayList<>();
    PriceAdapter adapter_price;
    MyReceiver myReceiver;
    private static final int BAIDU_READ_PHONE_STATE =100;
    Handler handler;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            unregisterReceiver(myReceiver);
        }
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("QUIT");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String str) {
                finish();
            }
        });
        adapter_p = new Zhensuoitema(HomeDoctorActivity.this, provience);
        adapter_price=new PriceAdapter(this,list_price);
        lvPrice.setAdapter(adapter_price);
        adapter_fu = new Zhensuoitem(HomeDoctorActivity.this, fuzhensuo_list);
        adapter_c = new QuickAdapter<ProvinceCity.City>(HomeDoctorActivity.this, R.layout.doctor_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, ProvinceCity.City item) {
                helper.setText(R.id.doctor, item.getName_cn());
            }
        };
        adapter_zi = new QuickAdapter<Zhnsuo.Sub_dept_name>(this, R.layout.doctor_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, Zhnsuo.Sub_dept_name item) {
                helper.setText(R.id.doctor, item.getName());
            }
        };
        loadMoreList.setPullLoadEnable(false);
        adapter_all = new QuickAdapter<FamialyDoctors>(this, R.layout.familydoctor_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, FamialyDoctors item) {
                helper.setText(R.id.doctor_name, item.getDoctor_name() != null && !item.getDoctor_name().equals("") ? item.getDoctor_name() : "")
                        .setText(R.id.doctor_dept, item.getDept_name() != null && !item.getDept_name().equals("") ? item.getDept_name() : "")
                        .setText(R.id.doctor_title, item.getTitle() != null && !item.getTitle().equals("") ? item.getTitle() : "")
                        .setText(R.id.doctor_hospitalName, item.getHospital_name() != null && !item.getHospital_name().equals("") ? item.getHospital_name() : "")
                        .setText(R.id.doctor_goodSubjects, item.getGood_subjects() != null && !item.getGood_subjects().equals("") ? item.getGood_subjects() : "")
                        .setText(R.id.price, item.getPrice() != null && !item.getPrice().equals("")? item.getPrice()+"/元起" : "");
                SimpleDraweeView image = helper.getView(R.id.logo);
                image.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine/pub/member_logo/" + item.getMember_id() + "/" + item.getMember_id() + ".png"));
            }
        };
        loadMoreList.setAdapter(adapter_all);
        loadMoreList.setXListViewListener(this);
        handler = new Handler();
        loadMoreList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent in=new Intent(HomeDoctorActivity.this, DoctorDetialActivity.class);
                in.putExtra("member_id",all_list.get(i-1).getMember_id());
                startActivity(in);
            }
        });

        lvProvice.setAdapter(adapter_p);
        lvCity.setAdapter(adapter_c);
        lvFukeshi.setAdapter(adapter_fu);
        lvZikeshi.setAdapter(adapter_zi);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_doctor);
        ButterKnife.inject(this);
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        getfamilydoctorlist();
        //获取城市
        getcity();
        //获取诊所
        getzhensuo();
        //获取价格
        getprice();
    }


    @OnClick({R.id.left_btn, R.id.rl_all_city, R.id.rl_all_keshi, R.id.rl_all_price, R.id.serc,R.id.ll_diqu,R.id.ll_zhensuo,R.id.ll_price,R.id.iv_near})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.ll_diqu:
                llDiqu.setVisibility(View.GONE);
                tvCity.setTextColor(Color.BLACK);
                ivCity.setImageResource(R.mipmap.under);
                diqu = true;
                break;
            case R.id.ll_zhensuo:
                llZhensuo.setVisibility(View.GONE);
                tvKeshi.setTextColor(Color.BLACK);
                ivKeshi.setImageResource(R.mipmap.under);
                zhensuos = true;
                break;
            case R.id.ll_price:
                llPrice.setVisibility(View.GONE);
                ivPrice.setImageResource(R.mipmap.under);
                tvPrice.setTextColor(Color.BLACK);
                price = true;
                break;
            case R.id.serc:
                startActivity(new Intent(HomeDoctorActivity.this, Family_Doctor_SearchActivity.class));
                break;
            case R.id.rl_all_city:
                if (diqu) {
                    llZhensuo.setVisibility(View.GONE);
                    tvKeshi.setTextColor(Color.BLACK);
                    ivKeshi.setImageResource(R.mipmap.under);
                    ivPrice.setImageResource(R.mipmap.under);
                    tvPrice.setTextColor(Color.BLACK);
                    llPrice.setVisibility(View.GONE);
                    zhensuos = true;
                    llDiqu.setVisibility(View.VISIBLE);
                    tvCity.setTextColor(getResources().getColor(R.color.colorAccent));
                    ivCity.setImageResource(R.mipmap.lanshang);
                    diqu = false;
                } else {
                    llDiqu.setVisibility(View.GONE);
                    tvCity.setTextColor(Color.BLACK);
                    ivCity.setImageResource(R.mipmap.under);
                    diqu = true;
                }
                break;
            case R.id.rl_all_keshi:
                if (zhensuos) {
                    llDiqu.setVisibility(View.GONE);
                    tvCity.setTextColor(Color.BLACK);
                    ivCity.setImageResource(R.mipmap.under);
                    ivPrice.setImageResource(R.mipmap.under);
                    tvPrice.setTextColor(Color.BLACK);
                    llPrice.setVisibility(View.GONE);
                    diqu = true;
                    tvKeshi.setTextColor(getResources().getColor(R.color.colorAccent));
                    ivKeshi.setImageResource(R.mipmap.lanshang);
                    tvCity.setTextColor(Color.BLACK);
                    llZhensuo.setVisibility(View.VISIBLE);
                    zhensuos = false;
                } else {
                    llZhensuo.setVisibility(View.GONE);
                    tvKeshi.setTextColor(Color.BLACK);
                    ivKeshi.setImageResource(R.mipmap.under);
                    zhensuos = true;
                }
                break;
            case R.id.rl_all_price:
                if (price){
                    llDiqu.setVisibility(View.GONE);
                    tvCity.setTextColor(Color.BLACK);
                    ivCity.setImageResource(R.mipmap.under);
                    tvKeshi.setTextColor(Color.BLACK);
                    ivKeshi.setImageResource(R.mipmap.under);
                    llZhensuo.setVisibility(View.GONE);
                    price = false;
                    tvPrice.setTextColor(getResources().getColor(R.color.colorAccent));
                    ivPrice.setImageResource(R.mipmap.lanshang);
                    llPrice.setVisibility(View.VISIBLE);
                }else {
                    llPrice.setVisibility(View.GONE);
                    ivPrice.setImageResource(R.mipmap.under);
                    tvPrice.setTextColor(Color.BLACK);
                    price = true;
                }
                break;
            //附近的医生
            case R.id.iv_near:
                if (Build.VERSION.SDK_INT >= 23) {
                    showContacts();
                } else {
                    UIKit.open(this,NearHomeDoctorActivity.class);//调用具体方法
                }

                    break;
            default:
                break;

        }
    }
    public void showContacts(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(this,"没有权限,请手动开启定位权限",Toast.LENGTH_SHORT).show();
            // 申请一个（或多个）权限，并提供用于回调返回的获取码（用户定义）
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, BAIDU_READ_PHONE_STATE);
        }else{
            UIKit.open(this,NearHomeDoctorActivity.class);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case BAIDU_READ_PHONE_STATE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 获取到权限，作相应处理（调用定位SDK应当确保相关权限均被授权，否则可能引起定位失败）
                    UIKit.open(this,NearHomeDoctorActivity.class);
                } else {
                    // 没有获取到权限，做特殊处理
                   // Toast.makeText(this, "获取位置权限失败，请手动开启", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    private void getprice() {
        adapter_price.clear();
        list_price.add("从低到高");
        list_price.add("从高到低");
        adapter_price.notifyDataSetChanged();
        lvPrice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                adapter_price.changeSelected(i);
                tvPrice.setTextColor(Color.BLACK);
                ivPrice.setImageResource(R.mipmap.under);
                if (i==0){
                    tvPrice.setText("从低到高");
                    sort="ASC";

                }else {
                    tvPrice.setText("从高到低");
                    sort="DESC";
                }
                star=0;
                price=true;
                all_list.clear();
                adapter_all.clear();
                getfamilydoctorlist();
                llPrice.setVisibility(View.GONE);
            }
        });
    }

    private void getzhensuo() {
        firstpagermager.get_zhensuo(new SubscriberAdapter<ListProResult<Zhnsuo>>() {
            @Override
            public void success(final ListProResult<Zhnsuo> zhnsuo) {
                super.success(zhnsuo);
                adapter_fu.clear();
                for (int i = 0; i < zhnsuo.getData().size(); i++) {
                    fuzhensuo_list.add(zhnsuo.getData().get(i).getParent_dept_name());
                }
                if (fuzhensuo_list.size() > 0) {
                    adapter_fu.notifyDataSetChanged();
                }
                lvFukeshi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, final int p, long l) {
                        adapter_fu.changeSelected(p);
                        adapter_zi.clear();
                        zizhensuo_list.clear();
                        if (p > 0) {
                            Zhnsuo.Sub_dept_name sub = new Zhnsuo.Sub_dept_name();
                            sub.setName("全部");
                            sub.setId(zhnsuo.getData().get(p).getSub_dept_name().get(0).getId());
                            zhnsuo.getData().get(p).getSub_dept_name().set(0, sub);
                            zizhensuo_list.addAll(zhnsuo.getData().get(p).getSub_dept_name());
                            if (zizhensuo_list.size() > 0) {
                                adapter_zi.addAll(zizhensuo_list);
                            }
                        } else {
                            dept_id = "0";
                            star = 0;
                            all_list.clear();
                            adapter_all.clear();
                            getfamilydoctorlist();
                            llZhensuo.setVisibility(View.GONE);
                            tvKeshi.setTextColor(Color.BLACK);
                            ivKeshi.setImageResource(R.mipmap.under);
                            zhensuos = true;
                            tvKeshi.setText("全部诊所");
                        }
                        lvZikeshi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i == 0) {
                                    tvKeshi.setText(fuzhensuo_list.get(p));
                                } else {
                                    tvKeshi.setText(zizhensuo_list.get(i).getName());
                                }
                                dept_id = zizhensuo_list.get(i).getId();
                                star = 0;
                                all_list.clear();
                                adapter_all.clear();
                                getfamilydoctorlist();
                                llZhensuo.setVisibility(View.GONE);
                                tvKeshi.setTextColor(Color.BLACK);
                                ivKeshi.setImageResource(R.mipmap.under);
                                zhensuos = true;
                            }
                        });
                    }
                });
            }
        });
    }

    private void getcity() {
        firstpagermager.get_prvince_city(new SubscriberAdapter<ListProResult<ProvinceCity>>() {
            @Override
            public void success(final ListProResult<ProvinceCity> provinceCityListProResult) {
                super.success(provinceCityListProResult);
                adapter_p.clear();
                city.clear();
                for (int i = 0; i < provinceCityListProResult.getData().size(); i++) {
                    provience.add(provinceCityListProResult.getData().get(i).getProvince());
                }
                if (provience.size() > 0) {
                    adapter_p.notifyDataSetChanged();
                }
                adapter_c.addAll(city);
                lvProvice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, final int p, long l) {
                        adapter_p.changeSelected(p);
                        adapter_c.clear();
                        city.clear();
                        if (p > 0) {
                            ProvinceCity.City citya = new ProvinceCity.City();
                            citya.setName_cn("全部");
                            citya.setId(provinceCityListProResult.getData().get(p).getCity().get(0).getId());
                            provinceCityListProResult.getData().get(p).getCity().set(0, citya);
                            city.addAll(provinceCityListProResult.getData().get(p).getCity());
                            adapter_c.addAll(city);
                        } else {
                            llDiqu.setVisibility(View.GONE);
                            tvCity.setTextColor(Color.BLACK);
                            ivCity.setImageResource(R.mipmap.under);
                            diqu = true;
                            city_id = "0";
                            city_name = "";
                            star = 0;
                            tvCity.setText("全部地区");
                            all_list.clear();
                            adapter_all.clear();
                            getfamilydoctorlist();

                        }
                        lvCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i == 0) {
                                    tvCity.setText(provience.get(p));
                                    city_name = provience.get(p);
                                } else {
                                    tvCity.setText(city.get(i).getName_cn());
                                    city_name = city.get(i).getName_cn();
                                }
                                city_id = city.get(i).getId();
                                star = 0;
                                all_list.clear();
                                adapter_all.clear();
                                getfamilydoctorlist();
                                llDiqu.setVisibility(View.GONE);
                                tvCity.setTextColor(Color.BLACK);
                                ivCity.setImageResource(R.mipmap.under);
                                diqu = true;
                            }
                        });
                    }
                });
            }
        });
    }

    private void getfamilydoctorlist() {
        firstpagermager.getFamilyDoctorList(city_id, dept_id, sort, star + "", length, new SubscriberAdapter<ListProResult<FamialyDoctors>>() {
            @Override
            public void onError(Throwable e) {
               // super.onError(e);
                dismissLoadingDialog();
            }

            @Override
            public void success(ListProResult<FamialyDoctors> famialyDoctorsProResult) {
                super.success(famialyDoctorsProResult);
                    all_list.addAll(famialyDoctorsProResult.getData());
                    if (all_list.size() > 0) {
                        adapter_all.replaceAll(all_list);
                        if (all_list.size()>=10){
                            loadMoreList.setPullLoadEnable(true);
                        }else {
                            loadMoreList.setPullLoadEnable(false);
                        }
                }
            }
        });
    }


    @Override
    public void onRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                star = 0;
                all_list.clear();
                getfamilydoctorlist();
                onLoad();
            }
        }, 1000);
    }
    private void onLoad() {
        loadMoreList.stopRefresh();
        loadMoreList.stopLoadMore();
        loadMoreList.setRefreshTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }
    @Override
    public void onLoadMore() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                star=star+10;
                getfamilydoctorlist();
                onLoad();
            }
        }, 1000);
    }
}
