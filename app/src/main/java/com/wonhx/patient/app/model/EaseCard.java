package com.wonhx.patient.app.model;

/**
 * Created by nsd on 2017/6/6.
 * 名片
 */

public class EaseCard {
    public static final String myExtType = "type";
    public static final String cardOwnerId = "cardOwnerId";
    public static final String cardOwnerName = "cardOwnerName";
    public static final String cardOwnerHeadUrl= "cardOwnerHeadUrl";
    public static final String cardOwnerDept = "cardOwnerDept";
    public static final String cardOwnerType = "cardOwnerType";
}
