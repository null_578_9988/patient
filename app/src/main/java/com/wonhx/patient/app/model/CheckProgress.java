package com.wonhx.patient.app.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by apple on 2017/4/8.
 * 健康档案：体检流水实体
 */

public class CheckProgress  implements Serializable{
    private String record_id;
    private String recordname;
    private String result;
    private String effective_date;
    private List<Img_path>img_path;

    public List<Img_path> getImg_path() {
        return img_path;
    }

    public void setImg_path(List<Img_path> img_path) {
        this.img_path = img_path;
    }

    public String getEffective_date() {
        return effective_date;
    }

    public void setEffective_date(String effective_date) {
        this.effective_date = effective_date;
    }

    public String getRecord_id() {
        return record_id;
    }

    public void setRecord_id(String record_id) {
        this.record_id = record_id;
    }

    public String getRecordname() {
        return recordname;
    }

    public void setRecordname(String recordname) {
        this.recordname = recordname;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
    public class Img_path implements Serializable {
        String path_id;
        String path;

        public String getPath_id() {
            return path_id;
        }

        public void setPath_id(String path_id) {
            this.path_id = path_id;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }
}

}
