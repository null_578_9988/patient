package com.wonhx.patient.app.model;

/**
 * Created by Administrator on 2017/3/31 0031.
 *
 */

public class DoctorList {
    private String name;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
