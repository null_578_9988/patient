package com.wonhx.patient.app.activity.firstpage;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alipay.sdk.app.PayTask;
import com.tencent.mm.opensdk.constants.Build;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.unionpay.UPPayAssistEx;
import com.unionpay.uppay.PayActivity;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.user.MyFamilyDoctorActivity;
import com.wonhx.patient.app.activity.user.OrderPayActivity;
import com.wonhx.patient.app.activity.user.PayFinishActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Account;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.WeXinBean;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.PayResult;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FamilyPayActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.mPrice)
    TextView mPrice;
    @InjectView(R.id.phone_layout_phone)
    RelativeLayout phoneLayoutPhone;
    @InjectView(R.id.free_quan)
    TextView freeQuan;
    @InjectView(R.id.iv)
    ImageView iv;
    @InjectView(R.id.service_type_layout)
    RelativeLayout serviceTypeLayout;
    @InjectView(R.id.mAllPrice)
    TextView mAllPrice;
    @InjectView(R.id.phone_layout)
    RelativeLayout phoneLayout;
    @InjectView(R.id.yueImg)
    ImageView yueImg;
    @InjectView(R.id.tv_kryong)
    TextView tv_kryong;
    @InjectView(R.id.mYueCheck)
    ImageView mYueCheck;
    @InjectView(R.id.mYueRelayout)
    RelativeLayout mYueRelayout;
    @InjectView(R.id.baoimg)
    ImageView baoimg;
    @InjectView(R.id.textView6)
    TextView textView6;
    @InjectView(R.id.mAlipayCheck)
    ImageView mAlipayCheck;
    @InjectView(R.id.mAlipayRelayout)
    RelativeLayout mAlipayRelayout;
    @InjectView(R.id.kaimg)
    ImageView kaimg;
    @InjectView(R.id.textView3)
    TextView textView3;
    @InjectView(R.id.mKaCheck)
    ImageView mKaCheck;
    @InjectView(R.id.mKaRelayout)
    RelativeLayout mKaRelayout;
    @InjectView(R.id.weixin)
    ImageView weixin;
    @InjectView(R.id.textView4)
    TextView textView4;
    @InjectView(R.id.weixin_iv)
    ImageView weixin_iv;
    @InjectView(R.id.rl_weixin)
    RelativeLayout rlWeixin;
    @InjectView(R.id.submit_btn)
    TextView submitBtn;
    String price="",orderid="" ,zhifubaopayInfo = "", yinlianpayinfo = "", tn="";
    UserManager userManager = new UserManagerImpl();
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    int mPayType = 0; // 0 余额，1 支付宝，2 银行卡
    private IWXAPI api;
    private String mMode = "00";//设置测试模式:01为测试 00为正式环境
    MyReceiver myReceiver;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            unregisterReceiver(myReceiver);
        }
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        title.setText("订单支付");
        api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);
        api.registerApp(Constants.APP_ID);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("QUIT");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String str) {
                //tv1.setText(str);
                finish();
            }
        });
        mYueCheck.setImageResource(R.mipmap.true_select);
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        Intent intent=getIntent();
        price=intent.getStringExtra("price");
        orderid=intent.getStringExtra("orderid");
        mPrice.setText(price+"元");
        mAllPrice.setText(price+"元");
        getAccount();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_pay);
        ButterKnife.inject(this);

    }
    @OnClick({R.id.left_btn, R.id.mPrice, R.id.service_type, R.id.mYueRelayout, R.id.mAlipayRelayout, R.id.mKaRelayout, R.id.rl_weixin, R.id.submit_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.mYueRelayout:
                mYueCheck.setImageResource(R.mipmap.true_select);
                mAlipayCheck.setImageResource(0);
                mKaCheck.setImageResource(0);
                weixin_iv.setImageResource(0);
                mPayType = 0;
                break;
            case R.id.mAlipayRelayout:
                mAlipayCheck.setImageResource(R.mipmap.true_select);
                mYueCheck.setImageResource(0);
                mKaCheck.setImageResource(0);
                weixin_iv.setImageResource(0);
                mPayType = 1;
                break;
            case R.id.mKaRelayout:
                mKaCheck.setImageResource(R.mipmap.true_select);
                mYueCheck.setImageResource(0);
                mAlipayCheck.setImageResource(0);
                weixin_iv.setImageResource(0);
                mPayType = 2;
                break;
            case R.id.rl_weixin:
                weixin_iv.setImageResource(R.mipmap.true_select);
                mYueCheck.setImageResource(0);
                mAlipayCheck.setImageResource(0);
                mKaCheck.setImageResource(0);
                mPayType = 3;
                break;
            case R.id.submit_btn:
                //支付
                switch (mPayType) {
                    case 0:
                        //账户余额支付
                        payWithBlance();
                        break;
                    case 1:
                        //支付宝
                        alipay();
                        break;
                    case 2:
                        //银行卡
                        payWithBankCard();
                        break;
                    case 3:
                        //微信
                        payWithWinXin();
                        break;
                }
                break;
        }
    }

    /**
     * 微信支付
     */
    private void payWithWinXin() {
        boolean sIsWXAppInstalledAndSupported   = api.isWXAppInstalled()
                && api.isWXAppSupportAPI();
        if (sIsWXAppInstalledAndSupported ) {
            boolean isPaySupported = api.getWXAppSupportAPI() >= Build.PAY_SUPPORTED_SDK_INT;
            if (isPaySupported) {
                userManager.weixin(orderid, "D2", new SubscriberAdapter<WeXinBean>() {
                    @Override
                    public void success(WeXinBean result) {
                        super.success(result);
                        weixinpay(result);
                    }
                });
            }else {
                Toaster.showShort(FamilyPayActivity.this,"此版本还不支持微信支付");
            }
        }else {
            Toaster.showShort(FamilyPayActivity.this,"还未安装微信");
        }
    }
    private void weixinpay(WeXinBean result) {
        PayReq payreq=new PayReq();
        payreq.appId=result.getAppid();
        payreq.partnerId=result.getPartnerid();
        payreq.prepayId=result.getPrepayid();
        payreq.nonceStr=result.getNoncestr();
        payreq.timeStamp=result.getTimestamp();
        payreq.packageValue="Sign=WXPay";
        payreq.sign=result.getSign();
        Constants.flag=4;
        api.sendReq(payreq);
    }
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        startActivity(new Intent(FamilyPayActivity.this, MyFamilyDoctorActivity.class));
                        quit();
                        finish();

                        Toaster.showShort(FamilyPayActivity.this, "支付出成功" + "");
                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toaster.showShort(FamilyPayActivity.this, "支付结果确认中" + "");
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            //Toast.makeText(ZhifuActivity.this, "支付失败", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                }
                case  2:{
                    if (msg.obj == null || ((String) msg.obj).length() == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(FamilyPayActivity.this);
                        builder.setTitle("错误提示");
                        builder.setMessage("网络连接失败,请重试!");
                        builder.setNegativeButton("确定",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.create().show();
                    } else {
                        tn = (String) msg.obj;
                        doStartUnionPayPlugin(FamilyPayActivity.this, tn, mMode);
                    }
                }
                default:
                    break;
            }
        }
    };
    private void payWithBankCard(){
        userManager.yinlian(orderid,"D1",new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                yinlianpayinfo=result.getTn();
                Runnable runnable=new Runnable() {
                    @Override
                    public void run() {
                        // 调用支付接口，获取支付结果
                        Message msg = new Message();
                        msg.what = 2;
                        msg.obj = yinlianpayinfo;
                        handler.sendMessage(msg);
                    }
                };
                Thread payThread = new Thread(runnable);
                payThread.start();

            }
        });
    }

    /**
     * 支付宝支付
     */
    private void alipay() {
        userManager.zhifubao(orderid, "D", new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                zhifubaopayInfo = result.getData();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        PayTask alipay = new PayTask(FamilyPayActivity.this);
                        // 调用支付接口，获取支付结果
                        String result = alipay.pay(zhifubaopayInfo, true);
                        Message msg = new Message();
                        msg.what = 1;
                        msg.obj = result;
                        handler.sendMessage(msg);
                    }
                };
                Thread payThread = new Thread(runnable);
                payThread.start();
            }
        });
    }

    /**
     * 余额支付
     */
    private void payWithBlance(){
        firstPagerMager.payWithBlanceFamily(orderid,new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                startActivity(new Intent(FamilyPayActivity.this, MyFamilyDoctorActivity.class));
                quit();
                finish();
                Toaster.showShort(FamilyPayActivity.this,result.getMsg());
            }
        });
    }
    private  void quit(){
        Intent intent = new Intent();
        intent.setAction("QUIT");
        sendBroadcast(intent);
    }
    private void getAccount() {
       userManager .getaccount(AbSharedUtil.getString(FamilyPayActivity.this,"userId"),new SubscriberAdapter<ProResult<Account>>(){
            @Override
            public void success(ProResult<Account> accountProResult) {
                super.success(accountProResult);
                if (accountProResult.getData().getAccount_balance()!=null&&!accountProResult.getData().getAccount_balance().equals("")) {
                    tv_kryong.setText("可用余额："+accountProResult.getData().getAccount_balance());
                    ;
                }else {
                    tv_kryong.setText("可用余额："+"0");
                }
            }
        });
    }
    public void doStartUnionPayPlugin(Activity activity, String tn, String mode) {
        UPPayAssistEx.startPayByJAR(activity, PayActivity.class, null, null,
                tn, mode);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        String msg = "";
        /*
         * 支付控件返回字符串:success、fail、cancel 分别代表支付成功，支付失败，支付取消
         */
        String str = data.getExtras().getString("pay_result");
        Log.e("zftphone", "2 "+data.getExtras().getString("merchantOrderId"));
        if (str.equalsIgnoreCase("success")) {
            msg = "支付成功！";
            startActivity(new Intent(FamilyPayActivity.this, MyFamilyDoctorActivity.class));
            quit();
            finish();
        } else if (str.equalsIgnoreCase("fail")) {
            msg = "支付失败！";
        } else if (str.equalsIgnoreCase("cancel")) {
            msg = "用户取消了支付";
        }
        Toaster.showShort(FamilyPayActivity.this,msg);
        //支付完成,处理自己的业务逻辑!
    }
}
