package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.model.LatLng;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by nsd on 2017/6/13.
 * 诊所位置
 */
public class ClinicLocationActivity extends BaseActivity {
    @InjectView(R.id.bmapView)
    MapView mMapView;
    @InjectView(R.id.title)
    TextView mTitle;
    String la="",ln="",address="";
    @InjectView(R.id.placeTitle)
    TextView tv_name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initialize SDK with context, should call this before setContentView
        SDKInitializer.initialize(getApplicationContext());
        setContentView(R.layout.activity_clinic_location);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("位置信息");
        Intent intent=getIntent();
        la=intent.getStringExtra("la");
        ln=intent.getStringExtra("ln");
        address=intent.getStringExtra("address");
        tv_name.setText(address);
        BaiduMap mBaiduMap = mMapView.getMap();
        LatLng center = new LatLng(Double.parseDouble(la), Double.parseDouble(ln));
        //定义地图状态
        MapStatus mMapStatus = new MapStatus.Builder()
                .target(center)
                .zoom(18)
                .build();
        //定义MapStatusUpdate对象，以便描述地图状态将要发生的变化
        MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mMapStatus);
        //改变地图状态
        mBaiduMap.setMapStatus(mMapStatusUpdate);
        //准备 marker 的图片
        BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(R.mipmap.icon_marka);
        //准备 marker option 添加 marker 使用
        MarkerOptions markerOptions = new MarkerOptions().icon(bitmap).position(center);
        markerOptions.animateType(MarkerOptions.MarkerAnimateType.drop);
        //获取添加的 marker 这样便于后续的操作
        Marker marker = (Marker) mBaiduMap.addOverlay(markerOptions);
        // 删除百度地图LoGo
        mMapView.removeViewAt(1);

    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @OnClick(R.id.left_btn)
    public void back() {
        finish();
    }


}