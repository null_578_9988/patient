package com.wonhx.patient.app.activity.calldoctor;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.hedgehog.ratingbar.RatingBar;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.CallDoctorManager;
import com.wonhx.patient.app.manager.calldoctor.CallDoctorManagerImpl;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class  CallEvaluateActivity extends BaseActivity {
    @InjectView(R.id.ratingbar)
    RatingBar mRatingBar;
    @InjectView(R.id.edit_evaluate)
    EditText mContent;
    CallDoctorManager callDoctorManager = new CallDoctorManagerImpl();
    String service_id;
    int mRatingCount = 0;
    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.right_btn)
    TextView rightBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_evaluate);
        ButterKnife.inject(this);
        title.setText("评价");
        rightBtn.setText("提交");
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mRatingBar.setOnRatingChangeListener(
                new RatingBar.OnRatingChangeListener() {
                    @Override
                    public void onRatingChange(float RatingCount) {
                        mRatingCount = (int) RatingCount;
                    }
                }
        );
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        service_id = getIntent().getStringExtra("service_id");
    }

    @OnClick({R.id.left_btn, R.id.right_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.right_btn:
                if (mRatingCount == 0) {
                    Toaster.showShort(CallEvaluateActivity.this, "请选择评论星级！");
                    return;
                }
                if (StrKit.isBlank(mContent.getText().toString().trim())) {
                    Toaster.showShort(CallEvaluateActivity.this, "请输入评论信息！");
                    return;
                }
                callDoctorManager.evaluateRecord(service_id, String.valueOf(mRatingCount), mContent.getText().toString().trim(), new SubscriberAdapter<Result>() {
                    @Override
                    public void success(Result result) {
                        super.success(result);
                        Toaster.showShort(CallEvaluateActivity.this, result.getMsg());
                        finish();
                    }
                });
                break;
        }
    }
}
