package com.wonhx.patient.app.activity.firstpage;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.AskOrderResult;
import com.wonhx.patient.app.model.DoctorDetial;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.FileKit;
import com.wonhx.patient.kit.ImageKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.view.LoginDialog;
import com.wonhx.patient.view.NoScrollGridView;
import com.wonhx.patient.view.QuiteDialog;

import org.xutils.x;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

/**
 * Created by nsd on 2017/4/21.
 * 电话咨询
 */

public class PhoneVideoAskDoctorActivity extends BaseActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    @InjectView(R.id.right_btn)
    TextView mSubmit;
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.free_publish_img_grid)
    NoScrollGridView mImgGridView;
    @InjectView(R.id.description_Edit)
    EditText mDescription;
    @InjectView(R.id.hospital)
    EditText mHostipal;
    @InjectView(R.id.tishi)
    TextView mPicTiShi;
    @InjectView(R.id.maketime)
    TextView mMakeTime;
    @InjectView(R.id.illtime)
    TextView mIllTime;
    @InjectView(R.id.patient_descriptions)
    EditText mIllDescrip;
    @InjectView(R.id.ed_jibinhg)
    EditText ed_jibing;
    /**
     * 待提交图片列表
     */
    ArrayList<String> mUpdateImgs = new ArrayList<>();
    /**
     * 压缩图片列表
     */
    List<String> mThumbMediaUrls = new ArrayList<>();
    QuickAdapter<String> mImgAdapter;
    String mAddImgPath = "ADD_IMG_PATH";
    QuiteDialog mDeleteDialog;
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    DoctorDetial mDetail;
    // 存放缩略图的文件夹
    String savePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Patient/Camera/";
    DoctorDetial.Schedule time;
    int type = 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activityf_phoneask_doctor);
    }


    @Override
    protected void onInitView() {
        super.onInitView();
        mSubmit.setText("提交");
        mImgAdapter = new QuickAdapter<String>(this, R.layout.pic_img_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, String item) {
                ImageView imgView = helper.getView(R.id.img_grid_item);
                if (item.equals(mAddImgPath)) {
                    imgView.setImageResource(R.mipmap.healthadd);
                } else {
                    x.image().bind(imgView, item);
                }
            }
        };
        mImgGridView.setAdapter(mImgAdapter);
        mImgGridView.setOnItemClickListener(this);
        mImgGridView.setOnItemLongClickListener(this);
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        mUpdateImgs.add(mAddImgPath);
        mImgAdapter.addAll(mUpdateImgs);
        type = getIntent().getIntExtra("type",3);
        mDetail = (DoctorDetial) getIntent().getSerializableExtra("detail");
        if (type==3){
            mTitle.setText("电话咨询");
        }else if (type==4){
            mTitle.setText("视频咨询");
        }
    }

    @OnClick(R.id.left_btn)
    void back() {
        finish();
    }

    @OnClick(R.id.selectTime)
    void selectTime() {
        Intent intent = new Intent(PhoneVideoAskDoctorActivity.this, SelectMakeTimeActivity.class);
        intent.putExtra("schedules", (Serializable) mDetail.getSchedule());
        intent.putExtra("type",type);
        startActivityForResult(intent, 1002);
    }

    @OnClick(R.id.selectIllTime)
    void selectIllTime() {
        Intent intent = new Intent(PhoneVideoAskDoctorActivity.this, SelectITimeActivity.class);
        startActivityForResult(intent, 1003);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;
        switch (requestCode) {
            case 1001:
                // Get the result list of select image paths
                List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                mUpdateImgs.clear();
                mImgAdapter.clear();
                mUpdateImgs.addAll(path);
                if (mUpdateImgs.size() != 9) {
                    mUpdateImgs.add(mAddImgPath);
                }
                if (mUpdateImgs.size() > 1) {
                    mPicTiShi.setVisibility(View.GONE);
                } else {
                    mPicTiShi.setVisibility(View.VISIBLE);
                }
                mImgAdapter.addAll(mUpdateImgs);
                break;
            case 1002:
                if (data != null) {
                    time = (DoctorDetial.Schedule) data.getSerializableExtra("select_time");
                    mMakeTime.setText(time.getStart_time());
                }
                break;
            case 1003:
                if (data!=null){
                    String illTime = data.getStringExtra("ill_time");
                    mIllTime.setText(illTime);
                }
                break;
        }
    }

    /**
     * 提交咨询订单信息
     */
    @OnClick(R.id.right_btn)
    void submit() {

        if (mDetail != null) {
            if (mUpdateImgs.size() != 1) {
                setDismessDialog(false);
            }
            if (time==null){
                Toaster.showShort(PhoneVideoAskDoctorActivity.this,"请选择预约时间！");
                return;
            }
            firstPagerMager.submitPicAskOrder(AbSharedUtil.getString(PhoneVideoAskDoctorActivity.this, "userId"), mDetail.getDoctorId()
                    , String.valueOf(type), mDescription.getText().toString().trim()
                    , time.getId(), mIllTime.getText().toString(), mHostipal.getText().toString(), mIllDescrip.getText().toString().trim(), ed_jibing.getText().toString(), new SubscriberAdapter<ProResult<AskOrderResult>>() {
                        @Override
                        public void success(ProResult<AskOrderResult> result) {
                            super.success(result);
                           // Toaster.showShort(PhoneVideoAskDoctorActivity.this, result.getMsg());
                            if (mUpdateImgs.size() == 1) {
                                showPayDialog(result.getData());
                            } else {
                                //压缩图片   之后上传
                                makeThumbImageFile();
                                for (int i = 0; i < mThumbMediaUrls.size(); i++) {
                                    setShowDialog(false);
                                    if (i == (mThumbMediaUrls.size() - 1)) {
                                        setDismessDialog(true);
                                        updateImgs(result.getData(), mThumbMediaUrls.get(i), true);
                                    } else {
                                        updateImgs(result.getData(), mThumbMediaUrls.get(i), false);
                                    }
                                }
                            }
                        }
                    });
        }
    }

    /**
     * 提交档案图片
     *
     * @param askOrderResult
     * @param path
     */
    private void updateImgs(final AskOrderResult askOrderResult, String path, final boolean isOver) {
        String imageContent = "";
        try {
            imageContent = FileKit.encodeBase64File(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        firstPagerMager.updateOrderImg(AbSharedUtil.getString(PhoneVideoAskDoctorActivity.this, "userId")
                , askOrderResult.getConsultation_request_id(), "病例档案", imageContent, new SubscriberAdapter<Result>() {
                    @Override
                    public void success(Result result) {
                        super.success(result);
                        if (isOver) {
                          //  Toaster.showShort(PhoneVideoAskDoctorActivity.this, "提交成功！");
                            //提交成功后  清空缩略图文件夹
                            for (String path : mThumbMediaUrls) {
                                File file = new File(path);
                                if (file.exists()) {
                                    file.delete();
                                }
                            }
                            showPayDialog(askOrderResult);
                        }
                    }
                });
    }

    /**
     * 列表点击事件
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mImgAdapter.getItem(position).contains(mAddImgPath)) {
            mUpdateImgs.remove(mAddImgPath);
            PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(PhoneVideoAskDoctorActivity.this, new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE}, new PermissionsResultAction() {
                @Override
                public void onGranted() {
                    MultiImageSelector.create(PhoneVideoAskDoctorActivity.this)
                            .showCamera(true) // show camera or not. true by default
                            .count(9) // max select image size, 9 by default. used width #.multi()
                            .multi() // multi mode, default mode;
                            .origin(mUpdateImgs) // original select data set, used width #.multi()
                            .start(PhoneVideoAskDoctorActivity.this, 1001);
                }
                @Override
                public void onDenied(String permission) {
                    //没有权限
                    Toast.makeText(PhoneVideoAskDoctorActivity.this, "没有照相权限！", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            //查看大图
            Bundle bundle = new Bundle();
            ArrayList<String> imageUrls = new ArrayList<>();
            imageUrls.addAll(mUpdateImgs);
            imageUrls.remove(mAddImgPath);
            bundle.putStringArrayList("image_urls", imageUrls);
            bundle.putInt("image_index", position);
            UIKit.open(PhoneVideoAskDoctorActivity.this, ImagePagerActivity.class, bundle);
        }
    }

    /**
     * 列表长按事件
     * @param parent
     * @param view
     * @param position
     * @param id
     * @return
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        //临时不添加删除
        if (!mImgAdapter.getItem(position).contains(mAddImgPath)) {
            //弹窗提示是否删除，之后刷新
            deleteDialog(mImgAdapter.getItem(position));
        }
        return false;
    }

    /**
     * 删除图片提示框
     */
    private void deleteDialog(final String path) {
        mDeleteDialog = new QuiteDialog(PhoneVideoAskDoctorActivity.this);
        mDeleteDialog.setContent("您确定要删除该图片么？");
        mDeleteDialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteDialog.dismiss();
                if (!mUpdateImgs.contains(mAddImgPath) && mUpdateImgs.size() == 9) {
                    mUpdateImgs.remove(path);
                    mImgAdapter.clear();
                    mUpdateImgs.add(mAddImgPath);
                    mImgAdapter.addAll(mUpdateImgs);
                } else {
                    mUpdateImgs.remove(path);
                    mImgAdapter.clear();
                    mImgAdapter.addAll(mUpdateImgs);
                }
                if (mUpdateImgs.size() > 1) {
                    mPicTiShi.setVisibility(View.GONE);
                } else {
                    mPicTiShi.setVisibility(View.VISIBLE);
                }
            }
        });
        mDeleteDialog.setOnNegativeListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mDeleteDialog.dismiss();
            }
        });
        mDeleteDialog.show();
    }

    private void showPayDialog(final AskOrderResult askOrderResult) {
        final LoginDialog dialog = new LoginDialog(PhoneVideoAskDoctorActivity.this);
        if (type==3){
            dialog.setContent("请您放心购买本次电话咨询服务! \n 1、如通话时间在1分钟内，本次服务将全额退款。\n 2、预约时间开始前24小时以上可取消订单，不到24小时后，订单不能取消。\n 3、约定通话时间前30分钟，将发送短信或微信提醒您，请您提请准备好设备和网络。\n 4、巨医网将在预约时间拨通您的电话，电话号码为0532-88888888，请您确保电话畅通，准时接听。");
        }else{
            dialog.setContent("请您放心购买本次视频咨询服务! \n 1、如视频时间在1分钟内，本次服务将全额退款。\n 2、预约时间开始前24小时以上可取消订单， 24小时以内，订单不能取消。\n 3、约定通话时间前30分钟，将发送短信或微信提醒您，请您提请准备好设备和网络。 \n 4、请您准时联系医生开启本次服务，如您晚于预约时间15分钟以上，本次咨询将失效，平台将退款到您的账户。");
        }

        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //跳转到支付页面
                Bundle bundle = new Bundle();
                bundle.putSerializable("detail", mDetail);
                bundle.putSerializable("orderResult", askOrderResult);
                bundle.putInt("type",type);
                UIKit.open(PhoneVideoAskDoctorActivity.this, PayConfirmActivity.class, bundle);
                finish();
            }
        });
        dialog.show();
    }

    /**
     * 压缩图片
     */
    private void makeThumbImageFile() {
        for (String path : mUpdateImgs) {
            String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            //缩略图名字
            String thumbName = "thumb_" + timeStamp + FileKit.getFileName(path);
            //缩略图存放路径
            String thubmPath = savePath + thumbName;
            //是否存在
            if (new File(path).exists()) {
                if (new File(thubmPath).exists()) {
                    mThumbMediaUrls.add(thubmPath);
                } else {
                    try {
                        //压缩为宽度800的照片
                        ImageKit.createImageThumbnail(PhoneVideoAskDoctorActivity.this, path, thubmPath, 800, 50);
                        mThumbMediaUrls.add(thubmPath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
