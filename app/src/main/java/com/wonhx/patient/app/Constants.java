package com.wonhx.patient.app;

/**
 * Constants
 */
public class Constants {
    public static String FIRST_LOGIN = "first_login"; // 是否第一次登录
    public static String IS_LOGIN = "is_login";// 是否登录
    public static final String MESSAGE_ATTR_IS_VOICE_CALL = "is_voice_call";
    public static final String MESSAGE_ATTR_IS_VIDEO_CALL = "is_video_call";
    /**
     * REST Endpoint
     */
    public static final String REST_ENDPOINT = "http://www.9999md.com/mysql_api";
    /**
     * REST UserAvater
     */
    public static final String REST_ORGIN = "http://www.9999md.com";
    public static final String MIANZE = "file:///android_asset/html/mianze.html";
    public static final String ZHUCE = "file:///android_asset/html/zhuce.html";
    public static final String HELP_CENTER = "http://www.9999md.com/lunbotu/helpcenter.html";
    public static final String CHINA_HEATH = "http://www.9999md.com/chinese_body/";
    public static final String APP_ID = "wx5121878e46dc64ae";
    public static final String PUSH_ID = "j9ntwYNQoyFY39gOMLj6kYyD-gzGzoHsz";
    public static final String PUSH_SERECT = "vmxaRykvQptLSR0taYVrIUb6";
    public static int flag = 0;
    public static final String UPDATE = "http://www.9999md.com/mysql_api/doctor_message/version";
    public static final String STATUS = "http://www.9999md.com/mysql_api/consultation_service/get_order_status";
    public static final String SYS_TIME = "http://www.9999md.com/mysql_api/family_doctor/get_sys_time";

}
