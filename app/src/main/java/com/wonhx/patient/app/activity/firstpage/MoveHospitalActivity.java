package com.wonhx.patient.app.activity.firstpage;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.adapter.Zhensuoitem;
import com.wonhx.patient.app.adapter.Zhensuoitema;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.MoveHospital;
import com.wonhx.patient.app.model.ProvinceCity;
import com.wonhx.patient.app.model.Zhnsuo;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.view.XListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

public class MoveHospitalActivity extends BaseActivity implements XListView.IXListViewListener {
    @InjectView(R.id.tv_zhensuo)
    TextView tvZhensuo;
    @InjectView(R.id.iv_zhensuo)
    ImageView ivZhensuo;
    @InjectView(R.id.rl_zhensuo)
    RelativeLayout rlZhensuo;
    @InjectView(R.id.tv_diqu)
    TextView tvDiqu;
    @InjectView(R.id.iv_diqu)
    ImageView ivDiqu;
    @InjectView(R.id.rl_diqu)
    RelativeLayout rlDiqu;
    @InjectView(R.id.activity_message)
    LinearLayout activityMessage;
    @InjectView(R.id.load_more_list)
    XListView loadMoreListView;
    Zhensuoitema adapter_p;
    QuickAdapter<ProvinceCity.City> adapter_c;
    QuickAdapter<Zhnsuo.Sub_dept_name> adapter_zi;
    QuickAdapter<MoveHospital> adapter_all;
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    private static final int BAIDU_READ_PHONE_STATE =100;
    String city_id = "0", dept_id = "0", length = "10",city_name="";
    int start = 0;
    List<MoveHospital> list_all = new ArrayList<>();
    List<String> fuzhensuo_list = new ArrayList<>();
    List<Zhnsuo.Sub_dept_name> zizhensuo_list = new ArrayList<>();
    Zhensuoitem adapter_fu;
    List<ProvinceCity.City>city=new ArrayList<>();
    List<String>provience=new ArrayList<>();
    @InjectView(R.id.lll)
    LinearLayout ll_top;
    @InjectView(R.id.ll_diqu)
    LinearLayout ll_diqu;
    @InjectView(R.id.ll_zhensuo)
    LinearLayout ll_zhensuo;
    @InjectView(R.id.lv_provice)
    ListView lv_p;
    @InjectView(R.id.lv_city)
    ListView lv_c;
    @InjectView(R.id.lv_fu)
    ListView lv_all;
    @InjectView(R.id.lv_zi)
    ListView lv_one;
    @InjectView(R.id.v_diqu)
    TextView v_diqu;
    @InjectView(R.id.v_zhensuo)
    TextView v_zhensuo;
    boolean diqu=true,zhensuos=true;
    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move_hospital);
    }
    @Override
    protected void onInitData() {
        super.onInitData();
        getlist();
        //获取城市
        getcity();
        //获取诊所
        getzhensuo();
    }
    @Override
    protected void onInitView() {
        super.onInitView();
        handler=new Handler();
        adapter_p = new Zhensuoitema(MoveHospitalActivity.this, provience);
        adapter_fu = new Zhensuoitem(MoveHospitalActivity.this, fuzhensuo_list);
        adapter_c = new QuickAdapter<ProvinceCity.City>(MoveHospitalActivity.this, R.layout.doctor_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, ProvinceCity.City item) {
                helper.setText(R.id.doctor, item.getName_cn());
            }
        };
        adapter_zi = new QuickAdapter<Zhnsuo.Sub_dept_name>(this, R.layout.doctor_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, Zhnsuo.Sub_dept_name item) {
                helper.setText(R.id.doctor, item.getName());
            }
        };
        loadMoreListView.setPullLoadEnable(false);
        adapter_all = new QuickAdapter<MoveHospital>(this, R.layout.movehospital_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, final MoveHospital item) {
                helper.setText(R.id.doctor_name, item.getClinic_name())
                        .setText(R.id.doctor_hospitalName, "创建医师：" + item.getDoc_name())
                        .setText(R.id.miaoshui, item.getInfo());
                ImageView iv=helper.getView(R.id.d);
                if (item.getAddress()!=null&&!item.getAddress().equals("")){
                    helper.getView(R.id.doctor_add).setVisibility(View.VISIBLE);
                    helper.getView(R.id.city).setVisibility(View.GONE);
                    helper.setText(R.id.doctor_add,item.getAddress());
                    iv.setVisibility(View.VISIBLE);
                }else {
                    helper.setText(R.id.city,item.getCity_name());
                    helper.getView(R.id.doctor_add).setVisibility(View.GONE);
                    helper.getView(R.id.city).setVisibility(View.VISIBLE);
                    iv.setVisibility(View.GONE);
                }
                SimpleDraweeView head = helper.getView(R.id.logo);
                head.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine" + item.getLogo_img_path()));
               helper.getView(R.id.rl_add).setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                       Intent in = new Intent(MoveHospitalActivity.this, ClinicLocationActivity.class);
                       in.putExtra("la", item.getLat());
                       in.putExtra("ln", item.getLng());
                       in.putExtra("address", item.getAddress());
                       startActivity(in);
                   }
               });
            }
        };
        loadMoreListView.setXListViewListener(this);
        loadMoreListView.setAdapter(adapter_all);
        loadMoreListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MoveHospital moveHospital = adapter_all.getItem(position-1);
                Bundle bundle = new Bundle();
                bundle.putString("CLINICID", moveHospital.getClinic_id());
                UIKit.open(MoveHospitalActivity.this, HospitalDetailActivity.class, bundle);
            }
        });
        lv_p.setAdapter(adapter_p);
        lv_c.setAdapter(adapter_c);
        lv_all.setAdapter(adapter_fu);
        lv_one.setAdapter(adapter_zi);
    }

    @OnClick({R.id.left_btn, R.id.rl_zhensuo, R.id.rl_diqu, R.id.serc,R.id.ll_zhensuo,R.id.ll_diqu,R.id.iv_near})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
            case R.id.ll_diqu:
                ll_diqu.setVisibility(View.GONE);
                tvDiqu.setTextColor(Color.BLACK);
                ivDiqu.setImageResource(R.mipmap.under);
                diqu=true;
                break;
            case R.id.ll_zhensuo:
                ll_zhensuo.setVisibility(View.GONE);
                tvZhensuo.setTextColor(Color.BLACK);
                ivZhensuo.setImageResource(R.mipmap.under);
                zhensuos=true;
                break;
            //搜索
            case R.id.serc:
                UIKit.open(MoveHospitalActivity.this, SearchHospitalActivity.class);
                break;
            case R.id.rl_zhensuo:
                if (zhensuos){
                    ll_diqu.setVisibility(View.GONE);
                    tvDiqu.setTextColor(Color.BLACK);
                    ivDiqu.setImageResource(R.mipmap.under);
                    diqu=true;
                    tvZhensuo.setTextColor(getResources().getColor(R.color.colorAccent));
                    ivZhensuo.setImageResource(R.mipmap.lanshang);
                    ll_zhensuo.setVisibility(View.VISIBLE);
                    zhensuos=false;
                }else {
                    ll_zhensuo.setVisibility(View.GONE);
                    tvZhensuo.setTextColor(Color.BLACK);
                    ivZhensuo.setImageResource(R.mipmap.under);
                    zhensuos=true;
                }
                break;
            case R.id.rl_diqu:
                if (diqu){
                    ll_zhensuo.setVisibility(View.GONE);
                    tvZhensuo.setTextColor(Color.BLACK);
                    ivZhensuo.setImageResource(R.mipmap.under);
                    zhensuos=true;
                    ll_diqu.setVisibility(View.VISIBLE);
                    tvDiqu.setTextColor(getResources().getColor(R.color.colorAccent));
                    ivDiqu.setImageResource(R.mipmap.lanshang);
                    diqu=false;
                }else {
                    ll_diqu.setVisibility(View.GONE);
                    tvDiqu.setTextColor(Color.BLACK);
                    ivDiqu.setImageResource(R.mipmap.under);
                    diqu=true;
                }
                break;
            //附近的医生
            case R.id.iv_near:
                if (Build.VERSION.SDK_INT >= 23) {
                    showContacts();
                } else {
                    UIKit.open(this,NearClinctActivity.class);//调用具体方法
                }

                break;
            default:
                break;
        }
    }
    public void showContacts(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(this,"没有权限,请手动开启定位权限",Toast.LENGTH_SHORT).show();
            // 申请一个（或多个）权限，并提供用于回调返回的获取码（用户定义）
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE}, BAIDU_READ_PHONE_STATE);
        }else{
            UIKit.open(this,NearClinctActivity.class);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case BAIDU_READ_PHONE_STATE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 获取到权限，作相应处理（调用定位SDK应当确保相关权限均被授权，否则可能引起定位失败）
                    UIKit.open(this,NearClinctActivity.class);
                } else {
                    // 没有获取到权限，做特殊处理
                    //Toast.makeText(this, "获取位置权限失败，请手动开启", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void getzhensuo() {
        firstPagerMager.get_zhensuo(new SubscriberAdapter<ListProResult<Zhnsuo>>() {
            @Override
            public void success(final ListProResult<Zhnsuo> zhnsuo) {
                super.success(zhnsuo);
                adapter_fu.clear();
                for (int i = 0; i < zhnsuo.getData().size(); i++) {
                    fuzhensuo_list.add(zhnsuo.getData().get(i).getParent_dept_name());
                }
                if (fuzhensuo_list.size() > 0) {
                    adapter_fu.notifyDataSetChanged();
                }
                lv_all.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, final int p, long l) {
                        adapter_fu.changeSelected(p);
                        adapter_zi.clear();
                        zizhensuo_list.clear();
                        if (p > 0) {
                            Zhnsuo.Sub_dept_name sub=new Zhnsuo.Sub_dept_name();
                            sub.setName("全部");
                            sub.setId(zhnsuo.getData().get(p).getSub_dept_name().get(0).getId());
                            zhnsuo.getData().get(p).getSub_dept_name().set(0,sub);
                            zizhensuo_list.addAll(zhnsuo.getData().get(p).getSub_dept_name());
                            if (zizhensuo_list.size() > 0) {
                                adapter_zi.addAll(zizhensuo_list);
                            }
                        } else {
                            dept_id = "0";
                            start = 0;
                            list_all.clear();
                            adapter_all.clear();
                            getlist();
                            ll_zhensuo.setVisibility(View.GONE);
                            tvZhensuo.setTextColor(Color.BLACK);
                            ivZhensuo.setImageResource(R.mipmap.under);
                            zhensuos=true;
                            tvZhensuo.setText("全部诊所");
                        }
                        lv_one.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i==0){
                                    tvZhensuo.setText(fuzhensuo_list.get(p));
                                }else {
                                    tvZhensuo.setText(zizhensuo_list.get(i).getName());
                                }
                                dept_id = zizhensuo_list.get(i).getId();
                                start = 0;
                                list_all.clear();
                                adapter_all.clear();
                                getlist();
                                ll_zhensuo.setVisibility(View.GONE);
                                tvZhensuo.setTextColor(Color.BLACK);
                                ivZhensuo.setImageResource(R.mipmap.under);
                                zhensuos=true;
                            }
                        });
                    }
                });
            }
        });
    }
    private void getcity() {
        firstPagerMager.get_prvince_city(new SubscriberAdapter<ListProResult<ProvinceCity>>() {
            @Override
            public void success(final ListProResult<ProvinceCity> provinceCityListProResult) {
                super.success(provinceCityListProResult);
                adapter_p.clear();
                city.clear();
                for (int i = 0; i < provinceCityListProResult.getData().size(); i++) {
                    provience.add(provinceCityListProResult.getData().get(i).getProvince());
                }
                if (provience.size() > 0) {
                    adapter_p.notifyDataSetChanged();
                }
                adapter_c.addAll(city);
                lv_p.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, final int p, long l) {
                        adapter_p.changeSelected(p);
                        adapter_c.clear();
                        city.clear();
                        if (p > 0) {
                            ProvinceCity.City citya=new ProvinceCity.City();
                            citya.setName_cn("全部");
                            citya.setId(provinceCityListProResult.getData().get(p).getCity().get(0).getId());
                            provinceCityListProResult.getData().get(p).getCity().set(0,citya);
                            city .addAll(provinceCityListProResult.getData().get(p).getCity());
                            adapter_c.addAll(city);
                        } else {
                            ll_diqu.setVisibility(View.GONE);
                            tvDiqu.setTextColor(Color.BLACK);
                            ivDiqu.setImageResource(R.mipmap.under);
                            diqu=true;
                            city_id = "0";
                            city_name="";
                            start = 0;
                            tvDiqu.setText("全部地区");
                            list_all.clear();
                            adapter_all.clear();
                            getlist();
                        }
                        lv_c.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i==0){
                                    tvDiqu.setText(provience.get(p));
                                    city_name=provience.get(p);
                                }else {
                                    tvDiqu.setText(city.get(i).getName_cn());
                                    city_name=city.get(i).getName_cn();
                                }
                                city_id = city.get(i).getId();
                                start = 0;
                                list_all.clear();
                                adapter_all.clear();
                                getlist();
                                ll_diqu.setVisibility(View.GONE);
                                tvDiqu.setTextColor(Color.BLACK);
                                ivDiqu.setImageResource(R.mipmap.under);
                                diqu = true;
                            }
                        });
                    }
                });
            }
        });
    }
    private void getlist() {
        firstPagerMager.getmovehospital(city_id,city_name, dept_id, start + "", length, new SubscriberAdapter<ListProResult<MoveHospital>>() {
            @Override
            public void onError(Throwable e) {
               // super.onError(e);
                dismissLoadingDialog();
            }
            @Override
            public void success(ListProResult<MoveHospital> moveHospitalListProResult) {
                super.success(moveHospitalListProResult);
                    list_all = moveHospitalListProResult.getData();
                    if (list_all.size() > 0) {
                        adapter_all.replaceAll(list_all);
                        if (list_all.size()>=10){
                            loadMoreListView.setPullLoadEnable(true);
                        }else {
                            loadMoreListView.setPullLoadEnable(false);
                        }
                    }
            }
        });
    }

    @Override
    public void onRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start = 0;
                list_all.clear();
                getlist();
                onLoad();
            }
        }, 1000);
    }
    private void onLoad() {
        loadMoreListView.stopRefresh();
        loadMoreListView.stopLoadMore();
        loadMoreListView.setRefreshTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }
    @Override
    public void onLoadMore() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start=start+10;
                getlist();
                onLoad();
            }
        }, 1000);
    }
}
