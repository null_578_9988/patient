package com.wonhx.patient.app.model;

/**
 * Created by apple on 2017/4/8.
 * 健康档案：生活信息
 */

public class LifeInfo {
    private String id;
    private String healthrecord_id;
    private String foodhibats;
    private String is_smook;
    private String is_exercise;
    private String exercisetime;
    private String exerciseyear;
    private String exerciseway;
    private String is_drink;
    private String drink_kinds;
    private String is_sleep;
    private String is_longdose;
    private String create_date;
    private String update_date;

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getDrink_kinds() {
        return drink_kinds;
    }

    public void setDrink_kinds(String drink_kinds) {
        this.drink_kinds = drink_kinds;
    }

    public String getExercisetime() {
        return exercisetime;
    }

    public void setExercisetime(String exercisetime) {
        this.exercisetime = exercisetime;
    }

    public String getExerciseway() {
        return exerciseway;
    }

    public void setExerciseway(String exerciseway) {
        this.exerciseway = exerciseway;
    }

    public String getExerciseyear() {
        return exerciseyear;
    }

    public void setExerciseyear(String exerciseyear) {
        this.exerciseyear = exerciseyear;
    }

    public String getFoodhibats() {
        return foodhibats;
    }

    public void setFoodhibats(String foodhibats) {
        this.foodhibats = foodhibats;
    }

    public String getHealthrecord_id() {
        return healthrecord_id;
    }

    public void setHealthrecord_id(String healthrecord_id) {
        this.healthrecord_id = healthrecord_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIs_drink() {
        return is_drink;
    }

    public void setIs_drink(String is_drink) {
        this.is_drink = is_drink;
    }

    public String getIs_exercise() {
        return is_exercise;
    }

    public void setIs_exercise(String is_exercise) {
        this.is_exercise = is_exercise;
    }

    public String getIs_longdose() {
        return is_longdose;
    }

    public void setIs_longdose(String is_longdose) {
        this.is_longdose = is_longdose;
    }

    public String getIs_sleep() {
        return is_sleep;
    }

    public void setIs_sleep(String is_sleep) {
        this.is_sleep = is_sleep;
    }

    public String getIs_smook() {
        return is_smook;
    }

    public void setIs_smook(String is_smook) {
        this.is_smook = is_smook;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }
}
