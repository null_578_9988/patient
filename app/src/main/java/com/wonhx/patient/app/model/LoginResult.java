package com.wonhx.patient.app.model;

/**
 * Created by apple on 17/3/23.
 * 登录返回结果
 */
public class LoginResult {
    private String code;
    private String msg;
    private String member_id;
    private String doctor_id;
    private String username;
    private String phone;
    private String name;
    private String patient_name;
    private String logo_img_path;
    private String huanxin_username;
    private String huanxin_pw;

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public String getLogo_img_path() {
        return logo_img_path;
    }

    public void setLogo_img_path(String logo_img_path) {
        this.logo_img_path = logo_img_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getCode() {
        return code.equals("0");
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getHuanxin_pw() {
        return huanxin_pw;
    }

    public void setHuanxin_pw(String huanxin_pw) {
        this.huanxin_pw = huanxin_pw;
    }

    public String getHuanxin_username() {
        return huanxin_username;
    }

    public void setHuanxin_username(String huanxin_username) {
        this.huanxin_username = huanxin_username;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
