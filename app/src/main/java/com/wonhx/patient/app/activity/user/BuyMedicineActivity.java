package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.firstpage.DoctorDetialActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.BuyMedicineList;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.TabEntity;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.view.HJloginDialog;
import com.wonhx.patient.view.ListViewForScrollView;
import com.wonhx.patient.view.ListViewUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class BuyMedicineActivity extends BaseActivity implements OnTabSelectListener {
    HJloginDialog dialog;
    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.menutab)
    CommonTabLayout menutab;
    @InjectView(R.id.rl_nobook)
    RelativeLayout rlNobook;
    @InjectView(R.id.listview)
    ListView listview;
    String[] mTitles = {"全部", "未付款", "已付款"};
    UserManager userManager=new UserManagerImpl();
    ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    QuickAdapter<BuyMedicineList>adapter;
    List<BuyMedicineList>list_all=new ArrayList<>();
    List<BuyMedicineList>list_wait=new ArrayList<>();
    List<BuyMedicineList>list_over=new ArrayList<>();
    List<BuyMedicineList.medicine>list_mednce=new ArrayList<>();
    int positionflag=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_medicine);
        ButterKnife.inject(this);
    }
    @Override
    protected void onInitView() {
        super.onInitView();
        title.setText("购药清单");
        title.setTextColor(getResources().getColor(R.color.colorAccent));
        mTabEntities.clear();
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i]));
        }
        menutab.setTabData(mTabEntities);
        menutab.setOnTabSelectListener(this);
        menutab.setCurrentTab(0);
        adapter = new QuickAdapter<BuyMedicineList>(BuyMedicineActivity.this, R.layout.medinceitem) {
            @Override
            protected void convert(BaseAdapterHelper helper, final BuyMedicineList item) {
                LinearLayout linearLayout = helper.getView(R.id.mainview);
                addView(linearLayout,item);
                helper.setText(R.id.doctorname, item.getName());
                TextView textView = helper.getView(R.id.isPay);
                if (item.getIsPay().equals("0")) {
                    textView.setText("未付款");
                    textView.setTextColor(Color.RED);
                    helper.getView(R.id.delect).setVisibility(View.VISIBLE);
                    helper.getView(R.id.pay).setVisibility(View.VISIBLE);
                } else {
                    textView.setText("已付款");
                    textView.setTextColor(getResources().getColor(R.color.fukuan));
                    helper.getView(R.id.delect).setVisibility(View.VISIBLE);
                    helper.getView(R.id.pay).setVisibility(View.GONE);
                }
                helper.getView(R.id.delect).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Toaster.showShort(BuyMedicineActivity.this,"删除");
                        showDialogd(item.getOrderId());

                    }

                });
                helper.getView(R.id.pay).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Toaster.showShort(BuyMedicineActivity.this,"付款");
                        Intent in = new Intent(BuyMedicineActivity.this, OrderPayActivity.class);
                        in.putExtra("orderId", item.getOrderId());
                        in.putExtra("totalPrice", item.getTotalPrice());
                        in.putExtra("member_id", item.getMember_id());
                        startActivity(in);
                    }
                });
                //查看医生详情
                helper.getView(R.id.head).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent in = new Intent(BuyMedicineActivity.this, DoctorDetialActivity.class);
                        in.putExtra("member_id", item.getMember_id());
                        startActivity(in);
                    }
                });
                SimpleDraweeView simpleDraweeView = helper.getView(R.id.headimage);
                simpleDraweeView.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine" + item.getLogoImgPath()));
                list_mednce.clear();
                list_mednce.addAll(item.getMedicine());
                int num = 0;
                for (int i = 0; i < item.getMedicine().size(); i++) {
                    num = num + Integer.parseInt(item.getMedicine().get(i).getNum());
                }
                helper.setText(R.id.heji, "共" + num + "件药品" + "  合计：￥" + item.getTotalPrice() + "(含运费 ￥" + item.getPostage_price() + ")");
            }
        };
        listview.setAdapter(adapter);
    }

    /**
     * 动态添加药品view
     * @param layout
     * @param item
     */
    private void addView(LinearLayout layout,BuyMedicineList item){
        layout.removeAllViews();
        List<BuyMedicineList.medicine> itemlist = new ArrayList<>();
        itemlist.addAll(item.getMedicine());
        for (BuyMedicineList.medicine  medicine : itemlist){
            View view = (View) LayoutInflater.from(this).inflate(R.layout.medicinelvitem, null);
            TextView name = (TextView) view.findViewById(R.id.name);
            TextView specification = (TextView) view.findViewById(R.id.specification);
            TextView retailPrice = (TextView) view.findViewById(R.id.retailPrice);
            TextView num = (TextView) view.findViewById(R.id.num);
            name.setText(medicine.getName());
            specification.setText(medicine.getSpecification());
            retailPrice.setText("￥"+medicine.getRetailPrice());
            num.setText("*"+ medicine.getNum());
            layout.addView(view);
        }
    }

    private void delectbyorderid(String id) {
        userManager.delectmedicinelist(id,new SubscriberAdapter<Result>(){
            @Override
            public void success(Result result) {
                super.success(result);
                Toaster.showShort(BuyMedicineActivity.this,result.getMsg());
                getList();
            }
        });
    }
    @OnClick(R.id.left_btn)
    public void onViewClicked() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getList();
    }
    private void getList() {
        userManager.buymedicinelist(AbSharedUtil.getString(BuyMedicineActivity.this,"userId"),new SubscriberAdapter<ListProResult<BuyMedicineList>>(){
            @Override
            public void onError(Throwable e) {
                super.onError(e);
                rlNobook.setVisibility(View.VISIBLE);
            }
            @Override
            public void success(ListProResult<BuyMedicineList> buyMedicineListListProResult) {
                super.success(buyMedicineListListProResult);
                rlNobook.setVisibility(View.GONE);
                adapter.clear();
                list_all.clear();
                list_over.clear();
                list_wait.clear();
                list_all=buyMedicineListListProResult.getData();
                for (int i = 0; i <list_all.size() ; i++) {
                    if (list_all.get(i).getIsPay().equals("0")){
                        list_wait.add(list_all.get(i));
                    }else {
                        list_over.add(list_all.get(i));
                    }
                }
                if (positionflag==0){
                    menutab.setCurrentTab(0);
                    adapter.clear();
                    adapter.addAll(list_all);
                }else if (positionflag==1){
                    menutab.setCurrentTab(1);
                    adapter.clear();
                    adapter.addAll(list_wait);
                }else if (positionflag==2){
                    menutab.setCurrentTab(2);
                    adapter.clear();
                    adapter.addAll(list_over);
                }
                //查看详情
                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent in=new Intent(BuyMedicineActivity.this,BuyMedicineDetialActivity.class);
                        if (menutab.getCurrentTab()==0){
                            positionflag=0;
                            in.putExtra("orderId",list_all.get(i).getOrderId());
                            in.putExtra("name",list_all.get(i).getName());
                            in.putExtra("logoImgPath",list_all.get(i).getLogoImgPath());
                            in.putExtra("title",list_all.get(i).getTitle());
                            in.putExtra("dept_name",list_all.get(i).getDept_name());
                            in.putExtra("isPay",list_all.get(i).getIsPay());
                            in.putExtra("disease",list_all.get(i).getDisease());
                            in.putExtra("address",list_all.get(i).getAddress());
                            in.putExtra("totalPrice",list_all.get(i).getTotalPrice());
                            in.putExtra("doctorAdvice",list_all.get(i).getDoctorAdvice());
                            in.putExtra("member_id",list_all.get(i).getMember_id());
                            in.putExtra("postage_price",list_all.get(i).getPostage_price());
                            in.putExtra("doctor_id",list_all.get(i).getDoctorId());
                        }else if (menutab.getCurrentTab()==1){
                            positionflag=1;
                            in.putExtra("orderId",list_wait.get(i).getOrderId());
                            in.putExtra("name",list_wait.get(i).getName());
                            in.putExtra("logoImgPath",list_wait.get(i).getLogoImgPath());
                            in.putExtra("title",list_wait.get(i).getTitle());
                            in.putExtra("dept_name",list_wait.get(i).getDept_name());
                            in.putExtra("isPay",list_wait.get(i).getIsPay());
                            in.putExtra("disease",list_wait.get(i).getDisease());
                            in.putExtra("address",list_wait.get(i).getAddress());
                            in.putExtra("totalPrice",list_wait.get(i).getTotalPrice());
                            in.putExtra("doctorAdvice",list_wait.get(i).getDoctorAdvice());
                            in.putExtra("member_id",list_wait.get(i).getMember_id());
                            in.putExtra("postage_price",list_wait.get(i).getPostage_price());
                            in.putExtra("doctor_id",list_wait.get(i).getDoctorId());
                        }else if (menutab.getCurrentTab()==2){
                            positionflag=2;
                            in.putExtra("orderId",list_over.get(i).getOrderId());
                            in.putExtra("name",list_over.get(i).getName());
                            in.putExtra("logoImgPath",list_over.get(i).getLogoImgPath());
                            in.putExtra("title",list_over.get(i).getTitle());
                            in.putExtra("dept_name",list_over.get(i).getDept_name());
                            in.putExtra("isPay",list_over.get(i).getIsPay());
                            in.putExtra("disease",list_over.get(i).getDisease());
                            in.putExtra("address",list_over.get(i).getAddress());
                            in.putExtra("totalPrice",list_over.get(i).getTotalPrice());
                            in.putExtra("doctorAdvice",list_over.get(i).getDoctorAdvice());
                            in.putExtra("member_id",list_over.get(i).getMember_id());
                            in.putExtra("postage_price",list_over.get(i).getPostage_price());
                            in.putExtra("doctor_id",list_over.get(i).getDoctorId());
                        }
                        startActivity(in);
                    }
                });
            }
        });
    }

    @Override
    public void onTabSelect(int position) {
        if (position==0){
            //menutab.setCurrentTab(0);
            positionflag=0;
            adapter.clear();
            adapter.addAll(list_all);
        }else if (position==1){
            //menutab.setCurrentTab(1);
            positionflag=1;
            adapter.clear();
            adapter.addAll(list_wait);
        }else if (position==2){
           // menutab.setCurrentTab(2);
            positionflag=2;
            adapter.clear();
            adapter.addAll(list_over);
        }

    }

    @Override
    public void onTabReselect(int position) {

    }
    /**
     * 提示对话框
     */
    public void showDialogd(final String id) {
        dialog = new HJloginDialog(BuyMedicineActivity.this);
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delectbyorderid(id);
                dialog.dismiss();
            }
        });
        dialog.setOnCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setTitle("确认删除订单？");
        dialog.setContentMessage("订单删除后不能恢复");
        dialog.setCancelMessage("取消");
        dialog.setPositiveMessage("确认");
        dialog.setPositiveColor(this.getResources().getColor(R.color.colorAccent));
        dialog.setCancelColor(this.getResources().getColor(R.color.colorAccent));
        dialog.show();
    }
}
