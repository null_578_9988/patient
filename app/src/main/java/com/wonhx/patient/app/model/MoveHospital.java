package com.wonhx.patient.app.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/5/18.
 *
 */

public class MoveHospital {
    String id;
    String group_id;
    String medicine_logo_img;
    String hospital_name;
    String clinic_id;
    String clinic_name;
    String info;
    String doc_name;
    String title;
    String logo_img_path;
    String city_name;
    String dept_name;
    String good_subjects;
    String address;
    String lng;
    String lat;
    String bulletin;
    String business_hours;
    String is_recruit;
    List<Medicine> medicine_num = new ArrayList<>();
    List<DoctorTime> medicine_rota = new ArrayList<>();

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getBulletin() {
        return bulletin;
    }

    public void setBulletin(String bulletin) {
        this.bulletin = bulletin;
    }

    public String getBusiness_hours() {
        return business_hours;
    }

    public void setBusiness_hours(String business_hours) {
        this.business_hours = business_hours;
    }

    public List<DoctorTime> getMedicine_rota() {
        return medicine_rota;
    }

    public void setMedicine_rota(List<DoctorTime> medicine_rota) {
        this.medicine_rota = medicine_rota;
    }

    public String getGood_subjects() {
        return good_subjects;
    }

    public void setGood_subjects(String good_subjects) {
        this.good_subjects = good_subjects;
    }

    public String getIs_recruit() {
        return is_recruit;
    }

    public void setIs_recruit(String is_recruit) {
        this.is_recruit = is_recruit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getMedicine_logo_img() {
        return medicine_logo_img;
    }

    public void setMedicine_logo_img(String medicine_logo_img) {
        this.medicine_logo_img = medicine_logo_img;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public List<Medicine> getMedicine_num() {
        return medicine_num;
    }

    public void setMedicine_num(List<Medicine> medicine_num) {
        this.medicine_num = medicine_num;
    }

    public String getClinic_id() {
        return clinic_id;
    }

    public void setClinic_id(String clinic_id) {
        this.clinic_id = clinic_id;
    }

    public String getClinic_name() {
        return clinic_name;
    }

    public void setClinic_name(String clinic_name) {
        this.clinic_name = clinic_name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogo_img_path() {
        return logo_img_path;
    }

    public void setLogo_img_path(String logo_img_path) {
        this.logo_img_path = logo_img_path;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }
    public class Medicine{
        String result;
        String title;//医生职称 1 主任医师 2 副主任医师 3 主治医师 4 住院医师

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
    public class DoctorTime{
        public String id;
        public String doctor_id;
        public String member_id;
        public String doc_name;
        public String title;
        public String medicine_logo_img;
        public String good_subjects;
        public String hospital_name;
        public String dept_name;
        public String start_time;
        public String end_time;
        public String time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDoctor_id() {
            return doctor_id;
        }

        public void setDoctor_id(String doctor_id) {
            this.doctor_id = doctor_id;
        }

        public String getMember_id() {
            return member_id;
        }

        public void setMember_id(String member_id) {
            this.member_id = member_id;
        }

        public String getDoc_name() {
            return doc_name;
        }

        public void setDoc_name(String doc_name) {
            this.doc_name = doc_name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMedicine_logo_img() {
            return medicine_logo_img;
        }

        public void setMedicine_logo_img(String medicine_logo_img) {
            this.medicine_logo_img = medicine_logo_img;
        }

        public String getGood_subjects() {
            return good_subjects;
        }

        public void setGood_subjects(String good_subjects) {
            this.good_subjects = good_subjects;
        }

        public String getHospital_name() {
            return hospital_name;
        }

        public void setHospital_name(String hospital_name) {
            this.hospital_name = hospital_name;
        }

        public String getDept_name() {
            return dept_name;
        }

        public void setDept_name(String dept_name) {
            this.dept_name = dept_name;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getEnd_time() {
            return end_time;
        }

        public void setEnd_time(String end_time) {
            this.end_time = end_time;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }
}
