package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.adapter.TuijianKeshiAbstratorInfo;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.Tuijiankeshi;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

public class NowConstantActivity extends BaseActivity {
    @InjectView(R.id.title)
    TextView tv_title;
    @InjectView(R.id.right_btn)
    TextView tv_right;
    @InjectView(R.id.gv_gridView)
    GridView gridView;
    FirstPagerMager firstPagerMager=new FirstPagerMangerImal();
    TuijianKeshiAbstratorInfo adapter;
    List<Tuijiankeshi>list=new ArrayList<>();
    @Override
    protected void onInitView() {
        super.onInitView();
        tv_right.setText("");
        tv_title.setText("找医生");
        adapter=new TuijianKeshiAbstratorInfo(this,R.layout.tuijiankeshi_item,list);
        gridView.setAdapter(adapter);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_now_constant);
    }
    @Override
    protected void onInitData() {
        super.onInitData();
        firstPagerMager.getTuijiankeshi(new SubscriberAdapter<ListProResult<Tuijiankeshi>>(){
            @Override
            public void success(ListProResult<Tuijiankeshi> tuijiankeshiListProResult) {
                super.success(tuijiankeshiListProResult);
                list.addAll(tuijiankeshiListProResult.getData());
                if (list.size()>0){
                    adapter.notifyDataSetChanged();
                }
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent in=new Intent(NowConstantActivity.this,TuijiankeshiDetialActivity.class);
                in.putExtra("deptid",list.get(i).getDeptid());
                in.putExtra("deptName",list.get(i).getDeptname());
                startActivity(in);
            }
        });
    }
    @OnClick(R.id.left_btn)
    void back() {
        finish();
    }
    //搜索
    @OnClick(R.id.search_edit)
    void search(){
        Intent in=new Intent(NowConstantActivity.this,TuijiankeshiDetialActivity.class);
        in.putExtra("deptid","");
        startActivity(in);
    }
}
