package com.wonhx.patient.app.activity.firstpage;

import android.os.Bundle;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by nsd on 2017/5/19.
 *  诊所简介
 */

public class HospitalBriefActivity extends BaseActivity {
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.brief)
    TextView mBrief;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_brief);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("诊所简介");
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        String brief = getIntent().getStringExtra("BRIEF");
        if (brief!=null){
            mBrief.setText(brief);
        }
    }

    @OnClick(R.id.left_btn)
    void back(){
        finish();
    }
}
