package com.wonhx.patient.app.fragment.main;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.EaseConstant;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.activity.firstpage.DoctorDetialActivity;
import com.wonhx.patient.app.activity.servicehistory.EvaluateDoctorActivity;
import com.wonhx.patient.app.activity.servicehistory.ServiceHistoryDetialActivity;
import com.wonhx.patient.app.activity.user.LoginActivity;
import com.wonhx.patient.app.activity.user.MessageActivity;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.ease.ConversationActivity;
import com.wonhx.patient.app.ease.Message;
import com.wonhx.patient.app.ease.VideoCallActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.manager.ServiceHistoryManger;
import com.wonhx.patient.app.manager.servicehhistory.ServiceHistoryMangerImal;
import com.wonhx.patient.app.model.HistoryRecord;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.PushMessage;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.TabEntity;
import com.wonhx.patient.app.model.UserInfo;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.LoginDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/22.
 * 服务记录
 */
public class ServiceHistoryFragment extends BaseFragment implements OnTabSelectListener {

    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.menutab)
    CommonTabLayout mMenuTab;
    @InjectView(R.id.listview)
    ListView lv;
    @InjectView(R.id.rl_nobook)
    RelativeLayout ll_no;
    @InjectView(R.id.rl_nologin)
    RelativeLayout ll_nologin;
    @InjectView(R.id.id_swipe_ly)
    SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.notice_layout)
    FrameLayout noticeLayout;
    private String[] mTitles = {"正在咨询", "待评价", "已结束"};
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    ServiceHistoryManger serviceHistoryManger = new ServiceHistoryMangerImal();
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    QuickAdapter<HistoryRecord> adapter;
    List<HistoryRecord> list_quest = new ArrayList<>();
    List<HistoryRecord> list_all = new ArrayList<>();
    List<HistoryRecord> list_assess = new ArrayList<>();
    List<HistoryRecord> list_over = new ArrayList<>();
    UserInfo userDao = new UserInfo();
    LoginDialog  dialog;
    String membetId = "";
    int position = 0;
    MyReceiver myReceiver;
    List<PushMessage>all_message=new ArrayList<>();
    List<PushMessage>noread_message=new ArrayList<>();
    PushMessage pushMessage=new PushMessage();
    @InjectView(R.id.red_point)
    TextView tv_red_point;
    @InjectView(R.id.tv_textbooka)
    TextView tv_textbooka;
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_tab_servicehistory, container, false);
        ButterKnife.inject(this, viewGroup);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light, android.R.color.holo_green_light, android.R.color.holo_blue_bright, android.R.color.holo_orange_light);
        adapter = new QuickAdapter<HistoryRecord>(getActivity(), R.layout.adapter_order) {
            @Override
            protected void convert(final BaseAdapterHelper helper, final HistoryRecord item) {
                helper.setText(R.id.disease_tv, item.getContent()!=null&&!item.getContent().equals("")?item.getContent():"")
                        .setText(R.id.doctor_keshi, item.getStartTime()!=null&&!item.getStartTime().equals("")?item.getStartTime():"")
                        .setText(R.id.time_tv1, item.getServiceTypeString())
                        .setText(R.id.time_tv, " | " +  item.getDoctorName());
                ImageView pic_logo=helper.getView(R.id.pic_logo);
                if (item.getServiceType().equals("2")){
                    pic_logo.setImageResource(R.mipmap.bubbless);
                }else  if (item.getServiceType().equals("3")){
                    pic_logo.setImageResource(R.mipmap.phones);
                }else if (item.getServiceType().equals("4")){
                    pic_logo.setImageResource(R.mipmap.shipin);
                }
                // 2图文咨询 不用判断其他状态 直接跳转到聊天界面
                if (item.getServiceType().equals("2")){
                    //status:/1待付费 2已购买 3已解答 4已结束 5 已退款 6 已超时 7 已取消
                    if (item.getStatus().equals("2")) {
                        helper.setText(R.id.order_tv, "立即咨询");
                    } else if (item.getStatus().equals("3")) {
                        helper.setText(R.id.order_tv, "去评价");
                    } else if (item.getStatus().equals("4") || item.getStatus().equals("5") || item.getStatus().equals("6") ) {
                        helper.setText(R.id.order_tv, "查看详情");
                    }
                    // 3电话咨询 需要判断
                }else if (item.getServiceType().equals("3")){
                    //status:/1待付费 2已购买 3已解答 4已结束 5 已退款 6 已超时 7 已取消
                    if (item.getStatus() .equals("2")){
                        if (item.getByeType().equals("0") ) {
                            helper.setText(R.id.order_tv, "等待解答");
                        }else{
                            helper.setText(R.id.order_tv, "等待咨询");
                        }
                    }else if(item.getStatus() .equals("4")){
                        helper.setText(R.id.order_tv, "查看详情");
                    }else if(item.getStatus() .equals("3")){
                        helper.setText(R.id.order_tv, "去评价");
                    }else if(item.getStatus() .equals("5")){
                        helper.setText(R.id.order_tv, "已退款");
                    }else if(item.getStatus() .equals("6")){
                        helper.setText(R.id.order_tv, "已超时");
                    }else if(item.getStatus() .equals("7")){
                        helper.setText(R.id.order_tv, "已取消");
                    }
                    // 4视频咨询
                }else if (item.getServiceType().equals("4")){
                    //status:/1待付费 2已购买 3已解答 4已结束 5 已退款 6 已超时 7 已取消
                    if (item.getStatus() .equals("2")){
                        helper.setText(R.id.order_tv, content(item.getStartTime(),item.getIsFinish()));
                    }else if(item.getStatus() .equals("4")){
                        helper.setText(R.id.order_tv, "查看详情");
                    }else if(item.getStatus() .equals("3")){
                        helper.setText(R.id.order_tv, "去评价");
                    }else if(item.getStatus() .equals("5")){
                        helper.setText(R.id.order_tv, "已退款");
                    }else if(item.getStatus() .equals("6")){
                        helper.setText(R.id.order_tv, "已超时");
                    }else if(item.getStatus() .equals("7")){
                        helper.setText(R.id.order_tv, "已取消");
                    }
                }
                final TextView tv=helper.getView(R.id.order_tv);
                //立即咨询 查看详情 立即评价点击事件
                helper.getView(R.id.order_tv).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userDao = userDao.findById(Integer.parseInt(membetId));
                        //获取环信用户名
                        firstPagerMager.getDoctorEMCName(item.getDoctorMemberId(), new SubscriberAdapter<Result>() {
                            @Override
                            public void success(Result result) {
                                super.success(result);
                                //图文的时候的立即咨询
                                if (tv.getText().toString().equals("立即咨询")){
                                    if (item.getServiceType().equals("2")) {
                                        Bundle args = new Bundle();
                                        args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                                        args.putString(EaseConstant.EXTRA_USER_ID, result.getHuanxin_username());
                                        args.putString("DT_RowId", item.getId());
                                        args.putString("DOC_NAME", item.getDoctorName());
                                        UIKit.open(getActivity(), ConversationActivity.class, args);
                                        //电话的时候的立即咨询
                                    }
                                    //视频的时候的立即咨询
                                    else if (item.getServiceType().equals("4")){
                                        Bundle bundle = new Bundle();
                                        bundle.putString("username", result.getHuanxin_username());
                                        bundle.putBoolean("isComingCall", false);
                                        bundle.putString("req_id",item.getId());
                                        bundle.putString("member_id",item.getDoctorMemberId());
                                        bundle.putString("schedule_id",item.getScheduleId());
                                        UIKit.open(getActivity(), VideoCallActivity.class,bundle);
                                    }
                                }else if (tv.getText().toString().equals("去评价")){
                                    Bundle bundle = new Bundle();
                                    bundle.putString("service_id", item.getConsultationId());
                                    UIKit.open(getActivity(), EvaluateDoctorActivity.class, bundle);
                                }else if (tv.getText().toString().equals("查看详情")){
                                    Intent intent = new Intent(getActivity(), ServiceHistoryDetialActivity.class);
                                    String conslutionId = "";
                                    String serviceType = "";
                                    String scheduleId = "";
                                    String content=tv.getText().toString();
                                    if (mMenuTab.getCurrentTab() == 0) {
                                        position = 0;
                                        conslutionId = item.getId();
                                        serviceType= item.getServiceType();
                                        scheduleId= item.getScheduleId();
                                    } else if (mMenuTab.getCurrentTab() == 1) {
                                        position = 1;
                                        conslutionId = item.getId();
                                        serviceType= item.getServiceType();
                                        scheduleId= item.getScheduleId();
                                    }
                                    if (mMenuTab.getCurrentTab() == 2) {
                                        position = 2;
                                        conslutionId = item.getId();
                                        serviceType= item.getServiceType();
                                        scheduleId= item.getScheduleId();
                                    }
                                    intent.putExtra("orderId", conslutionId);
                                    intent.putExtra("scheduleId", scheduleId);
                                    intent.putExtra("serviceType", serviceType);
                                    intent.putExtra("content", content);
                                    startActivity(intent);
                                }else if (tv.getText().toString().equals("等待解答")){
                                    dialog(2);
                                }else if (tv.getText().toString().equals("等待咨询")){
                                    //电话的时候的等待咨询
                                    if (item.getServiceType().equals("3")){
                                        dialog(7);
                                        //视频的时候的等待咨询
                                    }else if (item.getServiceType().equals("4")){
                                        dialog(3);
                                    }
                                }else if (tv.getText().toString().equals("已退款")){
                                    //电话的时候的退款
                                    if (item.getServiceType().equals("3")){
                                        dialog(8);
                                        //视频的时候的退款
                                    }else if (item.getServiceType().equals("4")){
                                        dialog(4);
                                    }
                                }else if (tv.getText().toString().equals("已超时")){
                                    dialog(5);
                                }else if (tv.getText().toString().equals("已取消")){
                                    dialog(6);
                                }else if (tv.getText().toString().equals("再次咨询")){
                                    //继续咨询
                                    Intent in=new Intent(getActivity(), DoctorDetialActivity.class);
                                    in.putExtra("member_id",item.getDoctorMemberId());
                                    startActivity(in);
                                }else if (tv.getText().toString().equals("点击退款")){
                                    serviceHistoryManger.tuikuan(item.getId(),new SubscriberAdapter<Result>(){
                                        @Override
                                        public void success(Result result) {
                                            super.success(result);
                                            Toaster.showShort(getActivity(),result.getMsg());
                                            getRecodeLists();
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
        };
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), ServiceHistoryDetialActivity.class);
                String conslutionId = "";
                TextView tv = (TextView) view.findViewById(R.id.order_tv);
                String content=tv.getText().toString();
                String serviceType="";
                String scheduleId="";
                if (mMenuTab.getCurrentTab() == 0) {
                    position = 0;
                    conslutionId = list_quest.get(i).getId();
                    serviceType=list_quest.get(i).getServiceType();
                    scheduleId=list_quest.get(i).getScheduleId();
                } else if (mMenuTab.getCurrentTab() == 1) {
                    position = 1;
                    conslutionId = list_assess.get(i).getId();
                    serviceType=list_assess.get(i).getServiceType();
                    scheduleId=list_assess.get(i).getScheduleId();
                }
                if (mMenuTab.getCurrentTab() == 2) {
                    position = 2;
                    conslutionId = list_over.get(i).getId();
                    serviceType=list_over.get(i).getServiceType();
                    scheduleId=list_over.get(i).getScheduleId();
                }
                intent.putExtra("orderId", conslutionId);
                intent.putExtra("scheduleId", scheduleId);
                intent.putExtra("content", content);
                intent.putExtra("serviceType", serviceType);
                startActivity(intent);
            }
        });
        return viewGroup;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //登录或者退出之后刷新
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("REFRESH");
        intentFilter.addAction("UPDATE_MSG_COUNT");
        getActivity().registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String memberId) {
                if (memberId.equals("UPDATE_MSG_COUNT")){
                    noread_message.clear();
                    all_message = pushMessage.findAll();
                    //更新消息数量
                    int unreadMsgCountTotal = 0;
                    //获取环信未读消息数量
                    for (int i = 0; i < all_message.size(); i++) {
                        if (all_message.get(i).getIsread().equals("1")) {
                            noread_message.add(all_message.get(i));
                        }
                    }
                    Message msgDao = new Message();
                    List<Message> mAllDatas = new ArrayList<>();
                    mAllDatas.addAll(msgDao.findAll());
                    for (Message msg : mAllDatas){
                        if (!msg.isRead()){
                            unreadMsgCountTotal++;
                        }
                    }
                    unreadMsgCountTotal = unreadMsgCountTotal + noread_message.size();
                    tv_red_point.setText(unreadMsgCountTotal+"");
                    if (unreadMsgCountTotal > 0) {
                        tv_red_point.setVisibility(View.VISIBLE);
                    } else {
                        tv_red_point.setVisibility(View.GONE);
                    }
                }else {
                    if (memberId .equals("2")) {
                        //登录
                        ll_nologin.setVisibility(View.GONE);
                        getRecodeLists();
                    } else if (memberId.equals("1")){
                        ll_nologin.setVisibility(View.VISIBLE);
                    }else{
                        tv_red_point.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("服务记录");
        mTabEntities.clear();
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i]));
        }
        mMenuTab.setTabData(mTabEntities);
        mMenuTab.setOnTabSelectListener(this);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //刷新数据
                getRecodeLists();
            }
        });
    }

    /**
     * menu切换监听
     *
     * @param position1
     */
    @Override
    public void onTabSelect(int position1) {
        if (position1 == 0) {
            if (list_quest.size() > 0) {
                ll_no.setVisibility(View.GONE);
                position = 0;
                adapter.clear();
                adapter.addAll(list_quest);
            } else {
                tv_textbooka.setText("目前没有正在进行中的咨询");
                ll_no.setVisibility(View.VISIBLE);
            }
        } else if (position1 == 1) {
            if (list_assess.size() > 0) {
                ll_no.setVisibility(View.GONE);
                position = 1;
                adapter.clear();
                adapter.addAll(list_assess);
            } else {
                tv_textbooka.setText("目前没有未评价的咨询");
                ll_no.setVisibility(View.VISIBLE);
            }
        } else if (position1 == 2) {
            if (list_over.size() > 0) {
                ll_no.setVisibility(View.GONE);
                position = 2;
                adapter.clear();
                adapter.addAll(list_over);
            } else {
                tv_textbooka.setText("您还没有在巨医网进行过任何服务");
                ll_no.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onTabReselect(int position) {
    }

    @OnClick(R.id.tab_doctor_logout_login)
    void login() {
        if (mMenuTab.getCurrentTab() == 0) {
            position = 0;
        } else if (mMenuTab.getCurrentTab() == 1) {
            position = 1;
        } else if (mMenuTab.getCurrentTab() == 2) {
            position = 2;
        }
        UIKit.open(getActivity(), LoginActivity.class);
    }
    @Override
    public void onStart() {
        super.onStart();
        int noread=0;
        int unReadCount=0;
        membetId= AbSharedUtil.getString(getActivity(),"userId");
        if (membetId!=null&&!membetId.equals("")) {
            all_message = pushMessage.findAll();
            for (int i = 0; i < all_message.size(); i++) {
                if (all_message.get(i).getIsread().equals("1")) {
                    noread++;
                }
            }
            Message msgDao = new Message();
            List<Message> mAllDatas = new ArrayList<>();
            mAllDatas.addAll(msgDao.findAll());
            for (Message msg : mAllDatas){
                if (!msg.isRead()){
                    unReadCount++;
                }
            }
            noread =noread+ unReadCount;
            tv_red_point.setText(noread+"");
            if (noread > 0) {
                tv_red_point.setVisibility(View.VISIBLE);
            } else {
                tv_red_point.setVisibility(View.GONE);
            }
            ll_nologin.setVisibility(View.GONE);
            getRecodeLists();
        } else {
            ll_nologin.setVisibility(View.VISIBLE);
        }
    }

    private void getRecodeLists() {
        membetId= AbSharedUtil.getString(getActivity(),"userId");
        serviceHistoryManger.getServicelist(membetId, new SubscriberAdapter<ListProResult<HistoryRecord>>() {
            @Override
            public void onError(Throwable e) {
                // super.onError(e);
                dissmissProgressDialog();
                swipeRefreshLayout.setRefreshing(false);
                ll_no.setVisibility(View.VISIBLE);
            }

            @Override
            public void success(ListProResult<HistoryRecord> historyRecordListProResult) {
                super.success(historyRecordListProResult);
                swipeRefreshLayout.setRefreshing(false);
                if (historyRecordListProResult.getCode()) {
                    if (historyRecordListProResult.getData().size() > 0 && historyRecordListProResult.getData() != null) {
                        ll_no.setVisibility(View.GONE);
                        lv.setVisibility(View.VISIBLE);
                        list_assess.clear();
                        list_over.clear();
                        list_quest.clear();
                        list_all.clear();
                        if (historyRecordListProResult.getData().size() > 0)
                            list_all.addAll(historyRecordListProResult.getData());
                        for (int i = 0; i < list_all.size(); i++) {
                            if (list_all.get(i).getStatus().equals("2")) {
                                list_quest.add(list_all.get(i));
                            } else if (list_all.get(i).getStatus().equals("3")) {
                                list_assess.add(list_all.get(i));
                            } else if (list_all.get(i).getStatus().equals("4") || list_all.get(i).getStatus().equals("5") || list_all.get(i).getStatus().equals("6")) {
                                list_over.add(list_all.get(i));
                            }
                        }
                        if (position == 0) {
                            if (list_quest.size() > 0) {
                                ll_no.setVisibility(View.GONE);
                                mMenuTab.setCurrentTab(0);
                                adapter.clear();
                                adapter.addAll(list_quest);
                            } else {
                                ll_no.setVisibility(View.VISIBLE);
                                tv_textbooka.setText("目前没有正在进行中的咨询");
                            }
                        } else if (position == 1) {
                            if (list_assess.size() > 0) {
                                ll_no.setVisibility(View.GONE);
                                position = 1;
                                adapter.clear();
                                adapter.addAll(list_assess);
                            } else {
                                tv_textbooka.setText("目前没有未评价的咨询");
                                ll_no.setVisibility(View.VISIBLE);
                            }
                        } else if (position == 2) {
                            if (list_over.size() > 0) {
                                ll_no.setVisibility(View.GONE);
                                position = 2;
                                adapter.clear();
                                adapter.addAll(list_over);
                            } else {
                                tv_textbooka.setText("您还没有在巨医网进行过任何服务");
                                ll_no.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                } else {
                    ll_no.setVisibility(View.VISIBLE);
                    lv.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (myReceiver != null) {
            getActivity().unregisterReceiver(myReceiver);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick(R.id.notice_layout)
    public void notice_layout() {
        String memberId= AbSharedUtil.getString(getActivity(),"userId");
        if (memberId!=null&&!memberId.equals("")) {
            UIKit.open(getActivity(), MessageActivity.class);
        }else {
            dialog(1);
        }
    }
    // 弹窗
    private void dialog(final int a) {
        dialog= new LoginDialog(getActivity());
        if (a==1){
            dialog.setContent("暂未登录，请登录☺");
        }else if (a==2){
            dialog.setContent("本次咨询服务已结束，请您耐心等待解答，谢谢使用！");
        }else if (a==3){
            dialog.setContent("还未到预约时间，请耐心等待！");
        }else if (a==4){
            dialog.setContent("在视频咨询预约时间内您未进行咨询，系统将会自动退回部分咨询款项到您的账户中，请耐心等待。");
        }else if (a==5){
            dialog.setContent("此服务已超时");
        }else if (a==8){
            dialog.setContent("由于您在本次电话咨询服务中未接听咨询电话，系统将会扣除部分款项，剩余部分将会退回您的账户中，谢谢使用。");
        }else if (a==6){
            dialog.setContent("此服务已取消");
        }else if (a==7){
            dialog.setContent("还未到您与医生预约的时间，请耐心等待！到达预约时间后请注意接听号码010- 88888888（号码需要固定） 的来电，接听此号码后请耐心等待医生接听！");
        }
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a==1) {
                    dialog.dismiss();
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), LoginActivity.class);
                    startActivity(intent);
                }else {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }
    private String content(String befor,String isFinish) {
        //  电话回拨的状态 0 未发起电话回拨 1 到预约时间发起电话回拨 2 电话未接通， 5分钟后重新回拨  3 电话未接通  30分钟后重拨
        // 4  退款 5已给当前服务记录医患双方发过预约通知短信(这几个状态只是在service_type为3时)，视频是否接通    0  未发起视频咨询
        // 1  已接通视频咨询 2 已给该条记录发过短信 3 未接通视频咨询(service_type为4的状态)
        String content="";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            if (befor!=null&&!TextUtils.isEmpty(befor)) {
                Date date = sdf.parse(befor);
                Date after = new Date(date.getTime() + 900000);
                Date now = new Date();
                String nowtime = sdf.format(now);
                String gettime = sdf.format(after);
                String before = sdf.format(date);
                java.text.DateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                java.util.Calendar c1 = java.util.Calendar.getInstance();
                java.util.Calendar c2 = java.util.Calendar.getInstance();
                java.util.Calendar c3 = java.util.Calendar.getInstance();
                c2.setTime(df.parse(gettime));
                c1.setTime(df.parse(nowtime));
                c3.setTime(df.parse(before));
                int a = c1.compareTo(c3);
                int result = c1.compareTo(c2);
                if (a >= 0) {
                    if (result <= 0) {
                        if (isFinish.equals("0") || isFinish.equals("2") || isFinish.equals("3")) {
                            content = "立即咨询";
                        } else if (isFinish.equals("1")) {
                            content = "等待解答";
                        }
                    } else {
                        if (isFinish.equals("1")) {
                            content = "查看详情";
                        } else if (isFinish.equals("3")) {
                            content = "点击退款";
                        }
                    }
                } else {
                    content = "等待咨询";
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return content;
    }

}
