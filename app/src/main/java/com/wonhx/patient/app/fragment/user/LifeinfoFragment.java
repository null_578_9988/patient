package com.wonhx.patient.app.fragment.user;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wonhx.patient.R;
import com.wonhx.patient.app.activity.user.FoodActivity;
import com.wonhx.patient.app.activity.user.WinActivity;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.LifeInfo;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UpdateUIListenner;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 2017/4/7.
 * 健康档案：生活信息
 */

public class LifeinfoFragment extends BaseFragment {
    UserManager userManager = new UserManagerImpl();
    LifeInfo mLifeInfo;
    @InjectView(R.id.tv_food)
    TextView tvFood;
    @InjectView(R.id.changePwd)
    RelativeLayout changePwd;
    @InjectView(R.id.tv_smoke)
    TextView tvSmoke;
    @InjectView(R.id.tv_wine)
    TextView tvWine;
    @InjectView(R.id.tv_sports)
    TextView tvSports;
    @InjectView(R.id.tv_sleep)
    TextView tvSleep;
    @InjectView(R.id.tv_pill)
    TextView tvPill;
    @InjectView(R.id.commit_btn)
    Button commitBtn;
    String mHealthyId;
    @InjectView(R.id.tv_wine_type)
    TextView tvWineType;
    @InjectView(R.id.rl_win)
    RelativeLayout rlWin;
    @InjectView(R.id.tv_sports_time)
    EditText tvSportsTime;
    @InjectView(R.id.tv_sports_repeat)
    EditText tvSportsRepeat;
    @InjectView(R.id.tv_sports_type)
    EditText tvSportsType;
    @InjectView(R.id.ll_sport)
    LinearLayout llSport;
    MyReceiver myReceiver;
    String Foodhibats = "", Drink_kinds = "";
    boolean enable = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_life_info, container, false);
        ButterKnife.inject(this, viewGroup);
        return viewGroup;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("FLAG");
        getActivity().registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String helthId) {
                if (helthId != null) {
                    //登录
                    getmessagea();
                    setEnable(enable);
                }
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick({R.id.tv_food, R.id.tv_smoke, R.id.tv_wine, R.id.tv_wine_type, R.id.tv_sports, R.id.tv_sleep, R.id.tv_pill, R.id.commit_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_food:
                Intent in = new Intent(getActivity(), FoodActivity.class);
                in.putExtra("msg", Foodhibats);
                startActivityForResult(in, 101);
                break;
            case R.id.tv_smoke:
                choose_operation(1);
                break;
            case R.id.tv_wine:
                choose_operation(2);
                break;
            case R.id.tv_wine_type:
                Intent in2 = new Intent(getActivity(), WinActivity.class);
                in2.putExtra("msg", Drink_kinds);
                startActivityForResult(in2, 103);
                break;
            case R.id.tv_sports:
                choose_operation(3);
                break;
            case R.id.tv_sleep:
                choose_operation(4);
                break;
            case R.id.tv_pill:
                choose_blood_type();
                break;
            case R.id.commit_btn:
                if (enable){
                    commit();
                }else{
                    enable=true;
                    setEnable(true);
                }
                break;
        }
    }

    private void getmessagea() {
        mHealthyId = AbSharedUtil.getString(getActivity(), "helthId");
        if (mHealthyId != null) {
            userManager.getLifeInfo(mHealthyId, new SubscriberAdapter<ProResult<LifeInfo>>() {
                @Override
                public void onError(Throwable e) {
                    //super.onError(e);
                    dissmissProgressDialog();
                    commitBtn.setText("保存");
                }

                @Override
                public void success(ProResult<LifeInfo> lifeInfoProResult) {
                    super.success(lifeInfoProResult);
                    if (lifeInfoProResult.getCode()) {
                        commitBtn.setText("编辑");
                        mLifeInfo = lifeInfoProResult.getData();
                        if (mLifeInfo != null) {
                            Foodhibats = mLifeInfo.getFoodhibats();
                            tvFood.setText(Foodhibats);
                            tvSmoke.setText(mLifeInfo.getIs_smook());
                            tvWine.setText(mLifeInfo.getIs_drink());
                            if (tvWine.getText().equals("是")) {
                                rlWin.setVisibility(View.VISIBLE);
                            } else {
                                rlWin.setVisibility(View.GONE);
                            }
                            tvSports.setText(mLifeInfo.getIs_exercise());
                            if (tvSports.getText().equals("是")) {
                                llSport.setVisibility(View.VISIBLE);
                            } else {
                                llSport.setVisibility(View.GONE);
                            }
                            tvSleep.setText(mLifeInfo.getIs_sleep());
                            tvPill.setText(mLifeInfo.getIs_longdose());
                            tvSportsRepeat.setText(mLifeInfo.getExerciseyear() + "年");
                            tvSportsTime.setText(mLifeInfo.getExercisetime() + "分钟/次");
                            tvSportsType.setText(mLifeInfo.getExerciseway());
                            Drink_kinds = mLifeInfo.getDrink_kinds();
                            tvWineType.setText(Drink_kinds);
                        }
                    } else {
                        commitBtn.setText("保存");
                    }

                }
            });
        }
    }

    private void choose_blood_type() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.select_dialog_blood, null);
        final Dialog dialog = new AlertDialog.Builder(getActivity()).create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.mydialogstyle);  //添加动画
        window.setBackgroundDrawable(new ColorDrawable());
        dialog.show();
        window.setContentView(view);
        TextView o = (TextView) view.findViewById(R.id.o);
        o.setText("镇痛药");
        TextView ab = (TextView) view.findViewById(R.id.ab);
        ab.setText("镇痛催眠药");
        TextView a = (TextView) view.findViewById(R.id.a);
        a.setText("无");
        TextView b = (TextView) view.findViewById(R.id.b);
        b.setVisibility(View.GONE);
        TextView cancle = (TextView) view.findViewById(R.id.cancle);
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                tvPill.setText("无");
            }
        });
        ab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                tvPill.setText("镇痛催眠药");
            }
        });
        o.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                tvPill.setText("镇痛药");
            }
        });

    }

    private void commit() {
        mLifeInfo = new LifeInfo();
        mLifeInfo.setFoodhibats(Foodhibats);
        mLifeInfo.setIs_smook(tvSmoke.getText().toString().trim());
        mLifeInfo.setIs_drink(tvWine.getText().toString().trim());
        mLifeInfo.setIs_exercise(tvSports.getText().toString().trim());
        mLifeInfo.setIs_sleep(tvSleep.getText().toString().trim());
        mLifeInfo.setDrink_kinds(tvWineType.getText().toString().trim());
        mLifeInfo.setExercisetime(tvSportsTime.getText().toString().trim());
        mLifeInfo.setExerciseway(tvSportsType.getText().toString().trim());
        mLifeInfo.setExerciseyear(tvSportsRepeat.getText().toString().trim());
        mLifeInfo.setIs_longdose(tvPill.getText().toString().trim());
        String msg = new Gson().toJson(mLifeInfo);
        userManager.submitLifeinfo(mHealthyId, msg, new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                Toaster.showShort(getActivity(), result.getMsg());
                enable =false;
                setEnable(false);
            }
        });
    }

    /**
     * 是否能编辑
     *
     * @param enable
     */
    private void setEnable(boolean enable) {
        tvSmoke.setEnabled(enable);
        tvWine.setEnabled(enable);
        tvSports.setEnabled(enable);
        tvSleep.setEnabled(enable);
        tvWineType.setEnabled(enable);
        tvSportsTime.setEnabled(enable);
        tvSportsType.setEnabled(enable);
        tvSportsRepeat.setEnabled(enable);
        tvPill.setEnabled(enable);
        tvFood.setEnabled(enable);
        if (enable){
            commitBtn.setText("保存");
        }else{
            commitBtn.setText("编辑");
        }
    }

    private void choose_operation(final int i) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.select_dialog, null);
        final Dialog dialog = new AlertDialog.Builder(getActivity()).create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.mydialogstyle);  //添加动画
        window.setBackgroundDrawable(new ColorDrawable());
        dialog.show();
        window.setContentView(view);
        TextView camera = (TextView) view.findViewById(R.id.camera);
        camera.setText("是");
        final TextView picfile = (TextView) view.findViewById(R.id.picfile);
        picfile.setText("否");
        TextView cancle = (TextView) view.findViewById(R.id.cancle);
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //拍照
                dialog.dismiss();
                if (i == 1) {
                    tvSmoke.setText("是");
                } else if (i == 2) {
                    tvWine.setText("是");
                    rlWin.setVisibility(View.VISIBLE);
                } else if (i == 3) {
                    tvSports.setText("是");
                    llSport.setVisibility(View.VISIBLE);
                } else if (i == 4) {
                    tvSleep.setText("是");
                }
            }
        });
        picfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //相册
                dialog.dismiss();
                if (i == 1) {
                    tvSmoke.setText("否");
                } else if (i == 2) {
                    tvWine.setText("否");
                    rlWin.setVisibility(View.GONE);
                } else if (i == 3) {
                    tvSports.setText("否");
                    llSport.setVisibility(View.GONE);
                } else if (i == 4) {
                    tvSleep.setText("否");
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == 102) {
            Foodhibats = data.getStringExtra("msg");
            tvFood.setText(Foodhibats);
        }
        if (requestCode == 103 && resultCode == 202) {
            Drink_kinds = data.getStringExtra("msg");
            tvWineType.setText(Drink_kinds);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (myReceiver != null) {
            getActivity().unregisterReceiver(myReceiver);
        }
    }
}
