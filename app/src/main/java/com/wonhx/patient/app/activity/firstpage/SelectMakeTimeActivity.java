package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.model.DoctorDetial;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by nsd on 2017/4/21.
 * 选择预约时间
 */

public class SelectMakeTimeActivity extends BaseActivity {
    @InjectView(R.id.listview)
    ListView mListView;
    @InjectView(R.id.title)
    TextView mTitle;
    ArrayList<DoctorDetial.Schedule> mScheduleDatas = new ArrayList<>();
    QuickAdapter<DoctorDetial.Schedule> mListAdapter;
    int type = 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_maketime);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mTitle.setText("选择预约时间");
        mListAdapter = new QuickAdapter<DoctorDetial.Schedule>(this,R.layout.listitem_select) {
            @Override
            protected void convert(BaseAdapterHelper helper, DoctorDetial.Schedule item) {
                helper.setText(R.id.content,item.getStart_time());
            }
        };
        mListView.setAdapter(mListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DoctorDetial.Schedule schedule = mListAdapter.getItem(position);
                Intent intent = new Intent();
                intent.putExtra("select_time",schedule);
                setResult(RESULT_OK,intent);
                finish();
            }
        });
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        mScheduleDatas = (ArrayList<DoctorDetial.Schedule>) getIntent().getSerializableExtra("schedules");
        type = getIntent().getIntExtra("type",3);
        List<DoctorDetial.Schedule> schedules = new ArrayList<>();
        for (DoctorDetial.Schedule schedule : mScheduleDatas){
            if (schedule.getService_type().equals(String.valueOf(type))){
                schedules.add(schedule);
            }
        }
        mListAdapter.addAll(schedules);
    }
    @OnClick(R.id.left_btn)
    void back(){
        finish();
    }
}
