package com.wonhx.patient.app.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.model.TuijianDoctor;

import java.util.List;


public class TuijianDoctorAbstratorInfo extends BaseAdapter {

    private List<TuijianDoctor > data;
    /**
     * LayoutInflater 类是代码实现中获取布局文件的主要形式 LayoutInflater layoutInflater =
     * LayoutInflater.from(context); View convertView =
     * layoutInflater.inflate();
     * LayoutInflater的使用,在实际开发种LayoutInflater这个类还是非常有用的,它的作用类似于 findViewById(),
     * 不同点是LayoutInflater是用来找layout下xml布局文件，并且实例化！ 而findViewById()是找具体xml下的具体
     * widget控件(如:Button,TextView等)。
     */
    private LayoutInflater layoutInflater;
    private Context context;
    // 适配显示的图片数组
    private int layoutId;

    public TuijianDoctorAbstratorInfo(Context context) {

        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }
    public void replaceAll(List<TuijianDoctor> elem) {
        data.clear();
        data.addAll(elem);
        notifyDataSetChanged();
    }

    public TuijianDoctorAbstratorInfo(Context context,  List<TuijianDoctor > data) {

        this.context = context;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public TuijianDoctorAbstratorInfo(Context context, int layoutId,
                                      List<TuijianDoctor > data) {
        this.context = context;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
        this.layoutId = layoutId;
    }

    public void setData( List<TuijianDoctor > data) {
        this.data = data;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (data != null && data.size() != 0) {
            return data.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (data != null && data!= null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        // 组装数据
        if (convertView == null) {
            convertView = this.layoutInflater.inflate(this.layoutId, null);

            holder.doctor_name = (TextView) convertView
                    .findViewById(R.id.doctor_name);
            holder.doctor_hospitalName = (TextView) convertView
                    .findViewById(R.id.doctor_hospitalName);
            holder.doctor_dept = (TextView) convertView
                    .findViewById(R.id.doctor_dept);
            holder.doctor_goodSubjects = (TextView) convertView
                    .findViewById(R.id.doctor_goodSubjects);
            holder.doctor_title = (TextView) convertView
                    .findViewById(R.id.doctor_title);
            holder.listview = (LinearLayout) convertView
                    .findViewById(R.id.listview);
            holder.logo = (SimpleDraweeView) convertView.findViewById(R.id.logo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.doctor_name.setText(this.data
                .get(position).getName());
        holder.doctor_hospitalName.setText(this.data
                .get(position).getHospitalName());
        holder.doctor_dept.setText(this.data
                .get(position).getDept()
                + "");
        holder.doctor_goodSubjects.setText(this.data
                .get(position).getGoodSubjects());
        holder.doctor_title.setText(this.data
                .get(position).getTitle());
        holder.logo.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine/pub/member_logo/" + data.get(position).getMemberId()+"/"+data.get(position).getMemberId()+".png"));
        return convertView;
    }

    public class ViewHolder {
        TextView doctor_name; // 名字
        TextView doctor_hospitalName;// 医院
        TextView doctor_dept;// 科室
        TextView doctor_goodSubjects;// 主治
        TextView doctor_title;// 职位
        SimpleDraweeView logo;
        LinearLayout listview;
    }
}