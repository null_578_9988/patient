package com.wonhx.patient.app.ease;

import com.wonhx.patient.app.model.BaseModel;
import com.wonhx.patient.db.annotation.Table;

/**
 * Created by nsd on 2017/5/10.
 *
 */
@Table(version = 1)
public class Message extends BaseModel<Message> {
    public String orderId = "0";
    public String content;
    public String doctor;
    public String type;
    public long time;
    public String endTime;
    public boolean isRead = false;
    public int unReadCount;
    public String nickName;
    public String memberId;
    public String homeOrderId ;
    public String clinicId;

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getHomeOrderId() {
        return homeOrderId;
    }

    public void setHomeOrderId(String homeOrderId) {
        this.homeOrderId = homeOrderId;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getUnReadCount() {
        return unReadCount;
    }

    public void setUnReadCount(int unReadCount) {
        this.unReadCount = unReadCount;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
