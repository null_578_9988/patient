package com.wonhx.patient.app.fragment.user;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.google.gson.Gson;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.BaseInfo;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.Toaster;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 2017/4/7.
 * 健康档案：基本信息
 */

public class BaseInfoFragment extends BaseFragment {
    @InjectView(R.id.name)
    EditText mUserName;
    @InjectView(R.id.gender)
    TextView mUserSex;
    @InjectView(R.id.age)
    EditText mUserAge;
    @InjectView(R.id.birthday_year)
    TextView mBirthday;
    @InjectView(R.id.idCard)
    EditText mIdCard;
    @InjectView(R.id.telephone)
    EditText mTelePhone;
    @InjectView(R.id.address)
    EditText mAdress;
    @InjectView(R.id.work)
    EditText mWork;
    @InjectView(R.id.marryOrNot)
    TextView mMarryOrNot;
    @InjectView(R.id.contact_person)
    EditText mContactPerson;
    @InjectView(R.id.contact_phone)
    EditText mContactPhone;
    //    @InjectView(R.id.create_time)
//    EditText mCreateTime;
    @InjectView(R.id.creater)
    EditText mCreater;
    UserManager userManager = new UserManagerImpl();
    BaseInfo mBaseInfo;
    String mMemberId;
    TimePickerView pvTime;
    @InjectView(R.id.tv_commit)
    TextView tv_commit;
    String age = "";
    /**
     * 是否能编辑
     */
    boolean enable = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_base_info, container, false);
        ButterKnife.inject(this, viewGroup);
        Calendar c = Calendar.getInstance();
        pvTime = new TimePickerView.Builder(getActivity(), new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                mBirthday.setText(getTime(date));
            }
        })
                .setType(TimePickerView.Type.YEAR_MONTH_DAY)//默认全部显示
                .setCancelText("取消")//取消按钮文字
                .gravity(Gravity.CENTER)
                .setSubmitText("确定")//确认按钮文字
                .setContentSize(18)//滚轮文字大小
                .setTitleSize(20)//标题文字大小
                .setTitleText("")//标题文字
                .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(true)//是否循环滚动
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setSubmitColor(Color.BLUE)//确定按钮文字颜色
                .setCancelColor(Color.BLUE)//取消按钮文字颜色
                .setTitleBgColor(getResources().getColor(R.color.line_grey))//标题背景颜色 Night mode
                .setBgColor(Color.WHITE)//滚轮背景颜色 Night mode
                .setRange(c.get(Calendar.YEAR) - 100, c.get(Calendar.YEAR))//默认是1900-2100年
                // .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
                //.setRangDate(startDate,endDate)//起始终止年月日设定
                .setLabel("年", "月", "日", "", "", "")
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                // .isDialog(true)//是否显示为对话框样式
                .build();
        return viewGroup;
    }

    private String getTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        mMemberId = AbSharedUtil.getString(getActivity(), "userId");
        getBaseinfo();
        setEnable(enable);
    }

    private void getBaseinfo() {
        userManager.getBaseInfo(mMemberId, new SubscriberAdapter<ProResult<BaseInfo>>() {
            @Override
            public void onError(Throwable e) {
                //super.onError(e);
                tv_commit.setText("保存");
                dissmissProgressDialog();
            }

            @Override
            public void success(ProResult<BaseInfo> baseInfoProResult) {
                super.success(baseInfoProResult);
                if (baseInfoProResult.code.equals("0")) {
                    tv_commit.setText("编辑");
                    mBaseInfo = baseInfoProResult.getData();
                    if (mBaseInfo != null) {
                        AbSharedUtil.putString(getActivity(), "helthId", mBaseInfo.getHealth_id());
                        //退出成功后  刷新 服务记录与我的医生
                        Intent intent = new Intent();
                        intent.putExtra("key", mBaseInfo.getHealth_id());
                        intent.setAction("FLAG");
                        getActivity().sendBroadcast(intent);
                        mUserName.setText(mBaseInfo.getName());
                        mUserSex.setText(mBaseInfo.getSex());
                        age = mBaseInfo.getAge();
                        mUserAge.setText(age);
//                        Calendar a=Calendar.getInstance();
//                        int ages=a.get(Calendar.YEAR);
//                        int agea=ages-Integer.parseInt(age);
//                        if (agea==ages){
//                            mUserAge.setText("您还未设置出生日期");
//                        }else if (agea>=0){
//                            mUserAge.setText(agea);
//                        }else  if (agea<0){
//                            mUserAge.setText("您设置出生日期有误！");
//                        }
                        mBirthday.setText(mBaseInfo.getBirthday());
                        mIdCard.setText(mBaseInfo.getId_card());
                        mTelePhone.setText(mBaseInfo.getPhone());
                        mAdress.setText(mBaseInfo.getAddress());
                        mWork.setText(mBaseInfo.getProfession());
                        mMarryOrNot.setText(mBaseInfo.getIs_married());
                        mContactPerson.setText(mBaseInfo.getContectperson());
                        mContactPhone.setText(mBaseInfo.getContectphone());
                        mCreater.setText(mBaseInfo.getPerson());
                        //mCreateTime.setText(mBaseInfo.getPerson_time());
                    }
                } else {
                    tv_commit.setText("保存");
                }
            }
        });
    }

    @OnClick(R.id.gender)
    void choosemale() {
        malechoose();
    }

    @OnClick(R.id.birthday_year)
    void birth() {
        pvTime.show();
    }

    @OnClick(R.id.marryOrNot)
    void choosemarry() {
        choosrMarry();
    }

    @OnClick(R.id.tv_commit)
    void commit() {
        if (enable){
            mBaseInfo = new BaseInfo();
            mBaseInfo.setName(mUserName.getText().toString().trim());
            mBaseInfo.setSex(mUserSex.getText().toString().trim());
            mBaseInfo.setAge(mUserAge.getText().toString().trim());
            mBaseInfo.setBirthday(mBirthday.getText().toString().trim());
            mBaseInfo.setId_card(mIdCard.getText().toString().trim());
            mBaseInfo.setPhone(mTelePhone.getText().toString().trim());
            mBaseInfo.setAddress(mAdress.getText().toString().trim());
            mBaseInfo.setProfession(mWork.getText().toString().trim());
            mBaseInfo.setIs_married(mMarryOrNot.getText().toString().trim());
            mBaseInfo.setContectperson(mContactPerson.getText().toString().trim());
            mBaseInfo.setContectphone(mContactPhone.getText().toString());
            String msg = new Gson().toJson(mBaseInfo);
            userManager.submitBaseInfo(mMemberId, msg, new SubscriberAdapter<Result>() {
                @Override
                public void success(Result result) {
                    super.success(result);
                    Toaster.showShort(getActivity(), result.getMsg());
                    AbSharedUtil.putString(getActivity(), "helthId", result.getData());
                    Intent intent = new Intent();
                    intent.putExtra("key", result.getData());
                    intent.setAction("FLAG");
                    getActivity().sendBroadcast(intent);
                    enable =false;
                    setEnable(false);
                }
            });
        }else{
            enable = true;
            setEnable(enable);
        }


    }

    /**
     * 是否能编辑
     *
     * @param enable
     */
    private void setEnable(boolean enable) {
        mUserName.setEnabled(enable);
        mUserSex.setEnabled(enable);
        mUserAge.setEnabled(enable);
        mBirthday.setEnabled(enable);
        mIdCard.setEnabled(enable);
        mTelePhone.setEnabled(enable);
        mAdress.setEnabled(enable);
        mWork.setEnabled(enable);
        mMarryOrNot.setEnabled(enable);
        mContactPerson.setEnabled(enable);
        mContactPhone.setEnabled(enable);
        if (enable){
            tv_commit.setText("保存");
        }else{
            tv_commit.setText("编辑");
        }

    }

    private void malechoose() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.select_dialog, null);
        final Dialog dialog = new AlertDialog.Builder(getActivity()).create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.mydialogstyle);  //添加动画
        window.setBackgroundDrawable(new ColorDrawable());
        dialog.show();
        window.setContentView(view);
        TextView camera = (TextView) view.findViewById(R.id.camera);
        camera.setText("男");
        TextView picfile = (TextView) view.findViewById(R.id.picfile);
        picfile.setText("女");
        TextView cancle = (TextView) view.findViewById(R.id.cancle);
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserSex.setText("男");
                //拍照
                dialog.dismiss();
            }
        });
        picfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserSex.setText("女");
                //相册
                dialog.dismiss();
            }
        });
    }

    private void choosrMarry() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.select_dialog, null);
        final Dialog dialog = new AlertDialog.Builder(getActivity()).create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.mydialogstyle);  //添加动画
        window.setBackgroundDrawable(new ColorDrawable());
        dialog.show();
        window.setContentView(view);
        TextView camera = (TextView) view.findViewById(R.id.camera);
        camera.setText("是");
        TextView picfile = (TextView) view.findViewById(R.id.picfile);
        picfile.setText("否");
        TextView cancle = (TextView) view.findViewById(R.id.cancle);
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMarryOrNot.setText("是");
                //拍照
                dialog.dismiss();
            }
        });
        picfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMarryOrNot.setText("否");
                //相册
                dialog.dismiss();
            }
        });
    }


}
