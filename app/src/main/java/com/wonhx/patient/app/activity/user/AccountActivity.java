package com.wonhx.patient.app.activity.user;

import android.os.Bundle;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Account;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;

import butterknife.InjectView;
import butterknife.OnClick;

public class AccountActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView tv_title;
    @InjectView(R.id.right_btn)
    TextView tv_right;
    @InjectView(R.id.accountBalance)
    TextView tv_accountBalance;
    UserManager userManager=new UserManagerImpl();
    String memberid="";

    @Override
    protected void onInitView() {
        super.onInitView();
        tv_right.setText("账单明细");
        tv_title.setText("账户余额");
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        memberid= AbSharedUtil.getString(this,"userId");
        getAccount();
    }
    private void getAccount() {
        userManager.getaccount(memberid,new SubscriberAdapter<ProResult<Account>>(){
            @Override
            public void success(ProResult<Account> accountProResult) {
                super.success(accountProResult);
                tv_accountBalance.setText(accountProResult.getData().getAccount_balance());
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        getAccount();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
    }
    @OnClick(R.id.left_btn)
    void back(){
        finish();
    }
    //充值
    @OnClick(R.id.deposit_btn)
    void send_rmb(){
        UIKit.open(AccountActivity.this,DepositActivity.class);
    }
    //提现
    @OnClick(R.id.li_cash)
    void pull(){
        Toaster.showShort(AccountActivity.this,"暂时未开通！");
    }
    //账单明细
    @OnClick(R.id.right_btn)
    void detial(){
        UIKit.open(AccountActivity.this,AccountDetialActivity.class);
    }
}
