package com.wonhx.patient.app.manager;

import rx.Subscriber;
import rx.Subscription;

/**
 * Created by apple on 17/3/21.
 *  用户模块接口
 */
public interface UserManager {

    /**
     *  登录
     * @param username
     * @param password
     * @param member_type
     * @param device_type
     * @param device_token
     * @param subscriber
     * @return
     */
    public Subscription login(String username,String password
            ,String member_type
            ,String device_type
            ,String device_token
            ,Subscriber subscriber);

    /**
     * 获取注册手机验证码
     * @param phone
     * @param ident_code
     * @param subscriber
     * @return
     */
    public Subscription getRegisterCode(String phone ,String ident_code,String member_type,Subscriber subscriber);

    /**
     * 注册
     * @param phone
     * @param password
     * @param member_type
     * @param sms_code
     * @param device_type
     * @param device_token
     * @param subscriber
     * @return
     */
    public Subscription register(String phone,String password,String member_type,String sms_code,String device_type,String device_token,Subscriber subscriber);

    /**
     * 修改密码获取短信验证码接口
     * @param phone
     * @param subscriber
     * @return
     */
    public Subscription getForgetCode(String phone,Subscriber subscriber);

    /**
     * 修改密码
     * @param phone
     * @param password
     * @param member_type
     * @param sms_code
     * @param subscriber
     * @return
     */
    public Subscription modifyPwd(String phone,String password,String member_type,String sms_code,Subscriber subscriber);

    /**
     * 上传用户头像
     * @param head_image
     * @param memberId
     * @param subscriber
     * @return
     */
    public Subscription updateUserHead(String head_image,String memberId,Subscriber subscriber);

    /**
     * 获取个人信息
     * @param memberId
     * @param subscriber
     * @return
     */
    public Subscription getUserInfo(String memberId,Subscriber subscriber);

    /**
     * 提交个人信息
     * @param member_id
     * @param name
     * @param birthday
     * @param sex
     * @param address
     * @param phone
     * @param subscriber
     * @return
     */
    public Subscription submitUserInfo(String member_id
            ,String name
            ,String birthday
            ,String sex
            ,String address
            ,String phone
            ,Subscriber subscriber);

    /**
     * 普通用户退出
     * @param member_id
     * @param subscriber
     * @return
     */
    public Subscription quit(String member_id,Subscriber subscriber);

    /**
     * 获取首页推荐医生列表
     * @param subscriber
     * @return
     */
    public Subscription getDoctor(Subscriber subscriber);

    /**
     * 患者提交反馈意见
     * @param member_id
     * @param content
     * @param subscriber
     * @return
     */
    public Subscription suggestion(String member_id,String content,Subscriber subscriber);

    /**
     * 修改密码
     * @param member_id
     * @param old_password
     * @param new_password
     * @param subscriber
     * @return
     */
    public Subscription change_pwd(String member_id,String old_password,String new_password,Subscriber subscriber);
    /**
     * 获取健康档案基本信息
     * @param patient_member_id
     * @param subscriber
     * @return
     */
    public Subscription getBaseInfo(String patient_member_id,Subscriber subscriber);

    /**
     * 健康档案： 获取健康信息
     * @param health_id
     * @param subscriber
     * @return
     */
    public Subscription getHealthyInfo(String health_id,Subscriber subscriber);

    /**
     * 健康档案：获取生活信息
     * @param health_id
     * @param subscriber
     * @return
     */
    public Subscription getLifeInfo(String health_id, Subscriber subscriber);

    /**
     * 健康档案：获取体检流水
     * @param health_id
     * @param subscriber
     * @return
     */
    public Subscription getCheckProgress(String health_id,Subscriber subscriber);

    /**
     * 健康档案：提交基本信息
     * @param member_id
     * @param msg
     * @param subscriber
     * @return
     */
    public Subscription submitBaseInfo(String member_id,String msg,Subscriber subscriber);

    /**
     * 健康档案：提交健康信息
     * @param health_id
     * @param msg
     * @param subscriber
     * @return
     */
    public Subscription submitHealthyInfo(String health_id,String msg,Subscriber subscriber);

    /**
     * 健康档案：提交生活信息
     * @param health_id
     * @param msg
     * @param subscriber
     * @return
     */
    public Subscription submitLifeinfo(String health_id,String msg,Subscriber subscriber);

    /**
     * 健康档案：提交体检流程图片
     * @param member_id 患者的会员注册主键ID
     * @param record_id 健康档案主键ID
     * @param health_img 体检流水的图片，上传base64加密后的字符串
     * @param subscriber
     * @return
     */
    public Subscription updateCheckImg(String member_id,String record_id,String health_img,Subscriber subscriber);
    /**
     * 健康档案：提交体检流程名称
     * @param member_id
     * @param health_id
     * @param subscriber
     * @return
     */
    public Subscription submitCheckName(String member_id,String health_id,String msg,String record_id, Subscriber subscriber );
    /**
     * 获取账户余额
     * @param member_id
     * @param subscriber
     * @return
     */
    public Subscription getaccount(String member_id,Subscriber subscriber);

    /**
     * 获取账单明细
     * @param member_id
     * @param subscriber
     * @return
     */
    public Subscription getaccountdetial(String member_id,String start,String length,Subscriber subscriber);

    /**
     * 充值
     * @param member_id
     * @param price
     * @param subscriber
     * @return
     */
    public Subscription send_money(String member_id,String price,Subscriber subscriber);

    /**
     * 支付宝支付
     * @param out_trade_no
     * @param business_type
     * @param subscriber
     * @return
     */
    public Subscription zhifubao(String out_trade_no,String business_type,Subscriber subscriber);

    /**
     * 银联支付
     * @param orderId
     * @param business_type
     * @param subscriber
     * @return
     */
    public Subscription yinlian(String orderId,String business_type,Subscriber subscriber);

    /**
     * 微信支付
     * @param out_trade_no
     * @param business_type
     * @param subscriber
     * @return
     */
    public Subscription weixin(String out_trade_no,String business_type,Subscriber subscriber);

    /**
     * 获取购药清单列表
     * @param member_id
     * @param subscriber
     * @return
     */
    public Subscription buymedicinelist(String member_id,Subscriber subscriber);

    /**
     * 删除购药清单
     * @param order_id
     * @param subscriber
     * @return
     */
    public Subscription delectmedicinelist(String order_id,Subscriber subscriber);

    /**
     * 获取订单详情
     * @param order_id
     * @param subscriber
     * @return
     */
    public Subscription orderdetial(String order_id,Subscriber subscriber);

    /**
     * 余额支付药品
     * @param order_id
     * @param member_id
     * @param subscriber
     * @return
     */
    public Subscription BanlancePay(String order_id,String member_id,Subscriber subscriber);

    /**
     * 药品支付的时候先提交地址
     * @param order_id
     * @param address
     * @param subscriber
     * @return
     */
    public Subscription commitAdd(String order_id,String  address,Subscriber subscriber);

    /**
     * 根据环信用户名获取用户信息
     * @param huanxin_username
     * @param subscriber
     * @return
     */
    public Subscription getUserInfoByEaseName(String huanxin_username,Subscriber subscriber);

    /**
     * 电话回拨申请
     * @param member_id
     * @param order_id
     * @param cont
     * @param subscriber
     * @return
     */
    public Subscription familycallcontent(String member_id,String order_id,String cont,Subscriber subscriber);

    /**
     * 紧急电话拨打
     * @param order_id
     * @param subscriber
     * @return
     */
    public Subscription familycallon(String order_id,Subscriber subscriber);

    /**
     * 结束家庭服务
     * @param family_id
     * @param subscriber
     * @return
     */
    public Subscription delectfamilylist(String family_id,Subscriber subscriber);


}
