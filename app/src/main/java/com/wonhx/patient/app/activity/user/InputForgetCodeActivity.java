package com.wonhx.patient.app.activity.user;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/25.
 * 忘记密码输入验证码页面
 */
public class InputForgetCodeActivity extends BaseActivity{
    @InjectView(R.id.et_yzm)
    EditText mCodeEdit;
    @InjectView(R.id.iv_clear)
    ImageView mClearCode;
    @InjectView(R.id.btn_reset)
    Button mRegisterBtn;
    @InjectView(R.id.txt_send_time)
    TextView mSendTime;
    @InjectView(R.id.registServerCase)
    TextView mRegistServer;
    UserManager userManager = new UserManagerImpl();
    TimeCount time;
    String mPhoneNum,mCode;
    @InjectView(R.id.rl)
    RelativeLayout rl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_register);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        mRegisterBtn.setText("重置密码");
        rl.setVisibility(View.GONE);
        //开始倒计时
        time = new TimeCount(60000, 1000);
        time.start();
        mCodeEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (!StrKit.isBlank(mCodeEdit.getText().toString().trim())) {
                    mClearCode.setVisibility(View.VISIBLE);
                    mRegisterBtn.setBackgroundResource(R.drawable.changeblue);
                    mRegisterBtn.setEnabled(true);
                } else {
                    mClearCode.setVisibility(View.GONE);
                    mRegisterBtn.setBackgroundResource(R.drawable.reset);
                    mRegisterBtn.setEnabled(false);
                }

            }
        });
    }

    /**
     *  初始化数据
     */
    @Override
    protected void onInitData() {
        super.onInitData();
        mPhoneNum = getIntent().getStringExtra("phoneNum");
        mCode = getIntent().getStringExtra("code");
    }

    @OnClick(R.id.btn_reset)
    void register(){
        if (StrKit.isBlank(mCodeEdit.getText().toString().trim())){
            Toaster.showShort(InputForgetCodeActivity.this, "请输入验证码！");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("phoneNum",mPhoneNum);
        bundle.putString("sms_code",mCodeEdit.getText().toString().trim());
        UIKit.open(InputForgetCodeActivity.this, EditPwdActivity.class, bundle);
        finish();
    }
    @OnClick(R.id.tv_back)
    void back(){
        finish();
    }
    @OnClick(R.id.iv_clear)
    void  clearCode(){
        mCodeEdit.setText("");
    }
    @OnClick(R.id.txt_send_time)
    void getCode(){
        userManager.getForgetCode(mPhoneNum, new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                //重新发送验证码
                time = new TimeCount(60000, 1000);
                time.start();
                Toaster.showShort(InputForgetCodeActivity.this, result.getMsg());
            }
        });
    }
    //倒计时
    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }
        @Override
        public void onTick(long millisUntilFinished) {
            mSendTime.setClickable(false);
            mSendTime.setText(millisUntilFinished / 1000 + "秒后再次发送验证码");
            mSendTime.setTextColor(getResources().getColor(R.color.text_line_one));
        }
        @Override
        public void onFinish() {
            mSendTime.setText("重新发送验证码");
            mSendTime.setClickable(true);
            mSendTime.setTextColor(getResources().getColor(R.color.main_text_select));
        }

    }
}
