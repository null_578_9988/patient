package com.wonhx.patient.app.activity.user;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hyphenate.easeui.EaseConstant;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.calldoctor.CallDoctorLoginActivity;
import com.wonhx.patient.app.activity.firstpage.DoctorDetialActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.ease.ConversationActivity;
import com.wonhx.patient.app.ease.Message;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.FamialyDoctors;
import com.wonhx.patient.app.model.ListProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.Resultt;
import com.wonhx.patient.db.ModelBuilder;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.DateKit;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.view.HJloginDialog;
import com.wonhx.patient.view.XListView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MyFamilyDoctorActivity extends BaseActivity implements XListView.IXListViewListener {
    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.lv)
    XListView lv;
    QuickAdapter<FamialyDoctors> adapter;
    int start = 0, numtotal=0;
    List<FamialyDoctors> list_all = new ArrayList<>();
    TextView tvTuwen,  tv_cancle,tv_commit, tvDianhua, tvJinji,tv_cishu;
    LinearLayout llTuwen,llDianhua, llJinji;
    ImageView ivJinji,ivDianhua,ivTuwen;
    Message dao = new Message();
    int type=1;//1 图文咨询 2 电话回拨 3 紧急呼叫
    FirstPagerMager firstPagerMager=new FirstPagerMangerImal();
    String doctorid="",orderid="",DOC_NAME="", timeall="",endtime="",member_id="",endTime="",serviceId="",sys_time="" ;
    UserManager userManager=new UserManagerImpl();
    SimpleDateFormat df;
    HJloginDialog dialog;
    Handler handler;
    Date date;
    String newentime="";
    @Override
    protected void onInitView() {
        super.onInitView();
        title.setText("我的家庭医生");
        lv.setPullLoadEnable(false);
        adapter = new QuickAdapter<FamialyDoctors>(this, R.layout.familydoctor_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, FamialyDoctors item) {
                String time[]=item.getEnd_time().split(" ");
                String timeend=time[0];
                helper.setText(R.id.doctor_name, item.getDoctor_name() != null && !item.getDoctor_name().equals("") ? item.getDoctor_name() : "")
                        .setText(R.id.doctor_dept, item.getDept_name() != null && !item.getDept_name().equals("") ? item.getDept_name() : "")
                        .setText(R.id.doctor_title, item.getTitle() != null && !item.getTitle().equals("") ? item.getTitle() : "")
                        .setText(R.id.doctor_hospitalName, item.getHospital_name() != null && !item.getHospital_name().equals("") ? item.getHospital_name() : "")
                        .setText(R.id.doctor_goodSubjects, item.getGood_subjects() != null && !item.getGood_subjects().equals("") ? item.getGood_subjects() : "")
                        .setText(R.id.price,"到期时间 "+(item.getEnd_time() != null && !item.getEnd_time().equals("") ?timeend : "") );
                SimpleDraweeView image = helper.getView(R.id.logo);
                image.setImageURI(Uri.parse(Constants.REST_ORGIN + "/emedicine/pub/member_logo/" + item.getMember_id() + "/" + item.getMember_id() + ".png"));
            }
        };
        lv.setAdapter(adapter);
        lv.setXListViewListener(this);
        handler = new Handler();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                timeall=list_all.get(i-1).getEnd_time();
                sys_time=list_all.get(i-1).getSys_time();
                endtime=list_all.get(i-1).getEnd_time();
                serviceId=list_all.get(i-1).getService_id();
                type=1;
                doctorid = list_all.get(i-1).getDoctor_id();
                member_id=list_all.get(i-1).getMember_id();
                orderid = list_all.get(i-1).getOrder_id();
                DOC_NAME = list_all.get(i-1).getDoctor_name();
                endTime=list_all.get(i-1).getEnd_time();
                numtotal = Integer.parseInt(list_all.get(i-1).getTimes());
                AbSharedUtil.putString(MyFamilyDoctorActivity.this,"serviceId",serviceId);
               firstPagerMager.getSystime(new SubscriberAdapter<Resultt>(){
                   @Override
                   public void success(Resultt result) {
                       super.success(result);
                       sys_time=result.getData();
                if (DateKit.compare_date(sys_time,endtime)<0) {
                    imageChooseItem(i);
                }else {
                    showDialog();
                }
                   }
               });


            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_family_doctor);
        ButterKnife.inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        list_all.clear();
        adapter.clear();
        start=0;
        getfamilydoctorlist();


    }

    @OnClick({ R.id.left_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_btn:
                finish();
                break;
        }
    }
    /**
     * 提示对话框
     */
    private void showDialog() {
        dialog = new HJloginDialog(this);
        dialog.setOnPositiveListener(new View.OnClickListener() {
            //续约
            @Override
            public void onClick(View v) {
                Intent in=new Intent(MyFamilyDoctorActivity.this, DoctorDetialActivity.class);
                in.putExtra("member_id",member_id);
                startActivity(in);
                dialog.dismiss();
                finish();
            }
        });
        dialog.setOnCancelListener(new View.OnClickListener() {
            //忽略
            @Override
            public void onClick(View v) {
                userManager.delectfamilylist(serviceId,new SubscriberAdapter<Result>(){
                    @Override
                    public void success(Result result) {
                        super.success(result);
                        list_all.clear();
                        adapter.clear();
                        start=0;
                        getfamilydoctorlist();
                        dialog.dismiss();
                    }
                });
            }
        });
        dialog.setTitle("温馨提示");
        dialog.setContentMessage("医生服务时间已到期！");
        dialog.setCancelMessage("删除");
        dialog.setCancelColor(getResources().getColor(R.color.colorAccent));
        dialog.setPositiveMessage("继续购买");
        dialog.setPositiveColor(this.getResources().getColor(R.color.colorAccent));
        dialog.show();
    }
    /**
     * 服务选择
     */
    private void imageChooseItem(final int i) {
        View view = LayoutInflater.from(this).inflate(R.layout.fuwuxuanze, null);
        final Dialog dialog = new AlertDialog.Builder(this ,R.style.dialog).create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.mydialogstyle);   //添加动画
        window.setBackgroundDrawable(new ColorDrawable());
        dialog.show();
        window.setContentView(view);
        llTuwen= (LinearLayout) view.findViewById(R.id.ll_tuwen);
        llDianhua= (LinearLayout) view.findViewById(R.id.ll_dianhua);
        llJinji= (LinearLayout) view.findViewById(R.id.ll_jinji);
        tvTuwen= (TextView) view.findViewById(R.id.tv_tuwen);
        tvDianhua= (TextView) view.findViewById(R.id.tv_dianhua);
        tvJinji= (TextView) view.findViewById(R.id.tv_jinji);
        ivTuwen= (ImageView) view.findViewById(R.id.iv_tuwen);
        ivDianhua= (ImageView) view.findViewById(R.id.iv_dianhua);
        ivJinji= (ImageView) view.findViewById(R.id.iv_jinji);
        tv_cancle= (TextView) view.findViewById(R.id.cancle);
        tv_commit= (TextView) view.findViewById(R.id.commit);
        tv_cishu= (TextView) view.findViewById(R.id.tv_cishu);
        if (numtotal==0) {
            tv_cishu.setText("剩余" + 2 + "次");
        }else if (numtotal==1){
            tv_cishu.setText("剩余" + 1+ "次");
        }else {
            tv_cishu.setText("剩余" + 0+ "次");
        }
        tv_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tv_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                //图文咨询
                if (type==1){
                    type=1;
                    firstPagerMager.getDoctorEMCName(member_id,new SubscriberAdapter<Result>(){
                        @Override
                        public void success(Result result) {
                            super.success(result);
                            Bundle args = new Bundle();
                            args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                            args.putString(EaseConstant.EXTRA_USER_ID, result.getHuanxin_username());
                            args.putString("homeOrderId", serviceId);
                            args.putString("DOC_NAME", DOC_NAME);
                            args.putString("endTime", endTime);
                            UIKit.open(MyFamilyDoctorActivity.this, ConversationActivity.class, args);
                        }
                    });
                    //电话回拨
                }else if (type==2){
                    type=1;
                    Intent intent=new Intent(MyFamilyDoctorActivity.this,FamilyCallActivity.class);
                    intent.putExtra("orderid",orderid);
                    intent.putExtra("member_id",member_id);
                    startActivity(intent);
                }else  if (type==3){
                    type=1;
                    if (numtotal>=2){
                        Toaster.showShort(MyFamilyDoctorActivity.this,"此次紧急呼叫次数已经用完！");
                    }else {
                        userManager.familycallon(orderid,new SubscriberAdapter<Result>(){
                            @Override
                            public void success(Result result) {
                                super.success(result);
                                Toaster.showShort(MyFamilyDoctorActivity.this,result.getMsg());
                                list_all.clear();
                                adapter.clear();
                                start=0;
                                getfamilydoctorlist();
                            }
                        });
                    }
                }
            }
        });
        llTuwen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type=1;
                llTuwen .setBackgroundResource(R.mipmap.lansemy);
                ivTuwen.setImageResource(R.mipmap.tuwen);
                tvTuwen.setTextColor(getResources().getColor(R.color.colorAccent));
                llDianhua.setBackgroundResource(R.mipmap.huisemy);
                ivDianhua.setImageResource(R.mipmap.dianhua);
                tvDianhua.setTextColor(getResources().getColor(R.color.text_color_gey));
                llJinji.setBackgroundResource(R.mipmap.huisemy);
                ivJinji.setImageResource(R.mipmap.jinji);
                tvJinji.setTextColor(getResources().getColor(R.color.text_color_gey));
            }
        });
        llDianhua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type=2;
                llDianhua .setBackgroundResource(R.mipmap.lansemy);
                ivDianhua.setImageResource(R.mipmap.dianhuaon);
                tvDianhua.setTextColor(getResources().getColor(R.color.colorAccent));
                llJinji.setBackgroundResource(R.mipmap.huisemy);
                ivJinji.setImageResource(R.mipmap.jinji);
                tvJinji.setTextColor(getResources().getColor(R.color.text_color_gey));
                llTuwen.setBackgroundResource(R.mipmap.huisemy);
                ivTuwen.setImageResource(R.mipmap.tuwenoff);
                tvTuwen.setTextColor(getResources().getColor(R.color.text_color_gey));
            }
        });
        llJinji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type=3;
                llJinji .setBackgroundResource(R.mipmap.lansemy);
                ivJinji.setImageResource(R.mipmap.jinjion);
                tvJinji.setTextColor(getResources().getColor(R.color.colorAccent));
                llDianhua.setBackgroundResource(R.mipmap.huisemy);
                ivDianhua.setImageResource(R.mipmap.dianhua);
                tvDianhua.setTextColor(getResources().getColor(R.color.text_color_gey));
                llTuwen.setBackgroundResource(R.mipmap.huisemy);
                ivTuwen.setImageResource(R.mipmap.tuwenoff);
                tvTuwen.setTextColor(getResources().getColor(R.color.text_color_gey));
            }
        });
    }



    private void getfamilydoctorlist() {
        firstPagerMager.getmyfamilylist(AbSharedUtil.getString(this, "userId"), start + "", "10", new SubscriberAdapter<ListProResult<FamialyDoctors>>() {
            @Override
            public void onError(Throwable e) {
               // super.onError(e);
                dismissLoadingDialog();
            }
            @Override
            public void success(ListProResult<FamialyDoctors> famialyDoctorsListProResult) {
                super.success(famialyDoctorsListProResult);
                    list_all.addAll(famialyDoctorsListProResult.getData());
                    if (list_all.size() > 0) {
                        adapter.replaceAll(list_all);
                        if (list_all.size()>=10){
                            lv.setPullLoadEnable(true);
                        }else {
                            lv.setPullLoadEnable(false);
                        }
                    }


            }
        });
    }
    private void onLoad() {
        lv.stopRefresh();
        lv.stopLoadMore();
        lv.setRefreshTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }
    @Override
    public void onRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start = 0;
                list_all.clear();
                getfamilydoctorlist();
                onLoad();
            }
        }, 1000);

    }

    @Override
    public void onLoadMore() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                start=start+10;
                getfamilydoctorlist();
                onLoad();
            }
        }, 1000);
    }
}
