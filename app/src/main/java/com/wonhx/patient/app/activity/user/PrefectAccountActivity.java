package com.wonhx.patient.app.activity.user;

import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.LoginResult;
import com.wonhx.patient.app.model.UserInfo;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.StrKit;
import com.wonhx.patient.kit.Toaster;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 17/3/24.
 * 完善账户页
 */
public class PrefectAccountActivity extends BaseActivity {
    @InjectView(R.id.et_pwd)
    EditText mPwd;
    @InjectView(R.id.iv_eye)
    ImageView mImgEye;
    boolean flag = true;
    String mCode, mPhoneNum;
    UserManager userManager = new UserManagerImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prefect_account);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        mCode = getIntent().getStringExtra("sms_code");
        mPhoneNum = getIntent().getStringExtra("phoneNum");
    }

    @OnClick(R.id.btn_sure)
    void submit() {
        if (StrKit.isBlank(mPwd.getText().toString().trim())) {
            Toaster.showShort(PrefectAccountActivity.this, "请输入密码！");
            return;
        }
        String member_type = "1";
        userManager.register(mPhoneNum, mPwd.getText().toString().trim(), member_type, mCode, "2", AbSharedUtil.getString(PrefectAccountActivity.this,"device_token"), new SubscriberAdapter<LoginResult>() {
            @Override
            public void success(LoginResult loginResult) {
                super.success(loginResult);
                AbSharedUtil.putString(PrefectAccountActivity.this,"helthId",null);
                Toaster.showShort(PrefectAccountActivity.this, loginResult.getMsg());
                finish();
                AbSharedUtil.putString(PrefectAccountActivity.this, "userId", loginResult.getMember_id());
                //初始化数据库
                appContext.setIsCallDoctor(false);
                appContext.login();
                UserInfo userDao = new UserInfo();
                userDao.setMember_id(loginResult.getMember_id());
                userDao.setUsername(loginResult.getPatient_name());
                userDao.setPhone(loginResult.getPhone());
                userDao.setHuanxin_username(loginResult.getHuanxin_username());
                userDao.setHuanxin_pw(loginResult.getHuanxin_pw());
                userDao.setAvater(loginResult.getLogo_img_path());
                userDao.setUsername(loginResult.getName());
                userDao.setUsername(loginResult.getPatient_name());
                userDao.saveOrUpdate();
                signIn(loginResult.getHuanxin_username(), loginResult.getHuanxin_pw());
            }
        });
    }
    @OnClick(R.id.tv_back)
    void back(){
        finish();
    }

    @OnClick(R.id.iv_eye)
    void seePwd() {
        if (flag) {
            mPwd.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            mImgEye.setImageResource(R.mipmap.ellipse);
            flag = false;
        } else {
            mPwd.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            mImgEye.setImageResource(R.mipmap.eyes);
            flag = true;
        }
    }
    /**
     * 登录方法
     */
    private void signIn(String username, String password) {
        EMClient.getInstance().login(username, password, new EMCallBack() {
            /**
             * 登陆成功的回调
             */
            @Override
            public void onSuccess() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mDialog.dismiss();
                        // 加载所有会话到内存
                        EMClient.getInstance().chatManager().loadAllConversations();
                        // 加载所有群组到内存，如果使用了群组的话
                        // EMClient.getInstance().groupManager().loadAllGroups();
                        Toaster.showShort(PrefectAccountActivity.this, "会员注册成功！");
                        finish();
                    }
                });
            }

            /**
             * 登陆错误的回调
             * @param i
             * @param s
             */
            @Override
            public void onError(final int i, final String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mDialog.dismiss();
                        Log.d("lzan13", "登录失败 Error code:" + i + ", message:" + s);
                        /**
                         * 关于错误码可以参考官方api详细说明
                         * http://www.easemob.com/apidoc/android/chat3.0/classcom_1_1hyphenate_1_1_e_m_error.html
                         */
                        switch (i) {
                            // 网络异常 2
                            case EMError.NETWORK_ERROR:
                                Toast.makeText(PrefectAccountActivity.this, "网络错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无效的用户名 101
                            case EMError.INVALID_USER_NAME:
                                Toast.makeText(PrefectAccountActivity.this, "无效的用户名 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无效的密码 102
                            case EMError.INVALID_PASSWORD:
                                Toast.makeText(PrefectAccountActivity.this, "无效的密码 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 用户认证失败，用户名或密码错误 202
                            case EMError.USER_AUTHENTICATION_FAILED:
                                Toast.makeText(PrefectAccountActivity.this, "用户认证失败，用户名或密码错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 用户不存在 204
                            case EMError.USER_NOT_FOUND:
                                Toast.makeText(PrefectAccountActivity.this, "用户不存在 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无法访问到服务器 300
                            case EMError.SERVER_NOT_REACHABLE:
                                Toast.makeText(PrefectAccountActivity.this, "无法访问到服务器 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 等待服务器响应超时 301
                            case EMError.SERVER_TIMEOUT:
                                Toast.makeText(PrefectAccountActivity.this, "等待服务器响应超时 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 服务器繁忙 302
                            case EMError.SERVER_BUSY:
                                Toast.makeText(PrefectAccountActivity.this, "服务器繁忙 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 未知 Server 异常 303 一般断网会出现这个错误
                            case EMError.SERVER_UNKNOWN_ERROR:
                                Toast.makeText(PrefectAccountActivity.this, "未知的服务器异常 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(PrefectAccountActivity.this, "ml_sign_in_failed code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

            @Override
            public void onProgress(int i, String s) {

            }
        });
    }
}
