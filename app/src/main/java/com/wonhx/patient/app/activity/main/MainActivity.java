package com.wonhx.patient.app.activity.main;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.PushService;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.hyphenate.EMCallBack;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.exceptions.HyphenateException;
import com.wonhx.patient.R;
import com.wonhx.patient.app.adapter.MainPagerAdapter;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.ease.ConversationActivity;
import com.wonhx.patient.app.ease.EaseHelper;
import com.wonhx.patient.app.ease.VideoCallActivity;
import com.wonhx.patient.app.model.MainTabEntity;
import com.wonhx.patient.app.model.UserInfo;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.update.lib.UpdateBuilder;
import com.wonhx.patient.view.LoginDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;

public class MainActivity extends BaseActivity implements ViewPager.OnPageChangeListener,OnTabSelectListener{
    FragmentManager mFragmentManager;
    MainPagerAdapter mainPagerAdapter;
    @InjectView(R.id.pager)
    ViewPager mViewPager;
    @InjectView(R.id.mBottomTabView)
    CommonTabLayout mBottomTabView;
    private List<Drawable> tabDrawables = null;
    private String[] mMenus = {"首页", "服务记录","我的医生","个人中心"};
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private int[] mIconUnselectIds = {
            R.mipmap.homes, R.mipmap.notes,
            R.mipmap.friendss, R.mipmap.maleusers};
    private int[] mIconSelectIds = {
            R.mipmap.homesa, R.mipmap.notesa,
            R.mipmap.friendssa, R.mipmap.maleusersa};
    private boolean mOtherLogin = false;
    TextView textView;
    ViewGroup view;
    String mMemberId;
    UserInfo userDao = new UserInfo();
    EMMessage mMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (android.os.Build.VERSION.SDK_INT > 18) {
//            Window window = getWindow();
//            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        }
        setContentView(R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();
        // 创建TextView
        textView = new TextView(this);
        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, UIKit.getStatusBarHeight(this));
        textView.setBackgroundColor(Color.parseColor("#ffffff"));
        textView.setLayoutParams(lParams);
        // 获得根视图并把TextView加进去。
        view = (ViewGroup) getWindow().getDecorView();
        view.addView(textView);
        initView();
        //推送配置
        PushService.setDefaultPushCallback(this, MainActivity.class);
        //更新
        UpdateBuilder.create().check(MainActivity.this);//使用默认配置进行更新
    }
    @Override
    protected void onStart() {
        super.onStart();
        appContext.setActivity(this);
        List<Fragment> fragments = mFragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.setUserVisibleHint(true);
            }
        }
        mMemberId = AbSharedUtil.getString(MainActivity.this,"userId");
        if (mMemberId!=null){
            if (!EMClient.getInstance().isConnected()){
                userDao = userDao.findById(Integer.parseInt(mMemberId));
                signIn(userDao.getHuanxin_username(),userDao.getHuanxin_pw());
            }
        }
    }

    protected void initView() {
        super.onInitView();
        //菜单实体
        for (int i = 0; i < mMenus.length; i++) {
            mTabEntities.add(new MainTabEntity(mMenus[i], mIconSelectIds[i], mIconUnselectIds[i]));
        }
        mainPagerAdapter = new MainPagerAdapter(this, mFragmentManager);
        mViewPager.setAdapter(mainPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mBottomTabView.setTabData(mTabEntities);
        mBottomTabView.setCurrentTab(0);
        mViewPager.addOnPageChangeListener(this);
        mBottomTabView.setOnTabSelectListener(this);

    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mOtherLogin = intent.getBooleanExtra("otherLogin",false);
        if (mOtherLogin){
            mBottomTabView.setCurrentTab(0);
            mViewPager.setCurrentItem(0);
            Intent intent1 = new Intent();
            intent1.putExtra("key", "OTHER_LOGIN");
            intent1.setAction("REFRESH");
            sendBroadcast(intent1);
            //添加其他设备登录提示框，清空用户信Toaster.showShort(MainActivity.this,"你的帐号在其他设备登录！");
            showOtherLoginDialog();
        }
    }

    /**
     * viewpager监听
     * @param position
     * @param positionOffset
     * @param positionOffsetPixels
     */
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mBottomTabView.setCurrentTab(position);
        switch (position){
            case 0:
                textView.setBackgroundColor(Color.parseColor("#ffffff"));
                break;
            case 1:
                textView.setBackgroundColor(Color.parseColor("#ffffff"));
                break;
            case 2:
                textView.setBackgroundColor(Color.parseColor("#ffffff"));
                break;
            case 3:
                textView.setBackgroundColor(Color.parseColor("#5b90ff"));
                break;

        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * menu变化监听
     * @param position
     */
    @Override
    public void onTabSelect(int position) {
        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onTabReselect(int position) {

    }


    /**
     * 登录方法
     */
    private void signIn(String username, String password) {
        EMClient.getInstance().login(username, password, new EMCallBack() {
            /**
             * 登陆成功的回调
             */
            @Override
            public void onSuccess() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mDialog.dismiss();
                        // 加载所有会话到内存
                        EMClient.getInstance().chatManager().loadAllConversations();
                        mMessage = getIntent().getParcelableExtra("message");
                        if (mMessage!=null){
                            goToConversation(mMessage);
                        }
                        // 加载所有群组到内存，如果使用了群组的话
                        // EMClient.getInstance().groupManager().loadAllGroups();
                    }
                });
            }

            /**
             * 登陆错误的回调
             * @param i
             * @param s
             */
            @Override
            public void onError(final int i, final String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mDialog.dismiss();
                        Log.d("lzan13", "登录失败 Error code:" + i + ", message:" + s);
                        /**
                         * 关于错误码可以参考官方api详细说明
                         * http://www.easemob.com/apidoc/android/chat3.0/classcom_1_1hyphenate_1_1_e_m_error.html
                         */
                        switch (i) {
                            // 网络异常 2
                            case EMError.NETWORK_ERROR:
                                Toast.makeText(MainActivity.this, "网络错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无效的用户名 101
                            case EMError.INVALID_USER_NAME:
                                Toast.makeText(MainActivity.this, "无效的用户名 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无效的密码 102
                            case EMError.INVALID_PASSWORD:
                                Toast.makeText(MainActivity.this, "无效的密码 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 用户认证失败，用户名或密码错误 202
                            case EMError.USER_AUTHENTICATION_FAILED:
                                Toast.makeText(MainActivity.this, "用户认证失败，用户名或密码错误 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 用户不存在 204
                            case EMError.USER_NOT_FOUND:
                                Toast.makeText(MainActivity.this, "用户不存在 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 无法访问到服务器 300
                            case EMError.SERVER_NOT_REACHABLE:
                                Toast.makeText(MainActivity.this, "无法访问到服务器 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 等待服务器响应超时 301
                            case EMError.SERVER_TIMEOUT:
                                Toast.makeText(MainActivity.this, "等待服务器响应超时 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 服务器繁忙 302
                            case EMError.SERVER_BUSY:
                                Toast.makeText(MainActivity.this, "服务器繁忙 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            // 未知 Server 异常 303 一般断网会出现这个错误
                            case EMError.SERVER_UNKNOWN_ERROR:
                                Toast.makeText(MainActivity.this, "未知的服务器异常 code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(MainActivity.this, "ml_sign_in_failed code: " + i + ", message:" + s, Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

            @Override
            public void onProgress(int i, String s) {
            }
        });
    }
    /**
     * 删除图片提示框
     */
    private void showOtherLoginDialog() {
        final LoginDialog mDeleteDialog = new LoginDialog(MainActivity.this);
        mDeleteDialog.setContent("您的账号在其他设备登录!请重新登录！");
        mDeleteDialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteDialog.dismiss();
            }
        });
        mDeleteDialog.show();
    }

    /**
     * 点击通知栏后跳转到会话或者视频页面
     */
    private void goToConversation(EMMessage message){
       Intent intent = new Intent(appContext, ConversationActivity.class);
        if(EaseHelper.getInstance().isVideoCalling){
            intent = new Intent(appContext, VideoCallActivity.class);
            intent.putExtra("username", message.getFrom());
            intent.putExtra("isComingCall", true);
            startActivity(intent);
        }else{
            JSONObject json = null;
            String conslutionId = null;
            String nickName = null;
            String memberid = null;
            String clinicId = null;
            String homeOrderId  = null;
            String endTime  = null;
            try {
                json = message.getJSONObjectAttribute("em_apns_ext");
            } catch (HyphenateException e) {
                e.printStackTrace();
            }
            try {
                try {
                    conslutionId = message.getStringAttribute("conslutionId");
                    memberid = json.getString("memberId");
                    nickName = json.getString("name");
                } catch (HyphenateException e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                memberid = json.getString("memberId");
                nickName = json.getString("name");
                clinicId = json.getString("clinicId");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                memberid = json.getString("memberId");
                nickName = json.getString("name");
                homeOrderId  = json.getString("homeOrderId");
                endTime  = json.getString("endTime");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (clinicId!=null){
                Bundle args = new Bundle();
                args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                args.putString(EaseConstant.EXTRA_USER_ID, message.getFrom());
                args.putString("DOC_NAME", nickName);
                args.putString("CLINIC_ID",clinicId);
                intent.putExtras(args);
                startActivity(intent);
            }else if (homeOrderId!=null){
                Bundle args = new Bundle();
                args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                args.putString(EaseConstant.EXTRA_USER_ID, message.getFrom());
                args.putString("DOC_NAME", nickName);
                args.putString("homeOrderId",homeOrderId);
                intent.putExtras(args);
                startActivity(intent);
            }else if(conslutionId!= null &&!conslutionId.equals("")&&!conslutionId.equals("0")) {
                Bundle args = new Bundle();
                args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
                args.putString(EaseConstant.EXTRA_USER_ID, message.getFrom());
                args.putString("DT_RowId", conslutionId);
                args.putString("DOC_NAME", nickName);
                intent.putExtras(args);
                startActivity(intent);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(false);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

//    /**
//     * 返回退出
//     */
//    @Override
//    public void onBackPressed() {
//        UIKit.appExit(this);
//    }
}
