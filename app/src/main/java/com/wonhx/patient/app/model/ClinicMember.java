package com.wonhx.patient.app.model;

/**
 * Created by nsd on 2017/5/19.
 *
 */

public class ClinicMember {
    String member_id;
    String doc_name;
    String title;
    String medicine_logo_img;
    String hospital_name;
    String dept_name;
    String id;
    String admissions;
    String doctor_id;
    String good_subjects;
    String rota_time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdmissions() {
        return admissions;
    }

    public void setAdmissions(String admissions) {
        this.admissions = admissions;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getGood_subjects() {
        return good_subjects;
    }

    public void setGood_subjects(String good_subjects) {
        this.good_subjects = good_subjects;
    }

    public String getRota_time() {
        return rota_time;
    }

    public void setRota_time(String rota_time) {
        this.rota_time = rota_time;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMedicine_logo_img() {
        return medicine_logo_img;
    }

    public void setMedicine_logo_img(String medicine_logo_img) {
        this.medicine_logo_img = medicine_logo_img;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }
}
