package com.wonhx.patient.app.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.wonhx.patient.R;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.model.PushMessage;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MessageListActivity extends BaseActivity {

    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.lv)
    ListView lv;
    @InjectView(R.id.rl_no)
    RelativeLayout rl_no;
    QuickAdapter<PushMessage>adapter;
    List<PushMessage>list=new ArrayList<>();
    PushMessage pushMessage=new PushMessage();
    @Override
    protected void onStart() {
        super.onStart();
        title.setText("消息列表");
        list.clear();
        list= pushMessage.findAll();
        adapter=new QuickAdapter<PushMessage>(MessageListActivity.this,R.layout.messagelistitem) {
            @Override
            protected void convert(BaseAdapterHelper helper, PushMessage item) {
           helper.setText(R.id.title,item.getTitle())
        .setText(R.id.time,item.getTimestamp())
        .setText(R.id.alert,item.getAlert());
                TextView tv_num=helper.getView(R.id.tv_num);
                if (item.getIsread().equals("1")){
                    tv_num.setVisibility(View.VISIBLE);
                }else {
                    tv_num.setVisibility(View.GONE);
                }
            }

        };
        lv.setAdapter(adapter);
        if (list.size()>0){
            lv.setVisibility(View.VISIBLE);
            rl_no.setVisibility(View.GONE);
            adapter.addAll(list);
        }else {
            lv.setVisibility(View.GONE);
            rl_no.setVisibility(View.VISIBLE);
        }
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (list.get(i).getIsread().equals("1")){
                    list.get(i).setIsread("0");
                    list.get(i).saveOrUpdate();
                }
                Intent intent=new Intent(MessageListActivity.this,MessageDetialActivity.class);
                intent.putExtra("title",list.get(i).getTitle());
                intent.putExtra("alert",list.get(i).getAlert());
                intent.putExtra("time",list.get(i).getTimestamp());
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        ButterKnife.inject(this);

    }

    @OnClick(R.id.left_btn)
    public void onViewClicked() {
        finish();
    }
}
