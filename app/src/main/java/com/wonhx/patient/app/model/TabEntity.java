package com.wonhx.patient.app.model;

import com.flyco.tablayout.listener.CustomTabEntity;

/**
 * Created by apple on 17/3/23.
 * 主页面menu菜单实体
 */
public class TabEntity implements CustomTabEntity {
    public String title;
    public TabEntity(String title) {
        this.title = title;
    }

    @Override
    public String getTabTitle() {
        return title;
    }

    @Override
    public int getTabSelectedIcon() {
        return 0;
    }

    @Override
    public int getTabUnselectedIcon() {
        return 0;
    }

}