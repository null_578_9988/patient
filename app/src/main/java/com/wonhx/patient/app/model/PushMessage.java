package com.wonhx.patient.app.model;


import com.wonhx.patient.db.annotation.Table;

/**
 * Created by Administrator on 2017/5/5.
 *
 */
@Table(version = 1)
public class PushMessage extends BaseModel<PushMessage>{
    public String alert;
    public String title;
    public String timestamp;
    public String messageType;
    public String isread;


    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getIsread() {
        return isread;
    }

    public void setIsread(String isread) {
        this.isread = isread;
    }
}
