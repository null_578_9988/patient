package com.wonhx.patient.app.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Window;
import android.view.WindowManager;

import com.orhanobut.logger.Logger;
import com.wonhx.patient.app.AppContext;
import com.wonhx.patient.app.AppException;
import com.wonhx.patient.app.AppManager;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.view.CustomProgressDialog;

import butterknife.ButterKnife;
import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

/**
 * 基础Activity
 */
public abstract class BaseActivity extends FragmentActivity {
    /**
     * 全局Context
     */
    protected AppContext appContext;
    /**
     * 传入的参数
     */
    protected Bundle mExtras;
    /**
     * 碎片管理器
     */
    protected FragmentManager mFragmentManager;
    private CompositeSubscription mSubscriptions;
    private CustomProgressDialog progressDialog;
    private boolean isShowDialog = true;
    private boolean isDismessDialog = true;

    public void setDismessDialog(boolean dismessDialog) {
        isDismessDialog = dismessDialog;
    }

    public void setShowDialog(boolean showDialog) {

        isShowDialog = showDialog;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 将activity添加到堆栈
        AppManager.me().addActivity(this);
        // 获取全局Context
        appContext = (AppContext) getApplication();
        mFragmentManager = getSupportFragmentManager();
        mSubscriptions = new CompositeSubscription();
        mExtras = getIntent().getExtras();
        if (mExtras == null)
            mExtras = new Bundle(0);
        onInitArgs(); // onInitArgs
        
    }
    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        // InjectViews
        ButterKnife.inject(this);
        onInitView();       // onInitView
        onInitData();       // onInitData
    }

    /**
     * InitArgs
     */
    protected void onInitArgs() {
    }

    /**
     * InitView
     */
    protected void onInitView() {
    }

    /**
     * InitData
     */
    protected void onInitData() {
    }

    /**
     * RxSubscriberAdapter
     *
     * @param <T>
     */
    protected class SubscriberAdapter<T> extends Subscriber<T> {
        public SubscriberAdapter() {
            if (mSubscriptions == null)
                mSubscriptions = new CompositeSubscription();
            mSubscriptions.add(this);
        }

        public void success(T t) {

        }

        @Override
        public void onNext(T t) {
            if (isDismessDialog){
                dismissLoadingDialog();
            }
            dismissLoadingDialog();
            success(t);
        }

        @Override
        public void onStart() {
            if (isShowDialog) {
                showLoadingDialog();
            }
             showLoadingDialog();
        }

        @Override
        public void onError(Throwable e) {
             dismissLoadingDialog();
            Logger.e((Exception) e);
            if (e instanceof AppException) {
                AppException ex = (AppException) e;
                if (ex.getType() == AppException.TYPE_CUSTOM)
                    //showAlertDialog("提示", e.getMessage());
                    Toaster.showShort(BaseActivity.this, e.getMessage());
                else
                    ex.makeToast(BaseActivity.this);
                return;
            }
        }

        @Override
        public void onCompleted() {
        }
    }

    public void showLoadingDialog() {
        if (progressDialog == null){
            progressDialog = CustomProgressDialog.createDialog(this);
            //progressDialog.setMessage("数据加载中");
        }
        progressDialog.show();
    }


    public void dismissLoadingDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (mSubscriptions != null)
            mSubscriptions.unsubscribe();
        super.onDestroy();
        // 将activity从堆栈中移除
        AppManager.me().finishActivity(this);
    }
}
