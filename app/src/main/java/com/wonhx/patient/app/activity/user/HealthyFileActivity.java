package com.wonhx.patient.app.activity.user;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.TextView;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.wonhx.patient.R;
import com.wonhx.patient.app.adapter.HealthyFilePagerAdapter;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.model.TabEntity;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.NoScrollViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 2017/4/7.
 * 健康档案
 */

public class HealthyFileActivity extends BaseActivity implements OnTabSelectListener {

    FragmentManager mFragmentManager;
    HealthyFilePagerAdapter mPagerAdapter;
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.pager)
    NoScrollViewPager mViewPager;
    @InjectView(R.id.menutab)
    CommonTabLayout mMenuTab;
    MyReceiver myReceiver;
    String[] mTitles = {"基本信息", "健康信息", "生活信息", "体检流程"};
    ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_healthy_file);
        mFragmentManager = getSupportFragmentManager();
        initView();
    }
    @Override
    protected void onStart() {
        super.onStart();
        List<Fragment> fragments = mFragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.setUserVisibleHint(true);
            }
        }
    }

    /**
     * 初始化视图
     */
    protected void initView() {
        mTitle.setText("健康档案");
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i]));
        }
        mMenuTab.setTabData(mTabEntities);
        mMenuTab.setCurrentTab(0);
        mMenuTab.setOnTabSelectListener(this);
        mPagerAdapter = new HealthyFilePagerAdapter(this, mFragmentManager);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);

    }

    @OnClick(R.id.left_btn)
    void back() {
        finish();
    }



    /**
     * TabMenu监听
     *
     * @param position
     */
    @Override
    public void onTabSelect(int position) {
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("FLAG");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String helthId) {
                if (helthId!=null){
                    AbSharedUtil.putString(HealthyFileActivity.this,"helthId",helthId);
                }
            }
        });
        switch (position) {
            case 0:
                mViewPager.setCurrentItem(0);
                break;
            case 1:
                if (AbSharedUtil.getString(HealthyFileActivity.this,"helthId")!=null&&!AbSharedUtil.getString(HealthyFileActivity.this,"helthId").equals("")) {
                    mViewPager.setCurrentItem(1);
                }else {
                    mMenuTab.setCurrentTab(0);
                    mViewPager.setCurrentItem(0);
                    Toaster.showShort(HealthyFileActivity.this,"请先填写基本信息");
                }
                break;
            case 2:
                if (AbSharedUtil.getString(HealthyFileActivity.this,"helthId")!=null&&!AbSharedUtil.getString(HealthyFileActivity.this,"helthId").equals("")) {
                    mViewPager.setCurrentItem(2);
                }else {
                    mMenuTab.setCurrentTab(0);
                    mViewPager.setCurrentItem(0);
                    Toaster.showShort(HealthyFileActivity.this,"请先填写基本信息");
                }

                break;
            case 3:
                if (AbSharedUtil.getString(HealthyFileActivity.this,"helthId")!=null&&!AbSharedUtil.getString(HealthyFileActivity.this,"helthId").equals("")) {
                    mViewPager.setCurrentItem(3);
                }else {
                    mMenuTab.setCurrentTab(0);
                    mViewPager.setCurrentItem(0);
                    Toaster.showShort(HealthyFileActivity.this,"请先填写基本信息");
                }
                break;
        }
    }

    @Override
    public void onTabReselect(int position) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            unregisterReceiver(myReceiver);
        }
    }
}
