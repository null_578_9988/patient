package com.wonhx.patient.app.manager;

import rx.Subscriber;
import rx.Subscription;

/**
 * Created by apple on 17/3/30.
 * 呼叫医生模块接口
 */
public interface CallDoctorManager {
    /**
     * 登录
     * @param username
     * @param password
     * @param subscriber
     * @return
     */
    public Subscription login(String username,String password,String device_type,String device_token, Subscriber subscriber);

    /**
     * 退出
     * @param user_id
     * @param subscriber
     * @return
     */
    public Subscription quit(String user_id, Subscriber subscriber);
    /**
     * 获取科室列表
     * @return
     */
    public Subscription obtaindoctorlist(Subscriber subscribers);

    /**
     * 提交电话服务
     * @param dept_id
     * @param phone
     * @param user_id
     * @return
     */
    public  Subscription phoneserver(String dept_id,String phone,String user_id,Subscriber subscriber);

    /**
     * 视频服务
     * @param user_id
     * @param dept_id
     * @param user_name
     * @param subscriber
     * @return
     */
    public  Subscription videoserver(String user_id,String dept_id,String user_name,Subscriber subscriber);

    /**
     * 获取服务记录列表数据
     * @param user_id
     * @return
     */
    public  Subscription getRecords(String user_id , Subscriber subscriber);

    /**
     * 获取服务记录详情
     * @param service_id
     * @param subscriber
     * @return
     */
    public Subscription getRecordInfo(String service_id , Subscriber subscriber);

    /**
     * 评价服务
     * @param service_id
     * @param scroe
     * @param content
     * @param subscriber
     * @return
     */
    public Subscription evaluateRecord(String service_id ,String scroe,String content,Subscriber subscriber);

    /**
     * 重新拨打电话
     * @param dept_id
     * @param order_id
     * @param phone
     * @param subscriber
     * @return
     */
    public Subscription reCallPhone(String dept_id, String order_id,String phone,Subscriber subscriber);

    /**
     * 重新拨打视频
     * @param dept_id
     * @param service_id
     * @param order_id
     * @param subscriber
     * @return
     */
    public Subscription reCallVideo(String dept_id, String service_id,String order_id,Subscriber subscriber);

    /**
     * 忘记密码获取手机验证码
     * @param username
     * @param subscriber
     * @return
     */
    public Subscription getPhoneCode(String username , Subscriber subscriber);

    /**
     * 修改密码
     * @param username
     * @param sms_code
     * @param password
     * @param subscriber
     * @return
     */
    public Subscription newPwd(String username,String sms_code,String password,String phone,Subscriber subscriber);


}
