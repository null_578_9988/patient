package com.wonhx.patient.app.activity.firstpage;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.wonhx.patient.R;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.activity.user.LoginActivity;
import com.wonhx.patient.app.base.BaseActivity;
import com.wonhx.patient.app.manager.FirstPager.FirstPagerMangerImal;
import com.wonhx.patient.app.manager.FirstPagerMager;
import com.wonhx.patient.app.model.DoctorDetial;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.app.model.ResultPrice;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UIKit;
import com.wonhx.patient.kit.UpdateUIListenner;
import com.wonhx.patient.view.HJloginDialog;
import com.wonhx.patient.view.LoginDialog;
import com.wonhx.patient.view.XCFlowLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

public class DoctorDetialActivity extends BaseActivity {
    @InjectView(R.id.title)
    TextView tv_title;
    @InjectView(R.id.right_btn)
    TextView tv_right;
    @InjectView(R.id.myName)
    TextView tv_name;
    @InjectView(R.id.keshi)
    TextView tv_keshi;
    @InjectView(R.id.myhospital)
    TextView tv_hospital;
    @InjectView(R.id.title1)
    TextView t_title;
    @InjectView(R.id.doctorHead)
    SimpleDraweeView iv_head;
    @InjectView(R.id.rec_flowlayout)
    XCFlowLayout xcf_tuijianredu;
    @InjectView(R.id.pat_flowlayout)
    XCFlowLayout xcf_huanzheyinxiang;
    @InjectView(R.id.guanzhuButton)
    TextView tv_guanzhu;
    @InjectView(R.id.pic_price)
    TextView mPicPrice;
    @InjectView(R.id.phone_price)
    TextView mPhonePrice;
    @InjectView(R.id.video_price)
    TextView mVideoPrice;
    @InjectView(R.id.family_price)
    TextView mFamilyPrice;
    @InjectView(R.id.tuwenPic)
    LinearLayout mPicLayout;
    @InjectView(R.id.telphonetells)
    LinearLayout mPhoneLayout;
    @InjectView(R.id.tel_video)
    LinearLayout mVideoLayout;
    @InjectView(R.id.familydoctor)
    LinearLayout mFamilyLayout;
    String member_id = "",doctor_id="";
    int a=0,b=0;
    ViewGroup.MarginLayoutParams lp;
    FirstPagerMager firstPagerMager = new FirstPagerMangerImal();
    boolean flag = true;
    private String recname[] = {"感冒一天就好了", "发烧治得很好", "头痛缓轻了很多", "流鼻涕"};
    private String patname[] = {"非常专业", "非常耐心", "顶级医院", "医生很棒", "我的病完全好了"};
    private String mMemberId;
    DoctorDetial mDetail;
    PopupWindow popupWindow;
    MyReceiver myReceiver = null;
    HJloginDialog dialog1;
    ArrayList<ResultPrice.FamilyPrice> mfamilyinfo=new ArrayList<ResultPrice.FamilyPrice>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_detial);
    }

    @Override
    protected void onInitView() {
        super.onInitView();

        lp = new ViewGroup.MarginLayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = 10;
        lp.rightMargin = 10;
        lp.topMargin = 10;
        lp.bottomMargin = 15;
        for (int i = 0; i < recname.length; i++) {
            TextView view = new TextView(this);
            view.setText(recname[i]);
            view.setTextColor(getResources().getColor(R.color.black));
            view.setBackgroundDrawable(getResources().getDrawable(
                    R.drawable.recommend));
            xcf_tuijianredu.addView(view, lp);
        }
        for (int i = 0; i < patname.length; i++) {
            TextView view = new TextView(this);
            view.setText(patname[i]);
            view.setTextColor(getResources().getColor(R.color.black));
            view.setBackgroundDrawable(getResources().getDrawable(
                    R.drawable.recommend));
            xcf_huanzheyinxiang.addView(view, lp);
        }
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("GOTO_HISTORY");
        intentFilter.addAction("QUIT");
        registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String str) {
                //tv1.setText(str);
                finish();
            }
        });
    }

    @Override
    protected void onInitData() {
        super.onInitData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPicLayout.setEnabled(false);
        mPhoneLayout.setEnabled(false);
        mVideoLayout.setEnabled(false);
        getDoctorDetail();
    }
    //家庭医生价格选择
    @OnClick(R.id.familydoctor)
    void family_doctor(){
        String memberId= AbSharedUtil.getString(this,"userId");
        if (memberId!=null&&!memberId.equals("")) {
            Intent intent=new Intent(DoctorDetialActivity.this,HomeDoctorDetialActivity.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable("mfamilyinfo",mfamilyinfo);
            bundle.putString("doctor_id",doctor_id);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }else {
            showLoginDialog();
        }

    }
    /**
     * 获取医生详情
     */
    private void getDoctorDetail(){
        mMemberId = AbSharedUtil.getString(this, "userId");
        member_id = getIntent().getStringExtra("member_id");
        firstPagerMager.doctordetial(member_id, mMemberId, new SubscriberAdapter<ProResult<DoctorDetial>>() {
            @Override
            public void success(ProResult<DoctorDetial> doctorDetialProResult) {
                super.success(doctorDetialProResult);
                mDetail = doctorDetialProResult.getData();
                tv_title.setText(mDetail.getName());
                tv_name.setText(mDetail.getName());
                tv_keshi.setText(mDetail.getDeptName());
                tv_hospital.setText(mDetail.getHospitalName());
                t_title.setText(mDetail.getTitle());
                doctor_id=mDetail.getDoctorId();
                //1已经关注， 2 没有关注
                if (mDetail.getFollowed_status()!=null){
                    if (mDetail.getFollowed_status().equals("1")){
                        tv_guanzhu.setText("取消关注");
                        flag = false;
                    }else{
                        tv_guanzhu.setText("+关注");
                        flag = true;
                    }
                }
                if (mDetail.getLog_img_path() != null) {
                    iv_head.setImageURI(Uri.parse(Constants.REST_ORGIN +"/emedicine"+ mDetail.getLog_img_path()));
                }

            }
        });
        firstPagerMager.getDoctorPrice(member_id,new SubscriberAdapter<ProResult<ResultPrice>>(){
            @Override
            public void success(ProResult<ResultPrice> resultPrice) {
                super.success(resultPrice);
                List<ResultPrice.Serviceinfo> mPrices = new ArrayList<ResultPrice.Serviceinfo>();
                mPrices.addAll(resultPrice.getData().getServiceinfo());
                List<ResultPrice.Schedule> schedules = new ArrayList<ResultPrice.Schedule>();
                schedules.addAll(resultPrice.getData().getSchedule());
                mfamilyinfo.clear();
                mfamilyinfo.addAll(resultPrice.getData().getFamilyPrice());
                if (mfamilyinfo.size()!=0){
                        mFamilyLayout.setEnabled(true);
                        mFamilyPrice.setText("¥" + mfamilyinfo.get(0).getService_price() + "元/起");
                        mFamilyPrice.setTextColor(getResources().getColor(R.color.main_text_select));
                }else {
                    mFamilyLayout.setEnabled(false);
                }
                if (mPrices.size() != 0) {
                    for (ResultPrice.Serviceinfo price : mPrices) {
                        switch (Integer.parseInt(price.getServiceId())) {
                            case 2:
                                mPicLayout.setEnabled(true);
                                mPicPrice.setText("¥" + price.getPrice() + "元/次");
                                mPicPrice.setTextColor(getResources().getColor(R.color.main_text_select));
                                break;
                            case 3:
                                mPhonePrice.setText("¥" + price.getPrice() + "元/次");
                                mPhoneLayout.setEnabled(true);
                                a=1;
                                if (schedules.size() != 0) {
                                    for (ResultPrice.Schedule schedule : schedules) {
                                        if (schedule.getServiceType().equals("3")) {
                                            a=2;
                                            mPhoneLayout.setEnabled(true);
                                            mPhonePrice.setTextColor(getResources().getColor(R.color.main_text_select));
                                        }
                                    }
                                }
                                break;
                            case 4:
                                mVideoPrice.setText("¥" + price.getPrice() + "元/次");
                                mVideoLayout.setEnabled(true);
                                b=1;
                                if (schedules.size() != 0) {
                                    for (ResultPrice.Schedule schedule : schedules) {
                                        if (schedule.getServiceType().equals("4")) {
                                            b=2;
                                            mVideoLayout.setEnabled(true);
                                            mVideoPrice.setTextColor(getResources().getColor(R.color.main_text_select));
                                        }
                                    }
                                }
                                break;

                        }
                    }
                }
            }
        });
    }

    @OnClick(R.id.tuwenPic)
    void picAsk() {
        showPopuWindow(2,mPicPrice.getText().toString());
    }
    @OnClick(R.id.telphonetells)
    void phoneAsk(){
        if (a==1){
            showDialog("医生还未设置时间！");
        }else if (a==2){
            showPopuWindow(3,mPhonePrice.getText().toString());
        }

    }
    @OnClick(R.id.tel_video)
    void videoAsk(){
        if (b==1){
            showDialog("医生还未设置时间！");
        }else {
            showPopuWindow(4,mVideoPrice.getText().toString());
        }
    }
    private void showDialog(String message) {
        dialog1 = new HJloginDialog(DoctorDetialActivity.this);
        dialog1.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.setTitle("提示");
        dialog1.setContentMessage(message);
        dialog1.setPositiveMessage("确定");
        dialog1.setCancelVisibility(false);
        dialog1.setPositiveColor(this.getResources().getColor(R.color.base_color));
        dialog1.show();
    }

    /**
     * 跳转
     * @param type
     */
    private void showPopuWindow(final int type,String price){
        View contentView = getLayoutInflater().inflate(R.layout.popu_ifask, null);
        TextView doctorname = (TextView) contentView.findViewById(R.id.doctorname);
        TextView content = (TextView) contentView.findViewById(R.id.content);
        TextView titlebottom = (TextView) contentView.findViewById(R.id.titlebottom);
        doctorname.setText(mDetail.getName());
        TextView picprice = (TextView) contentView.findViewById(R.id.picprice);
        picprice.setText(price);
        TextView now_go = (TextView) contentView.findViewById(R.id.now_go);
        switch (type){
            case 2:
                titlebottom.setText("图文咨询");
                content.setText("通过文字、图片、语音进行咨询");
                break;
            case 3:
                titlebottom.setText("电话咨询");
                content.setText("通过电话进行咨询");
                break;
            case 4:
                titlebottom.setText("视频咨询");
                content.setText("通过视频进行咨询");
                break;
        }
        now_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                if ((AbSharedUtil.getString(DoctorDetialActivity.this, "userId")) != null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("detail", mDetail);
                    switch (type){
                        case 2:
                            bundle.putInt("type",2);
                            UIKit.open(DoctorDetialActivity.this, PicAskDoctorActivity.class, bundle);
                            break;
                        case 3:
                            bundle.putInt("type",3);
                            UIKit.open(DoctorDetialActivity.this, PhoneVideoAskDoctorActivity.class, bundle);
                            break;
                        case 4:
                            bundle.putInt("type",4);
                            UIKit.open(DoctorDetialActivity.this, PhoneVideoAskDoctorActivity.class, bundle);
                            break;
                    }
                } else {
                    showLoginDialog();
                }
            }
        });
        popupWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);// 取得焦点
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        /** 设置PopupWindow弹出和退出时候的动画效果 */
        popupWindow.setAnimationStyle(R.style.animation);
        popupWindow.showAtLocation(mPicLayout, Gravity.BOTTOM, 0, 0);
    }

    @OnClick(R.id.left_btn)
    void back() {
        finish();
    }

    @OnClick(R.id.rl_guanzhu)
    void guanzhu() {
        if ( mMemberId == null) {
            UIKit.open(DoctorDetialActivity.this, LoginActivity.class);
        } else {
            if (flag) {
                firstPagerMager.guanzhuyisheng(mMemberId, mDetail.getDoctorId(), "1", new SubscriberAdapter<Result>() {
                    @Override
                    public void success(Result result) {
                        super.success(result);
                        tv_guanzhu.setText("取消关注");
                        flag = false;
                        Toaster.showShort(DoctorDetialActivity.this, result.getMsg());
                    }
                });
            } else if (!flag) {
                firstPagerMager.guanzhuyisheng(mMemberId, mDetail.getDoctorId(), "2", new SubscriberAdapter<Result>() {
                    @Override
                    public void success(Result result) {
                        super.success(result);
                        tv_guanzhu.setText("+关注");
                        flag = true;
                        Toaster.showShort(DoctorDetialActivity.this, result.getMsg());
                    }
                });
            }
        }
    }

    // 弹窗
    private void showLoginDialog() {
        final LoginDialog dialog = new LoginDialog(this);
        dialog.setOnPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                UIKit.open(DoctorDetialActivity.this, LoginActivity.class);
            }
        });
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myReceiver!=null){
            unregisterReceiver(myReceiver);
        }
    }
}
