package com.wonhx.patient.app.fragment.user;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.google.gson.Gson;
import com.wonhx.patient.R;
import com.wonhx.patient.app.activity.user.DisabilityActivity;
import com.wonhx.patient.app.activity.user.FamilyActivity;
import com.wonhx.patient.app.activity.user.PastActivity;
import com.wonhx.patient.app.activity.user.PillActivity;
import com.wonhx.patient.app.base.BaseFragment;
import com.wonhx.patient.app.manager.UserManager;
import com.wonhx.patient.app.manager.user.UserManagerImpl;
import com.wonhx.patient.app.model.HealthyInfo;
import com.wonhx.patient.app.model.ProResult;
import com.wonhx.patient.app.model.Result;
import com.wonhx.patient.kit.AbSharedUtil;
import com.wonhx.patient.kit.MyReceiver;
import com.wonhx.patient.kit.Toaster;
import com.wonhx.patient.kit.UpdateUIListenner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by apple on 2017/4/7.
 * 健康档案：健康信息
 */

public class HealthyInfoFragment extends BaseFragment {
    //身高
    @InjectView(R.id.height)
    EditText height;
    //体重
    @InjectView(R.id.weight)
    EditText weight;
    //BMI
    @InjectView(R.id.bmi)
    EditText bmi;
    //血型
    @InjectView(R.id.blood_type)
    TextView blood_type;
    //体温
    @InjectView(R.id.temperature)
    EditText temperature;
    //左脉率
    @InjectView(R.id.pr)
    EditText pr;
    //右脉率
    @InjectView(R.id.br)
    EditText br;
    //腰围
    @InjectView(R.id.waistline)
    EditText waistline;
    //左血压
    @InjectView(R.id.bp_left)
    EditText bp_left;
    //右血压
    @InjectView(R.id.bp_right)
    EditText bp_right;
    //过敏史
    @InjectView(R.id.allergic_history)
    TextView allergic_history;
    //既往病史
    @InjectView(R.id.past_medical_history)
    TextView past_medical_history;
    //有无手术
    @InjectView(R.id.operation)
    TextView operation;
    //手术时间
    @InjectView(R.id.operation_time)
    TextView operation_time;
    //手术名称
    @InjectView(R.id.operation_name)
    EditText operation_name;
    //有无外伤
    @InjectView(R.id.injury)
    TextView injury;
    //外伤名称
    @InjectView(R.id.injury_name)
    EditText injury_name;
    //外伤时间
    @InjectView(R.id.injury_time)
    TextView injury_time;
    //有无输血
    @InjectView(R.id.blood)
    TextView blood;
    //输血时间
    @InjectView(R.id.blood_time)
    TextView blood_time;
    //输血名称
    @InjectView(R.id.blood_name)
    EditText blood_name;
    //家族病史
    @InjectView(R.id.family_medical_history)
    TextView family_medical_history;
    //残疾情况
    @InjectView(R.id.disability)
    TextView disability;
    @InjectView(R.id.commit_btn)
    TextView tv_commmit;
    UserManager userManager = new UserManagerImpl();
    HealthyInfo mHealthyInfo;
    String mHealthyId;
    @InjectView(R.id.o)
    LinearLayout ll_is_opretion;
    @InjectView(R.id.ll_t)
    LinearLayout ll_is_injury;
    @InjectView(R.id.ll_d)
    LinearLayout ll_id_blood;
    TimePickerView pvTime;
    MyReceiver myReceiver;
    int a = 1;
    String Family_medical_history = "", Allergic_history = "", Disability = "", Medical_history = "";
    //是否可以编辑
    boolean enable = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_healthy_info, container, false);
        ButterKnife.inject(this, viewGroup);
        return viewGroup;
    }

    @Override
    protected void onInitData() {
        super.onInitData();
        getmessage();
        setEnable(enable);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("FLAG");
        getActivity().registerReceiver(myReceiver, intentFilter);
        myReceiver.SetOnUpdateUIListenner(new UpdateUIListenner() {
            @Override
            public void UpdateUI(String helthId) {
                if (helthId != null) {
                    //登录
                    getmessage();
                }
            }
        });
    }

    private void getmessage() {
        mHealthyId = AbSharedUtil.getString(getActivity(), "helthId");
        if (mHealthyId != null) {
            userManager.getHealthyInfo(mHealthyId, new SubscriberAdapter<ProResult<HealthyInfo>>() {
                @Override
                public void onError(Throwable e) {
                    //super.onError(e);
                    tv_commmit.setText("保存");
                    dissmissProgressDialog();
                }

                @Override
                public void success(ProResult<HealthyInfo> healthyInfoProResult) {
                    super.success(healthyInfoProResult);
                    if (healthyInfoProResult.getCode()) {
                        tv_commmit.setText("编辑");
                        mHealthyInfo = healthyInfoProResult.getData();
                        if (mHealthyInfo != null) {
                            height.setText(mHealthyInfo.getHeight() + "CM");
                            weight.setText(mHealthyInfo.getWeight() + "KG");
                            blood_type.setText(mHealthyInfo.getBloodtype());
                            bmi.setText(mHealthyInfo.getBmi());
                            temperature.setText(mHealthyInfo.getTemperature() + "℃");
                            pr.setText(mHealthyInfo.getPr() + "次/秒");
                            br.setText(mHealthyInfo.getBr() + "次/秒");
                            waistline.setText(mHealthyInfo.getWaistline() + "CM");
                            bp_left.setText(mHealthyInfo.getBp_left() + "/mmHg");
                            bp_right.setText(mHealthyInfo.getBp_right() + "/mmHg");
                            Allergic_history = mHealthyInfo.getAllergic_history();
                            allergic_history.setText(Allergic_history);
                            Medical_history = mHealthyInfo.getMedical_history();
                            past_medical_history.setText(Medical_history);
                            operation.setText(mHealthyInfo.getIs_operation());
                            if (operation.getText().equals("有")) {
                                ll_is_opretion.setVisibility(View.VISIBLE);
                            } else {
                                ll_is_opretion.setVisibility(View.GONE);
                            }
                            operation_name.setText(mHealthyInfo.getFirst_operation());
                            operation_time.setText(mHealthyInfo.getFirst_ortime());
                            injury.setText(mHealthyInfo.getIs_tourims());
                            if (injury.getText().equals("有")) {
                                ll_is_injury.setVisibility(View.VISIBLE);
                            } else {
                                ll_is_injury.setVisibility(View.GONE);
                            }
                            injury_name.setText(mHealthyInfo.getFirst_tourims());
                            injury_time.setText(mHealthyInfo.getFirst_tortime());
                            blood.setText(mHealthyInfo.getIs_transfusion());
                            if (blood.getText().equals("有")) {
                                ll_id_blood.setVisibility(View.VISIBLE);
                            } else {
                                ll_id_blood.setVisibility(View.GONE);
                            }
                            blood_name.setText(mHealthyInfo.getFirst_transfusion());
                            blood_time.setText(mHealthyInfo.getFirst_trantime());
                            Family_medical_history = mHealthyInfo.getFamily_medical_history();
                            family_medical_history.setText(Family_medical_history);
                            Disability = mHealthyInfo.getDisability();
                            disability.setText(Disability);
                        }
                    } else {
                        tv_commmit.setText("保存");
                    }
                }
            });
        }
    }

    private String getTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    @Override
    protected void onInitView() {
        super.onInitView();
        Calendar c = Calendar.getInstance();
        pvTime = new TimePickerView.Builder(getActivity(), new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                if (a == 1) {
                    operation_time.setText(getTime(date));
                } else if (a == 2) {
                    injury_time.setText(getTime(date));
                } else if (a == 3) {
                    blood_time.setText(getTime(date));
                }
            }
        })
                .setType(TimePickerView.Type.YEAR_MONTH_DAY)//默认全部显示
                .setCancelText("取消")//取消按钮文字
                .gravity(Gravity.CENTER)
                .setSubmitText("确定")//确认按钮文字
                .setContentSize(18)//滚轮文字大小
                .setTitleSize(20)//标题文字大小
                .setTitleText("")//标题文字
                .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(true)//是否循环滚动
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setSubmitColor(Color.BLUE)//确定按钮文字颜色
                .setCancelColor(Color.BLUE)//取消按钮文字颜色
                .setTitleBgColor(getResources().getColor(R.color.line_grey))//标题背景颜色 Night mode
                .setBgColor(Color.WHITE)//滚轮背景颜色 Night mode
                .setRange(c.get(Calendar.YEAR) - 100, c.get(Calendar.YEAR))//默认是1900-2100年
                // .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
                //.setRangDate(startDate,endDate)//起始终止年月日设定
                .setLabel("年", "月", "日", "", "", "")
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                // .isDialog(true)//是否显示为对话框样式
                .build();
    }


    @OnClick(R.id.commit_btn)
    void commit() {
        if (enable){
            submit();
        }else {
            enable = true;
            setEnable(enable);
        }
    }

    /**
     * 提交编辑内容
     */
    private void submit(){
        mHealthyInfo = new HealthyInfo();
        mHealthyInfo.setHeight(height.getText().toString().trim());
        mHealthyInfo.setWeight(weight.getText().toString().trim());
        mHealthyInfo.setBloodtype(blood_type.getText().toString());
        mHealthyInfo.setBmi(bmi.getText().toString().trim());
        mHealthyInfo.setTemperature(temperature.getText().toString().trim());
        mHealthyInfo.setPr(pr.getText().toString().trim());
        mHealthyInfo.setBr(br.getText().toString().trim());
        mHealthyInfo.setWaistline(waistline.getText().toString());
        mHealthyInfo.setBp_left(bp_left.getText().toString());
        mHealthyInfo.setBp_right(bp_right.getText().toString());
        mHealthyInfo.setAllergic_history(Allergic_history);
        mHealthyInfo.setMedical_history(Medical_history);
        mHealthyInfo.setOperation(operation.getText().toString().trim());
        mHealthyInfo.setFirst_operation(operation_name.getText().toString().trim());
        mHealthyInfo.setFirst_ortime(operation_time.getText().toString().trim());
        mHealthyInfo.setIs_tourims(injury.getText().toString().trim());
        mHealthyInfo.setFirst_tortime(injury_time.getText().toString().trim());
        mHealthyInfo.setFirst_tourims(injury_name.getText().toString().trim());
        mHealthyInfo.setIs_transfusion(blood.getText().toString().trim());
        mHealthyInfo.setFirst_transfusion(blood_name.getText().toString().trim());
        mHealthyInfo.setFirst_trantime(blood_time.getText().toString().trim());
        mHealthyInfo.setFamily_medical_history(Family_medical_history);
        mHealthyInfo.setDisability(Disability);
        String msg = new Gson().toJson(mHealthyInfo);
        userManager.submitHealthyInfo(mHealthyId, msg, new SubscriberAdapter<Result>() {
            @Override
            public void success(Result result) {
                super.success(result);
                Toaster.showShort(getActivity(), result.getMsg());
                enable=false;
                setEnable(enable);
            }
        });
    }

    /**
     * 是否能编辑
     *
     * @param enable
     */
    private void setEnable(boolean enable) {
        height.setEnabled(enable);
        weight.setEnabled(enable);
        blood.setEnabled(enable);
        bmi.setEnabled(enable);
        blood_type.setEnabled(enable);
        temperature.setEnabled(enable);
        pr.setEnabled(enable);
        br.setEnabled(enable);
        waistline.setEnabled(enable);
        bp_left.setEnabled(enable);
        bp_right.setEnabled(enable);
        operation.setEnabled(enable);
        operation_name.setEnabled(enable);
        operation_time.setEnabled(enable);
        injury.setEnabled(enable);
        injury_name.setEnabled(enable);
        injury_time.setEnabled(enable);
        blood_name.setEnabled(enable);
        blood_time.setEnabled(enable);
        allergic_history.setEnabled(enable);
        past_medical_history.setEnabled(enable);
        family_medical_history.setEnabled(enable);
        disability.setEnabled(enable);
        if (enable){
            tv_commmit.setText("保存");
        }else{
            tv_commmit.setText("编辑");
        }
    }

    @OnClick(R.id.blood_type)
    void blood_type() {
        choose_blood_type();
    }

    @OnClick(R.id.operation)
    void operation() {
        choose_operation(1);
    }

    @OnClick(R.id.injury)
    void injury() {
        choose_operation(2);
    }

    @OnClick(R.id.blood)
    void blood() {
        choose_operation(3);
    }

    @OnClick(R.id.blood_time)
    void blood_time() {
        a = 3;
        pvTime.show();
    }

    @OnClick(R.id.injury_time)
    void injury_time() {
        a = 2;
        pvTime.show();
    }

    @OnClick(R.id.operation_time)
    void operation_time() {
        a = 1;
        pvTime.show();
    }

    private void choose_operation(final int i) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.select_dialog, null);
        final Dialog dialog = new AlertDialog.Builder(getActivity()).create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.mydialogstyle);  //添加动画
        window.setBackgroundDrawable(new ColorDrawable());
        dialog.show();
        window.setContentView(view);
        TextView camera = (TextView) view.findViewById(R.id.camera);
        camera.setText("有");
        final TextView picfile = (TextView) view.findViewById(R.id.picfile);
        picfile.setText("无");
        TextView cancle = (TextView) view.findViewById(R.id.cancle);
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //拍照
                dialog.dismiss();
                if (i == 1) {
                    operation.setText("有");
                    ll_is_opretion.setVisibility(View.VISIBLE);
                } else if (i == 2) {
                    injury.setText("有");
                    ll_is_injury.setVisibility(View.VISIBLE);
                } else if (i == 3) {
                    blood.setText("有");
                    ll_id_blood.setVisibility(View.VISIBLE);
                }
            }
        });
        picfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //相册
                dialog.dismiss();
                if (i == 1) {
                    operation.setText("无");
                    ll_is_opretion.setVisibility(View.GONE);
                } else if (i == 2) {
                    injury.setText("无");
                    ll_is_injury.setVisibility(View.GONE);
                } else if (i == 3) {
                    blood.setText("无");
                    ll_id_blood.setVisibility(View.GONE);
                }
            }
        });
    }

    private void choose_blood_type() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.select_dialog_blood, null);
        final Dialog dialog = new AlertDialog.Builder(getActivity()).create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.mydialogstyle);  //添加动画
        window.setBackgroundDrawable(new ColorDrawable());
        dialog.show();
        window.setContentView(view);
        TextView o = (TextView) view.findViewById(R.id.o);
        TextView ab = (TextView) view.findViewById(R.id.ab);
        TextView a = (TextView) view.findViewById(R.id.a);
        TextView b = (TextView) view.findViewById(R.id.b);
        TextView cancle = (TextView) view.findViewById(R.id.cancle);
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                blood_type.setText("A");
            }
        });
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                blood_type.setText("B");
            }
        });
        ab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                blood_type.setText("AB");
            }
        });
        o.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                blood_type.setText("O");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick({R.id.allergic_history, R.id.past_medical_history, R.id.family_medical_history, R.id.disability})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.allergic_history:
                Intent in = new Intent(getActivity(), PillActivity.class);
                in.putExtra("msg", Allergic_history);
                startActivityForResult(in, 101);
                break;
            case R.id.family_medical_history:
                Intent in12 = new Intent(getActivity(), FamilyActivity.class);
                in12.putExtra("msg", Family_medical_history);
                startActivityForResult(in12, 301);
                break;
            case R.id.disability:
                Intent in12s = new Intent(getActivity(), DisabilityActivity.class);
                in12s.putExtra("msg", Disability);
                startActivityForResult(in12s, 302);
                break;
            case R.id.past_medical_history:
                Intent in1 = new Intent(getActivity(), PastActivity.class);
                in1.putExtra("msg", Medical_history);
                startActivityForResult(in1, 102);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == 104) {
            Allergic_history = data.getStringExtra("msg");
            allergic_history.setText(Allergic_history);
        } else if (requestCode == 102 && resultCode == 100) {
            Medical_history = data.getStringExtra("msg");
            past_medical_history.setText(Medical_history);
        } else if (requestCode == 301 && resultCode == 304) {
            Family_medical_history = data.getStringExtra("msg");
            family_medical_history.setText(Family_medical_history);
        } else if (requestCode == 302 && resultCode == 104) {
            Disability = data.getStringExtra("msg");
            disability.setText(Disability);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (myReceiver != null) {
            getActivity().unregisterReceiver(myReceiver);
        }
    }
}
