package com.wonhx.patient.app.model;

import com.google.gson.annotations.SerializedName;
import com.wonhx.patient.db.DbKit;
import com.wonhx.patient.db.Model;
import com.wonhx.patient.db.annotation.Constraint;
import com.wonhx.patient.kit.DateKit;
import com.wonhx.patient.kit.StrKit;

import java.util.Date;
import java.util.List;

import rx.Observable;

/**
 * 基础Bean
 */
public abstract class BaseModel<M extends BaseModel> extends Model<M> {
    /**
     * ID
     */
    @Constraint({ Constraint.Type.PRIMARY_KEY })
    private int id;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @SerializedName("if_delete")
    private int isDelete; // 1-删除
    public boolean IsDelete() {
        return isDelete == 1;
    }
    @SerializedName("update_time")
    public Date updateTime;
    @SerializedName("create_time")
    public Date createTime;
    /**
     * 获取本表最新的更新时间（不存在更新时间则使用创建时间）
     * @return
     */
    public String lastUpdateTime(String where) {
        String sql = "select * from " + Table();
        if (StrKit.notBlank(where))
            sql += " where " + where;
        sql += " order by updateTime desc, createTime desc limit 1";
        BaseModel model = findFirst(sql);
        if (model == null)
            return null;
        if (StrKit.notBlank(model.updateTime))
            return DateKit.format(model.updateTime);
        if (StrKit.notBlank(model.createTime))
            return DateKit.format(model.createTime);
        return null;
    }
    public List<M> findAll() {
        return find(String.format("select * from %s where isDelete=0 order by createTime desc", Table()));
    }

    public Observable<List<M>> findAllRx() {
        return findRx(String.format("select * from %s where isDelete=0 order by createTime desc", Table()));
    }

    public void putAll(List<M> list) {
        DbKit.beginTransaction();
        for (M model : list) {
            model.saveOrUpdate();
        }
        DbKit.setTransactionSuccessful();
        DbKit.endTransaction();
    }

    public long saveOrUpdate() {
        int result = update();
        return result > 0 ? result : save();
    }

}
