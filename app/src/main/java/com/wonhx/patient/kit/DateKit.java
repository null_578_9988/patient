package com.wonhx.patient.kit;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * DateKit.
 */
public class DateKit {
    /**
     * 年月日
     */
    public final static String PATTERN_DATE = "yyyy-MM-dd";
    /**
     * 年月日时分秒
     */
    public final static String PATTERN_DATETIME = "yyyy-MM-dd HH:mm:ss";

    /**
     * 解析字符串
     *
     * @param date
     * @return
     */
    public static Date parse(String date) {
        SimpleDateFormat format = new SimpleDateFormat(PATTERN_DATETIME);
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }
    /**
     * 获取网络时间
     */
    public  static  String getNowTime(){
        URL url= null;//取得资源对象
        String time="";
        try {
            url = new URL("http://www.bjtime.cn");
            URLConnection uc= null;//生成连接对象
            try {
                uc = url.openConnection();
                uc.connect(); //发出连接
            } catch (IOException e) {
                e.printStackTrace();
            }

            long ld=uc.getDate(); //取得网站日期时间
            Date date=new Date(ld); //转换为标准时间对象
            //分别取得时间中的小时，分钟和秒，并输出
            time=date.getYear()+"-"+date.getMonth()+"-"+date.getDay()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"";
            return time;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
     return time;
    }

    /**
     * 解析字符串
     *
     * @param date
     * @return
     */
    public static Date parse(String date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String getCurrentTime() {

        return null;
    }

    /**
     * 格式化日期
     *
     * @param date
     * @return
     */
    public static String format(Date date) {
        return format(date, PATTERN_DATETIME);
    }

    /**
     * 格式化日期
     *
     * @param date
     * @return
     */
    public static String format2Date(Date date) {
        return format(date, PATTERN_DATE);
    }

    /**
     * 格式化日期
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String format(Date date, String pattern) {
        if (date == null) return "";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    /**
     * 友好显示历史时间（只接受历史时间，无未来时间规则）
     *
     * @param date
     * @return
     */
    public static String friendly(Date date) {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int day = now.get(Calendar.DAY_OF_YEAR);
        Calendar target = Calendar.getInstance();
        target.setTime(date);
        int year2 = target.get(Calendar.YEAR);
        int day2 = target.get(Calendar.DAY_OF_YEAR);
        if (year == year2 && day == day2) { // 今天-显示时间
            return format(date, "ahh:mm"); // eg.上午10:10
        } else if (year == year2 && day - day2 == 1) { // 昨天
            return "昨天";
        } else if (year == year2 && day - day2 < 7) { // 一周内-显示星期
            return format(date, "EEEE"); // eg.星期一
        } else {
            return format(date, PATTERN_DATE);
        }
    }

    /**
     * 判断时间是否在时间段内*
     *
     * @param date         当前时间 yyyy-MM-dd HH:mm:ss
     * @param strDateBegin 开始时间 00:00:00
     * @param strDateEnd   结束时间 00:05:00
     * @return
     */
    public static boolean isInDate(Date date, String strDateBegin, String strDateEnd) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(date);
        // 截取当前时间时分秒
        int strDateH = Integer.parseInt(strDate.substring(11, 13));
        int strDateM = Integer.parseInt(strDate.substring(14, 16));
        int strDateS = Integer.parseInt(strDate.substring(17, 19));
        // 截取开始时间时分秒
        int strDateBeginH = Integer.parseInt(strDateBegin.substring(0, 2));
        int strDateBeginM = Integer.parseInt(strDateBegin.substring(3, 5));
        int strDateBeginS = Integer.parseInt(strDateBegin.substring(6, 8));
        // 截取结束时间时分秒
        int strDateEndH = Integer.parseInt(strDateEnd.substring(0, 2));
        int strDateEndM = Integer.parseInt(strDateEnd.substring(3, 5));
        int strDateEndS = Integer.parseInt(strDateEnd.substring(6, 8));
        if ((strDateH >= strDateBeginH && strDateH <= strDateEndH)) {
            // 当前时间小时数在开始时间和结束时间小时数之间
            if (strDateH > strDateBeginH && strDateH < strDateEndH) {
                return true;
                // 当前时间小时数等于开始时间小时数，分钟数在开始和结束之间
            } else if (strDateH == strDateBeginH && strDateM >= strDateBeginM && strDateM <= strDateEndM) {
                return true;
                // 当前时间小时数等于开始时间小时数，分钟数等于开始时间分钟数，秒数在开始和结束之间
            } else if (strDateH == strDateBeginH && strDateM == strDateBeginM && strDateS >= strDateBeginS && strDateS <= strDateEndS) {
                return true;
            }
            // 当前时间小时数大等于开始时间小时数，等于结束时间小时数，分钟数小等于结束时间分钟数
            else if (strDateH >= strDateBeginH && strDateH == strDateEndH && strDateM <= strDateEndM) {
                return true;
                // 当前时间小时数大等于开始时间小时数，等于结束时间小时数，分钟数等于结束时间分钟数，秒数小等于结束时间秒数
            } else if (strDateH >= strDateBeginH && strDateH == strDateEndH && strDateM == strDateEndM && strDateS <= strDateEndS) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    /**
     * 将java.util.Date 格式转换为字符串格式'yyyy-MM-dd HH:mm:ss'(24小时制)<br>
     * 如Sat May 11 17:24:21 CST 2002 to '2002-05-11 17:24:21'<br>
     * @param time Date 日期<br>
     * @return String   字符串<br>
     */


    public static String dateToString(Date time){
        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        String ctime = formatter.format(time);

        return ctime;
    }
    /**
     * 判断时间大小
     */
    public static int compare_date(String DATE1, String DATE2) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        try {
            Date dt1 = df.parse(DATE1);
            Date dt2 = df.parse(DATE2);
            if (dt1.getTime() > dt2.getTime()) {
                return 1;
            } else if (dt1.getTime() < dt2.getTime()) {
                return -1;
            } else {
                return 0;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return 0;
    }
}
