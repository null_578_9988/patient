package com.wonhx.patient.kit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.Toast;

import com.wonhx.patient.R;
import com.wonhx.patient.app.AppManager;

import java.lang.reflect.Field;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 界面工具
 */
public class UIKit {
    /**
     * 界面打开动画开始位置参数名
     */
    public static final String ARG_REVEAL_START_LOCATION = "reveal_start_location";
    /**
     * 界面打开动画时长
     */
    public static final int ANIM_DURATION_REVEAL = 500;
    /**
     * 工具栏动画时长
     */
    public static final int ANIM_DURATION_TOOLBAR = 300;

    /**
     * 打开界面
     * @param context
     * @param cls
     */
    public static void open(Activity context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }
    /**
     * 打开界面
     * @param context
     * @param cls
     */
    public static void open(Activity context, Class<?> cls, Bundle extras) {
        Intent intent = new Intent(context, cls);
        intent.putExtras(extras);
        context.startActivity(intent);
    }

    /**
     * 打开界面
     * @param context
     * @param cls
     * @param finish 关闭本界面
     */
    public static void open(Activity context, Class<?> cls, boolean finish) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
        if (finish) context.finish();
    }

    /**
     * 打开界面（播放动画）
     * @param context
     * @param cls
     * @param location 打开动画开始位置
     */
    public static void open(Activity context, Class<?> cls, int[] location) {
        Intent intent = new Intent(context, cls);
        intent.putExtra(ARG_REVEAL_START_LOCATION, location);
        context.startActivity(intent);
        context.overridePendingTransition(0, 0);
    }



    private static final String APP_ID_QQ = "1104711050";
    private static final String APP_KEY_QQ = "POFiwq4pxgtZ3q65";
    private static final String APP_ID_WX = "wx4c3563da096816b1";
    private static final String APP_SECRET_WX = "636f65dac4e131865d674bcb842d6eac";

    public static final int REQUEST_FOR_SHARE = 0x4;
    /**
     * 分享到...
     * @param context
     * @param subject
     * @param text
     */
    public static void forShare(Activity context, String subject, String text) {
        Intent intent=new Intent(Intent.ACTION_SEND);
        intent.setType("text/*");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivityForResult(Intent.createChooser(intent, "分享到..."), REQUEST_FOR_SHARE);
    }

    /**
     * 调用系统浏览器打开URL
     * @param context
     * @param url
     */
    public static void url(Activity context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }

    public static final int REQUEST_FOR_CONTACTS = 0x1;

    /**
     * 获取联系人
     * @param context
     */
    public static void forContacts(Activity context) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        context.startActivityForResult(intent, REQUEST_FOR_CONTACTS);
    }



    /**
     * 发送App异常崩溃报告
     *
     * @param ctx
     * @param crashReport
     */
    public static void sendAppCrashReport(final Context ctx, final String crashReport) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle(R.string.app_error);
        builder.setMessage(R.string.app_error_message);
        builder.setPositiveButton(R.string.submit_report, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // 发送异常报告
                Intent i = new Intent(Intent.ACTION_SEND);
                // i.setType("text/plain"); //模拟器
                i.setType("message/rfc822"); // 真机
                i.putExtra(Intent.EXTRA_EMAIL, new String[] { ctx.getString(R.string.report_email) });
                i.putExtra(Intent.EXTRA_SUBJECT, ctx.getString(R.string.app_name) + ctx.getString(R.string.android_error_report));
                i.putExtra(Intent.EXTRA_TEXT, crashReport);
                ctx.startActivity(Intent.createChooser(i, ctx.getString(R.string.send_error_report)));
                // 退出
                AppManager.me().appExit(ctx);
            }
        });
        builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // 退出
                AppManager.me().appExit(ctx);
            }
        });
        builder.show();
    }

    /**
     * 双击退出程序标记
     */
    private static boolean isExit = false;

    /**
     * 双击退出程序
     * @param context
     */
    public static void appExit(Context context) {
        Timer tExit = null;
        if (isExit == false) {
            isExit = true; // 准备退出
            Toast.makeText(context, context.getResources().getString(R.string.back_exit_tips), Toast.LENGTH_SHORT).show();
            tExit = new Timer();
            tExit.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false; // 取消退出
                }
            }, 2000); // 如果2秒钟内没有按下返回键，则启动定时器取消掉刚才执行的任务
        } else {
            // 退出
            AppManager.me().appExit(context);
        }
    }
    // 通过反射机制获取手机状态栏高度
    public static int getStatusBarHeight(Activity activity) {
        Class<?> c = null;
        Object obj = null;
        Field field = null;
        int x = 0, statusBarHeight = 0;
        try {
            c = Class.forName("com.android.internal.R$dimen");
            obj = c.newInstance();
            field = c.getField("status_bar_height");
            x = Integer.parseInt(field.get(obj).toString());
            statusBarHeight = activity.getResources().getDimensionPixelSize(x);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return statusBarHeight;
    }

}
