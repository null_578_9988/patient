package com.wonhx.patient.kit;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.view.WindowManager;

/**
 * 资源工具
 */
public class ResKit {
    /**
     * DP to PX
     * @param dp
     * @return
     */
    public static int dp2Px(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    /**
     * 获取屏幕高度
     * @param ctx
     * @return
     */
    public static int getScreenHeight(Context ctx) {
        Point window = new Point();
        WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getSize(window);
        return window.y;
    }
    /**
     * 获取屏幕宽度
     * @param ctx
     * @return
     */
    public static int getScreenWidth(Context ctx) {
        Point window = new Point();
        WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getSize(window);
        return window.x;
    }

    /**
     * 获取屏幕宽高中大的长度
     * @param ctx
     * @return
     */
    public static int getScreenSizeMax(Context ctx) {
        Point window = new Point();
        WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getSize(window);
        return Math.max(window.x, window.y);
    }
    /**
     * 获取屏幕宽高中小的长度
     * @param ctx
     * @return
     */
    public static int getScreenSizeMin(Context ctx) {
        Point window = new Point();
        WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getSize(window);
        return Math.min(window.x, window.y);
    }
}
