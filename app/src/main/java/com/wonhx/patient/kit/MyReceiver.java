package com.wonhx.patient.kit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Administrator on 2015/10/13.
 *
 */
public class MyReceiver extends BroadcastReceiver {

    UpdateUIListenner updateUIListenner;

    @Override
    public void onReceive(Context context, Intent intent) {
        String key = intent.getStringExtra("key");
        updateUIListenner.UpdateUI(key);

    }

    /**
     * 监听广播接收器的接收到的数据
     * @param updateUIListenner
     */
    public void SetOnUpdateUIListenner(UpdateUIListenner updateUIListenner) {
        this.updateUIListenner = updateUIListenner;

    }
}
