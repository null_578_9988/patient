package com.wonhx.patient.kit;

/**
 * 字符串工具
 */
public class StrKit {
    /**
     * convert camel name to underscore name
     * @return
     */
    public static String camel2underscore(String camelName){
        //先把第一个字母大写
        camelName = capitalize(camelName);

        String regex = "([A-Z][a-z]+)";
        String replacement = "$1_";

        String underscoreName = camelName.replaceAll(regex, replacement);
        //output: Pur_Order_Id_ 接下来把最后一个_去掉，然后全部改小写

        underscoreName = underscoreName.toLowerCase().substring(0, underscoreName.length()-1);

        return underscoreName;
    }

    /**
     * convert underscore name to camel name
     * @param underscoreName
     * @return
     */
    public static String underscore2camel(String underscoreName){
        String[] sections = underscoreName.split("_");
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<sections.length;i++){
            String s = sections[i];
            if(i==0){
                sb.append(s);
            }else{
                sb.append(capitalize(s));
            }
        }
        return sb.toString();
    }

    /**
     * capitalize the first character
     * @param str
     * @return
     */
    public static String capitalize(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return str;
        }
        return new StringBuilder(strLen)
                .append(Character.toTitleCase(str.charAt(0)))
                .append(str.substring(1))
                .toString();
    }
    /**
     * 首字母变小写
     */
    public static String firstCharToLowerCase(String str) {
        char firstChar = str.charAt(0);
        if (firstChar >= 'A' && firstChar <= 'Z') {
            char[] arr = str.toCharArray();
            arr[0] += ('a' - 'A');
            return new String(arr);
        }
        return str;
    }

    /**
     * 首字母变大写
     */
    public static String firstCharToUpperCase(String str) {
        char firstChar = str.charAt(0);
        if (firstChar >= 'a' && firstChar <= 'z') {
            char[] arr = str.toCharArray();
            arr[0] -= ('a' - 'A');
            return new String(arr);
        }
        return str;
    }

    /**
     * 字符串为 null 或者为  "" 时返回 true
     */
    public static boolean isBlank(String str) {
        return str == null || "".equals(str.trim()) ? true : false;
    }
    public static boolean isBlank(Object obj) {
        return obj == null || isBlank(obj.toString());
    }

    /**
     * 字符串不为 null 而且不为  "" 时返回 true
     */
    public static boolean notBlank(String str) {
        return str == null || "".equals(str.trim()) ? false : true;
    }
    public static boolean notBlank(Object obj) {
        return obj != null && notBlank(obj.toString());
    }

    public static boolean notBlank(String... strings) {
        if (strings == null)
            return false;
        for (String str : strings)
            if (str == null || "".equals(str.trim()))
                return false;
        return true;
    }

    public static boolean notNull(Object... paras) {
        if (paras == null)
            return false;
        for (Object obj : paras)
            if (obj == null)
                return false;
        return true;
    }
/**
 * 正则验证手机号
 *
 * @param mobiles
 * @return
 */
    /**
     * 验证手机格式
     */
    public static boolean isMobileNO(String mobiles) {
    /*
    移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188
    联通：130、131、132、152、155、156、185、186
    电信：133、153、180、189、（1349卫通）
    总结起来就是第一位必定为1，第二位必定为3或5或8，其他位置的可以为0-9
    */
        String telRegex = "[1][34578]\\d{9}";//"[1]"代表第1位为数字1，"[34578]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
        if (isBlank(mobiles)) return false;
        else return mobiles.matches(telRegex);
    }
}
