package com.wonhx.patient.kit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * GsonKit
 */
public class GsonKit {
    /**
     * 年月日时分秒
     */
    public static final String PATTERN_TIME = "yyyy-MM-dd HH:mm:ss";
    /**
     * 年月日
     */
    public static final String PATTERN_DATE = "yyyy-MM-dd";
    /**
     * 日期格式（年月日时分秒）
     */
    private static Gson gson = new GsonBuilder().setDateFormat(PATTERN_TIME).create();
    /**
     * 日期格式（年月日）
     */
    private static Gson gson2 = new GsonBuilder().setDateFormat(PATTERN_DATE).create();
    public static Gson Gson() {
        return gson;
    }
    /**
     * 对象转JSON
     * @param src
     * @return
     * @throws JsonSyntaxException
     */
    public static String toJson(Object src) throws JsonSyntaxException {
        return gson.toJson(src);
    }
    /**
     * JSON转对象-日期格式（年月日时分秒）
     * @param json
     * @param typeOfT
     * @return
     */
    public static <T> T fromJson(String json, Type typeOfT) {
        return gson.fromJson(json, typeOfT);
    }
    /**
     * JSON转对象-日期格式（年月日时分秒）
     * @param json
     * @param classOfT
     * @return
     */
    public static  <T> T fromJson(String json, Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }
    /**
     * JSON转对象-日期格式（年月日）
     * @param json
     * @param typeOfT
     * @return
     */
    public static <T> T fromJson2(String json, Type typeOfT) {
        return gson2.fromJson(json, typeOfT);
    }
    /**
     * JSON转对象-日期格式（年月日）
     * @param json
     * @param classOfT
     * @return
     */
    public static <T> T fromJson2(String json, Class<T> classOfT) {
        return gson2.fromJson(json, classOfT);
    }
    /**
     * 根据 key得到json里面的value值
     * @param jsonStr
     * @param key
     * @return
     */
    public static Object getJsonValue(String jsonStr, String key) {
        Object rulsObj = null;
        Map<?, ?> rulsMap = jsonToMap(jsonStr);
        if (rulsMap != null && rulsMap.size() > 0) {
            rulsObj = rulsMap.get(key);
        }
        return rulsObj;
    }
    /**
     * 将json格式转换成map对象
     * @param jsonStr
     * @return
     */
    public static Map<?, ?> jsonToMap(String jsonStr) {
        Map<?, ?> objMap = null;
        if (gson != null) {
            Type type = new TypeToken<Map<?, ?>>(){}.getType();
            objMap = gson.fromJson(jsonStr, type);
        }
        return objMap;
    }
}
