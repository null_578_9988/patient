package com.wonhx.patient.kit;

import android.os.Environment;

import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMImageMessageBody;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMMessage.Direct;
import com.hyphenate.chat.EMMessage.Status;
import com.hyphenate.chat.EMMessage.Type;
import com.hyphenate.chat.EMVoiceMessageBody;
import com.wonhx.patient.app.Constants;
import com.wonhx.patient.app.model.Chat_list;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChattingRecordsUtils {
	/**
	 * 聊天记录数据
	 */
	public static EMMessage chattingRecordsData(Chat_list myMessage) {
		long l;
		// 类型：txt文本， img 图片，loc地理位置，audio音频， video视频
		String msg_type = myMessage.getMsg_type();
		String from = myMessage.getFrom(); // 来自
		String to = myMessage.getTo(); // 去到
		String request_id = myMessage.getRequest_id();
		String msg_content = myMessage.getMsg_content();// 文本内容
		String file_url = Constants.REST_ORGIN+"/emedicine"+myMessage.getFile_url(); // 路径
		String addr = myMessage.getAddr(); // 地理位置描述
		// 消息时间转为时间戳
		String created = myMessage.getCreated();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d = null;
		try {
			d = sdf.parse(created);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		l = d.getTime();
		if (msg_type.equals("txt")) { // 文本消息
			EMMessage message = EMMessage.createTxtSendMessage(msg_content, to);
			message.setMsgTime(l);
			if (from.equals(EMClient.getInstance().getCurrentUser())) { // 右边
				message.setDirection(Direct.SEND);
				message.setStatus(Status.SUCCESS);
			} else {
				message.setDirection(Direct.RECEIVE);
			}
			return message;
		} else if (msg_type.equals("img")) { // 图片
			String imageName = file_url.substring(file_url.lastIndexOf("/") + 1, file_url.length());
			String imagePath = Environment.getExternalStorageDirectory() + "/" + imageName;
			// 保存文件到本地
			downloadNet(file_url, imageName);

			EMMessage message = EMMessage.createReceiveMessage(Type.IMAGE);
			EMImageMessageBody emImageMessageBody = new EMImageMessageBody(new File(imagePath));
			message.addBody(emImageMessageBody);
			message.setMsgTime(l);
			if (from.equals(EMClient.getInstance().getCurrentUser())) { // 右边
				message.setDirection(Direct.SEND);
				message.setStatus(Status.SUCCESS);
			} else {
				message.setDirection(Direct.RECEIVE);
			}
			return message;
		} else if (msg_type.equals("loc")) { // 地理位置
			EMMessage message = EMMessage.createLocationSendMessage(Double.parseDouble(myMessage.getLat()), Double.parseDouble(myMessage.getLng()), addr, to);
			message.setMsgTime(l);
			if (from.equals(EMClient.getInstance().getCurrentUser())) { // 右边
				message.setDirection(Direct.SEND);
				message.setStatus(Status.SUCCESS);
			} else {
				message.setDirection(Direct.RECEIVE);
			}
			return message;
		} else if (msg_type.equals("audio")) { // 语音
			String audioName = file_url.substring(file_url.lastIndexOf("/") + 1, file_url.length());
			String audioPath = Environment.getExternalStorageDirectory() + "/" + audioName;
			// 保存文件到本地
			downloadNet(file_url, audioName);

			EMMessage message;
			if (from.equals(EMClient.getInstance().getCurrentUser())) { // 右边
				message = EMMessage.createSendMessage(Type.VOICE);
				message.setDirection(Direct.SEND);
				message.setStatus(Status.SUCCESS);
			} else {
				message = EMMessage.createReceiveMessage(Type.VOICE);
				message.setDirection(Direct.RECEIVE);
			}
			EMVoiceMessageBody emVoiceMessageBody = new EMVoiceMessageBody(new File(audioPath), Integer.parseInt(myMessage.getLength()));
			message.addBody(emVoiceMessageBody);
			message.setMsgTime(l);
			return message;
		}
		return null;
	}

	/**
	 * 保存文件到本地
	 */
	public static void downloadNet(final String file_url, final String name) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				// 下载网络文件
				int bytesum = 0;
				int byteread = 0;
				try {
					URL url = new URL(file_url);
					URLConnection conn = url.openConnection();
					InputStream inStream = conn.getInputStream();
					FileOutputStream fs = new FileOutputStream(Environment.getExternalStorageDirectory() + "/" + name);
					byte[] buffer = new byte[1024];
					int length;
					while ((byteread = inStream.read(buffer)) != -1) {
						bytesum += byteread;
						fs.write(buffer, 0, byteread);
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
}
