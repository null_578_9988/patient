package com.wonhx.patient.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

import com.wonhx.patient.R;

public class LoginDialog extends Dialog {

    private Button btn_sure;
    private TextView mContent;
    public LoginDialog(Context context) {
        super(context, R.style.dialog);
        setLoginDialog();
    }

    private void setLoginDialog() {
        View mView = LayoutInflater.from(getContext()).inflate(
                R.layout.dialog_login, null);
        btn_sure = (Button) mView.findViewById(R.id.btn_sure);
        mContent = (TextView) mView.findViewById(R.id.content);
        super.setContentView(mView);
    }

    @Override
    public void setContentView(int layoutResID) {
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
    }

    @Override
    public void setContentView(View view) {
    }

    /**
     * 确定键监听器
     *
     * @param listener
     */
    public void setOnPositiveListener(View.OnClickListener listener) {
        btn_sure.setOnClickListener(listener);
    }

    /**
     * 设置显示文字
     * @param content
     */
    public  void setContent(String content){
        mContent.setText(content);
    }

}
