package com.wonhx.patient.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

import com.wonhx.patient.R;

public class HJloginDialog extends Dialog {

	private Button btn_sure;
	private TextView txt_title;
	private TextView txt_content;
	private TextView btn_cancel;

	public HJloginDialog(Context context) {
		super(context, R.style.dialog);
		setLoginDialog();
	}

	private void setLoginDialog() {
		View mView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_hj_login, null);
		btn_sure = (Button) mView.findViewById(R.id.btn_sure);
		txt_title = (TextView) mView.findViewById(R.id.txt_title);
		txt_content = (TextView) mView.findViewById(R.id.txt_content);
		btn_cancel = (TextView) mView.findViewById(R.id.btn_cancel);
		super.setContentView(mView);
	}

	@Override
	public void setContentView(int layoutResID) {
	}

	@Override
	public void setContentView(View view, LayoutParams params) {
	}

	@Override
	public void setContentView(View view) {
	}

	/**
	 * 确定键监听器
	 * 
	 * @param listener
	 */
	public void setOnPositiveListener(View.OnClickListener listener) {
		btn_sure.setOnClickListener(listener);
	}
	public void setOnCancelListener(View.OnClickListener listener) {
		btn_cancel.setOnClickListener(listener);
	}
	
	
	public void setPositiveMessage(String positive) {
		btn_sure.setText(positive);
	}
	public void setPositiveColor(int color) {
		btn_sure.setTextColor(color);
	}
	
	public void setCancelMessage(String positive) {
		btn_cancel.setText(positive);
	}
	public void setCancelColor(int color) {
		btn_cancel.setTextColor(color);
	}
	public void setCancelVisibility(boolean visibility) {
		if (visibility) {
			btn_cancel.setVisibility(View.VISIBLE);
		}else {
			btn_cancel.setVisibility(View.GONE);
		}
	}
	
	public void setTitle(String title) {
		txt_title.setText(title);
	}
	public void setTitleVisibility(boolean visibility){
		if (visibility) {
			txt_title.setVisibility(View.VISIBLE);
		}else {
			txt_title.setVisibility(View.GONE);
		}
	}
	public void setContentMessage(String message) {
		txt_content.setText(message);
	}
	public void setContentVisibility(boolean visibility){
		if (visibility) {
			txt_content.setVisibility(View.VISIBLE);
		}else {
			txt_content.setVisibility(View.GONE);
		}
	}
	
}
