package com.wonhx.patient.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.wonhx.patient.R;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
  
/** 
 * @author Sodino E-mail:sodinoopen@hotmail.com 
 * @version Time：2011-8-20 下午08:41:25 
 */  
public class ChinaCityAct extends Activity implements OnItemClickListener,OnClickListener {  
    private ChinaAlphabetComparator comparator;  							//排序类  
    private Hashtable<String, Hashtable<String, String[]>> hashtable;  		//全部省，市，县
    private String[] arrProvince, arrCity, arrRegion;  						//省，市，县
    private String province, city, region;  								//单个省，市，县
	private ListView list_province;
	private ListView list_city;
	private ListView list_region;
	private TextView txt_submit;
	private TextView txt_province;
	private TextView txt_city;
	private TextView txt_region;
	private LinearLayout llayout_province;
	private LinearLayout llayout_city;
	private LinearLayout llayout_region;
  
    @Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gy_city);  
        comparator = new ChinaAlphabetComparator();  			//排序类
        hashtable = ChinaCityUtil.initChinaCitysHashtable();  	//初始化全部城市
        initView();
        //查找省
        arrProvince = ChinaCityUtil.findAreaStringArr(hashtable, ChinaCityUtil.TYPE_PROVINCE);  
        ChinaCityAdapter adapterProvince = getArrayAdapter(arrProvince);  
        list_province.setAdapter(adapterProvince);  
    }  
  
    private void initView() {
    	list_province = (ListView) findViewById(R.id.list_province);
    	list_province.setVisibility(View.VISIBLE);
    	list_province.setOnItemClickListener(this);
    	list_city = (ListView) findViewById(R.id.list_city);
    	list_city.setVisibility(View.GONE);
    	list_city.setOnItemClickListener(this);
    	list_region = (ListView) findViewById(R.id.list_region);
    	list_region.setVisibility(View.GONE);
    	list_region.setOnItemClickListener(this);
    	
    	txt_province = (TextView) findViewById(R.id.txt_province);
    	txt_city = (TextView) findViewById(R.id.txt_city);
    	txt_region = (TextView) findViewById(R.id.txt_region);
    	llayout_province = (LinearLayout) findViewById(R.id.llayout_province);
    	llayout_city = (LinearLayout) findViewById(R.id.llayout_city);
    	llayout_region = (LinearLayout) findViewById(R.id.llayout_region);
    	txt_province.setFocusable(true);
    	txt_province.setFocusableInTouchMode(true);
    	llayout_province.setBackgroundColor(getResources().getColor(R.color.gy_line_blue));
    	
    	txt_submit = (TextView) findViewById(R.id.txt_submit);
    	txt_submit.setOnClickListener(this);
	}

	private ChinaCityAdapter getArrayAdapter(String[] arr) { 
    	Arrays.sort(arr, comparator);		//排序
    	List<String> list = Arrays.asList(arr);
    	ChinaCityAdapter adapter = new ChinaCityAdapter(list, this);
        return adapter;  
    }  
  
    //城市
    private void modifyCity(String province) {  
        arrCity = ChinaCityUtil.findAreaStringArr(hashtable, ChinaCityUtil.TYPE_CITY, province);  
        ChinaCityAdapter adapterCity = getArrayAdapter(arrCity);  
        list_city.setAdapter(adapterCity);  
    }  
  
    //区县
    private void modifyRegion(String province, String city) {  
        arrRegion = ChinaCityUtil.findAreaStringArr(hashtable, ChinaCityUtil.TYPE_REGION, province,city);  
        ChinaCityAdapter adapterRegion = getArrayAdapter(arrRegion);  
        list_region.setAdapter(adapterRegion);  
    }  
	@Override
	public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3) {
		if (parent == list_province) {
			  	province = arrProvince[position];  
	            modifyCity(province);  
	            txt_province.setText(province);
	            list_province.setVisibility(View.GONE);
	            list_city.setVisibility(View.VISIBLE);
	            //改变颜色
	            setViewFocusableChange(txt_city,txt_province);
	            llayout_province.setBackgroundColor(getResources().getColor(R.color.white));
	            llayout_city.setBackgroundColor(getResources().getColor(R.color.gy_line_blue));
	        } else if (parent == list_city) {  
	            city = arrCity[position];  
	            modifyRegion(province, city); 
	            txt_city.setText(city);
	            list_city.setVisibility(View.GONE);
	            list_region.setVisibility(View.VISIBLE);
	            //改变颜色
	            setViewFocusableChange(txt_region,txt_city);
	            llayout_city.setBackgroundColor(getResources().getColor(R.color.white));
	            llayout_region.setBackgroundColor(getResources().getColor(R.color.gy_line_blue));
	        } else if (parent == list_region) {  
	            region = arrRegion[position];  
	            txt_region.setText(region);
	        }  
	}
	private void setViewFocusableChange(View show,View hide){
		show.setFocusable(true);
		show.setFocusableInTouchMode(true);
		hide.setFocusable(false);
		hide.setFocusableInTouchMode(false);
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.txt_submit:		//确定
			
			break;
		default:
			break;
		}
	}  
}  