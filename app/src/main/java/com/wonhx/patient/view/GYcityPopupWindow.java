package com.wonhx.patient.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wonhx.patient.R;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;


public class GYcityPopupWindow extends PopupWindow implements OnItemClickListener,OnClickListener{
	//选择城市
	
    private ChinaAlphabetComparator comparator;  							//排序类  
    private Hashtable<String, Hashtable<String, String[]>> hashtable;  		//全部省，市，县
    private String[] arrProvince, arrCity, arrRegion;  						//省，市，县
    private String province, city, region;  								//单个省，市，县
	private ListView list_province;
	private ListView list_city;
	private ListView list_region;
	private TextView txt_submit;
	private TextView txt_province;
	private TextView txt_city;
	private TextView txt_region;
	private LinearLayout llayout_province;
	private LinearLayout llayout_city;
	private LinearLayout llayout_region;
	
	private Context mContext;
	private View view;
	private int height;	//屏幕高度
	private TextView view_region;
	public GYcityPopupWindow(Context mContext,TextView view_region) {
		this.mContext = mContext;
		this.view = LayoutInflater.from(mContext).inflate(R.layout.activity_gy_city, null);
		WindowManager wm = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
		height = wm.getDefaultDisplay().getHeight();
		this.view_region = view_region;
		
		initView();
		setPopupWindow();
        comparator = new ChinaAlphabetComparator();  			//排序类
        hashtable = ChinaCityUtil.initChinaCitysHashtable();  	//初始化全部城市
        //查找省
        arrProvince = ChinaCityUtil.findAreaStringArr(hashtable, ChinaCityUtil.TYPE_PROVINCE);  
        ChinaCityAdapter adapterProvince = getArrayAdapter(arrProvince);  
        list_province.setAdapter(adapterProvince);  
	}
	   private void initView() {
	    	list_province = (ListView) view.findViewById(R.id.list_province);
	    	list_province.setVisibility(View.VISIBLE);
	    	list_province.setOnItemClickListener(this);
	    	list_city = (ListView) view.findViewById(R.id.list_city);
	    	list_city.setVisibility(View.GONE);
	    	list_city.setOnItemClickListener(this);
	    	list_region = (ListView) view.findViewById(R.id.list_region);
	    	list_region.setVisibility(View.GONE);
	    	list_region.setOnItemClickListener(this);
	    	
	    	txt_province = (TextView) view.findViewById(R.id.txt_province);
	    	txt_city = (TextView) view.findViewById(R.id.txt_city);
	    	txt_region = (TextView) view.findViewById(R.id.txt_region);
	    	llayout_province = (LinearLayout) view.findViewById(R.id.llayout_province);
	    	llayout_city = (LinearLayout) view.findViewById(R.id.llayout_city);
	    	llayout_region = (LinearLayout) view.findViewById(R.id.llayout_region);
	    	txt_province.setFocusable(true);
	    	txt_province.setFocusableInTouchMode(true);
	    	llayout_province.setBackgroundColor(mContext.getResources().getColor(R.color.gy_line_blue));
	    	
	    	txt_submit = (TextView) view.findViewById(R.id.txt_submit);
	    	txt_submit.setOnClickListener(this);
		}

		private ChinaCityAdapter getArrayAdapter(String[] arr) { 
	    	Arrays.sort(arr, comparator);		//排序
	    	List<String> list = Arrays.asList(arr);
	    	ChinaCityAdapter adapter = new ChinaCityAdapter(list, mContext);
	        return adapter;  
	    }  
	  
	    //城市
	    private void modifyCity(String province) {  
	        arrCity = ChinaCityUtil.findAreaStringArr(hashtable, ChinaCityUtil.TYPE_CITY, province);  
	        ChinaCityAdapter adapterCity = getArrayAdapter(arrCity);  
	        list_city.setAdapter(adapterCity);  
	    }  
	  
	    //区县
	    private void modifyRegion(String province, String city) {  
	        arrRegion = ChinaCityUtil.findAreaStringArr(hashtable, ChinaCityUtil.TYPE_REGION, province,city);  
	        ChinaCityAdapter adapterRegion = getArrayAdapter(arrRegion);  
	        list_region.setAdapter(adapterRegion);  
	    }  
		@Override
		public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3) {
			if (parent == list_province) {
				  	province = arrProvince[position];  
		            modifyCity(province);  
		            txt_province.setText(province);
		            list_province.setVisibility(View.GONE);
		            list_city.setVisibility(View.VISIBLE);
		            //改变颜色
		            setViewFocusableChange(txt_city,txt_province);
		            llayout_province.setBackgroundColor(mContext.getResources().getColor(R.color.white));
		            llayout_city.setBackgroundColor(mContext.getResources().getColor(R.color.gy_line_blue));
		        } else if (parent == list_city) {  
		            city = arrCity[position];  
		            modifyRegion(province, city); 
		            txt_city.setText(city);
		            list_city.setVisibility(View.GONE);
		            list_region.setVisibility(View.VISIBLE);
		            //改变颜色
		            setViewFocusableChange(txt_region,txt_city);
		            llayout_city.setBackgroundColor(mContext.getResources().getColor(R.color.white));
		            llayout_region.setBackgroundColor(mContext.getResources().getColor(R.color.gy_line_blue));
		        } else if (parent == list_region) {  
		            region = arrRegion[position];  
		            txt_region.setText(region);
		        }  
		}
		private void setViewFocusableChange(View show,View hide){
			show.setFocusable(true);
			show.setFocusableInTouchMode(true);
			hide.setFocusable(false);
			hide.setFocusableInTouchMode(false);
		}

		@Override
		public void onClick(View arg0) {
			switch (arg0.getId()) {
			case R.id.txt_submit:		//确定
				if (region == null || region.equals("null")) {
					Toast.makeText(mContext, "请选择具体区域", Toast.LENGTH_LONG).show();
					return;
				}
				if (province.equals(city)) {
					view_region.setText(province+" "+region);
				}else {
					view_region.setText(province+" "+city+" "+region);
				}
				dismiss();
				break;
			default:
				break;
			}
		}  
	private void setPopupWindow() {
		// 设置外部可点击
		this.setOutsideTouchable(true);
		// mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
		this.view.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				int height = view.findViewById(R.id.llayout_popup).getTop();
				int y = (int) event.getY();
				if (event.getAction() == MotionEvent.ACTION_UP) {
					if (y < height) {
						dismiss();
					}
				}
				return true;
			}
		});

		/* 设置弹出窗口特征 */
		// 设置视图
		this.setContentView(this.view);
		// 设置弹出窗体的宽和高
		this.setHeight(height/2);
		this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
		// 设置弹出窗体可点击
		this.setFocusable(true);

		// 实例化一个ColorDrawable颜色为半透明
		ColorDrawable dw = new ColorDrawable(0xb0000000);
		// 设置弹出窗体的背景
		this.setBackgroundDrawable(dw);
		// 设置弹出窗体显示时的动画，从底部向上弹出	(过程时间为200)
		this.setAnimationStyle(R.style.gy_popup_anim);
		
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run() {
				//设置添加屏幕的背景透明度
				backgroundAlpha((Activity)mContext,0.5f);//0.0-1.0
			}
		}, 200);
		this.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() { // 关闭
				backgroundAlpha((Activity) mContext, 1f);
			}
		});
	}
    /**
     * 设置添加屏幕的背景透明度
     */
    public void backgroundAlpha(Activity context, float bgAlpha)
    {
        WindowManager.LayoutParams lp = context.getWindow().getAttributes();
        lp.alpha = bgAlpha;
        context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        context.getWindow().setAttributes(lp);
    }
}
