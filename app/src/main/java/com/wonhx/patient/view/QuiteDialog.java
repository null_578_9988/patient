package com.wonhx.patient.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.wonhx.patient.R;

/**
 * Created by Administrator on 2017/4/1 0001.
 */

public class QuiteDialog extends Dialog {

    private Button btn_quite;
    private Button btn_sure;
    private TextView mContent;

    public QuiteDialog(Context context) {
        super(context, R.style.dialog);
        setQuiteDialog();
    }

    private void setQuiteDialog() {
        View mView = LayoutInflater.from(getContext()).inflate(
                R.layout.dialog_quite, null);
        btn_quite = (Button) mView.findViewById(R.id.btn_quite);
        btn_sure = (Button) mView.findViewById(R.id.btn_sure);
        mContent = (TextView) mView.findViewById(R.id.content);
        super.setContentView(mView);
    }

    @Override
    public void setContentView(int layoutResID) {
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
    }

    @Override
    public void setContentView(View view) {
    }

    /**
     * 确定键监听器
     *
     * @param listener
     */
    public void setOnPositiveListener(View.OnClickListener listener) {
        btn_sure.setOnClickListener(listener);
    }

    /**
     * 取消键监听器
     *
     * @param listener
     */
    public void setOnNegativeListener(View.OnClickListener listener) {
        btn_quite.setOnClickListener(listener);
    }
    public void setContent(String content){
        mContent.setText(content);
    }
    public void setBackText(String str){
        btn_quite.setText(str);
    }
    public void setSureText(String str){
        btn_sure.setText(str);
    }
}
