package com.wonhx.patient.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;
import com.orhanobut.logger.Logger;
import com.wonhx.patient.R;
import com.wonhx.patient.app.AppException;
import com.wonhx.patient.app.manager.CallDoctorManager;
import com.wonhx.patient.app.manager.calldoctor.CallDoctorManagerImpl;
import com.wonhx.patient.app.model.DoctorList;
import com.wonhx.patient.app.model.ListProResult;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;


/**
 * 选择科室界面
 *
 * @author Administrator
 *
 */
public class HJkeshiPopupWindow extends PopupWindow {
    private Context mContext;
    private CustomProgressDialog progressDialog;
    private View view;
    private LinearLayout llayout_all;
    private TextView txt_all_keshi;
    private ListView listview_keshi;
    private CompositeSubscription mSubscriptions;
    private CallDoctorManager callDoctorManager=new CallDoctorManagerImpl();
    private KeShiCallback mCallback;
    private QuickAdapter<DoctorList>adapter;
    public interface KeShiCallback {
        public void click(View v, String keshi_id, String keshi_name);
    }

    public HJkeshiPopupWindow(Context mContext, KeShiCallback mCallback) {
        this.mContext = mContext;
        this.mCallback = mCallback;
        this.view = LayoutInflater.from(mContext).inflate(
                R.layout.popup_hj_keshi, null);
        initView();
        initHttp();
        setPopupWindow();
    }

    private void initView() {
        llayout_all = (LinearLayout) view.findViewById(R.id.llayout_all);
        txt_all_keshi = (TextView) view.findViewById(R.id.txt_all_keshi);
        listview_keshi = (ListView) view.findViewById(R.id.listview_keshi);
        llayout_all.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        txt_all_keshi.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.click(v, "all", "全科");
                HJkeshiPopupWindow.this.dismiss();
            }
        });
        adapter=new QuickAdapter<DoctorList>(mContext,R.layout.doctor_item) {
            @Override
            protected void convert(BaseAdapterHelper helper, DoctorList item) {
                helper.setText(R.id.doctor,item.getName());
            }
        };
        listview_keshi.setAdapter(adapter);
        listview_keshi.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DoctorList item = (DoctorList) adapter.getItem(position);
                mCallback.click(view, item.getId(), item.getName());
                HJkeshiPopupWindow.this.dismiss();
           }
        });

    }

    /**
     * 获取科室
     */
    private void initHttp() {
       callDoctorManager.obtaindoctorlist(new SubscriberAdapter<ListProResult<DoctorList>>(){
           @Override
           public void success(ListProResult<DoctorList> doctorListProResult) {
               super.success(doctorListProResult);
               List<DoctorList> doctorLists = new ArrayList<DoctorList>();
               doctorLists.addAll(doctorListProResult.getData());
               adapter.addAll(doctorLists);
           }
       });
    }

    private void setPopupWindow() {
        // 设置外部可点击
        this.setOutsideTouchable(true);
        // mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        llayout_all.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    dismiss();
                }
                return true;
            }
        });
		/* 设置弹出窗口特征 */
        // 设置视图
        this.setContentView(this.view);
        // 设置弹出窗体的宽和高
        this.setHeight(RelativeLayout.LayoutParams.MATCH_PARENT);
        this.setWidth(RelativeLayout.LayoutParams.WRAP_CONTENT);
        // 设置弹出窗体可点击
        this.setFocusable(true);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        // 设置弹出窗体的背景
        this.setBackgroundDrawable(dw);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // 设置添加屏幕的背景透明度
                backgroundAlpha((Activity) mContext, 0.5f);// 0.0-1.0
            }
        }, 50);
        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() { // 关闭
                backgroundAlpha((Activity) mContext, 1f);
            }
        });
    }

    /**
     * 设置添加屏幕的背景透明度
     */
    public void backgroundAlpha(Activity context, float bgAlpha) {
        WindowManager.LayoutParams lp = context.getWindow().getAttributes();
        lp.alpha = bgAlpha;
        context.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        context.getWindow().setAttributes(lp);
    }


    protected class SubscriberAdapter<T> extends Subscriber<T> {
        public SubscriberAdapter() {
            if (mSubscriptions == null)
                mSubscriptions = new CompositeSubscription();
            mSubscriptions.add(this);
        }

        public void success(T t) {

        }

        @Override
        public void onNext(T t) {
            //dismissLoadingDialog();
            success(t);
        }

        @Override
        public void onStart() {
           // showLoadingDialog();
        }

        @Override
        public void onError(Throwable e) {
            //dismissLoadingDialog();
            Logger.e((Exception) e);
            if (e instanceof AppException) {
                AppException ex = (AppException) e;
                if (ex.getType() == AppException.TYPE_CUSTOM)
                    //showAlertDialog("提示", e.getMessage());
                return;
            }
        }

        @Override
        public void onCompleted() {
        }
        public void showLoadingDialog() {
            if (progressDialog == null){
                progressDialog = CustomProgressDialog.createDialog(mContext);
                //progressDialog.setMessage("数据加载中");
            }
            progressDialog.show();
        }



        public void dismissLoadingDialog() {
            if (progressDialog != null)
                progressDialog.dismiss();
        }
    }
}
