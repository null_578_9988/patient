package com.wonhx.patient.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.wonhx.patient.R;

public class CustomDialog extends Dialog {

	private SimpleDraweeView imgCodeView;
	private EditText et_input; 
	private ImageView iv_clear;
	private Button btn_confirmcode;
	private Button btn_back;
	//	private int flag=1;

	public CustomDialog(Context context) {
		super(context,R.style.dialog);
		setCustomDialog();
	}
//	protected void onCreate(Bundle savedInstanceState) {
//	    super.onCreate(savedInstanceState);
//	    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//	}
	private void setCustomDialog() {
		View mView = LayoutInflater.from(getContext()).inflate(R.layout.activity_imagevalidatecode, null);
		imgCodeView=(SimpleDraweeView)mView.findViewById(R.id.imgCodeView);
		et_input=(EditText)mView.findViewById(R.id.et_input);
		iv_clear=(ImageView)mView.findViewById(R.id.iv_clear);
		btn_back=(Button)mView.findViewById(R.id.btn_back);
		btn_confirmcode=(Button)mView.findViewById(R.id.btn_confirmcode);
		super.setContentView(mView);
	}
	public View getEditText(){
		return et_input;
	}
	public View getImageView()
	{
		return imgCodeView;	
	}
	public View getImageClear()
	{
		return iv_clear;
	}

	@Override
	public void setContentView(int layoutResID) {
	}

	@Override
	public void setContentView(View view, LayoutParams params) {
	}

	@Override
	public void setContentView(View view) {
	}

	/** 
	 * 确定键监听器 
	 * @param listener 
	 */  
	public void setOnPositiveListener(View.OnClickListener listener){  
		btn_confirmcode.setOnClickListener(listener);  
	}  
	/** 
	 * 取消键监听器 
	 * @param listener 
	 */  
	public void setOnNegativeListener(View.OnClickListener listener){  
		btn_back.setOnClickListener(listener);  
	}
}
