package com.wonhx.patient.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class ServiceListView extends ListView {
	public ServiceListView(Context context) {
		super(context);
	}

	public ServiceListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ServiceListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,

				MeasureSpec.AT_MOST);

		super.onMeasure(widthMeasureSpec, expandSpec);

	}
}
