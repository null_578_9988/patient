package com.wonhx.patient.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.wonhx.patient.R;

import java.util.List;

public class ChinaCityAdapter extends BaseAdapter {
	
	private List<String> list;
	private Context context;
	public ChinaCityAdapter(List<String> list, Context context) {
		super();
		this.list = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View view, ViewGroup arg2) {
		ViewHodle viewHodle;
		String data = list.get(position);
		if (view == null) {
			view = LayoutInflater.from(context).inflate(R.layout.activity_gy_city_item, null);
			viewHodle = new ViewHodle(view);
			view.setTag(viewHodle);
		} else {
			viewHodle = (ViewHodle) view.getTag();
		}
		viewHodle.txt_city.setText(data);
		return view;
	}
	public class ViewHodle {
		private TextView txt_city;
	    public ViewHodle(View view) {
	    	txt_city = (TextView) view.findViewById(R.id.txt_city);
	      }
	}
}
