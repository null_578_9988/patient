/**
 * @Title: CommonBaseTitle.java
 * @Package com.nodyang.webutil
 * @Description: TODO(用一句话描述该文件做什么)
 * @author 周建伟
 * @date 2015年12月23日 下午8:46:33    
 * @version V1.0
 */
package com.wonhx.patient.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wonhx.patient.R;

/**
 * 公共头部布局
 */
public class CommonBaseTitle extends RelativeLayout {

    private Context mcontext;

    private View view;
    // 左侧返回箭头
    private ImageView iv_back;
    // 头部文字
    private TextView tv_title;
    /* 下一步 */
    private TextView tv_next;

    private ProgressBar progressBar;

    private FrameLayout notice_layout;
    private TextView red_point;

    public CommonBaseTitle(Context context) {
        super(context);
        mcontext = context;
    }

    public CommonBaseTitle(Context context, AttributeSet attrs) {
        super(context, attrs);
        // 在构造函数中将Xml中定义的布局解析出来。
        mcontext = context;

        view = LayoutInflater.from(context).inflate(R.layout.activity_head,
                this, true);

		/* 头部文字 */
       tv_title = (TextView) view.findViewById(R.id.tv_title);
		/* 返回箭头 */
       iv_back = (ImageView) view.findViewById(R.id.iv_back);
		/* 下一步 */
        tv_next = (TextView) view.findViewById(R.id.tv_next);

       // progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        progressBar.setVisibility(View.GONE);

        notice_layout = (FrameLayout) view.findViewById(R.id.notice_layout);
        red_point = (TextView) view.findViewById(R.id.red_point);
        iv_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                ((Activity) mcontext).finish();
            }
        });
    }

    /**
     * 设置填入头部文字
     *
     * @param title
     *            头部文字
     */
    public void setTitle(String title) {
        tv_title.setText(title);
    }

    /**
     * @return the tv_next
     */
    public TextView getTv_next() {
        return tv_next;
    }

    /**
     * @param tv_next
     *            the tv_next to set
     */
    public void setRightTitle(String title) {
        tv_next.setText(title);
    }

    public ProgressBar getProgressBar() {
        return this.progressBar;
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

}