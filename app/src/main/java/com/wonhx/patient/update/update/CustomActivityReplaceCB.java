package com.wonhx.patient.update.update;

import android.app.Activity;

import com.wonhx.patient.update.ActivityStack;
import com.wonhx.patient.update.lib.callback.ActivityReplaceCB;


/**
 * Created by admin on 16/8/31.
 */
public class CustomActivityReplaceCB implements ActivityReplaceCB {
    @Override
    public Activity replace(Activity oldActivity) {
        return ActivityStack.current();
    }
}
