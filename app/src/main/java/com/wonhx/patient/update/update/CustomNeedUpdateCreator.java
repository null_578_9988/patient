package com.wonhx.patient.update.update;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;

import com.wonhx.patient.R;
import com.wonhx.patient.update.lib.creator.DialogCreator;
import com.wonhx.patient.update.lib.model.Update;
import com.wonhx.patient.update.lib.util.SafeDialogOper;


/**
 * 自定义检查出有新版本需要更新时的Dialog创建器
 */
public class CustomNeedUpdateCreator extends DialogCreator {
    /**
     * @param update  更新数据实体类
     * @param context 此Dialog创建器使用的activity实例。默认为使用UpdateBuilder.check(activity)方法传入。
     *                需注意：此activity传入后使用弱引用进行保存。故有可能使用时activity为null。或者已经finished.,
     * @return 需要显示的Dialog
     */
    @Override
    public Dialog create(final Update update, final Activity context) {
        String updateContent = context.getText(R.string.update_version_name)
                + ": " + update.getVersionName() + "\n\n\n"
                + update.getUpdateContent();
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setMessage(updateContent)
                .setTitle(R.string.update_title)
                .setPositiveButton(R.string.update_immediate, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // 发送下载请求
                        sendDownloadRequest(update, context);
                        SafeDialogOper.safeDismissDialog((Dialog) dialog);
                    }
                });
        if (update.isIgnore() && !update.isForced()) {
            builder.setNeutralButton(R.string.update_ignore, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // 用户忽略此版本更新点击
                    sendUserIgnore(update);
                    SafeDialogOper.safeDismissDialog((Dialog) dialog);
                }
            });
        }

        if (!update.isForced()) {
            builder.setNegativeButton(R.string.update_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // 用户取消更新
                    sendUserCancel();
                    SafeDialogOper.safeDismissDialog((Dialog) dialog);
                }
            });
        }
        builder.setCancelable(false);
        return builder.create();
    }
}
