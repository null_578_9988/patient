package com.wonhx.patient.update.lib.creator;

import android.app.Activity;

import com.wonhx.patient.update.lib.callback.UpdateDownloadCB;
import com.wonhx.patient.update.lib.model.Update;


/**
 * Download dialog creator
 *
 * @author lzh
 */
public interface DownloadCreator {

    /**
     * To create a download dialog when should be shown,this method returns a {@link UpdateDownloadCB},
     * the download callback will be used in {@link lvxingshang.app.update.lib.business.DownloadWorker},
     * and also called with it to update download progress,
     *
     * @param update   The update instance created by {@link lvxingshang.app.update.lib.model.UpdateParser#parse(String)}
     * @param activity The activity instance,cause it is be saved with weak ref,so the context
     *                 will be null or finished sometimes when you finish you activity before,
     */
    UpdateDownloadCB create(Update update, Activity activity);
}
