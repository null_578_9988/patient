package com.wonhx.patient.update.lib.callback;

import android.app.Activity;
import android.app.Dialog;

import com.wonhx.patient.update.lib.UpdateBuilder;
import com.wonhx.patient.update.lib.creator.InstallCreator;
import com.wonhx.patient.update.lib.model.Update;
import com.wonhx.patient.update.lib.util.Recycler;
import com.wonhx.patient.update.lib.util.SafeDialogOper;

import java.io.File;
import java.lang.ref.WeakReference;


public class DefaultDownloadCB implements UpdateDownloadCB, Recycler.Recycleable {

    private WeakReference<Activity> actRef = null;
    private UpdateBuilder builder;

    private UpdateDownloadCB downloadCB;
    private Update update;
    private UpdateDownloadCB innerCB;

    public DefaultDownloadCB(Activity activity) {
        actRef = new WeakReference<>(activity);
    }

    public void setBuilder(UpdateBuilder builder) {
        this.builder = builder;
        downloadCB = builder.getDownloadCB();
    }

    public void setUpdate(Update update) {
        this.update = update;
    }

    public void setDownloadCB(UpdateDownloadCB downloadCB) {
        this.downloadCB = downloadCB;
    }


    @Override
    public void onUpdateStart() {
        // replace activity when necessary
        if (builder.getReplaceCB() != null) {
            actRef = new WeakReference<>(builder.getReplaceCB().replace(actRef.get()));
        }
        if (downloadCB != null) {
            downloadCB.onUpdateStart();
        }
        innerCB = getInnerCB();
        if (innerCB != null) {
            innerCB.onUpdateStart();
        }
    }

    public UpdateDownloadCB getInnerCB() {
        if (innerCB == null && builder.getStrategy().isShowDownloadDialog()) {
            innerCB = builder.getDownloadDialogCreator().create(update, actRef.get());
        }
        return innerCB;
    }

    @Override
    public void onUpdateComplete(File file) {
        if (downloadCB != null) {
            downloadCB.onUpdateComplete(file);
        }

        if (innerCB != null) {
            innerCB.onUpdateComplete(file);
        }

        Activity current = actRef.get();
        if (builder.getReplaceCB() != null) {
            current = builder.getReplaceCB().replace(current);
        }

        InstallCreator creator = builder.getInstallDialogCreator();
        if (builder.getStrategy().isAutoInstall()) {
            creator.sendToInstall(file.getAbsolutePath());
        } else {
            creator.setCheckCB(builder.getCheckCB());
            Dialog dialog = creator.create(update, file.getAbsolutePath(), current);
            SafeDialogOper.safeShowDialog(dialog);
        }

        Recycler.release(this);
    }

    @Override
    public void onUpdateProgress(long current, long total) {
        if (downloadCB != null) {
            downloadCB.onUpdateProgress(current, total);
        }

        if (innerCB != null) {
            innerCB.onUpdateProgress(current, total);
        }
    }
    @Override
    public void onUpdateError(int code, String errorMsg) {
        if (downloadCB != null) {
            downloadCB.onUpdateError(code, errorMsg);
        }

        if (innerCB != null) {
            innerCB.onUpdateError(code, errorMsg);
        }

        Recycler.release(this);
    }

    @Override
    public void release() {
        this.actRef = null;
        this.builder = null;
        this.innerCB = null;
        this.downloadCB = null;
        this.update = null;
    }
}
