package com.wonhx.patient.update.lib.model;

/**
 * Created by lzh on 2016/7/15.
 */
public enum HttpMethod {
    GET, POST
}
